#include "manipulabilitycostfcn.h"
#include "VirtualRobot/MathTools.h"
#include "VirtualRobot/IK/GenericIKSolver.h"


namespace DMP {

double ManipulabilityCostFunction::getStateDependentCost(double timestamp, const DVec2d &x)
{
    //Step1: From State To TCP Pose
    Eigen::Matrix4f pose;

    VirtualRobot::IKSolver::CartesianSelection mode = VirtualRobot::IKSolver::All;
    if(x.size() == 6)
    {
        VirtualRobot::MathTools::rpy2eigen4f(x[3][0], x[4][0], x[5][0], pose);
    }
    else if(x.size() == 7)
    {
        pose = VirtualRobot::MathTools::quat2eigen4f(x[4][0], x[5][0], x[6][0], x[3][0]);
    }
    else if(x.size() == 3)
    {
        mode = VirtualRobot::IKSolver::Position;
        pose = Eigen::Matrix4f::Identity();
    }
    else
    {
        DMP::Exception("The size of given state is not correct. Please check if the state represents the TCP pose");
    }

    pose(0,3) = x[0][0];
    pose(1,3) = x[1][0];
    pose(2,3) = x[2][0];

    //Step2: From TCP Pose To Joint Values and get quality index
    VirtualRobot::GenericIKSolverPtr ikSolver(new VirtualRobot::GenericIKSolver(kc, VirtualRobot::JacobiProvider::eSVDDamped));

    double maniIndex;

    Eigen::Matrix4f globalPose = pose;
//    Eigen::Matrix4f globalPose = robot->getRootNode()->toGlobalCoordinateSystem(pose);
    if(ikSolver->solve(globalPose, mode))
    {
        VirtualRobot::PoseQualityExtendedManipulabilityPtr measure(new VirtualRobot::PoseQualityExtendedManipulability(kc));
        maniIndex = measure->getPoseQuality() * 100;
    }
    else
    {
//        std::cout << "ikSolver cannot get solution ... " << std::endl;
        maniIndex = 0;
    }

//    std::cout << "maniIndex: " << maniIndex << std::endl;
    // Step2: Alternative: use reachfile
//    std::string reachFile = "armar3-rightarm-extmanip-noselfdist1.bin";
//    VirtualRobot::ManipulabilityPtr reachSpace;
//    reachSpace.reset(new VirtualRobot::Manipulability(robot));
//    reachSpace->load(reachFile);

//    Eigen::Matrix4f globalPose = robot->getRootNode()->toGlobalCoordinateSystem(pose);
//    unsigned char entry = reachSpace->getEntry(globalPose);
//    double maniIndex = (double)entry / (double)reachSpace->getMaxEntry();

//    std::cout << "pose: " << std::endl;
//    std::cout << pose << std::endl;
//    std::cout << "globalPose: " << std::endl;
//    std::cout << globalPose << std::endl;
//    std::cout << "maniIndex: " << maniIndex << std::endl;

    double indCost = 10 - maniIndex;
    double trajCost = CostFunction::getStateDependentCost(timestamp, x);

    double cost = indCost + 0 * trajCost;
    return cost;

}

double ManipulabilityCostFunction::getTerminalCost(const DVec2d &x, const DVec &goal)
{
    return 0;
}

}
