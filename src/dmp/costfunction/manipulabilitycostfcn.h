#ifndef MANIPULABILITYCOSTFUNCTION_H
#define MANIPULABILITYCOSTFUNCTION_H

#include "costfcn.h"
#include "VirtualRobot/IK/PoseQualityExtendedManipulability.h"
#include "VirtualRobot/Workspace/Manipulability.h"

namespace DMP
{

class ManipulabilityCostFunction: public CostFunction
{
public:
    ManipulabilityCostFunction()
    {

    }

    ManipulabilityCostFunction(const double& quadTerm)
    {
        R = quadTerm;
    }


    virtual double getStateDependentCost(double timestamp, const DVec2d &x);
    virtual double getTerminalCost(const DVec2d& x, const DVec& goal);
    virtual  double getQuadPenalty(const Eigen::VectorXf& theta){return 0;}

    void setRobot(VirtualRobot::RobotPtr rob)
    {
        robot = rob;
    }

    VirtualRobot::RobotPtr getRobot()
    {
        return robot;
    }

    void setKinematicChain(VirtualRobot::RobotNodeSetPtr robSet)
    {
        kc = robSet;
    }

    VirtualRobot::RobotNodeSetPtr getKinematicChain()
    {
        return kc;
    }

protected:
    VirtualRobot::RobotPtr robot;
    VirtualRobot::RobotNodeSetPtr kc;
};

typedef boost::shared_ptr<ManipulabilityCostFunction> ManipulabilityCostFunctionPtr;


class ManipulabilityCostFunctionTest : public ManipulabilityCostFunction
{
public:
    ManipulabilityCostFunctionTest(){}


    double getStateDependentCost(double timestamp, const DVec2d &x)
    {
//        DVec jointStates = traj.getStates(timestamp,0);
//        jointStates[12] = x[0][0];

//        std::vector<float> states;
//        for(size_t i = 0; i < jointStates.size(); ++i)
//        {
//            states.push_back(jointStates[i]);
//        }

        double w = 0.8;
        DVec jointStates = traj.getStates(timestamp,0);

        std::vector<float> states;

        double statediff = 0;
        for(size_t i = 0; i < x.size(); ++i)
        {
            states.push_back(x[i][0]);

            statediff += (x[i][0] - jointStates[i]) * (x[i][0] - jointStates[i]);

        }




//        double cost = (states[0] - 0.5) * (states[0] - 0.5)
//               + (states[1] + 0.5) * (states[1] + 0.5);

        kc->setJointValues(states);
//        VirtualRobot::PoseQualityManipulabilityPtr measure(new VirtualRobot::PoseQualityManipulability(kc));
        // manipulability measure considering self-collision
        VirtualRobot::PoseQualityExtendedManipulabilityPtr measure(new VirtualRobot::PoseQualityExtendedManipulability(kc));

        VirtualRobot::RobotNodeSetPtr colModel1 = robot->getRobotNodeSet("RightHand");
        VirtualRobot::RobotNodeSetPtr colModel2 = robot->getRobotNodeSet("HeadTorso");

        Eigen::Vector3f p1,p2;
        robot->getCollisionChecker()->calculateDistance(colModel1, colModel2, p1, p2);
        Eigen::Vector3f dist = p2 - p1;
        measure->considerObstacles(true);
        measure->setObstacleDistanceVector(dist);
        double maniIndex = measure->getPoseQuality() * 10;

        double cost = w * (10 - maniIndex) + (1-w) * statediff;

        return cost;

    }
    double getTerminalCost(const DVec2d& x, const DVec& goal){return 0;}
    double getQuadPenalty(const Eigen::VectorXf& theta){return 0;}
};

typedef boost::shared_ptr<ManipulabilityCostFunctionTest> ManipulabilityCostFunctionTestPtr;

}



#endif
