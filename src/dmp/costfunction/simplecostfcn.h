#ifndef DMP_SIMPLECOSTFUNCTION_H
#define DMP_SIMPLECOSTFUNCTION_H

#include "costfcn.h"

namespace DMP
{

class SimpleCostFunction : public CostFunction
{
public:
    SimpleCostFunction()
    {
        hist = make_pair(0, DVec2d());
    }

    double getStateDependentCost(double timestamp, const DVec2d& x) override
    {
        double simpleCost = 0;

        // via-point cost (The DMP generated trajectory should go through y=2 at half)
        if(timestamp > 0.5 && timestamp < 0.6)
        {
            simpleCost += (2 - x[0][0]) * (2 - x[0][0]);
            simpleCost = sqrt(simpleCost);
        }

        double cost = /*10 * trajCost +*/ simpleCost;
        hist = make_pair(timestamp, x);

        return cost;
    }

private:

};
typedef boost::shared_ptr<SimpleCostFunction> SimpleCostFunctionPtr;

}



#endif
