%module pydmp

#ifdef MMM
%{
    #include "io/LegacyMMMConverter.h"
%}
#endif
%{
	#include "representation/trajectory.h"
	#include "representation/dmp/getlearningerrordmp.h"
    #include "representation/dmp/endveloforcefielddmp.h"

%}

%include "std_vector.i"
%include "std_string.i"

%template(VectorString) std::vector<std::string>;
namespace std {

	%template(StdVectorDouble) vector<double>;
	%template(StdVectorSampledTrajectoryV2Vec) vector<DMP::SampledTrajectoryV2>;
	// %template(StdVectorLWRGaussKernel) vector<DMP::LWR<DMP::GaussKernel>::KernelData>;
}

namespace DMP {
	/**************
	 *** Vector ***
	 **************/

	template <typename type>
	class Vec : public std::vector<type> {};

	typedef Vec<double> DVec;
	%template(DVec) Vec<double>;

	/******************
	 *** Trajectory ***
	 ******************/

	class SampledTrajectoryV2
	{
        public:
	        SampledTrajectoryV2();
		
		void addPositionsToDimension(unsigned int dimension, const DVec& x, const DVec& y);
		void removeDimension(unsigned int dimension);
		DVec getDimensionData(unsigned int dimension, unsigned int deriv = 0) const;
        DVec getTimestamps() const;
		unsigned int dim() const;
        unsigned int size() const;
        SampledTrajectoryV2 getPart(double startTime, double endTime, int numberOfDerivations = 0) const;
        void gaussianFilter(double filterRadius);
	};

	%template(SampledTrajectoryV2Vec) Vec<SampledTrajectoryV2>;

	/***************
	 *** Kernels ***
	 ***************/

	/*struct GaussKernelDataOutside
	{
		// Kernel kernel;
		double center;
		double width;
		double weight;
		double weightGradient;
	};

	template <typename Kernel = GaussKernel>
	class LWR : public FunctionApproximationInterface
	{
	public:
		struct KernelData
		{
			// Kernel kernel;
			double center;
			double width;
			double weight;
			double weightGradient;
		};
	};

	// http://www.swig.org/Doc2.0/SWIGPlus.html#SWIGPlus_nested_classes
	%nestedworkaround LWR<GaussKernel>::KernelData;

	%{
		namespace DMP { typedef LWR<GaussKernel>::KernelData GaussKernelDataOutside; }
	%}

	typedef Vec<LWR<GaussKernel>::KernelData> GaussKernelVec;
	// %template(GaussKernelVec) Vec<LWR<GaussKernel>::KernelData>; */

	/************
	 *** DMPs ***
	 ************/

	class BasicDMP // : public DMPInterfaceTemplate<DMPState>
	{
	public:
		void learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories);		
	};

	class SimpleEndVeloDMP : public BasicDMP {};
	class GetLearningErrorDMP : public SimpleEndVeloDMP {};
	class EndVeloForceFieldDMP : public DMP::SimpleEndVeloDMP {};

#ifdef MMM
    class LegacyMMMConverter
    {
    public:
        static SampledTrajectoryV2 getNodeTrajInTaskSpace(std::string motionFilePath, std::string motionName, const std::vector<std::string> &nodeSets, bool positionOnly = true, bool isLocal = true, bool isRPY = true);
    };
#endif
}
