#include "measure.h"

double DMP::PoseDistance::dist(const DMP::DVec &a, const DMP::DVec &b)
{
    double max_pos = fabs(a[0]);
    for(size_t i = 0; i < 3; i++)
    {
        if(fabs(a[i]) > max_pos)
        {
            max_pos = fabs(a[i]);
        }

        if(fabs(b[i]) > max_pos)
        {
            max_pos = fabs(b[i]);
        }
    }

    double dist = 0;
    for(size_t i = 0; i < 3; i++)
    {
        dist += pow((a[i] - b[i]) / max_pos,2);
    }

    for(size_t i = 3; i < 7; i++)
    {
        dist += pow((a[i] - b[i]), 2);
    }


    return dist;
}
