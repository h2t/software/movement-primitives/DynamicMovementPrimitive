#ifndef MEASURE_H
#define MEASURE_H

#include <dmp/representation/basicode.h>
#include <boost/serialization/shared_ptr.hpp>

namespace DMP{

class Distance
{
public:
    Distance(double minVal, double maxVal)
    {
        min_threshold = minVal;
        max_threshold = maxVal;
    }

    virtual double dist(const DMP::DVec& a, const DMP::DVec& b) = 0;

    double min_threshold{0};
    double max_threshold{0};

protected:
    Distance() = default;

private:

    friend class boost::serialization::access;

    template<class Archive>
    void serialize(Archive& ar, const unsigned int /*file_version*/)
    {
        ar& boost::serialization::make_nvp("min_threshold",min_threshold);
        ar& boost::serialization::make_nvp("max_threshold",max_threshold);
    }


};
typedef boost::shared_ptr<Distance> DistancePtr;

class PoseDistance : public Distance
{
    // Distance interface
public:

    PoseDistance(double minVal, double maxVal):Distance(minVal, maxVal) {}

    double dist(const DVec &a, const DVec &b) override;

private:

    friend class boost::serialization::access;
    PoseDistance() = default;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int /*file_version*/)
    {
        auto& base = boost::serialization::base_object<Distance>(*this);
        ar& boost::serialization::make_nvp("base",base);
    }
};


}




#endif

