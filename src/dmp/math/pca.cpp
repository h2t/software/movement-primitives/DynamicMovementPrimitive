/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "pca.h"

namespace DMP {

    PCA::PCA(const Eigen::MatrixXd &dataPoints)
    {
        compute(dataPoints);
    }

    PCA::ValuesAndIndices PCA::getXBestDimensions(int x)
    {
        if(x == -1)
            x = data.rows();
        ValuesAndIndices result;
        for (size_t d = 0; d < static_cast<size_t>(x); d++)
        {
            result.push_back(std::make_pair(vi.at(d).first, vi.at(d).second));
        }
        return result;
    }

    const Eigen::EigenSolver<Eigen::MatrixXd>::EigenvalueType& PCA::getEigenValues() const
    {
        return solver.eigenvalues();
    }

    Eigen::EigenSolver<Eigen::MatrixXd>::EigenvectorsType PCA::getEigenVectors()
    {
        return solver.eigenvectors();
    }


    void PCA::compute(const Eigen::MatrixXd& dataPoints)
    {
        this->data = dataPoints;
        using namespace Eigen;

        // substract centroid of all data points
        data = data.colwise() - data.rowwise().mean();


        // get the covariance matrix
        MatrixXd cov = data * data.transpose();
        // compute the eigenvalue on the Cov Matrix

        solver.compute(cov);
        VectorXd eigenvalues = solver.eigenvalues().real();

        // sort and get the permutation indices
        for (int i = 0 ; i < data.rows(); i++)
            vi.push_back(std::make_pair(eigenvalues(i), i));
        sort(vi.rbegin(), vi.rend());


    }

}
