#include "dualnumber.h"


DMP::DualQuaterniond::DualQuaterniond(const Eigen::Quaterniond &a, const Eigen::Quaterniond &b)
{
    r = a;
    d = b;
}


DMP::DualQuaterniond::DualQuaterniond(const Eigen::Vector3d &w, const Eigen::Vector3d &v)
{
    r = Quaterniond(0,w(0),w(1),w(2));
    d = Quaterniond(0,v(0),v(1),v(2));
}

DMP::DualQuaterniond::DualQuaterniond(const DMP::DVec &data)
{
    if(data.size() == 8){
        r.w() = data[0];
        r.x() = data[1];
        r.y() = data[2];
        r.z() = data[3];
        t.w() = data[4];
        t.x() = data[5];
        t.y() = data[6];
        t.z() = data[7];
    } else {
        r = Eigen::Quaterniond::Identity();
        d = Eigen::Quaterniond(0,0,0,0);
    }
}

DMP::DualQuaterniond::DualQuaterniond(double rw, double rx, double ry, double rz, double tw, double tx, double ty, double tz)
{
    r = Eigen::Quaterniond(rw,rx,ry,rz);
    d = Eigen::Quaterniond(tw,tx,ty,tz);

}



Quaterniond DMP::DualQuaterniond::getTranslation(const DualQuaterniond &a)
{
    Quaterniond rr = a.r;
    if(rr.norm() != 0)
        rr.normalize();
    else
        rr = Eigen::Quaterniond::Identity();

    return 2 * a.d * rr.conjugate();
}

DMP::DVec DMP::DualQuaterniond::norm()
{
    DVec res;
    res.push_back(r.norm());
    res.push_back(d.norm());
    return res;
}

DMP::DualQuaterniond DMP::DualQuaterniond::normalize()
{
    Quaterniond dpart = d / r.norm();
    r.normalize();
    d = dpart;
    return *this;
}

bool DMP::DualQuaterniond::isUnit()
{
    if(r.conjugate() * d + d.conjugate() * r == Quaterniond(0,0,0,0) && std::abs(r.norm() - 1) < 1e-3)
        return true;

    return false;
}

void DMP::DualQuaterniond::print()
{
    std::cout << "[ " << r.w() << " , "
                      << r.x() << " , "
                      << r.y() << " , "
                      << r.z() << " ][ "
                      << d.w() << " , "
                      << d.x() << " , "
                      << d.y() << " , "
                      << d.z() << " ]" << std::endl;

}
