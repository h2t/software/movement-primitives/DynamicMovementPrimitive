#include "gmm.h"

using namespace DMP;
using namespace Dist;

Gaussian::Gaussian(const vec &mu, const mat &Sig)
{
    dim = mu.n_elem;
    this->mu = mu;
    this->Sig = Sig;
    Lamb = inv_sympd(Sig);
}

double Gaussian::evaluate(const vec& x)
{
    mat nSig = Sig + 1e-5 * eye(Sig.n_rows, Sig.n_cols);
    double dSig = det(nSig);

    mat nuMat = (x - mu).t() * inv_sympd(nSig) * (x - mu);
    double nu = exp(- 0.5 * nuMat(0,0));
    double de = sqrt(pow(2 * datum::pi, dim) * dSig);

//    double res = std::max(nu/de, 1e-5);
    return nu/de;
}

std::vector<vec> Gaussian::sample(int num)
{
    mat regSig = Sig + 0.0001 * eye(dim, dim);
    mat L = chol(regSig, "lower");

    std::vector<vec> samples;
    for(int i = 0; i < num; ++i)
    {
        vec z = randn<vec>(dim);
        vec sample = mu + L * z;
        samples.push_back(sample);
    }

    return samples;
}



Dirichlet::Dirichlet(const vec &alpha)
{
    this->alpha = alpha;
    K = alpha.n_elem;
}

double Dirichlet::mode(int k)
{
    if(sum(alpha) == K)
    {
        std::cerr << "mode: sum of alpha is equal to K" << std::endl;
        return 0;
    }

    return (alpha(k) - 1) / (sum(alpha) - K);
}

vec Dirichlet::mode()
{
    vec res = alpha;
    for(int i = 0; i < K; ++i)
    {
        res(i) = mode(i);
    }

    return res;
}

double Dirichlet::expectation(int k)
{
    return alpha(k) / sum(alpha);
}

vec Dirichlet::expectation()
{
    vec res = alpha;
    for(int i = 0; i < K; ++i)
    {
        res(i) = expectation(i);
    }
    return res;
}

double Dirichlet::variance(int k)
{
    double alphaH = sum(alpha);
    double var = (alpha(k) * (alphaH - alpha(k)))/ (alphaH * alphaH * (alphaH + 1));
    return var;
}

vec Dirichlet::variance()
{
    vec res = alpha;
    for(int i = 0; i < K; ++i)
    {
        res(i) = variance(i);
    }
    return res;
}

double Dirichlet::evaluate(const vec &x)
{
    double dir = 1;
    for(int i = 0; i < K; ++i)
    {
        dir *= pow(x(i), alpha(i)-1);
    }
    return Cfun() * dir;
}


double Dirichlet::Cfun()
{
    double nu = tgamma(sum(alpha));
    double de = 1;

    for(int i = 0; i < K; ++i)
    {
        if(alpha(i) <= 0)
        {
            std::cerr << "Cfun: the " << i << "-th component of alpha is smaller than 0, the Dirichlet distribution cannot be normalized" << std::endl;
            return 0;
        }
        de *= tgamma(alpha(i));

    }

    return nu/de;
}

Wishart::Wishart(const mat &W, int v)
{
    this->W = W;
    this->v = v;
    D = this->W.n_rows;
}

mat Wishart::mode()
{
    return v * W;
}

double Wishart::evaluate(const mat &lamb)
{
    double vlamb = det(lamb);
    double trw = trace(W.i() * lamb);
    double res = B() * pow(vlamb, (v-D-1)/2.0) * exp(-0.5 * trw);
    return res;
}

double Wishart::B()
{
    double mdigamma = 1;

    for(int i = 0; i < D; ++i)
    {

        mdigamma *= boost::math::digamma((v+1 -i)/2);
    }

    double de = pow(2.0, v*D/2.0) * pow(M_PI, D * (D-1)/4.0) * mdigamma;
    double b = pow(det(W), -v/2.0) / de;
    return b;
}
