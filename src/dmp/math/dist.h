#ifndef DIST_H
#define DIST_H

#include <vector>
#include <cmath>
#include <Eigen/Core>
#include <Eigen/Eigen>
#include <armadillo>
#include <boost/shared_ptr.hpp>
#include <boost/math/special_functions/digamma.hpp>

using namespace arma;

namespace DMP {

    namespace Dist{
        class Gaussian
        {
        public:
            vec mu;
            mat Sig;
            mat Lamb;
            int dim;

            Gaussian(const vec& mu, const mat& Sig);

            double evaluate(const vec& x);

            vec mode(){return mu;}
            mat variance(){return Sig;}

            std::vector<vec > sample(int num=1);
        };
        typedef boost::shared_ptr<Gaussian> GaussianPtr;

        class Dirichlet
        {
        public:
            vec alpha;
            int K;

            Dirichlet(const vec& alpha);

            double mode(int k);
            vec mode();

            double expectation(int k);
            vec expectation();

            double variance(int k);
            vec variance();

            double evaluate(const vec& x);
        private:
            double Cfun();

        };

        class Wishart
        {
        public:
            mat W;
            int v;
            int D;

            Wishart(const mat& W, int v);

            mat mode();

            double evaluate(const mat& lamb);
        private:
            double B();


        };

    }// namespace Dist
} // namespace DMP

#endif // DIST_H
