/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::Statechart
* @author     Mirko Waechter( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/
#define BOOST_TEST_MODULE DMP::DiscreteDMPTest
#define DMP_BOOST_TEST
#include <dmp/testing/test.h>

#include <dmp/representation/dmp/forcefielddmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/dmp3rdorderforcefield.h>
#include <dmp/representation/dmp/adaptive3rdorderdmp.h>
#include <dmp/representation/trajectory.h>
#include <dmp/database/database.h>

#include <dmp/functionapproximation/functionapproximationregistration.h>
#include <dmp/representation/dmp/dmpregistration.h>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>

using namespace DMP;



BOOST_AUTO_TEST_CASE(testTrajectoryReproductionBasicDMP)
{
    // Load the trajectory
    SampledTrajectoryV2 traj;
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv", false, 1);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
    traj.differentiateDiscretly(1);

    // train the DMP

    //    SimpleEndVeloDMP dmp(1, false   ); //!!! need to change initial dmp value from 1 to 0!!!
    BasicDMP dmp(50,1);
    //    ForceFieldDMP dmp;
    dmp.learnFromTrajectories(traj);

    // Reproduction

    double maxError;
    double error = dmp.evaluateReproduction(1, maxError, false);
    std::cout << "Error: " << error << std::endl;
    BOOST_CHECK(error < 0.05);

}


BOOST_AUTO_TEST_CASE(testTrajectoryReproductionSimpleEndVeloDMP)
{
    std::cout << "testTrajectoryReproductionSimpleEndVeloDMP" << std::endl;
    // Load the trajectory
    SampledTrajectoryV2 traj;
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv");
    traj.removeDimension(1);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
    traj.differentiateDiscretly(1);

    // train the DMP

    SimpleEndVeloDMP dmp(50,1, false);    //!!! need to change initial dmp value from 1 to 0!!!
    //    ForceFieldDMP dmp;
    dmp.learnFromTrajectories(traj);

    // Reproduction

    double maxError;
    double error = dmp.evaluateReproduction(0, maxError, false);
    std::cout << "Error: " << error << std::endl;
    BOOST_CHECK(error < 0.05);
}

BOOST_AUTO_TEST_CASE(testTrajectoryReproductionForceFieldDMP)
{
    std::cout << "testTrajectoryReproductionForceFieldDMP" << std::endl;
    // Load the trajectory
    SampledTrajectoryV2 traj;
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv");
    traj.removeDimension(1);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
    traj.differentiateDiscretly(1);

    // train the DMP
    ForceFieldDMP dmp;
    dmp.learnFromTrajectories(traj);

    // Reproduction

    double maxError;
    double error = dmp.evaluateReproduction(0, maxError, false);
    std::cout << "Error: " << error << std::endl;
    BOOST_CHECK(error < 0.05);
}

BOOST_AUTO_TEST_CASE(testTrajectoryReproduction3rdOrderDMP)
{
    std::cout << "testTrajectoryReproductionForceFieldDMP" << std::endl;
    // Load the trajectory
    SampledTrajectoryV2 traj;
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv");
    traj.removeDimension(1);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
    traj.differentiateDiscretly(1);

    // train the DMP
    DMP3rdOrder dmp;
    dmp.learnFromTrajectories(traj);

    // Reproduction

    double maxError;
    double error = dmp.evaluateReproduction(0, maxError, false);
    std::cout << "Error: " << error << std::endl;
    BOOST_CHECK(error < 0.05);
}

BOOST_AUTO_TEST_CASE(testTrajectoryReproduction3rdOrderForceFieldDMP)
{
    std::cout << "testTrajectoryReproduction3rdOrderForceFieldDMP" << std::endl;
    // Load the trajectory
    SampledTrajectoryV2 traj;
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv");
    traj.removeDimension(1);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
    traj.differentiateDiscretly(1);

    // train the DMP
    DMP3rdOrderForceField dmp;
    dmp.learnFromTrajectories(traj);

    // Reproduction

    double maxError;
    double error = dmp.evaluateReproduction(0, maxError, false);
    std::cout << "Error: " << error << std::endl;
    BOOST_CHECK(error < 0.05);
}

BOOST_AUTO_TEST_CASE(testTrajectoryReproductionAdaptiveGoal3rdOrderForceFieldDMP)
{
    std::cout << "testTrajectoryReproductionAdaptiveGoal3rdOrderForceFieldDMP" << std::endl;
    // Load the trajectory
    SampledTrajectoryV2 traj;
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv");
    traj.removeDimension(1);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
    traj.differentiateDiscretly(1);

    // train the DMP
    AdaptiveGoal3rdOrderDMP dmp;
    dmp.learnFromTrajectories(traj);

    // Reproduction

    double maxError;
    double error = dmp.evaluateReproduction(0, maxError, false);
    std::cout << "Error: " << error << std::endl;
    BOOST_CHECK(error < 0.05);
}


BOOST_AUTO_TEST_CASE(testDMPDB)
{
    // Load the trajectory
    SampledTrajectoryV2 traj;
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv");
    traj.removeDimension(1);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
    traj.differentiateDiscretly(1);

    // train the DMP
    Database<BasicDMP> db;
    Database<BasicDMP>::DMPTypePtr dmp(new BasicDMP());
    dmp->learnFromTrajectories(traj);
    db.addDMP("pouring", dmp);

    std::stringstream str;
    {
        boost::archive::xml_oarchive ar(str);
        ar << BOOST_SERIALIZATION_NVP(db);
        std::cout << str.str() << std::endl;
    }

    Database<BasicDMP>  dmpDB2;
    {
        boost::archive::xml_iarchive ar(str);
        ar >> BOOST_SERIALIZATION_NVP(dmpDB2);
    }
    BOOST_CHECK(dmpDB2.getDMP("pouring"));

}

BOOST_AUTO_TEST_CASE(testDMPSerialization)
{
    // Load the trajectory
    SampledTrajectoryV2 traj;
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/sampletraj.csv");
    traj.removeDimension(1);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
    traj.differentiateDiscretly(1);

    // train the DMP

    DMPInterfacePtr dmp(new BasicDMP());
    dmp->learnFromTrajectories(traj);

    std::stringstream str;
//    std::ofstream ofs("aaa.xml");
//    {
//        boost::archive::xml_oarchive ar(ofs);
//        ar << boost::serialization::make_nvp("dmp", dmp);
//        //                std::cout << str.str() << std::endl;
//    }

    std::ifstream ifs("aaa.xml");
    DMPInterfacePtr dmp2;
    {
        boost::archive::xml_iarchive ar(ifs);
        ar >>  boost::serialization::make_nvp("dmp", dmp2);
    }

    std::cout << dmp2->getDMPDim() << std::endl;

    // Reproduction
    boost::shared_ptr<BasicDMP> dmp3 = boost::dynamic_pointer_cast<BasicDMP>(dmp2);
    dmp3->trainingTraj.push_back(traj);
    double maxError;
    double error = boost::dynamic_pointer_cast<BasicDMP>(dmp)->evaluateReproduction(1, maxError, false);
    double error2 = dmp3->evaluateReproduction(1, maxError, false);
    std::cout << "Error: " << error << std::endl;
    std::cout << "Error2: " << error << std::endl;
    BOOST_CHECK(fabs(error - error2) < 0.00005);
}
