/**
* This file is part of DMP Lib.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#define BOOST_TEST_MODULE DMP::TrajectoryTest
#define DMP_BOOST_TEST
#include <dmp/testing/test.h>


#include <dmp/representation/trajectory.h>

using namespace DMP;



BOOST_AUTO_TEST_CASE(testTrajectoryLoading)
{
    // Load the trajectory
    SampledTrajectoryV2 traj;
    //    traj.readFromCSVFile("pouring_x_short.csv");
    //    traj.readFromCSVFile("sampletraj_withvel_shortened.csv.txt");
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv");
    BOOST_CHECK(traj.begin()->getPosition(0) == 445.35);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
    DVec timestamps = traj.getTimestamps();
    traj.differentiateDiscretly(2);
    DVec d0 = traj.getDimensionData(0, 0);
    DVec d1 = traj.getDimensionData(0, 1);
    BOOST_CHECK(traj.begin()->getDeriv(0, 1) == d1.at(0));
    std::cout << "end" << std::endl;
}

#define SIGN_FACTOR(x) ((x>=0)?1:-1)

BOOST_AUTO_TEST_CASE(testExtrapolation)
{
    // Load the trajectory
    SampledTrajectoryV2 traj;
    //    traj.readFromCSVFile("pouring_x_short.csv");
    //    traj.readFromCSVFile("sampletraj_withvel_shortened.csv.txt");
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv");

    //    traj = SampledTrajectoryV2::normalizeTimestamps(traj,0,1);
    DVec timestamps = traj.getTimestamps();
    traj.differentiateDiscretly(2);
    double first = traj.begin()->getPosition(0);
    double derivFirst = traj.begin()->getDeriv(0, 1);
    double before = traj.getState(timestamps.at(0) - 5, 0, 0);
    BOOST_CHECK(before < SIGN_FACTOR(derivFirst) * first);
    std::cout << "end" << std::endl;
}

BOOST_AUTO_TEST_CASE(testInplaceInterpolation)
{
    // Load the trajectory
    SampledTrajectoryV2 traj;
    //    traj.readFromCSVFile("pouring_x_short.csv");
    //    traj.readFromCSVFile("sampletraj_withvel_shortened.csv.txt");
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv");
    DVec timestamps = traj.getTimestamps();
    double middle = (timestamps.at(1) + timestamps.at(0)) * 0.5;
    double before = traj.getState(timestamps.at(0));
    double after = traj.getState(timestamps.at(1));
    double interpolation = traj.getState(middle);
    BOOST_CHECK(before < interpolation && after > interpolation);
    std::cout << "end" << std::endl;
}

BOOST_AUTO_TEST_CASE(testDerivation)
{
    // Load the trajectory
    SampledTrajectoryV2 traj;
    //    traj.readFromCSVFile("pouring_x_short.csv");
    //    traj.readFromCSVFile("sampletraj_withvel_shortened.csv.txt");
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv");
    traj.differentiateDiscretly(1);
    traj.differentiateDiscretly(2);
    SampledTrajectoryV2 traj2;
    traj2.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv");
    traj2.differentiateDiscretly(2);

    BOOST_CHECK_EQUAL(traj.begin()->getDeriv(0, 2), traj2.begin()->getDeriv(0, 2));
    BOOST_CHECK_EQUAL(traj.rbegin()->getDeriv(0, 2), traj2.rbegin()->getDeriv(0, 2));
    BOOST_CHECK_EQUAL(traj.rbegin()->getDeriv(0, 1), traj2.rbegin()->getDeriv(0, 1));
    double middleTimestamp = traj.getTimestamps().at(0) + traj.getTimeLength() / 2;
    BOOST_CHECK_EQUAL(traj.getState(middleTimestamp, 0, 2), traj2.getState(middleTimestamp, 0, 2));
    BOOST_CHECK_EQUAL(traj.getState(middleTimestamp, 0, 1), traj2.getState(middleTimestamp, 0, 1));
    std::cout << "end" << std::endl;
}



BOOST_AUTO_TEST_CASE(testInplaceDerivation)
{
    // Load the trajectory
    SampledTrajectoryV2 traj;
    //    traj.readFromCSVFile("pouring_x_short.csv");
    //    traj.readFromCSVFile("sampletraj_withvel_shortened.csv.txt");
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pouring_x_short.csv");
    double d2 = traj.begin()->getDeriv(0, 2);
    BOOST_CHECK(d2 != 0);
    std::cout << "end" << std::endl;
}

BOOST_AUTO_TEST_CASE(testInplaceDerivation2)
{
    // Load the trajectory
    DVec timestamps;
    timestamps << 0, 1, 2;
    DVec positions;
    positions << 1, 2, 5;

    SampledTrajectoryV2 traj(timestamps, positions);
    SampledTrajectoryV2 traj2(timestamps, positions);
    traj.differentiateDiscretly(1);
    BOOST_CHECK_EQUAL(traj.getState(-1, 0, 1), traj2.getState(-1, 0, 1));
}

BOOST_AUTO_TEST_CASE(testInplaceDerivation3)
{
    // Load the trajectory
    DVec timestamps;
    timestamps << 0, 1, 2;
    DVec positions;
    positions << 1, 2, 5;

    SampledTrajectoryV2 traj(timestamps, positions);
    SampledTrajectoryV2 traj2(timestamps, positions);
    traj.differentiateDiscretly(2);
    BOOST_CHECK_EQUAL(traj.getState(-1, 0, 2), traj2.getState(-1, 0, 2));
}

BOOST_AUTO_TEST_CASE(testDerivationRemoval)
{
    // Load the trajectory
    DVec timestamps;
    timestamps << 0, 1, 2;
    DVec positions;
    positions << 1, 2, 5;

    SampledTrajectoryV2 traj(timestamps, positions);
    //    SampledTrajectoryV2 traj2(timestamps, positions);
    traj.differentiateDiscretly(2);
    const SampledTrajectoryV2::TrajData& trajData = *traj.begin();
    BOOST_CHECK_EQUAL(traj.begin()->getData().at(0)->size(), 3);
    traj.removeDerivation(1);
    BOOST_CHECK_EQUAL(traj.begin()->getData().at(0)->size(), 1);
}


#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>

BOOST_AUTO_TEST_CASE(testSerialization)
{
    // Load the trajectory
    DVec timestamps;
    timestamps << 0, 1, 2;
    DVec positions;
    positions << 1, 2, 5;

    SampledTrajectoryV2 traj(timestamps, positions);
    traj.differentiateDiscretly(2);
    std::stringstream str;
    {
        boost::archive::xml_oarchive ar(str);
        ar << BOOST_SERIALIZATION_NVP(traj);
        std::cout << str.str() << std::endl;
    }
    boost::archive::xml_iarchive ar(str);
    SampledTrajectoryV2 traj2;
    ar >> BOOST_SERIALIZATION_NVP(traj2);
    BOOST_CHECK_EQUAL(traj2.dim(), 1);
    //    const SampledTrajectoryV2::TrajData& trajData = *traj.begin();
    Vec<DVecPtr> data = traj2.begin()->getData();
    BOOST_CHECK(traj2.getTimeLength() > 0);
    DVec dimData = traj2.getDimensionData(0, 0);
    BOOST_CHECK_EQUAL(traj.begin()->getData().at(0)->size(), traj2.begin()->getData().at(0)->size());
    //    traj.removeDerivation(1);
    //    BOOST_CHECK_EQUAL(traj.begin()->getData().at(0)->size(), 1);
}


