#include "testing/testdataset.h"


void TestDataSet::readFromFile(string filename)
{
    FILE* fp = fopen(filename.c_str(), "r");

    while (!feof(fp))
    {
        char buf[1024];
        fgets(buf, 1024, fp);

        if (strlen(buf) == 1)
        {
            continue;
        }

        if (strcmp(buf, ";==========configuration==========\n") == 0)
        {
            fscanf(fp, "nPoints: %u\n", &m_nPoints);
            fscanf(fp, "Periodic Duration: %lf\n", &m_period_length);
            fscanf(fp, "Periodic Start Index: %u\n", &m_periodStartIndex);
        }
        else if (strcmp(buf, ";Anchor Point\n") == 0)
        {
            m_anchorPoint = readVectorFromFile(fp);
        }
        else if (strcmp(buf, ";Periodic Start Point\n") == 0)
        {
            m_periodStartPoint = readVectorFromFile(fp);
        }
        else if (strcmp(buf, ";Position and Orientation Data\n") == 0)
        {
            //m_positionData.resize(m_nPoints);
            //m_orientationData.resize(m_nPoints);
            unsigned int i = 0;

            while (!feof(fp))
            {
                DVec pos, ori;
                pos.resize(3);
                ori.resize(3);
                fscanf(fp, "(%lf/%lf/%lf), ", &pos[0], &pos[1], &pos[2]);
                fscanf(fp, "(%lf/%lf/%lf), ", &ori[0], &ori[1], &ori[2]);
                fscanf(fp, "\n");
                m_positionData.push_back(pos);
                m_orientationData.push_back(ori);
                ++i;
            }
        }
    }

    fclose(fp);
    cout << "file " << filename << endl;
    flush(cout);
    cout << "anchor read from  file " << m_anchorPoint << endl;
    flush(cout);
}

SampledComposedPeriodicTrajectory TestDataSet::trajectoryForPositionDimension(unsigned int d) const
{
    unsigned int count = m_positionData.size();
    Vec< DVec > samples(count);

    for (unsigned int i = 0; i < count; ++i)
    {
        samples[i].resize(2);
        samples[i][0] = i * m_delta_t;
        samples[i][1] = m_positionData[i][d];
    }

    return SampledComposedPeriodicTrajectory(m_periodStartIndex * m_delta_t, 0.0, m_period_length, m_anchorPoint[d], &samples);
}
SampledComposedPeriodicTrajectory TestDataSet::trajectoryForPositions() const {
    unsigned int dim = this->m_positionData[0].size();
    Vec <bool> used_dimensions(dim);
    used_dimensions.assign(dim, true);
    used_dimensions[2] = false; // TODO: this ommits the z-dim
    return trajectoryForPositions(used_dimensions);
}

SampledComposedPeriodicTrajectory TestDataSet::trajectoryForPositions(const Vec <bool> &used_dimensions) const {
    unsigned int count = m_positionData.size();
    Vec < DVec > samples;
    samples.resize(count);
    unsigned int dim = this->m_positionData[0].size();

    if(used_dimensions.size() != dim) {
        cout << "ERROR: used_dimensions.size() = " << used_dimensions.size() << " != " << "dim = " << dim << endl;
        exit(1);
    }

    for(unsigned int i=0; i<count; ++i) {
        samples[i].resize(0);
        samples[i].push_back(i*m_delta_t);

        for(unsigned int d=0; d<dim; ++d) {
            if(used_dimensions[d])
                samples[i].push_back(m_positionData[i][d]);
        }
    }
    DVec anchor_point;
    anchor_point.resize(0);
    for(unsigned int d=0; d<dim; ++d) {
        if(used_dimensions[d])
            anchor_point.push_back(m_anchorPoint[d]);
    }

    return SampledComposedPeriodicTrajectory(m_periodStartIndex*m_delta_t, 0.0, m_period_length, &anchor_point, &samples);
}

void TestDataSet::trajectoryForPositionsV2(SampledTrajectoryV2& traj, std::vector<int> usedDims)  {
    unsigned int count = m_positionData.size();
    Vec < DVec > samples;
    //samples.resize(count);
    unsigned int dim = this->m_positionData[0].size();


    DVec timestamps;
    for(unsigned int i=0; i<count; ++i) {

        timestamps.push_back(i*m_delta_t);
    }
    for(unsigned int d=0; d<dim; ++d)
    {
        //if(used_dimensions[d])
        //{
            DVec dimSamples;
            for(unsigned int i=0; i<count; ++i)
            {
                if (usedDims[d] != 1)
                    dimSamples.push_back(0.0);
                else
                    dimSamples.push_back(m_positionData[i][d]);
            }
            samples.push_back(dimSamples);
        //}
    }

    for(unsigned int d=0; d<dim; ++d)
    {

        traj.addDimension(timestamps,samples[d]);
    }
    traj.m_phi1 = 0.0;
    traj.m_anchorPoint = m_anchorPoint;
    traj.m_periodLength = m_period_length;
    traj.m_transientLength = m_periodStartIndex*m_delta_t;
    /*printf("Period Length in testdata %f %d",m_period_length,m_periodStartIndex);
    resultTrajectory.setTransientLength(m_periodStartIndex*m_delta_t);
    cout << "new anchor " << resultTrajectory.getAnchorPoint() << endl;
    m_positionData.clear();
    m_orientationData.clear();*/
    return;
}


void TestDataSet::cutConstantBeginning(double rel_threshold)
{
    DVec starting_point = m_positionData[0];

    unsigned int i = 1;

    while (norm2(starting_point - m_positionData[i]) / norm2(starting_point) < rel_threshold
           && i < m_positionData.size())
    {
        //      cout << "rel_distance = " << norm2(starting_point-m_positionData[i])/norm2(starting_point) << endl;
        ++i;
    }

    cout << "movement starts at index " << i << endl;

    m_positionData.erase(m_positionData.begin(), m_positionData.begin() + i);
    m_nPoints -= i;
    m_periodStartIndex -= i;
}

void TestDataSet::flip_x()
{
    m_anchorPoint[0] *= -1;

    for (unsigned int i = 0; i < m_positionData.size(); ++i)
    {
        m_positionData[i][0] *= -1;
    }
}

ostream& operator << (ostream& out, const TestDataSet& set)
{
    out << ";==========configuration==========" << endl;
    out << "nPoints: " << set.m_positionData.size() << endl;
    out << "Periodic Duration: " << set.m_period_length << endl;
    out << "Periodic Start Index: " << set.m_periodStartIndex << endl;

    out << endl;

    out << ";Anchor Point" << endl;
    out << set.m_anchorPoint << endl;

    out << endl;

    out << ";Periodic Start Point" << endl;
    out << set.m_periodStartPoint << endl;

    out << endl;

    out << ";Position and Orientation Data" << endl;

    for (Vec< DVec >::const_iterator i = set.m_positionData.begin(),
         j = set.m_orientationData.begin();
         i != set.m_positionData.end() && j != set.m_orientationData.end(); ++i, ++j)
    {
        out << "(" << (*i)[0] << "/" << (*i)[1] << "/" << (*i)[2] << "), ";
        out << "(" << (*j)[0] << "/" << (*j)[1] << "/" << (*j)[2] << "), " << endl;
    }

    return out;
}


