#ifndef NOISYSIMULATION_H
#define NOISYSIMULATION_H

#include <dmp/general/vec.h>
#include <dmp/representation/basicode.h>

namespace DMP
{

    class NoisySimulation : public ODE
    {
    public:
        NoisySimulation(double minCycleTime, double cycleVariance, double velocityVariance, DVec initialData, DVec initialDataDeriv);

        void reset();

        void addDataDeriv(double timestamp, const DVec& dataDeriv1);
        DVec getNextData(double& nextTimestamp, DVec& deriv);
        bool getNextDataStepByStep(double& curTimestamp, DVec& data, DVec& deriv);
        DVec getCurrentData();
        void flow(double t, const DVec& x, DVec& out) override;
        double minCycleTime;
        double cycleVariance;
        double velocityVariance;
        double m_fixStepSize;
    private:

        DVec currentData;
        DVec currentTargetDataDeriv1;
        DVec currentDataDeriv1;
        double currentTimestamp;
        double currentTargetTimestamp;
        DVec initialData;
        DVec initialDataDeriv;
    };

}

#endif // NOISYSIMULATION_H
