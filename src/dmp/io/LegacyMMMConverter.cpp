/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#include "LegacyMMMConverter.h"
#include <Eigen/Geometry>

#include "VirtualRobot/VirtualRobot.h"
#include "VirtualRobot/Robot.h"
#include "VirtualRobot/IK/IKSolver.h"
#include "VirtualRobot/IK/GenericIKSolver.h"

namespace DMP {

using namespace MMM;



SampledTrajectoryV2 LegacyMMMConverter::fromMMM(LegacyMotionPtr motion, bool positionOnly)
{
    Vec<DVec> pose;
    Vec<DVec> rpy;
    DVec timestamps;
    size_t dim;
    if(positionOnly)
        dim = 3;
    else dim = 7;
    pose.resize(dim);
    rpy.resize(dim);
    Eigen::Matrix3f rotMat;
    for(unsigned int f = 0; f < motion->getNumFrames(); f++)
    {

        MotionFramePtr frame = motion->getMotionFrame(f);
        timestamps.push_back(frame->timestep);
        pose[0].push_back(frame->getRootPos()[0]);
        pose[1].push_back(frame->getRootPos()[1]);
        pose[2].push_back(frame->getRootPos()[2]);
        rotMat = frame->getRootPose().block(0,0,3,3);
        Eigen::AngleAxisf aa(rotMat);
        if(!positionOnly)
        {
            pose[3].push_back(aa.angle());
            pose[4].push_back(aa.axis()[0]);
            pose[5].push_back(aa.axis()[1]);
            pose[6].push_back(aa.axis()[2]);
            rpy[4].push_back(frame->getRootRot()[0]);
            rpy[5].push_back(frame->getRootRot()[1]);
            rpy[6].push_back(frame->getRootRot()[2]);
        }

    }
    SampledTrajectoryV2 traj;
    SampledTrajectoryV2 traj2;


    traj.addDimension(timestamps, pose[0]);
    traj.addDimension(timestamps, pose[1]);
    traj.addDimension(timestamps, pose[2]);
    if(!positionOnly)
    {
        traj.addDimension(timestamps, pose[3]);
        traj.addDimension(timestamps, pose[4]);
        traj.addDimension(timestamps, pose[5]);
        traj.addDimension(timestamps, pose[6]);
        traj2.addDimension(timestamps, rpy[4]);
        traj2.addDimension(timestamps, rpy[5]);
        traj2.addDimension(timestamps, rpy[6]);
    }
    //        traj.plotAllDimensions(0, "plots/mmm");
    //        traj2.plotAllDimensions(0, "plots/mmmrpy");
    return traj;
}

LegacyMotionPtr LegacyMMMConverter::toMMM(const SampledTrajectoryV2 &traj)
{
    LegacyMotionPtr motion(new LegacyMotion("DMP"));
    for(const auto & it : traj)
    {
        MotionFramePtr frame(new MotionFrame(0));
        Eigen::Vector3f pos;
        pos << it.getPosition(0), it.getPosition(1), it.getPosition(2);
        frame->setRootPos(pos);
        motion->addMotionFrame(frame);
    }

    return motion;
}

SampledTrajectoryV2 LegacyMMMConverter::fromMMMJoints(LegacyMotionPtr motion)
{
    SampledTrajectoryV2 result;
    for(size_t j = 0; j < motion->getJointNames().size(); j++)
    {
        DVec jointValues;
        DVec timestamps;
        for(size_t i = 0; i < motion->getNumFrames(); i++)
        {
            timestamps.push_back(motion->getMotionFrame(i)->timestep);
            jointValues.push_back(motion->getMotionFrame(i)->joint[j]);
        }
        result.addDimension(timestamps, jointValues);
    }
    return result;
}

SampledTrajectoryV2 LegacyMMMConverter::getNodeTrajInTaskSpace(LegacyMotionPtr motion, const std::vector<std::string> &nodeSet, bool positionOnly, bool isLocal, bool isRPY, VirtualRobot::RobotPtr robot){
    size_t dim, dn;
    if(positionOnly)
        dn = 3;
    else if(isRPY)
        dn = 6;
    else
        dn = 7;

    dim = dn * nodeSet.size();

    if(!robot)
        robot = motion->getModel();
    VirtualRobot::RobotNodeSetPtr robotNodeSet = VirtualRobot::RobotNodeSet::createRobotNodeSet(robot, "robotnodeset", motion->getJointNames(), "", "", true);

    std::map<double, DVec > resultingMap;

    for(unsigned int f = 0; f < motion->getNumFrames(); f++)
    {
        DVec dimData;
        dimData.resize(dim);

        MotionFramePtr frame = motion->getMotionFrame(f);

        robot->setGlobalPose(frame->getRootPose(),true);
        robotNodeSet->setJointValues(frame->joint);

        for(size_t i = 0; i < nodeSet.size(); i++)
        {
            Eigen::Matrix4f curpose;

            if(isLocal){
                curpose = robot->getRobotNode(nodeSet[i])->getPoseInRootFrame();
            }else{
                curpose = robot->getRobotNode(nodeSet[i])->getGlobalPose();
            }

            dimData[dn * i] = curpose(0,3);
            dimData[dn * i + 1] = curpose(1,3);
            dimData[dn * i + 2] = curpose(2,3);

            if(!positionOnly)
            {
                if(isRPY)
                {
                    Eigen::Vector3f rpy;
                    VirtualRobot::MathTools::eigen4f2rpy(curpose, rpy);

                    dimData[dn * i + 3] = rpy(0);
                    dimData[dn * i + 4] = rpy(1);
                    dimData[dn * i + 5] = rpy(2);
                }
                else
                {
                    VirtualRobot::MathTools::Quaternion q = VirtualRobot::MathTools::eigen4f2quat(curpose);
                    dimData[dn * i + 3] = q.w;
                    dimData[dn * i + 4] = q.x;
                    dimData[dn * i + 5] = q.y;
                    dimData[dn * i + 6] = q.z;
                }
            }
        }

        resultingMap[frame->timestep] = dimData;

    }

    return SampledTrajectoryV2(resultingMap);

}

SampledTrajectoryV2 LegacyMMMConverter::getNodeTrajInTaskSpace(string motionFilePath, string motionName, const std::vector<string> &nodeSets, bool positionOnly, bool isLocal, bool isRPY)
{
    LegacyMotionReaderXML r;
    auto motion = r.loadMotion(motionFilePath,motionName);
    return getNodeTrajInTaskSpace(motion, nodeSets, positionOnly, isLocal, isRPY);
}

SampledTrajectoryV2 LegacyMMMConverter::getNodeTrajInJointSpace(LegacyMotionPtr motion, const std::vector<string> &nodeSet){
    MMM::ModelPtr robot = motion->getModel();
    VirtualRobot::RobotNodeSetPtr robotNodeSet = VirtualRobot::RobotNodeSet::createRobotNodeSet(robot, "robotnodeset", motion->getJointNames(), "", "", true);

    std::map<double, DVec > resultingMap;

    for(unsigned int f = 0; f < motion->getNumFrames(); f++)
    {
        DVec dimData;

        MotionFramePtr frame = motion->getMotionFrame(f);

        robot->setGlobalPose(frame->getRootPose(),true);
        robotNodeSet->setJointValues(frame->joint);

        for(const auto & i : nodeSet)
        {
            dimData.push_back(robot->getRobotNode(i)->getJointValue());
        }

        resultingMap[frame->timestep] = dimData;

    }

    return SampledTrajectoryV2(resultingMap);
}

MMM::LegacyMotionPtr LegacyMMMConverter::getMotionFromJointsTraj(const SampledTrajectoryV2 &traj, const std::vector<string> &nodeSet, LegacyMotionPtr muster){
    LegacyMotionPtr motion(new LegacyMotion("DMP"));

    VirtualRobot::RobotPtr robot = muster->getModel();
    VirtualRobot::RobotNodeSetPtr robotNodeSet = VirtualRobot::RobotNodeSet::createRobotNodeSet(robot, "robotnodeset", muster->getJointNames(), "", "", true);

    motion->setModel(muster->getProcessedModelWrapper());
    motion->setJointOrder(muster->getJointNames());

    int fn = 0;
    for(SampledTrajectoryV2::ordered_view::const_iterator it = traj.begin(); it != traj.end(); it++, fn++)
    {
        MMM::MotionFramePtr musterFrame = muster->getMotionFrame(fn);
        MMM::MotionFramePtr mf(new MMM::MotionFrame(musterFrame->getndof()));
        mf->setRootPose(musterFrame->getRootPose());
        robot->setGlobalPose(musterFrame->getRootPose(),true);
        robotNodeSet->setJointValues(musterFrame->joint);

        for(size_t i = 0; i < nodeSet.size(); ++i)
        {
            robot->getRobotNode(nodeSet[i])->setJointValue(it->getPosition(i));
        }

        std::vector<float> jvs = robotNodeSet->getJointValues();
        Eigen::VectorXf jv;
        jv.resize(jvs.size());

        for(size_t i = 0; i < jvs.size(); ++i)
        {
             jv(i) = jvs[i];
        }
        mf->joint = jv;
        mf->timestep = it->getTimestamp();
        motion->addMotionFrame(mf);
    }

    return motion;
}

MMM::LegacyMotionPtr LegacyMMMConverter::getMotionFromTCPTraj(const SampledTrajectoryV2 &traj, const std::vector<string> &kcnames, LegacyMotionPtr muster, bool isLocal, bool positionOnly, bool isRPY){
    LegacyMotionPtr motion(new LegacyMotion("tcpMotion_IK"));
    VirtualRobot::RobotPtr robot = muster->getModel();
    VirtualRobot::RobotNodeSetPtr robotNodeSet = VirtualRobot::RobotNodeSet::createRobotNodeSet(robot, "robotnodeset", muster->getJointNames(), "", "", true);

    VirtualRobot::IKSolver::CartesianSelection mode = VirtualRobot::IKSolver::All;

    if(positionOnly)
    {
        mode = VirtualRobot::IKSolver::Position;
    }

    int tcpDim;

    if(positionOnly)
        tcpDim = 3;
    else
    {
        if(isRPY)
        {
            tcpDim = 6;
        }
        else
        {
            tcpDim = 7;
        }
    }

    if(traj.dim() % tcpDim != 0){
        std::cout << "The input trajectory may not be corresponding to a meaningful task space motion. (because its size is not divided by 3)" << std::endl;
        return LegacyMotionPtr();
    }

    int fn = 0;

    std::vector<std::vector<float> > oldJoints;
    oldJoints.resize(kcnames.size());
    for(SampledTrajectoryV2::ordered_view::const_iterator itt = traj.begin(); itt != traj.end(); itt++, fn++)
    {

        MMM::MotionFramePtr musterFrame = muster->getMotionFrame(fn);
        MMM::MotionFramePtr mf = musterFrame->copy();

        robot->setGlobalPose(musterFrame->getRootPose(), true);
        robotNodeSet->setJointValues(musterFrame->joint);

        int k = 0;
        for(const auto & kcname : kcnames)
        {
            VirtualRobot::RobotNodeSetPtr kc = robot->getRobotNodeSet(kcname);

            VirtualRobot::GenericIKSolverPtr ikSolver(new VirtualRobot::GenericIKSolver(kc, VirtualRobot::JacobiProvider::eSVDDamped));
            ikSolver->setupJacobian(0.3f, 30);

            double x = itt->getPosition(k++);
            double y = itt->getPosition(k++);
            double z = itt->getPosition(k++);
            Eigen::Matrix4f pose;

            if(!positionOnly)
            {
                if(isRPY)
                {
                    double r = itt->getPosition(k++);
                    double p = itt->getPosition(k++);
                    double y = itt->getPosition(k++);


                    VirtualRobot::MathTools::rpy2eigen4f(r, p, y, pose);
                }
                else
                {
                    VirtualRobot::MathTools::Quaternion q;
                    q.w = itt->getPosition(k++);
                    q.x = itt->getPosition(k++);
                    q.y = itt->getPosition(k++);
                    q.z = itt->getPosition(k++);

                    pose = VirtualRobot::MathTools::quat2eigen4f(q);
                }
            }
            else
            {
                pose = Eigen::Matrix4f::Identity();
            }
            pose(0,3) = x;
            pose(1,3) = y;
            pose(2,3) = z;

            Eigen::Matrix4f globalPose;

            if(isLocal)
            {
                globalPose = robot->getRootNode()->toGlobalCoordinateSystem(pose);
            }
            else
            {
                globalPose = pose;
            }

            if(ikSolver->solve(globalPose, mode))
            {
                for(size_t nid = 0; nid < kc->getSize(); nid++)
                {
                    robot->setJointValue(kc->getNode(nid), kc->getNode(nid)->getJointValue());
                }
            }
            else
            {
                std::cout << "frame number: " << fn << std::endl;
                std::cerr << "Warning: ikSolver has no solution. Use the joint value in the muster motion !!!" << std::endl;
            }

        }

        std::vector<float> jvs = robotNodeSet->getJointValues();
        for(size_t i = 0; i < jvs.size(); ++i)
        {
            mf->joint(i) = jvs[i];
        }
        mf->timestep = itt->getTimestamp();

        motion->addMotionFrame(mf);

    }

    motion->setModel(muster->getProcessedModelWrapper());
    motion->setJointOrder(muster->getJointNames());

    return motion;
}

//    SampledTrajectoryV2 MMMConverter::fromMMM(MotionPtr motion, bool positionOnly)
//    {
//        throw Exception()<<"MMM Not found";

//    }
//    MotionPtr MMMConverter::toMMM(const SampledTrajectoryV2 &traj)
//    {
//        throw Exception()<<"MMM Not found";
//    }
//    SampledTrajectoryV2 MMMConverter::fromMMMJoints(MotionPtr motion)
//    {
//        throw Exception()<<"MMM Not found";
//    }
//    SampledTrajectoryV2 MMMConverter::toMMMJoints(MotionPtr motion)
//    {
//        throw Exception()<<"MMM Not found";
//    }



}

//#endif
