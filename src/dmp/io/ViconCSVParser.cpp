#include "ViconCSVParser.h"

#include <dmp/general/exception.h>
#include <boost/algorithm/string/trim.hpp>

using namespace DMP;

// first row is row 0
#define MARKERNAME_ROW 2
#define DIMENSIONS_PER_MARKER 3

ViconCSVParser::ViconCSVParser()
= default;

const string& ViconCSVParser::get(unsigned int rowIndex, unsigned int columnIndex) const
{
    if (rowIndex >= rows.size())
        //        throw Exception("row index out of boundaries: ") << rowIndex << " size: " <<  rows.size();
    {
        return emptyString;
    }

    const Row& row = rows[rowIndex];

    if (columnIndex >= row.size())
        //        throw Exception("col index out of boundaries: ") << columnIndex << " size: " <<  row.size();
    {
        return emptyString;
    }

    return row[columnIndex];
}

StringList ViconCSVParser::getColumn(int columnIndex) const
{
    int size = rows.size();
    StringList result(size);

    for (int row = 0; row < size; ++row)
    {
        result.at(row) = get(row, columnIndex);
    }

    return result;
}

const ViconCSVParser::Row& ViconCSVParser::getRow(int rowIndex) const
{
    return rows.at(rowIndex);
}


void ViconCSVParser::parse(std::string filepath, int lineCount)
{
    std::ifstream f(filepath.c_str());

    if (!f.is_open())
    {
        throw Exception("Could not open file: " + filepath);
    }

    this->filepath = filepath;
    std::cout << "Reading from file : " << filepath << std::endl;
    using Tokenizer = boost::tokenizer< boost::escaped_list_separator<char> >;

    rows.clear();

    string line;
    int lineNumber = 0;
    Vec<std::string> stringVec;

    while (getline(f, line) && (lineCount == -1 || lineNumber < lineCount))
    {
        boost::algorithm::trim(line);
        Tokenizer tok(line);

        stringVec.assign(tok.begin(), tok.end());
        rows.push_back(stringVec);
        lineNumber++;
    }

    std::cout << "reading done" << std::endl;

}


DVec ViconTrajectoryCSVParser::StringVecToDoubleVec(const StringList& strings)
{
    int size = strings.size();
    DVec result(size, 0.0);

    for (int i = 0; i < size; ++i)
    {
        result[i] = atof(strings[i].c_str());
    }

    return result;
}

void ViconTrajectoryCSVParser::getTrajectory(string markerName, SampledTrajectoryV2& result) const
{
    //    SampledTrajectoryV2 result ;

    int colIndex = getColumnOfMarker(markerName);

    if (colIndex == -1)
    {
        throw Exception("Marker with name ") << markerName << " does not exist";
    }

    std::vector<double> frames = getFrames();
    int frameCount = frames.size();

    for (int dim = 0; dim < DIMENSIONS_PER_MARKER; dim++)
    {
        StringList col = getColumn(colIndex + dim);
        StringList stringValues(col.begin() + MARKERNAME_ROW + 3, col.begin() + MARKERNAME_ROW + 3 + frameCount);
        DVec values = StringVecToDoubleVec(stringValues);

        result.addPositionsToDimension(dim, frames, values);
        result.differentiateDiscretlyForDim(dim, 2);
        //        result.plot(dim,0);
    }

}

int ViconTrajectoryCSVParser::getColumnOfMarker(const string& markerName) const
{
    size_t col = 0;
    Row row = rows.at(MARKERNAME_ROW);

    while (col < row.size())
    {
        if (markerName == row[col])
        {
            return col;
        }

        col++;
    }

    return -1;
}

std::vector<double> ViconTrajectoryCSVParser::getFrames() const
{
    std::vector<double> frames;
    StringList col = getColumn(0);

    for (StringList::iterator it = col.begin() + MARKERNAME_ROW + 3; it != col.end(); ++it)
    {
        if (!it->empty())
        {
            frames.push_back(atof(it->c_str()));
        }
        else
        {
            break;
        }
    }

    return frames;
}

int ViconTrajectoryCSVParser::getEndOfColumn(const StringList& column, int offset) const
{
    int size = column.size();

    for (int i = offset; i < size; ++i)
    {
        if (column[i].size() == 0)
        {
            return i;
        }
    }

    return size - 1;
}
