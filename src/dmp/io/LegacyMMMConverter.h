/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#ifndef DMP_LEGACYMMMCONVERTER_H
#define DMP_LEGACYMMMCONVERTER_H

#include <dmp/representation/trajectory.h>


#include <MMM/Motion/Legacy/LegacyMotion.h>
#include <MMM/Motion/Legacy/LegacyMotionReaderXML.h>
#include <MMM/Motion/Legacy/LegacyMotionReaderC3D.h>
#include <MMM/Model/ModelReaderXML.h>

#include <VirtualRobot/VirtualRobot.h>

namespace DMP {

    class LegacyMMMConverter
    {
    public:

//        #ifdef MMM_FOUND
        static SampledTrajectoryV2 fromMMM(MMM::LegacyMotionPtr motion, bool positionOnly = false);
        static MMM::LegacyMotionPtr toMMM(const SampledTrajectoryV2 &traj);
        static SampledTrajectoryV2 fromMMMJoints(MMM::LegacyMotionPtr motion);

        /**
         * @brief getNodeTrajInTaskSpace
         * @param motion
         * @param nodeSets
         * @param positionOnly
         * @param isLocal
         * @param isRPY
         * @param robot Used robot. If NULL, the robot model is created on the fly from the model stored in the motion. Mostly a performance tweak if this function is called alot.
         * @return
         */
        static SampledTrajectoryV2 getNodeTrajInTaskSpace(MMM::LegacyMotionPtr motion, const std::vector<std::string> &nodeSets, bool positionOnly = true, bool isLocal = true, bool isRPY = true, VirtualRobot::RobotPtr robot = NULL);
        static SampledTrajectoryV2 getNodeTrajInTaskSpace(std::string motionFilePath, std::string motionName, const std::vector<string> &nodeSets, bool positionOnly = true, bool isLocal = true, bool isRPY = true);
        static SampledTrajectoryV2 getNodeTrajInJointSpace(MMM::LegacyMotionPtr motion, const std::vector<std::string> &nodeSets);

        static MMM::LegacyMotionPtr getMotionFromJointsTraj(const SampledTrajectoryV2& traj, const std::vector<std::string>& nodeSet, MMM::LegacyMotionPtr muster);
        static MMM::LegacyMotionPtr getMotionFromTCPTraj(const SampledTrajectoryV2& traj, const std::vector<std::string>& kcnames, MMM::LegacyMotionPtr muster,bool isLocal = true, bool positionOnly = false, bool isRPY = true);
//#endif // contents of MMMConverter
    };

} // namespace DMP



#endif // #include
