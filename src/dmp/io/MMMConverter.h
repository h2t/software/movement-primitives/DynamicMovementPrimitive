/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#ifndef DMP_MMMCONVERTER_H
#define DMP_MMMCONVERTER_H

#include <dmp/representation/trajectory.h>


#include <MMM/Motion/Motion.h>
#include <MMM/Model/ModelReaderXML.h>

#include <VirtualRobot/VirtualRobot.h>


namespace DMP {

///
/// \brief The MMMConverter class
/// Converts MMM objects of the new format to DMP sampled trajectories
///
    class MMMConverter
    {
    public:

        ///
        /// \brief fromMMMJoints convert a MMM-Motion to a DMP-trajectory. The states of the DMP-trajectory are the joint angles of the MMM trajectory
        /// \param motion a pointer to a MMM-Motion
        /// \return
        ///
        static SampledTrajectoryV2 fromMMMJoints(MMM::MotionPtr motion);
        ///
        /// \brief fromMMMJoints convert a MMM-Motion to a DMP-trajectory. The states of the DMP-trajectory are the joint angles of the MMM trajectory
        /// \param motion a pointer to a MMM-Motion
        /// \param wantedDimensions vector that contains the joins that should be included into the DMP. The order of the DMP's dimensions is equal to the order in the vector. Pass an empty vector to get all dimensions
        /// \param t_start measurements with timestamps < t_start are being disregarded
        /// \param t_stop measurements with timestamps > t_stop are being disregarded
        /// \return
        ///
        static SampledTrajectoryV2 fromMMMJoints(MMM::MotionPtr motion,
                                                 std::vector<std::string> wantedDimensions,
                                                 float t_start = std::numeric_limits<float>::min(),
                                                 float t_stop = std::numeric_limits<float>::max());


    };

}

#endif
