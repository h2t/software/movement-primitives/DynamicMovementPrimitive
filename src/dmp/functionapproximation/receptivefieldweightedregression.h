#ifndef RECEPTIVEFIELDWEIGHTEDREGRESSION_H
#define RECEPTIVEFIELDWEIGHTEDREGRESSION_H

#include "functionapproximation.h"
#include "locallyweightedlearningbasisfunctions.h"


#include <dmp/general/helpers.h>
#include <dmp/io/rapidxml/rapidxml.hpp>
#include <dmp/io/rapidxml/rapidxml_print.hpp>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>


#include <map>
namespace DMP
{
class RFWR :  public DMP::FunctionApproximationInterface
{
public:
    struct KernelData
    {
        RFWRKernel kernel;
        Eigen::VectorXf beta;
        Eigen::MatrixXf P;
        double Wsum;
        double Esum;

        Eigen::VectorXf H;
        Eigen::MatrixXf R;
        Eigen::VectorXf r;
    };

    RFWR(double forgettingfactor = 0.99)
    {
        lambda = forgettingfactor;
    }

    // inherited from FunctionApproximationInterface
    void learn(const Vec<DVec>&x, const Vec<DVec> &y) override;
    void ilearn(const DVec &x, double y) override;
    const DVec operator()(const DVec& x) const override;
    void reset() override;
    FunctionApproximationInterfacePtr clone() override
    {
        throw std::logic_error{"Not yet implemented!"};
    }

    // evaluation
    double evaluate(const Vec<DVec> &x, const DVec& y, double* maxError = NULL, DVec* predictedValues = NULL);

    const Vec<KernelData>& getKernels()const
    {
        return __kernels;
    }

    void setKernels(const Vec<KernelData>& kernels) {
        if(kernels.size() != getKernelSize())
        {
            throw DMP::Exception("The given kernels' size is different from the required kernel size.");
        }
        __kernels = kernels;
    }

    vector<double> getWeights() override;

    size_t getKernelSize() override{
        return __kernels.size();
    }

    unsigned int dim()
    {
        return dimension;
    }


    bool supportsIncrementalLearning() override
    {
        return true;
    }

protected:
    double calcWeight(const RFWRKernel& kernel, const Vec<DVec>& x, const DVec& y, double guessedMinimum = 0)
    {
        return 0;
    }

    double getWeightValue(unsigned int kernelIndex) const;
    const Eigen::VectorXf getNetInput(const DVec &x, int kernelId) const;

private:
    DVec __widthFactor;

    double lambda;

    Vec<KernelData> __kernels;

    unsigned int dimension;

};



}


#endif
