/**
*
* @package    DMP::
* @author     Johannes Ernesti
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef DMP_DMP2DPERTURBATIONAPPROXIMATION_H
#define DMP_DMP2DPERTURBATIONAPPROXIMATION_H

#include <dmp/general/helpers.h>
#include "locallyweightedlearningbasisfunctions.h"

//#include <dmp/representation/newdmpformulation.h>
#include <dmp/io/rapidxml/rapidxml.hpp>
#include <dmp/io/rapidxml/rapidxml_print.hpp>
#include "functionapproximation.h"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>


namespace DMP
{

    class DMP2DPerturbationApproximation :  public DMP::FunctionApproximationInterface
    {
    public:
        Vec< LWLPhaseSpaceBasisFunction* > m_allFunctions;
        Vec< double > m_allWeights;

        DMP2DPerturbationApproximation() {}


        //  DMP2DPerturbationApproximation(const TransientBasisFunctionsDescription &trans_desc,
        //                                 const PeriodicBasisFunctionsDescription &per_desc);
        DMP2DPerturbationApproximation(const Vec< LWLPhaseSpaceBasisFunction* > basis_functions);
        ~DMP2DPerturbationApproximation();

        void learn(const Vec<DVec>&x, const Vec<DVec> &y) override;
        const DVec operator()(const DVec& x) const override
        {
            throw Exception("Implement this!");
        }
        void reset() override
        {
            throw Exception("Implement this!");
        }

        double operator()(double phi, double r) const;
        double operator()(double phi, double r, bool use_weights) const;

        void createTrajectory(const DVec& times, const Vec< DVec > &phi_r_samples, SampledTrajectory& out) const;

        void plot(const DVec& times, const Vec< DVec > &phi_r_samples,
                  const char* tmp_filename = "plots/tmp_traj.gp", bool show = true) const;
        void plotWithoutWeights(const DVec& times, const Vec< DVec > &phi_r_samples,
                                const char* tmp_filename = "plots/tmp_traj.gp") const;

        double getWeight(const LWLPhaseSpaceBasisFunction* basis_functions, const Vec< DVec > &phi_r_samples,
                         const DVec& ftarg);

        void learn(const Vec< DVec > &phi_r_samples, const DVec& ftarg);

        friend ostream& operator << (ostream& out, const DMP2DPerturbationApproximation& approx);
        void readFromFile(FILE* fp);



        // FunctionApproximationInterface interface
    public:
        FunctionApproximationInterfacePtr clone() override;

        // FunctionApproximationInterface interface
    public:
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            ar& boost::serialization::base_object<FunctionApproximationInterface>(*this);

        }
        friend class boost::serialization::access;
    };
    //    BOOST_CLASS_EXPORT_GUID(DMP2DPerturbationApproximation, "DMP2DPerturbationApproximation")



} // namespace DMP

#endif // DMP_DMP2DPERTURBATIONAPPROXIMATION_H
