/**
*
* @package    DMP::
* @author     Johannes Ernesti
* @date       2011
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/
#include "dmp2dperturbationapproximation.h"



namespace DMP
{


    class getWeight_target_params
    {
    public:
        const LWLPhaseSpaceBasisFunction* basis_function;
        const Vec< DVec > &phi_r_samples;
        const DVec& ftarg;

        getWeight_target_params(const LWLPhaseSpaceBasisFunction* f, const DVec& ftarg,
                                const Vec< DVec > &phi_r_samples)
            : basis_function(f), phi_r_samples(phi_r_samples), ftarg(ftarg)
        {
        }
    };
    double getWeight_target(double w, void* params)
    {
        getWeight_target_params* p = (getWeight_target_params*)params;

        double res = 0.0;

        for (unsigned int i = 0; i < p->ftarg.size(); ++i)
        {
            double phi = (p->phi_r_samples)[i][0],
                   r = (p->phi_r_samples)[i][1];

            double diff = (p->ftarg[i] - w);
            //printf("%e %e \n",p->ftarg[i],w);
            //printf("%e %e \n",(*p->basis_function)(phi, r),diff);
            res += (*p->basis_function)(phi, r) * diff * diff;
            //printf("retemps %e \n",res);
        }
     //printf("res %e \n",res);
        return res;
    }

    //DMP2DPerturbationApproximation::DMP2DPerturbationApproximation(const TransientBasisFunctionsDescription &trans_desc,
    //                                                             const PeriodicBasisFunctionsDescription &per_desc) {
    //  m_trans_desc = trans_desc;
    //  m_per_desc = per_desc;

    //  m_transientFunctions.reserve(m_trans_desc.N());
    //  m_periodicFunctions.reserve(m_per_desc.N());

    //  for(unsigned int i=0; i<m_trans_desc.N(); ++i) {
    //      DVec center(2);
    //      center = pol2cart(m_trans_desc.m_pj[i]);
    //      LWLDistanceBasedPhaseSpaceBasisFunction f(center, m_trans_desc.m_deltaj[i]);
    //      m_transientFunctions.push_back(f);
    //      m_allFunctions.push_back(&m_transientFunctions.back());
    //  }

    //  for(unsigned int i=0; i<m_per_desc.N(); ++i) {
    //      double center = 2*M_PI/m_per_desc.N()*i;

    //      JGAMSCombinedWithGaussTest f(center, m_per_desc.m_width, m_per_desc.m_mu, m_per_desc.m_eps);
    //      m_periodicFunctions.push_back(f);
    //      m_allFunctions.push_back((LWLPhaseSpaceBasisFunction*) &m_periodicFunctions.back());
    //  }

    //  m_allWeights.resize(m_allFunctions.size());
    //}

    DMP2DPerturbationApproximation::DMP2DPerturbationApproximation(const Vec< LWLPhaseSpaceBasisFunction* > basis_functions)
    {
        unsigned int D = basis_functions.size();
        m_allFunctions.assign(basis_functions.begin(), basis_functions.end());
        m_allWeights.resize(D);
    }

    DMP2DPerturbationApproximation::~DMP2DPerturbationApproximation()
    {
        for (unsigned int i = 0; i < m_allFunctions.size(); ++i)
        {
            delete m_allFunctions[i];
        }
    }

    double DMP2DPerturbationApproximation::operator()(double phi, double r) const
    {
        double res = 0.0, div = 0.0;

        for (unsigned int i = 0; i < m_allFunctions.size(); ++i)
        {
            double value = (*m_allFunctions[i])(phi, r);
            div += value;
            res += m_allWeights[i] * value;
        }

        if (div == 0.0)
        {
            return 0.0;
        }
        else
        {
            return res / div;
        }
    }

    double DMP2DPerturbationApproximation::operator()(double phi, double r, bool use_weights) const
    {
        double res = 0.0, div = 0.0;

        for (unsigned int i = 0; i < m_allFunctions.size(); ++i)
        {
            double value = (*m_allFunctions[i])(phi, r);
            div += value;

            if (use_weights)
            {
                res += m_allWeights[i] * value;
            }
            else
            {
                res += value;
            }
        }

        return res;
    }

    void DMP2DPerturbationApproximation::createTrajectory(
        const DVec& times, const Vec< DVec > &phi_r_samples,
        SampledTrajectory& out) const
    {
        DVec values(times.size());

        for (unsigned int i = 0; i < values.size(); ++i)
        {
            values[i] = (*this)(phi_r_samples[i][0], phi_r_samples[i][1]);
        }

        SampledTrajectory tmp(times, values);
        out = tmp;
    }

    void DMP2DPerturbationApproximation::plot(const DVec& times, const Vec< DVec > &phi_r_samples,
            const char* tmp_filename, bool show) const
    {
        SampledTrajectory tmp;
        createTrajectory(times, phi_r_samples, tmp);
        tmp.plot(tmp_filename, show);
    }

    void DMP2DPerturbationApproximation::plotWithoutWeights(const DVec& times, const Vec< DVec > &phi_r_samples,
            const char* tmp_filename) const
    {
        DVec values(times.size());

        for (unsigned int i = 0; i < values.size(); ++i)
        {
            values[i] = (*this)(phi_r_samples[i][0], phi_r_samples[i][1], false);
        }

        SampledTrajectory tmp(times, values);
        tmp.plot(tmp_filename);
    }


    // TODO: this method is very, very poorly implemented!
    //       instead of a multi-purpose minimizer, RFWR should be used!
    //       (See: Constructive Incremental Learning From Only Local Information; Schaal, Atkeson)
    double DMP2DPerturbationApproximation::getWeight(const LWLPhaseSpaceBasisFunction* basis_functions,
            const Vec< DVec > &phi_r_samples,
            const DVec& ftarg)
    {
        double res = 1337.0;
        int status;
        int iter = 0, max_iter = 200;
        const gsl_min_fminimizer_type* T;
        gsl_min_fminimizer* s;
        double a = -500000.0, b = 500000.0;
        gsl_function F;

        F.function = &getWeight_target;
        F.params = new getWeight_target_params(basis_functions, ftarg, phi_r_samples);

        T = gsl_min_fminimizer_brent;
        s = gsl_min_fminimizer_alloc(T);
        gsl_min_fminimizer_set(s, &F, 0.0, a, b);

        do
        {
            iter++;
            status = gsl_min_fminimizer_iterate(s);
            a = gsl_min_fminimizer_x_lower(s);
            b = gsl_min_fminimizer_x_upper(s);

            status = gsl_min_test_interval(a, b, 0.001, 0.0);

            if (status == GSL_SUCCESS)
            {
                res = gsl_min_fminimizer_x_minimum(s);
            }
        }
        while (status == GSL_CONTINUE && iter < max_iter);

        gsl_min_fminimizer_free(s);
        delete(getWeight_target_params*)F.params;

        //  double best_result = gsl_min_fminimizer_x_minimum (s);
        return res;
    }

    void DMP2DPerturbationApproximation::learn(const Vec<DVec> &x, const Vec<DVec> &y)
    {
        throw Exception("Implement this!");
    }

    void DMP2DPerturbationApproximation::learn(const Vec<DVec> &phi_r_samples, const DVec& ftarg)
    {
        unsigned int D = m_allFunctions.size();
        m_allWeights.assign(D, 0.0);

        #pragma omp parallel for

        for (unsigned int i = 0; i < D; ++i)
        {
            m_allWeights[i] = getWeight(m_allFunctions[i], phi_r_samples, ftarg);
        }

        //  cout << "allWeights: " << m_allWeights << endl;
    }

    void DMP2DPerturbationApproximation::readFromFile(FILE* fp)
    {
        char buf[J_LINE_BUF_SIZE];
        getNextLine(fp, buf);

        if (strcmp(buf, ";Weights\n") != 0)
        {
            cout << "ERROR: No weights found - aborting" << endl;
            return;
        }

        m_allWeights = readVectorFromFile(fp);
        m_allFunctions.clear();
        m_allFunctions.resize(m_allWeights.size());

        for (unsigned int i = 0; i < m_allWeights.size(); ++i)
        {
            getNextLine(fp, buf);
            m_allFunctions[i] = LWLPhaseSpaceBasisFunction::newFromString(buf);
        }


    }



    ostream& operator << (ostream& out, const DMP2DPerturbationApproximation& approx)
    {
        out << ";Weights" << endl;
        out << approx.m_allWeights << endl;

        for (unsigned int i = 0; i < approx.m_allFunctions.size(); ++i)
        {
            out << *approx.m_allFunctions.at(i) << endl;
        }

        return out;
    }

    FunctionApproximationInterfacePtr DMP2DPerturbationApproximation::clone()
    {
        return FunctionApproximationInterfacePtr(new DMP2DPerturbationApproximation(*this));
    }




} // namespace DMP
