#include "functionapproximation/locallyweightedlearning.h"

#include "dmp/general/helpers.h"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>
//#include <omp.h>

using namespace DMP;

double DMP::getLWRWeight_target(double w, void* params)
{
    getLWRWeight_target_params* p = (getLWRWeight_target_params*)params;

    double resSum = 0.0;

    if (!p->basis_function)
    {
        throw Exception("Basis function pointer is NULL");
    }

    unsigned int size = p->y.size();

    //#pragma omp parallel for
    for (unsigned int i = 0; i < size; ++i)
    {
        double x = p->x.at(i);
        double diff = fabs(p->y[i] - w);
        double result = (*p->basis_function)(x) * diff * diff;

        //#pragma omp atomic
        resSum += result;
    }

    return resSum;
}
double DMP::getLWRWeightGradient_target(double w, void* params)
{
    getLWRWeightGradient_target_params* p = (getLWRWeightGradient_target_params*)params;

    double resSum = 0.0;

    if (!p->basis_function)
    {
        throw Exception("Basis function pointer is NULL");
    }

    unsigned int size = p->y.size();

    //#pragma omp parallel for
    for (unsigned int i = 0; i < size; ++i)
    {
        double x = p->x.at(i);
        double interpolatedPosition = (p->weight + w * (x - p->center));
        double diff = fabs(p->y[i] - interpolatedPosition);
        double result =  pow((*p->basis_function)(x), 1) * diff * diff;
        //#pragma omp atomic
        resSum += result;
    }

    return resSum;
}


