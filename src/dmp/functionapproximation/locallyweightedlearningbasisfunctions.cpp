#include "locallyweightedlearningbasisfunctions.h"

namespace DMP
{
    ostream& operator << (ostream& out, const LWLPhaseSpaceBasisFunction& fct)
    {
        out << fct.toString();
        return out;
    }
}

using namespace DMP;

LWLPhaseSpaceBasisFunction* LWLPhaseSpaceBasisFunction::newFromString(string str)
{
    size_t pos = str.find(" (");
    string name = str.substr(0, pos);

    if (name == "JGAMSCombinedWithGaussAndIndicator")
    {
        return JGAMSCombinedWithGaussAndIndicator::newFromString(str);
    }
    else if (name == "JDistanceAndSeparationBasedPhaseSpaceBasisFunction")
    {
        return JDistanceAndSeparationBasedPhaseSpaceBasisFunction::newFromString(str);
    }

    cout << "ERROR: tried to read unknown basis function!" << endl;
    return nullptr;
}


string JDistanceAndSeparationBasedPhaseSpaceBasisFunction::toString() const
{
    stringstream out;
    out << "JDistanceAndSeparationBasedPhaseSpaceBasisFunction (";
    out << m_center << ", " << m_delta << ", " << m_mu1 << ", " << m_mu2 << ")";
    return out.str();
}

LWLPhaseSpaceBasisFunction* JDistanceAndSeparationBasedPhaseSpaceBasisFunction::newFromString(const string& str)
{
    size_t begin = str.find(" (") + 2;
    size_t end = str.rfind(")");
    string name = str.substr(0, begin);
    string parameters = str.substr(begin, end);

    DVec center;
    double delta, mu1, mu2;

    parameters = extractVectorFromString(parameters, center);
    sscanf(parameters.c_str() + 1, "%lf,%lf,%lf", &delta, &mu1, &mu2);

    return new JDistanceAndSeparationBasedPhaseSpaceBasisFunction(center, delta, mu1, mu2);
}

JGAMSCombinedWithGaussAndIndicator* JGAMSCombinedWithGaussAndIndicator::newFromString(const string& str)
{
    size_t begin = str.find(" (") + 2;
    size_t end = str.rfind(")");
    string name = str.substr(0, begin);
    string parameters = str.substr(begin, end);

    double center, width, mu, mu1, mu2;
    unsigned int k;
    sscanf(parameters.c_str(), "%lf,%lf,%lf,%lf,%lf,%u", &center, &width, &mu, &mu1, &mu2, &k);
    return new JGAMSCombinedWithGaussAndIndicator(center, width, mu, mu1, mu2, k);
}
