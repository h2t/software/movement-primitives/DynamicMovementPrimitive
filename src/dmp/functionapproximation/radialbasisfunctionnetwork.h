#ifndef RADIALBASISFUNCTIONINTERPOLATOR_H
#define RADIALBASISFUNCTIONINTERPOLATOR_H

#include "locallyweightedlearningbasisfunctions.h"


#include <dmp/general/helpers.h>
#include <dmp/io/rapidxml/rapidxml.hpp>
#include <dmp/io/rapidxml/rapidxml_print.hpp>

#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>


#include <map>
#include <armadillo>
using namespace arma;

namespace DMP
{

template <typename Kernel = GaussRadialBasisFunction>
class RBFN
{

#define LOW_WIDTH_LIMIT 0.01
public:
    RBFN()
    {
        errtol = 1e-5;
    }

    // inherited from FunctionApproximationInterface
    void learn(const DVec2d &x, const DVec2d &y);
    double evaluate(const DVec2d& x, const DVec2d& y, double* maxError, DVec2d* predictedValues);

    // create kernels

    const DVec operator()(const DVec& x) const;
    void reset();

    const Vec<Kernel>& getKernels()const
    {
        return __kernels;
    }

    mat getWeightMat();

    size_t getKernelSize(){
        return __kernels.size();
    }

    unsigned int dim()
    {
        return indim;
    }

    void setWidthFactor(DVec widthFactor)
    {
        __widthFactor = widthFactor;
    }

protected:

private:
    DVec __widthFactor;
    Vec<Kernel> __kernels;
    unsigned int outdim;
    unsigned int indim;

    double errtol;
    mat wMat;

public:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int file_version)
    {
        ar& boost::serialization::make_nvp("__widthFactor", __widthFactor);
        ar& boost::serialization::make_nvp("__kernels", __kernels);
        ar& boost::serialization::make_nvp("indim", indim);
        ar& boost::serialization::make_nvp("outdim", outdim);

    }
    template <typename ValueType>
    char* valueToAllocatedCharString(rapidxml::xml_document<> &doc, ValueType value) const
    {
        std::stringstream valueStream;
        valueStream << value;
        return doc.allocate_string(valueStream.str().c_str());
    }
};

template <typename Kernel>
void RBFN<Kernel>::reset()
{
    __kernels.clear();
}

template <typename Kernel>
void RBFN<Kernel>::learn(const DVec2d &x, const DVec2d &y)
{
    if (x.size() == 0 || y.size() == 0)
    {
        return;
    }

    double data_size = x.size();
    outdim = y[0].size();
    indim = x[0].size();
    while(__widthFactor.size() < indim)
    {
        std::cout << __widthFactor.size() << ", " << indim << std::endl;
        __widthFactor.push_back(1);
    }

    reset();


    // initialize kernel
    {
        const DVec& center = x.at(0);
        const DVec& cx = x.at(0);
        DVec width;
//        #pragma omp parallel
        for(size_t j = 0; j < center.size(); ++j)
        {
            double closestDistance = std::numeric_limits<double>::max();

            for (size_t k = 0; k < x.size(); ++k)
            {
                if(k == 0)
                    continue;

                if(fabs(x.at(k).at(j) - cx.at(j)) == 0)
                    continue;

                if (fabs(x.at(k).at(j) - cx.at(j)) < closestDistance)
                {
                    closestDistance = fabs(x.at(k).at(j) - cx.at(j));
                }
            }

            double cwidth = closestDistance * __widthFactor.at(j) * 2.5;
            width.push_back(cwidth);
        }

        __kernels.push_back(Kernel(center, width));
    }


    double toterr = 1000;
    while (toterr > errtol)
    {
        // calc weight matrix
        mat fX = zeros<mat>(data_size, __kernels.size());
        mat fY = zeros<mat>(data_size, outdim);

        for(size_t i = 0; i < data_size; ++i)
        {

            for(size_t j = 0; j < __kernels.size(); ++j)
            {
                double kval = __kernels.at(j)(x.at(i));

                fX(i,j) = kval;
            }

            for(size_t j = 0; j < outdim; ++j)
            {
                fY(i,j) = y.at(i).at(j);
            }
        }


        wMat = pinv(fX) * fY; // __kernels.size() x outdim
//        std::cout << wMat << std::endl;
        // calc errors
        toterr = 0;
        double maxError = 0;
        int maxErrorId = 0;
        for(size_t i = 0; i < data_size; ++i)
        {
            DVec cy = operator ()(x.at(i));
            DVec ty = y.at(i);

            double sqerr = 0;
            for (size_t j = 0; j < outdim; ++j)
            {
                sqerr += pow(ty.at(j) - cy.at(j),2);
            }
            sqerr = sqrt(sqerr);

            if(sqerr > maxError)
            {
                maxErrorId = i;
                maxError = sqerr;
            }

            toterr += sqerr;
        }
        toterr /= data_size;


        // add kernel
        if (toterr <= errtol || __kernels.size() == data_size)
            break;

        const DVec& center = x.at(maxErrorId);
        const DVec& cx = x.at(maxErrorId);
        DVec width;

        for(size_t j = 0; j < center.size(); ++j)
        {
            double closestDistance = std::numeric_limits<double>::max();

            for (size_t k = 0; k < x.size(); ++k)
            {
                if(k == maxErrorId)
                    continue;

                if(fabs(x.at(k).at(j) - cx.at(j)) == 0)
                    continue;

                if (fabs(x.at(k).at(j) - cx.at(j)) < closestDistance)
                {
                    closestDistance = fabs(x.at(k).at(j) - cx.at(j));
                }
            }

            double cwidth = closestDistance * __widthFactor.at(j) * 2.5;
            width.push_back(cwidth);
        }

        __kernels.push_back(Kernel(center, width));
    }

    std::cout << "Kernel size is: " << getKernelSize() << std::endl;
}

template <typename Kernel>
const DVec RBFN<Kernel>::operator()(const DVec& x) const
{
    checkValues(x);

    mat fX = zeros<mat>(1, __kernels.size());

    for(size_t j = 0; j < __kernels.size(); ++j)
    {
        double kval = __kernels.at(j)(x);

        fX(0,j) = kval;
    }

    mat fY = fX * wMat; // 1 x outdim

    DVec result;
    for(size_t i = 0; i < fY.n_cols; ++i)
    {
        result.push_back(fY(0,i));
    }

    return result;
}



template <typename Kernel>
double RBFN<Kernel>::evaluate(const DVec2d& x, const DVec2d& y, double* maxError, DVec2d* predictedValues)
{
    if (x.size() == 0 || y.size() == 0)
    {
        return 0;
    }

    double toterr = 0;
    for(size_t i = 0; i < x.size(); ++i)
    {
        DVec cx = x[i];
        DVec cy = operator ()(cx);
        DVec ty = y[i];

        double sqerr = 0;
        for (size_t j = 0; j < outdim; ++j)
        {
            sqerr += pow(ty[j] - cy[j],2);
        }
        sqerr = sqrt(sqerr);
        toterr += sqerr;
    }

    double meanError = toterr / x.size();
    return meanError;
}


//template <typename Kernel>
//void RBFInterpolator<Kernel>::createKernels(const DVec &xmin, const DVec &xmax, int kernelNum)
//{
//    if(xmin.size() != 2 || xmax.size() != 2)
//    {
//        DMP::Exception("createKernels: not implemented for dimension different from 2");
//    }

//    if(xmin.size() != xmax.size())
//    {
//        DMP::Exception("createKernels: cannot create kernels");
//    }

//    if(kernelNum == 0)
//    {
//        DMP::Exception("createKernels: kernel number cannot be zero.");
//    }

//    double xdist = (xmax[0] - xmin[0]) / (double)kernelNum;
//    double ydist = (xmax[1] - xmin[1]) / (double)kernelNum;

//    for(double x = xmin[0]; x <= xmax[0]; x = x + xdist)
//    {
//        for(double y = xmin[1]; y <= xmax[1]; y = y + ydist)
//        {
//            DVec center;
//            DVec width;

//            center.push_back(x);
//            center.push_back(y);

//            width.push_back(xdist * __widthFactor[0] * 2.5);
//            width.push_back(ydist * __widthFactor[1] * 2.5);

//            KernelData kernel;
//            kernel.kernel = Kernel(center, width);
//            kernel.width = width;
//            kernel.center = center;
//            kernel.weight = 0;
//            kernel.P = 1;

//            __kernels.push_back(kernel);
//        }
//    }
//}



}


#endif
