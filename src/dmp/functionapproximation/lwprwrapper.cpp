/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "lwprwrapper.h"
#include <cstdio>
using namespace DMP;


#ifdef USE_LWPR
void LWPRWrapper::reset()
{
    lwpr = LWPRPtr(new LWPR_Object(dimIn, dimOut));

    doubleVec initD(dimIn);
    for(double & i : initD)
    {
        i = 0.2;
    }

    lwpr->setInitD(initD);
    lwpr->updateD(true);
    //    lwpr->penalty(1.1);

    //     lwpr->setInitAlpha(0.25);
    //     lwpr->useMeta(true);

    //     lwpr->wGen(0.75);
    //     lwpr->penalty(1);
    //     lwpr->normIn(doubleVec(1,0.0005));
    doubleVec scaleFactors;
    if(lwpr->nIn() > 0)
    for(size_t i = 0; i < static_cast<size_t>(lwpr->nIn()); ++i)
        scaleFactors.push_back(scalingFactor);

    lwpr->normIn(scaleFactors);
    //     lwpr->normIn(doubleVec(1,0.04));

    //    lwpr->normOut(doubleVec(1,10000));

}


LWPRWrapper::LWPRWrapper(int dimIn, int dimOut, double scalingFactor,double forget) : scalingFactor(scalingFactor)
{
    this->dimIn = dimIn;
    this->dimOut = dimOut;

    lambda = forget;
    reset();
    lwpr->initLambda(forget);

}

void LWPRWrapper::learn(const Vec<DVec> &x, const Vec<DVec> &y)
{
    if(y.size() < x.size() && y.size() == 1) // PSL1
    {
        DVec r = y.at(0);
        unsigned int size = std::min(x.size(), r.size());

        for (unsigned int i = 0; i < size ; ++i)
        {
            DVec cx = x.at(i);

            doubleVec factors;
            for(unsigned int j = 0; j < cx.size(); ++j)
                factors.push_back(cx.at(j));

            doubleVec responds;
            responds.push_back(r.at(i));

            std::cout << "cx: " <<  x.at(i) << " value: " << r.at(i) << std::endl;

            lwpr->update(factors, responds);
//            std::cout << " prediction: " << lwpr->predict(doubleVec(1,x.at(0).at(i)), 0.0001).at(0) << std::endl;

        }

        std::cout << "Kernel Count: " << lwpr->numRFS() << std::endl;
    }
    else
    {
        throw DMP::Exception("PSL2: Not Implemented");
    }


}

const DVec LWPRWrapper::operator()(const DVec& x) const
{
    doubleVec uValues = x;

    return lwpr->predict(uValues);
}


string DMP::LWPRWrapper::toString() const
{
    char* fileName = tmpnam(nullptr);
    lwpr->writeXML(fileName);
    throw std::logic_error{"No return value"};
}

bool DMP::LWPRWrapper::fromString(const string& /*content*/)
{
    throw std::logic_error{"Not yet implemented!"};
}
#else
void LWPRWrapper::reset()
{
    throw Exception("LWPR not found, so not wrapper compiled");

}


LWPRWrapper::LWPRWrapper(double scalingFactor) : scalingFactor(scalingFactor)
{
    throw Exception("LWPR not found, so not wrapper compiled");
}

void LWPRWrapper::learn(const Vec<DVec> &x, const Vec<DVec> &y)
{
    throw Exception("LWPR not found, so not wrapper compiled");
}

const DVec LWPRWrapper::operator()(const DVec& x) const
{
    throw Exception("LWPR not found, so not wrapper compiled");
}


string DMP::LWPRWrapper::toString() const
{
    throw Exception("LWPR not found, so not wrapper compiled");

}

bool DMP::LWPRWrapper::fromString(const string& content)
{
    throw Exception("LWPR not found, so not wrapper compiled");
}
#endif
