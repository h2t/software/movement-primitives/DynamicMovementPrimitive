/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef DMP_FUNCTIONAPPROXIMATION_H
#define DMP_FUNCTIONAPPROXIMATION_H

#include "dmp/general/helpers.h"
#include "locallyweightedlearningbasisfunctions.h"

#include <dmp/general/vec.h>

#include <map>

#include <boost/shared_ptr.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>

namespace DMP
{
    class FunctionApproximationInterface;
    typedef boost::shared_ptr<FunctionApproximationInterface> FunctionApproximationInterfacePtr;

    class FunctionApproximationInterface
    {
    public:
        double lambda;
        /**
         * @brief update
         * @param x
         * @param y
         */
        virtual void learn(const Vec<DVec> & x, const Vec<DVec> &y) = 0;
        virtual void ilearn(const DVec &x, double y)
        {
            throw DMP::Exception("Incremental Learning is not supported or not implemented.");
        }
        virtual void createKernels(const DVec &xmin, const DVec &xmax, int kernelNum = 0)
        {
            throw DMP::Exception("Not Implemented");
        }

        virtual const DVec operator()(const DVec& x) const = 0;
        virtual const DVec getDeriv(const DVec& x) const
        {
            throw DMP::Exception("Not Implemented");
        }

        virtual bool supportsIncrementalLearning()
        {
            return false;
        }


        virtual void setForgettingFactor(double fo)
        {
            if(supportsIncrementalLearning())
            {
                lambda = fo;
            }
            else
            {
                std::cerr << "Notice: This function approximator does not support incremental learning. " << std::endl;
            }
        }

        virtual void reset() = 0;
        virtual FunctionApproximationInterfacePtr clone() = 0;

        virtual vector<double> getWeights()
        {
            vector<double> res;
            res.push_back(0);
            return res;
        }

        virtual void setWeights(const std::vector<double> & weights){}
        virtual void setupKernels(double xmin, double xmax){}

        virtual size_t getKernelSize()
        {
            return 1;
        }

        virtual vector<double> getBasisFunctionVal(const DVec& x)
        {
            return vector<double>();
        }

        virtual Eigen::VectorXf getBasisFunctionVec(const DVec& x)
        {
            return Eigen::VectorXf();
        }

    private:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            ar& boost::serialization::make_nvp("lambda",lambda);
        }

    };
    typedef boost::shared_ptr<FunctionApproximationInterface> FunctionApproximationInterfacePtr;
    typedef boost::shared_ptr<const FunctionApproximationInterface> FunctionApproximationInterfaceCPtr;

    class ExactReproduction :
        public FunctionApproximationInterface
    {
    public:
        void learn(const Vec<DVec> & x, const Vec<DVec> &y) override
        {
            DVec x1 = x.at(0);
            DVec y1 = y.at(0);

            data = SampledTrajectoryV2(x1, y1);
        }

        const DVec operator()(const DVec& x) const override
        {
            return data.getState(x.at(0), 0, 0);
        }
        void reset() override
        {
            data.clear();
        }


        FunctionApproximationInterfacePtr clone() override
        {
            return FunctionApproximationInterfacePtr(new ExactReproduction(*this));
        }

    private:
        mutable SampledTrajectoryV2 data;

        // FunctionApproximationInterface interface
    public:
        string toString() const;
        bool fromString(const string& content);

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            FunctionApproximationInterface& base = boost::serialization::base_object<FunctionApproximationInterface>(*this);
            ar& BOOST_SERIALIZATION_NVP(base);
            //            unsigned int dim = data.dim();
            //            ar & BOOST_SERIALIZATION_NVP(dim);
            //            std::vector<double> times = data.getTimestamps();
            //            ar & BOOST_SERIALIZATION_NVP(times);

            ar& BOOST_SERIALIZATION_NVP(data);
        }

    };


}
//BOOST_CLASS_EXPORT(DMP::ExactReproduction)




#endif // DMP_FUNCTIONAPPROXIMATION_H
