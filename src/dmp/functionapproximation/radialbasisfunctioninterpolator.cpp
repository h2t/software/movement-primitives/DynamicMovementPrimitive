#include "functionapproximation/radialbasisfunctioninterpolator.h"

#include "dmp/general/helpers.h"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_min.h>

using namespace DMP;

void DMP::getXTream(Vec<DVec> x, DVec& x0, DVec& xT)
{
    unsigned int size = x.size();
    if(size < 1)
    {
        return;
    }

    x0 = x[0];
    xT = x[0];

    for(size_t i = 0; i < size; ++i)
    {

        for(size_t j = 0; j < x0.size(); ++j)
        {
            if(x[i][j] < x0[j])
                x0[j] = x[i][j];
            if(x[i][j] > xT[j])
                xT[j] = x[i][j];
        }

    }


}


