/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/
#ifndef RECOGNITION_H_
#define RECOGNITION_H_

#include <vector>
#include <dmp/general/helpers.h>
#include <dmp/representation/trajectory.h>


using namespace DMP;

class Recognition
{
 public:
  Recognition();
  ~Recognition();
  double computeCoefficient(DVec feature1, DVec feature2);
  std::vector<DVec> computeSegmentFeatureVector(SampledTrajectoryV2 segment);
  double compareSegments(SampledTrajectoryV2 segment1, SampledTrajectoryV2 segment2);
  double compareSegmentFeatureVectors(std::vector<DVec> vec1, std::vector<DVec> vec2);
 private:

};

#endif
