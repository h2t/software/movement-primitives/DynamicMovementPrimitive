/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/



#include "recognition.h"
#include "PCA.h"

using namespace DMP;
Recognition::Recognition(){}

Recognition::~Recognition(){}

double Recognition::computeCoefficient(DVec feature1, DVec feature2)
{
          double coefficient = 0.0;
          double bhat_coefficient = 0.0;
          double featVectLengthi = 0.0;
          double featVectLengthj = 0.0;
          for (int k = 0; k < feature1.size(); k++){
	    featVectLengthi += feature1[k]*feature1[k];
	    featVectLengthj += feature2[k]*feature2[k];
          }
          featVectLengthi = sqrt(featVectLengthi);
          featVectLengthj = sqrt(featVectLengthj);
          for (int k = 0; k < feature1.size(); k++){
            coefficient += feature1[k]*feature2[k];

          }
          coefficient /=  featVectLengthi*featVectLengthj;
          coefficient = (coefficient*coefficient);
	  return coefficient;
}
std::vector<DVec> Recognition::computeSegmentFeatureVector(SampledTrajectoryV2 segment)
{
  std::vector<DVec> segmentFeatureVector;
   DVec timestamps = segment.getTimestamps();
   CPCA pca;
  std::vector<gsl::vector> subsegment;
  double errorThreshold = 3.0;

  for (int i = 0; i < timestamps.size(); i++)
    {
      gsl::vector trajectoryPointdd(3);
      for (int j = 0; j < 3; j++)
        trajectoryPointdd(j) = segment.getState(timestamps[i],j,2)/(timestamps[timestamps.size()-1]*timestamps[timestamps.size()-1]);
      subsegment.push_back(trajectoryPointdd);
      pca.computePCA_Cov(subsegment,1);
      std::vector<gsl::vector> subsegment_reduced = pca.reduceDim(subsegment);
      std::vector<gsl::vector> subsegment_restored = pca.restoreDim(subsegment_reduced);

      double error = 0;
      for (int j = 0; j < subsegment.size(); j++)
	{

      for (int k = 0; k < 3; k++){
        error += (subsegment[j](k)-subsegment_restored[j](k))*(subsegment[j](k)-subsegment_restored[j](k));
        //printf("subseg org %d: %f %f %f\n",j,subsegment[j](k),subsegment_restored[j](k));
      }
	}
      error = sqrt(error)/subsegment.size();
      //printf("ocer all error %f\n",error);
      if (error > errorThreshold || i == timestamps.size()-1)
	{
	  std::vector<gsl::vector> pc1_reduced;
      gsl::vector pc1_red_vec(1);
      pc1_red_vec(0) = 1.0;
      pc1_reduced.push_back(pc1_red_vec);
      std::vector<gsl::vector> pc1 = pca.restoreDim(pc1_reduced);
      DVec featureVector(3,0.0);
	  for (int k = 0; k < 3; k++)
        featureVector.at(k) = pc1[0](k);
      subsegment.clear();
      segmentFeatureVector.push_back(featureVector);
	  
    }
    }

  return segmentFeatureVector;
}

double Recognition::compareSegments(SampledTrajectoryV2 segment1, SampledTrajectoryV2 segment2)
{

  std::vector<DVec> featVec1 = computeSegmentFeatureVector(segment1);
  std::vector<DVec> featVec2 = computeSegmentFeatureVector(segment2);
  //printf("feature Vector 1 size %d\n",featVec1.size());
  //printf("feature Vector 2 size %d\n",featVec2.size());

  double score = compareSegmentFeatureVectors(featVec1, featVec2);
  //printf("score %f\n",score);
  return score;
}

double Recognition::compareSegmentFeatureVectors(std::vector<DVec> vec1, std::vector<DVec> vec2)
{
  double score = 0.0;
  std::vector<DVec> minVec;
  std::vector<DVec> maxVec;
  int vec1_length = vec1.size();
  int vec2_length = vec2.size();
  int minLength;
  int maxLength;
  if (vec1_length < vec2_length)
    {
      minVec = vec1;
      maxVec = vec2;
      minLength = vec1_length;
      maxLength = vec2_length;
    }
  else
    {
      minVec = vec2;
      maxVec = vec1;
      minLength = vec2_length;
      maxLength = vec1_length;
    }
  bool bSegmentFound = false;
  int currentJIndex = 0;
  std::vector<int> sequence;
  std::vector<double> seqCoeff;
  gsl::matrix coeffMatrix(minLength,maxLength);
  for (int i = 0; i < minLength; i++)
    {
      for (int j = 0; j < maxLength; j++)
	coeffMatrix(i,j) = computeCoefficient(minVec[i], maxVec[j]);
    }

 
 for (int i = 0; i < minLength; i++)
    {
      double maxCoeff = 0;
      int maxCoeffJIndex = -1;
      for (int j = 0; j < maxLength; j++)
	if (coeffMatrix(i,j) > maxCoeff)
	  {
	    maxCoeff = coeffMatrix(i,j);
	    maxCoeffJIndex = j;
	  }
      sequence.push_back(maxCoeffJIndex);
      seqCoeff.push_back(maxCoeff);
    }
 for (int i = 0; i < seqCoeff.size(); i++)
   score += seqCoeff[i];
  //take shorter sequence
  //check if is in side
  //cannot be found penalize by subtracting 1/cap(shorterSequence)
  //found at wrong position penalize by subtracting 1/cap(longerSequence)

  return score;
}

