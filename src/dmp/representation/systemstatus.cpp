/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "systemstatus.h"
#include <dmp/general/helpers.h>

#include <sstream>

using namespace DMP;



DMPState::DMPState(const DMPState& state)
{
    pos = state.pos;
    vel = state.vel;
}

DMPState::DMPState(const DVec& status)
{
    checkSize(status);
    pos = status[0];
    vel = status[1];
}

DMPState::DMPState(double pos, double vel)
{
    this->pos = pos;
    this->vel = vel;
}



_3rdOrderDMP::_3rdOrderDMP(const DVec& status) :
    DMPState(DVec(status.begin(), status.begin() + 2))
{
    checkSize(status);
    acc = status[2];
}

double& DMPState::getValue(unsigned int i)
{
    switch (i)
    {
        case 0:
            return pos;
        case 1:
            return vel;
        default:
            throw Exception("out of bounds: ") << i;

    }
}

const double& DMPState::getValue(unsigned int i) const
{
    switch (i)
    {
        case 0:
            return pos;
        case 1:
            return vel;
        default:
            throw Exception("out of bounds: ") << i;

    }
}

double& _3rdOrderDMP::getValue(unsigned int i)
{
    switch (i)
    {
        case 0:
            return pos;
        case 1:
            return vel;
        case 2:
            return acc;
        default:
            throw Exception("out of bounds: ") << i;

    }
}

const double& _3rdOrderDMP::getValue(unsigned int i) const
{
    switch (i)
    {
        case 0:
            return pos;
        case 1:
            return vel;
        case 2:
            return acc;
        default:
            throw Exception("out of bounds: ") << i;

    }
}


void SystemStatus::checkSize(const DVec& status) const
{
    if (status.size() < size())
    {
        std::stringstream str;
        str << "Size of input Vec does not match: " << status.size() << ", but it should be " << size() << std::endl;
        throw Exception(str.str());
    }
}


DVec SystemStatus::getValues() const
{

    DVec result;

    for (unsigned int i = 0; i < size(); i++)
    {
        result.push_back(getValue(i));
    }

    return result;
}

double& SystemStatus::operator [](unsigned int i)
{
    return getValue(i);
}

std::ostream& ::DMP::operator<<(std::ostream& str, const DMPState& value)
{
    str << "Pos: " << value.pos << ", Vel: " << value.vel;
    return str;
}
