/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef SYSTEMSTATUS_H
#define SYSTEMSTATUS_H

#include <dmp/general/vec.h>
#include <map>
#include <boost/shared_ptr.hpp>
#include <dmp/general/exception.h>

namespace DMP
{



    class SystemStatus
    {
    public:
        virtual ~SystemStatus() {}

        DVec getValues() const;
        double& operator[](unsigned int i);
        const double& operator[](unsigned int i) const;
        virtual double& getValue(unsigned int i) = 0;
        virtual const double& getValue(unsigned int i) const = 0;
        virtual unsigned int size() const = 0;

        // Conversion helper functions
        template <typename SystemStatusType>
        static DVec convertStatesToArray(const Vec<SystemStatusType>& states);
        template <typename SystemStatusType>
        static Vec<SystemStatusType> convertArrayToStates(const DVec& array);
        template <typename SystemStatusType>
        static SystemStatusType convertArrayPtrToState(const boost::shared_ptr<DVec>& array);


    protected:
        void checkSize(const DVec& status) const;

    };
    typedef boost::shared_ptr<SystemStatus> SystemStatusPtr;
    typedef std::map<double, SystemStatusPtr> SystemStatusMap;

    struct DMPState : SystemStatus
    {
        DMPState()
        {
            pos = vel = 0;
        }
        //        ~DMPState(){ std::cout << "dmpState: " << pos << std::endl;}
        DMPState(const DMPState& state);
        DMPState(const DVec& status);
        DMPState(double pos, double vel);
        double pos;
        double vel;
        double& getValue(unsigned int i) override;
        const double& getValue(unsigned int i) const override;
        unsigned int size() const override
        {
            return 2;
        }

        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            ar& boost::serialization::make_nvp("pos",pos);
            ar& boost::serialization::make_nvp("vel",vel);
        }

    };
    std::ostream& operator<<(std::ostream& str, const DMPState& value);
    typedef boost::shared_ptr<DMPState> DMPStatePtr;

    struct _3rdOrderDMP : DMPState
    {
        _3rdOrderDMP() {}
        _3rdOrderDMP(const DVec& status);
        double acc;
        double& getValue(unsigned int i) override;
        const double& getValue(unsigned int i) const override;
        unsigned int size() const override
        {
            return 3;
        }
    };
    typedef boost::shared_ptr<_3rdOrderDMP> _3rdOrderDMPPtr;


    template <typename SystemStatusType>
    DVec
    SystemStatus::convertStatesToArray(const Vec<SystemStatusType>& states)
    {
        DVec result;

        for (unsigned int i = 0; i < states.size(); i++)
        {
            DVec state = states[i].getValues();
            result.insert(result.end(), state.begin(), state.end());
        }

        return result;

    }

    template <typename SystemStatusType>
    Vec<SystemStatusType>
    SystemStatus::convertArrayToStates(const DVec& array)
    {
        Vec<SystemStatusType> result;

        int typeSize = SystemStatusType().size();

        if (array.size() % typeSize != 0)
        {
            throw Exception() << "Cannot convert array to SystemStatusType - size is wrong: " << array.size() << " must be a multiple of " << typeSize;
        }

        for (DVec::const_iterator it = array.begin(); it != array.end();)
        {
            DVec singleDimEntry(it, it + typeSize);
            it += typeSize;
            result.push_back(singleDimEntry);
        }

        return result;
    }

    template <typename SystemStatusType>
    SystemStatusType
    SystemStatus::convertArrayPtrToState(const boost::shared_ptr<DVec>& array)
    {
        int typeSize = SystemStatusType().size();

        if (array->size() < typeSize)
        {
            throw Exception() << "Cannot convert array to SystemStatusType - size is wrong: " << array->size() << " must be of atleast of size " << typeSize;
        }

        SystemStatusType result(*array);



        return result;
    }


    // alternative
    //    class DMPState : DVec {
    //        enum Types { ePos, eVelo};
    //        double operator()(Types type){ return operator[](type);}
    //    };



}
#endif // SYSTEMSTATUS_H
