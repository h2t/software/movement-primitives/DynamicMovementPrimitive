#include "trajectory.h"
#include <cstdlib>
#include <string>
#include <sstream>
#include <limits>
#include <dmp/general/exception.h>

using namespace std;
using namespace DMP;

SampledTrajectory::SampledTrajectory()
= default;

SampledTrajectory::SampledTrajectory(const DVec& x, const DVec& y) : Vec< DVec >()
{
    unsigned int N = std::min(x.size(), y.size());

    for (unsigned int i = 0; i < N; ++i)
    {
        DVec v(2);
        v[0] = x[i];
        v[1] = y[i];
        push_back(v);
    }
}

SampledTrajectory::SampledTrajectory(const DVec& x, const DVec& velo, double startPosition)
{
    unsigned int N = std::min(x.size(), velo.size());
    DVec v(2, 0);
    v[0] = x[0];
    v[1] = startPosition;
    push_back(v);

    for (unsigned int i = 1; i < N; ++i)
    {
        DVec v(2);
        v[0] = x[i];
        double diff = x[i] - x[i - 1];
        v[1] = velo[i] * diff + at(i - 1).at(1);
        push_back(v);

    }
}

SampledTrajectory::SampledTrajectory(const DVec& x, const DVec& acc, double startPosition, double startVelocity)
{
    unsigned int N = std::min(x.size(), acc.size());
    double prevVelo = startVelocity;
    DVec v(2);
    v[0] = x[0];
    v[1] = startPosition;
    push_back(v);

    for (unsigned int i = 1; i < N; ++i)
    {
        DVec v(2);
        v[0] = x[i];
        double diff = x[i] - x[i - 1];
        double velo = (prevVelo + acc[i] * diff);
        v[1] = velo * diff + at(i - 1).at(1);
        push_back(v);
        prevVelo = velo;
        std::cout << "pos: " << v[1] << " velo: " << velo << std::endl;
    }
}

//DVec SampledTrajectory::evaluate(double t, INTERPOLATION_METHOD interpolation_method) const {
//    // TODO: needed?
//}

//void SampledTrajectory::evaluate(DVec ts, Vec< DVec > &out, INTERPOLATION_METHOD interpolation_method) const {
//}


void SampledTrajectory::getPositions(SampledTrajectory& pos) const
{
    pos.assign(this->begin(), this->end());
}
void SampledTrajectory::getVelocities(SampledTrajectory& vel) const
{
    differentiateDiscretly(*this, vel);
}
void SampledTrajectory::getAccelerations(SampledTrajectory& acc) const
{
    SampledTrajectory tmp;
    differentiateDiscretly(*this, tmp);
    differentiateDiscretly(tmp, acc);
}

void SampledTrajectory::getPosition(unsigned int i, DVec& pos) const
{
    SampledTrajectory tmp;
    getPositions(tmp);
    pos.assign(tmp[i].begin() + 1, tmp[i].end());
}

void SampledTrajectory::getVelocity(unsigned int i, DVec& vel) const
{
    SampledTrajectory tmp;
    getVelocities(tmp);
    vel.assign(tmp[i].begin() + 1, tmp[i].end());
}

void SampledTrajectory::getAcceleration(unsigned int i, DVec& accel) const
{
    SampledTrajectory tmp;
    getAccelerations(tmp);
    accel.assign(tmp[i].begin() + 1, tmp[i].end());
}

DVec SampledTrajectory::getPosition(unsigned int i) const
{
    DVec tmp;
    getPosition(i, tmp);
    return tmp;
}
DVec SampledTrajectory::getVelocity(unsigned int i) const
{
    DVec tmp;
    getVelocity(i, tmp);
    return tmp;
}
DVec SampledTrajectory::getAcceleration(unsigned int i) const
{
    DVec tmp;
    getAcceleration(i, tmp);
    return tmp;
}

void SampledTrajectory::differentiateDiscretly(const SampledTrajectory& samples, SampledTrajectory& res) const
{
    unsigned int N = samples.size() - 1;

    res.resize(N + 1);

    for (unsigned int i = 0; i <= N; ++i)
    {
        res[i].resize(1 + dim());
        res[i][0] = samples[i][0];
    }

    for (unsigned int i = 1; i < N; ++i)
    {
        for (unsigned int j = 0; j < dim(); ++j)
        {
            res[i][j + 1] = (samples[i + 1][j + 1] - samples[i - 1][j + 1]) / (samples[i + 1][0] - samples[i - 1][0]);
        }
    }

    for (unsigned int j = 0; j < dim(); ++j)
    {
        res[0][j + 1] = (samples[1][j + 1] - samples[0][j + 1]) / (samples[1][0] - samples[0][0]);
        res[N][j + 1] = (samples[N][j + 1] - samples[N - 1][j + 1]) / (samples[N][0] - samples[N - 1][0]);
    }
}

void SampledTrajectory::timeSamples(DVec& times) const
{
    times.clear();

    for (const auto & i : *this)
    {

        times.push_back(i[0]);
    }
}

void SampledTrajectory::positionSamples(Vec< DVec > &positions) const
{
    positions.clear();

    for (const auto & i : *this)
    {
        DVec::const_iterator j = i.begin() + 1;
        positions.push_back(DVec(j, i.end()));
    }
}

void SampledTrajectory::positionSamples(int dim, DVec& positions) const
{
    positions.clear();

    for (const auto & i : *this)
    {
        positions.push_back(i.at(dim + 1));
    }
}

void SampledTrajectory::getPositionsDoubleArray(double** &res) const
{
    res = new double*[size()];

    for (unsigned int i = 0; i < size(); ++i)
    {
        res[i] = new double[dim()];

        for (unsigned int j = 0; j < dim(); ++j)
        {
            res[i][j] = (*this)[i][j + 1];
        }
    }
}

void SampledTrajectory::getVelocitiesDoubleArray(double** &res) const
{
    SampledTrajectory vel;
    getVelocities(vel);
    vel.getPositionsDoubleArray(res);
}

void SampledTrajectory::gaussianFilter(unsigned int filterRadius)
{
    unsigned int N = size();

    for (unsigned int i = 0; i < N; ++i)
    {
        for (unsigned int j = 0; j < dim(); ++j)
        {
            (*this)[i][j + 1] = __gaussianFilter(filterRadius, i, j + 1);
        }
    }
}


// gauss macro
#define g(x)  exp(-double((x)*(x))/(2*sigma*sigma)) / ( sigma * sqrt(2*3.14159265))
#define round(x) int(0.5+x)

double SampledTrajectory::__gaussianFilter(unsigned int filterRadius, unsigned int centerIndex, int dim)
{
    int start;
    start = centerIndex - filterRadius;
    int end;
    end = centerIndex + filterRadius;

    double sigma = filterRadius / 2.5;
    //-log(0.05) / pow(itemsToCheck+1+centerIndex,2) * ( sqrt(1.0/h) * sqrt(2*3.14159265));

    double weightedSum = 0;
    //    for(int i = start; i <= end; i++ )
    //    {
    //        double value;
    //        if(i <= 0 )
    //            value = at(0).at(dim);
    //        else if (i >= (int)size())
    //            value = (*rbegin()).at(dim);
    //        else
    //            value = at(i).at(dim);
    //        double squared =(i-centerIndex)*(i-centerIndex);
    //        const double gaussValue = exp(-squared/(2*sigma*sigma)) / ( sigma * sqrt(2*3.14159265));

    //        weightedSum +=  gaussValue * value;
    //    }

    // with integer weights
    int normFactor = 0;
    double g_k = g(filterRadius);

    for (int i = start; i <= end; i++)
    {
        double value;

        if (i < 0 || i >= (int)size())
        {
            continue;
        }
        else
        {
            value = at(i).at(dim);
        }

        double g_i = g(i - centerIndex);
        const double gaussValue = round(g_i / g_k);

        weightedSum +=  gaussValue * value;

        if (std::numeric_limits<double>::infinity() == weightedSum)
        {
            std::cout << "g_i: "  << g_i << " g_k: " << g_k << std::endl;
            throw DMP::Exception("weightedSum value is inf");
        }

        normFactor += round(g_i / g_k);
    }

    double result = weightedSum / normFactor;

    if (std::numeric_limits<double>::infinity() == result)
    {
        throw DMP::Exception("result value is inf");
    }

    if (result != result)
    {
        throw DMP::Exception("result value is nan");
    }

    return result;
}


void SampledTrajectory::writeGNUPlotFile(const char* filename, unsigned int num) const
{
    for (unsigned int d = 0; d < dim(); ++d)
    {
        char buf[1024];

        sprintf(buf, filename, num, d);

        FILE* fp = fopen(buf, "w");

        for (unsigned int i = 0; i < size(); ++i)
        {
            DVec sample = (*this)[i];
            fprintf(fp, "%lf\t%lf\n", sample[0], sample[1 + d]);
        }

        fclose(fp);
    }
}

void SampledTrajectory::plot(const char* tmp_filename, bool show, unsigned int num) const
{
    writeGNUPlotFile(tmp_filename, num);

    if (show)
    {
        for (unsigned int d = 0; d < dim(); ++d)
        {
            char buf[2048], buf_filename[1024];
            sprintf(buf_filename, tmp_filename, num, d);

            sprintf(buf, "gnuplot -e \"set style line 1 lt rgb \\\"blue\\\";"
                    "plot \\\"%s\\\" with lines ls 1\" -persist", buf_filename);
            int res = system(buf);
            if(res)
            {
                throw std::logic_error{"Command '" + std::string{buf} + "' returned " + std::to_string(res)};
            }
        }
    }
}

void SampledTrajectory::plotOverPhi(double phi0, double Omega, const char* tmp_filename, bool show, unsigned int num) const
{
    SampledTrajectory tmp_traj(*this);

    for (unsigned int i = 0; i < size(); ++i)
    {
        tmp_traj[i][0] = phi0 + ((*this)[i][0] - (*this)[0][0]) * Omega;
    }

    tmp_traj.plot(tmp_filename, show, num);
}

void SampledTrajectory::plot(const Vec<SampledTrajectory> &data, const char* tmp_filename, bool show)
{

    if (data.size() == 0)
    {
        return;
    }

    const unsigned int trajCount = data.size();

    unsigned int dim = data.at(0).dim();

    for (unsigned int d = 0; d < dim; d++)
    {

        for (unsigned int i = 0; i < trajCount; i++)
        {
            DVec timestamps;
            data.at(i).timeSamples(timestamps);
            DVec positions;
            data.at(i).positionSamples(d, positions);
            char specificFilename[1024];
            sprintf(specificFilename, tmp_filename, i, d);
            writeGNUPlotFileWithXAxis(timestamps, positions, specificFilename);
        }

        if (show)
        {
            char buf[1024];
            stringstream plots;

            for (unsigned int i = 0; i < trajCount; i++)
            {
                char buf_filename[255];
                sprintf(buf_filename, tmp_filename, i, d);
                plots << "\\\"" << buf_filename << "\\\" with lines ls " << i ;

                if (i != data.size() - 1)
                {
                    plots << ", ";
                }
            }

            sprintf(buf, "gnuplot -e \""
                    "plot %s \" -persist", plots.str().c_str());

            int res = system(buf);
            if(res)
            {
                throw std::logic_error{"Command '" + std::string{buf} + "' returned " + std::to_string(res)};
            }
        }
    }
}

void SampledTrajectory::showMultiplePlots(unsigned int min_num, unsigned int max_num, unsigned int step,
                                          const char* filename) const
{
    for (unsigned int d = 0; d < dim(); ++d)
    {
        string command = "gnuplot -persist -e \"set style line 1 lt rgb \\\"blue\\\"; plot ";

        for (unsigned int num = min_num; num <= max_num; num += step)
        {
            char buf[512], buf_filename[256];
            sprintf(buf_filename, filename, num, d);
            sprintf(buf, "\\\"%s\\\" with lines ls %d", buf_filename, num + 1);
            command += buf;

            if (num < max_num)
            {
                command += ",";
            }
        }

        command += "\"";

        //        cout << command << endl;
        int res = system(command.c_str());
        if(res)
        {
            throw std::logic_error{"Command '" + command + "' returned " + std::to_string(res)};
        }


    }
}


void SampledTrajectory::showParametricPlot(const char* filename, unsigned int min_num, unsigned int max_num, unsigned int step) const
{
    string command = "scripts/show_parametric_plot.py ";

    if (max_num < min_num)
    {
        max_num = min_num;
    }

    for (unsigned int num = min_num; num <= max_num; num += step)
    {
        for (unsigned int d = 0; d < 2; ++d)
        {
            char buf_filename[255];
            sprintf(buf_filename, filename, num, d);
            command += buf_filename;
            command += " ";
        }
    }

    command += "&";
    //    cout << command << endl;
    int res = system(command.c_str());
    if(res)
    {
        throw std::logic_error{"Command '" + command + "' returned " + std::to_string(res)};
    }
}


unsigned int SampledTrajectory::dim() const
{
    if (size() > 0)
    {
        return (*this)[0].size() - 1;
    }
    else
    {
        return 0;
    }
}

void SampledTrajectory::shiftTime(double shift)
{
    for (unsigned int i = 0; i < size(); ++i)
    {
        (*this)[i][0] -= shift;    // TODO: wrong sign... perhaps also in getPhaseShift[Error]
    }
}

void SampledTrajectory::shiftValue(const DVec& shift)
{
    DVec tmp(shift.size() + 1);
    tmp[0] = 0.0;
    tmp.insert(tmp.begin() + 1, shift.begin(), shift.end());

    for (unsigned int i = 0; i < size(); ++i)
    {
        (*this)[i] += tmp;
    }
}



//SampledPeriodicTrajectory::SampledPeriodicTrajectory(const SampledPeriodicTrajectory &src) : SampledTrajectory(),
//    m_phi1(src.m_phi1),
//    m_period_length(src.m_period_length),
//    m_anchor_point(src.m_anchor_point)
//{
//  this->assign(src.begin(), src.end());
//}

//SampledPeriodicTrajectory::SampledPeriodicTrajectory(double phi1, double periodLength, double anchor_point,
//                                                     Vec< DVec > *samples) : SampledTrajectory() {
//    m_phi1 = phi1;
//  m_period_length = periodLength;
//  m_anchor_point.assign(1,anchor_point);
//  if (samples)
//      this->assign(samples->begin(), samples->end());
//}

//SampledPeriodicTrajectory::SampledPeriodicTrajectory(double phi1, double periodLength, const DVec *anchor_point,
//                                                     Vec< DVec > *samples) : SampledTrajectory() {
//    m_phi1 = phi1;
//  m_period_length = periodLength;

//  if(anchor_point)
//      m_anchor_point = *anchor_point;

//  if (samples) {
//      this->assign(samples->begin(), samples->end());
//      if (!anchor_point && samples->size()>0)
//          m_anchor_point.assign(dim(), 0.0);
//  }
//}



struct interpolation_position
{
    unsigned int index_of_t;
    double t;
    unsigned int other_function_index_pre; //, other_function_index_post;
    char function_index;
    bool interpolation_necessary;
};

void evaluateTrajectoriesAtInterpolationPosition(const interpolation_position& p,
                                                 const SampledComposedPeriodicTrajectory& t1, const SampledComposedPeriodicTrajectory& t2,
                                                 double shift,
                                                 DVec& out1, DVec& out2)
{
    if (p.function_index == 0)
    {
        const DVec& out_t1_with_t = t1[p.index_of_t];
        out1.assign(out_t1_with_t.begin() + 1, out_t1_with_t.end());

        if (p.interpolation_necessary)
        {
            const DVec& pre2 = t2[p.other_function_index_pre],
                    &post2 = t2[p.other_function_index_pre + 1];
            DVec tmp = (out_t1_with_t[0] - (pre2[0] + shift)) * (pre2 - post2) / (pre2[0] - post2[0]) + pre2; // linear interpolation

            out2.assign(tmp.begin() + 1, tmp.end());
        }
        else
        {
            out2.assign(t2[p.other_function_index_pre + 1].begin() + 1, t2[p.other_function_index_pre + 1].end());
        }
    }
    else
    {
        const DVec& out_t2_with_t = t2[p.index_of_t];
        out2.assign(out_t2_with_t.begin() + 1, out_t2_with_t.end());

        if (p.interpolation_necessary)
        {
            const DVec& pre1 = t1[p.other_function_index_pre],
                    &post1 = t1[p.other_function_index_pre + 1];
            DVec tmp = ((out_t2_with_t[0] + shift) - pre1[0]) * (pre1 - post1) / (pre1[0] - post1[0]) + pre1; // linear interpolation

            out1.assign(tmp.begin() + 1, tmp.end());
        }
        else
        {
            out1.assign(t1[p.other_function_index_pre + 1].begin() + 1, t1[p.other_function_index_pre + 1].end());
        }
    }

    out1 -= t1.anchorPoint();
    out2 -= t2.anchorPoint();
}


double SampledComposedPeriodicTrajectory::getShiftError(const SampledComposedPeriodicTrajectory& traj, double shift) const
{
    double begin = std::max((*this)[1][0], traj[1][0] + shift),
            end = std::min((*this)[size() - 2][0], traj[traj.size() - 2][0] + shift);

    Vec< interpolation_position > tj;
    double t = begin;

    int pos_this = 0, pos_traj = 0;

    while ((*this)[pos_this][0] < begin)
    {
        pos_this++;
    }

    while (traj[pos_traj][0] + shift < begin)
    {
        pos_traj++;
    }

    pos_this--;
    pos_traj--;

    while (t < end)
    {
        double next_this = (*this)[pos_this + 1][0];
        double next_traj = traj[pos_traj + 1][0] + shift;

        t = std::min(next_this, next_traj);

        interpolation_position p;
        p.t = t;

        if (t == next_this)
        {
            p.function_index = 0;
            p.index_of_t = pos_this + 1;
            p.other_function_index_pre = pos_traj;
            p.interpolation_necessary = (t != next_traj);
            tj.push_back(p);
        }
        else
        {
            p.function_index = 1;
            p.index_of_t = pos_traj + 1;
            p.other_function_index_pre = pos_this;
            p.interpolation_necessary = true;
            tj.push_back(p);
        }

        if (t == next_this)
        {
            pos_this++;
        }

        if (t == next_traj)
        {
            pos_traj++;
        }
    }

    DVec diff;
    diff.assign((*this)[0].size() - 1, 0.0);

    for (unsigned int i = 0; i < tj.size() - 1; ++i)
    {
        DVec pre_this, post_this, pre_traj, post_traj;
        evaluateTrajectoriesAtInterpolationPosition(tj.at(i), *this, traj, shift, pre_this, pre_traj);
        evaluateTrajectoriesAtInterpolationPosition(tj.at(i + 1), *this, traj, shift, post_this, post_traj);

        //        cout << "-------------------------------------" << endl;
        //        cout << "pre_this=" << pre_this;
        //        cout << "post_this=" << post_this;
        //        cout << "pre_traj=" << pre_traj;
        //        cout << "post_traj=" << post_traj;

        // trapzian rule
        double t_post = tj.at(i + 1).t,
                t_pre = tj.at(i).t;

        DVec a1 = abs(post_this - post_traj);
        DVec a2 = abs(pre_this - pre_traj);

        DVec delta = (t_post - t_pre) / 2.0 * (a1 + a2);

        diff += delta;
    }

    double diff_norm = norm2(diff);
    return diff_norm;
}


double SampledComposedPeriodicTrajectory::getShift(const SampledComposedPeriodicTrajectory& traj) const
{
    // TODO: magic constants
    unsigned int ITERATIONS = 2;
    unsigned int COUNT = 101;
    double interval = m_period_length;
    double position = 0.0;

    for (unsigned int i = 0; i < ITERATIONS; ++i)
    {
        double best_error = INFINITY;
        double best_position = position;

        for (unsigned int s = 0; s < COUNT; ++s)
        {
            double shift = position - interval / 2 + interval / (COUNT - 1) * s;
            double error = getShiftError(traj, shift);
            //            cout << "error("<< shift << ") = " << error << endl;

            if (error < best_error)
            {
                best_error = error;
                best_position = shift;
            }
        }

        position = best_position;
        interval *= 2.0 / COUNT;
        //        cout << "new position = " << position << ", new interval size = " << interval << endl;
    }

    return position;
}

double SampledComposedPeriodicTrajectory::align(const SampledComposedPeriodicTrajectory& reference_traj)
{
    SampledComposedPeriodicTrajectory reference_periodic,
            this_periodic;

    reference_traj.getPeriodicPart(reference_periodic);
    getPeriodicPart(this_periodic);

    double time_shift = this_periodic.getShift(reference_periodic);
    shiftTime(time_shift);
    shiftValue(reference_traj.anchorPoint() - m_anchor_point);

    return time_shift;
}


SampledComposedPeriodicTrajectory::SampledComposedPeriodicTrajectory(const SampledComposedPeriodicTrajectory& src)
    : SampledTrajectory(),
      m_T(src.m_T),
      m_phi1(src.m_phi1),
      m_period_length(src.m_period_length),
      m_anchor_point(src.m_anchor_point)
{
    this->assign(src.begin(), src.end());
}

SampledComposedPeriodicTrajectory::SampledComposedPeriodicTrajectory(const SampledTrajectoryV2& src)
    : SampledTrajectory(),
      m_T(src.getTransientLength()),
      m_phi1(src.getPhi1()),
      m_period_length(src.getPeriodLength()),
      m_anchor_point(src.getAnchorPoint())
{
    DVec timestamps = src.getTimestamps();
    Vec < DVec > srcSamples;
    for (size_t i = 0; i < src.dim(); i++)
    {
        srcSamples.push_back(src.getDimensionData(i));
    }
    Vec < DVec > samples;
    for (size_t i = 0; i < timestamps.size(); i++)
    {
        DVec singleSample;
        singleSample.push_back(timestamps[i]);
        for (size_t j = 0; j < srcSamples.size(); j++)
        {
            singleSample.push_back(srcSamples[j][i]);
        }
        samples.push_back(singleSample);
    }
    cout << "org anchorpoint " << src.getAnchorPoint();
    cout << "cp anchorpoint " << m_anchor_point;
    this->assign(samples.begin(), samples.end());
    printf("Changed trajectory format\n");
}

SampledComposedPeriodicTrajectory::SampledComposedPeriodicTrajectory(double T, double phi1, double periodLength,
                                                                     double anchor_point, Vec< DVec > *samples)
    : SampledTrajectory()
{
    m_phi1 = phi1;
    m_period_length = periodLength;
    m_anchor_point.assign(1, anchor_point);

    if (samples)
    {
        this->assign(samples->begin(), samples->end());
    }

    m_T = T;
}

SampledComposedPeriodicTrajectory::SampledComposedPeriodicTrajectory(double T, double phi1, double periodLength,
                                                                     const DVec* anchor_point, Vec< DVec > *samples) : SampledTrajectory()
{
    m_T = T;
    m_phi1 = phi1;
    m_period_length = periodLength;

    if (anchor_point)
    {
        m_anchor_point = *anchor_point;
    }

    if (samples)
    {
        this->assign(samples->begin(), samples->end());

        if (!anchor_point && samples->size() > 0)
        {
            m_anchor_point.assign(dim(), 0.0);
        }
    }
}


void SampledComposedPeriodicTrajectory::getPeriodicPart(SampledComposedPeriodicTrajectory& periodic_part, bool shift_to_time_0) const
{
    periodic_part.clear();

    unsigned int periodic_start_index = 0;

    while ((*this)[periodic_start_index][0] /* TODO: -(*this)[0][0]*/ < m_T)
    {
        ++periodic_start_index;
    }

    double start_time = (*this)[periodic_start_index][0];

    periodic_part.resize(size() - periodic_start_index);
    periodic_part.assign(begin() + periodic_start_index, end());

    if (shift_to_time_0)
    {
        for (unsigned int i = 0; i < periodic_part.size(); ++i)
        {
            periodic_part[i][0] -= /* TODO: (*this)[0][0] +*/ start_time;
        }
    }

    periodic_part.setT(0);
    periodic_part.m_anchor_point = m_anchor_point;
    periodic_part.m_period_length = m_period_length;
    periodic_part.m_phi1 = m_phi1;
}


void SampledComposedPeriodicTrajectory::plot(bool highlight_entrance_time, bool highlight_period_length,
                                             const char* tmp_filename, bool show, unsigned int num) const
{
    SampledComposedPeriodicTrajectory tmp = *this;

    DVec max_value, anchor_point(dim() + 1);
    max_value.assign(dim() + 1, 0.0);
    anchor_point.insert(anchor_point.begin() + 1, m_anchor_point.begin(), m_anchor_point.end());

    if (highlight_entrance_time || highlight_period_length)
    {
        for (unsigned int k = 0; k < size(); ++k)
        {
            max_value = max(max_value, abs((*this)[k] - anchor_point));
        }
    }

    max_value *= 0.3;
    max_value[0] = 0.0;

    unsigned int i = 0;

    if (highlight_entrance_time)
    {
        for (i = 0; i < size(); ++i)
        {
            if ((*this)[i][0] - (*this)[0][0] >= m_T)
            {
                break;
            }
        }

        tmp[i] -= max_value;
    }

    if (highlight_period_length)
    {
        unsigned int j;
        double interesting_time = m_T + m_period_length;

        for (j = i; j < size(); ++j)
        {
            if ((*this)[j][0] - (*this)[0][0] >= interesting_time)
            {
                tmp[j] += max_value;
                interesting_time += m_period_length;
            }
        }
    }

    ((SampledTrajectory*)&tmp)->plot(tmp_filename, show, num);
}


//double SampledComposedPeriodicTrajectory::getShift(const SampledComposedPeriodicTrajectory &traj) const {
//    SampledComposedPeriodicTrajectory per_this, per_traj;
//    getPeriodicPart(per_this);
//    traj.getPeriodicPart(per_traj);

//    return per_this.getShift(per_traj);
//}

DVec SampledComposedPeriodicTrajectory::linearInterpolation(double t)  {
    unsigned int i = 1;
    while (i<size()-1 && (*this)[i][0]<t)
        ++i;

    return SampledComposedPeriodicTrajectory::linearInterpolationBase((*this)[i-1], (*this)[i], t);
}

double SampledComposedPeriodicTrajectory::linearInterpolationSingle(double t1, double y1, double t2, double y2, double t) {
    return (y2-y1)/(t2-t1)*(t-t1) + y1;
}

DVec SampledComposedPeriodicTrajectory::linearInterpolationVec(double t1, const DVec &y1, double t2, const DVec &y2, double t) {
    unsigned int dim= y1.size();
    DVec result;
    result.resize(dim);

    for(unsigned int d=0; d<dim; ++d)
        result[d] = linearInterpolationSingle(t1,y1[d], t2,y2[d], t);

    return result;
}

DVec SampledComposedPeriodicTrajectory::linearInterpolationBase(const DVec &sample1, const DVec &sample2, double t) {
    unsigned int dim = sample1.size()-1;
    DVec y1;
    DVec y2;
    y1.resize(dim);
    y2.resize(dim);

    copy(sample1.begin()+1, sample1.end(), y1.begin());
    copy(sample2.begin()+1, sample2.end(), y2.begin());

    return SampledComposedPeriodicTrajectory::linearInterpolationVec(sample1[0],y1, sample2[0],y2, t);
}

SampledTrajectoryV2::SampledTrajectoryV2(const SampledTrajectoryV2& source)
{
    CopyData(source, *this);
}



SampledTrajectoryV2::SampledTrajectoryV2(const DVec& x, const DVec& y)
{
    addDimension(x, y);
}


SampledTrajectoryV2::SampledTrajectoryV2(const std::map<double, DVec >& data)
{
    if (data.size() == 0)
    {
        return;
    }

    typename  std::map<double, DVec >::const_iterator it = data.begin();

    for (; it != data.end(); it++)
    {
        TrajData dataEntry(*this);
        dataEntry.timestamp = it->first;
        const DVec& dataVec = it->second;

        for (unsigned int i = 0; i < dataVec.size(); i++)
        {
            checkValue(dataVec[i]);
            dataEntry.data.push_back(DVecPtr(new DVec(1, dataVec[i])));
        }

        dataMap.insert(dataEntry);
    }
}

DMP::SampledTrajectoryV2::SampledTrajectoryV2(const std::vector<double> &data, double timestep)
{
    if (data.size() == 0)
    {
        return;
    }

    double timestamp = 0;
    for(double val : data)
    {
        TrajData dataEntry(*this);
        dataEntry.timestamp = timestamp;
        dataEntry.data.push_back(DVecPtr(new DVec(1,val)));
        dataMap.insert(dataEntry);

        timestamp += timestep;
    }
}


//
//    SampledTrajectoryV2::SampledTrajectoryV2(const TrajMap &map)
//    {
//        copyMap(map, dataMap);
//    }

SampledTrajectoryV2& SampledTrajectoryV2::operator=(const SampledTrajectoryV2& source)
{
    CopyData(source, *this);
    return *this;
}


double SampledTrajectoryV2::getState(double t, unsigned int dim, unsigned int deriv)
{
    if (dim >= this->dim())
    {
        throw Exception() << "dimension is to big: " << dim << " max: " << this->dim();
    }

    typename timestamp_view::iterator it = dataMap.find(t);

    if (it == dataMap.end() || !it->data.at(dim))
    {
        __fillBaseDataAtTimestamp(t);// interpolates and retrieves
        it = dataMap.find(t);
    }

    if (!it->data.at(dim))
        //        it->data.at(dim).reset(new DVec());
    {
        throw Exception() << "Vec ptr is NULL!?";
    }

    Vec<DVecPtr>& vec = it->data;

    if (deriv != 0 && vec.at(dim)->size() <= deriv)
    {
        //resize and calculate derivations
        unsigned int curDeriv = it->data.at(dim)->size();
        it->data.at(dim)->resize(deriv + 1);

        while (curDeriv <= deriv)
        {
            const TrajData& entry = *it;
            double derivValue = getDiscretDifferentiationForDimAtT(t, dim, curDeriv);
            checkValue(curDeriv);
            it->data.at(dim)->at(curDeriv) = derivValue;
            curDeriv++;
        }

    }

    //    std::cout << "dimensions: " <<it->data.size() << " derivations: " <<  it->data.at(dim)->size() << std::endl;
    double result = it->data.at(dim)->at(deriv);
    //    checkValue(result);
    return result;
}

double SampledTrajectoryV2::getState(double t, unsigned int dim, unsigned int deriv) const
{

    if (dim >= this->dim())
    {
        throw Exception() << "dimension is to big: " << dim << " max: " << this->dim();
    }

    typename timestamp_view::iterator it = dataMap.find(t);

    if (!dataExists(t, dim, deriv))
    {
        if (deriv == 0)
        {
            return __calcBaseDataAtTimestamp(t).at(dim)->at(deriv);    // interpolates and retrieves
        }
        else
        {
            return getDiscretDifferentiationForDimAtT(t, dim, deriv);
        }
    }
    else
    {
        //                std::cout << "dimensions: " <<it->data.size() << " derivations: " <<  it->data.at(dim)->size() << std::endl;
        double result = it->data.at(dim)->at(deriv);

        checkValue(result);


        return result;
    }
}

DVec SampledTrajectoryV2::getDerivations(double t, unsigned int dimension, unsigned int derivations) const
{
    if (dimension >= dim())
    {
        throw Exception() << "Dimension out of bounds: requested: " << dimension << " available: " << dim();
    }

    DVec result;

    for (unsigned int i = 0; i <= derivations; i++)
    {
        result.push_back(getState(t, dimension, i));
    }

    return result;
}

DVec
SampledTrajectoryV2::getDimensionData(unsigned int dimension, unsigned int deriv) const
{
    if (dimension >= dim())
    {
        throw Exception("dimension is out of range: ") << dimension << " actual dimensions: " << dim();
    }

    DVec result;

    const ordered_view& ordv = dataMap.get<TagOrdered>();
    typename ordered_view::const_iterator itMap = ordv.begin();

    for (; itMap != ordv.end(); itMap++)
    {
        DVecPtr dataPtr = itMap->data[dimension];
        result.push_back(itMap->getDeriv(dimension, deriv));
        //        if(dataPtr && dataPtr->size() > deriv)
        //            result.push_back(itMap->getDeriv(dimension, deriv));
        //        else result.push_back(getState(itMap->timestamp, dimension, deriv));
    }

    return result;

}

Eigen::VectorXd SampledTrajectoryV2::getDimensionDataAsEigen(unsigned int dimension, unsigned int derivation) const
{
    return getDimensionDataAsEigen(dimension, derivation, begin()->getTimestamp(), rbegin()->getTimestamp());
}

Eigen::VectorXd SampledTrajectoryV2::getDimensionDataAsEigen(unsigned int dimension, unsigned int derivation, double startTime, double endTime) const
{
    if (dimension >= dim())
    {
        throw Exception("dimension is out of range: ") << dimension << " actual dimensions: " << dim();
    }

    DVec result;

    const ordered_view& ordv = dataMap.get<TagOrdered>();
    ordered_view::iterator itO = ordv.lower_bound(startTime);
    ordered_view::iterator itOEnd = ordv.upper_bound(endTime);
    //    typename ordered_view::const_iterator itMap = ordv.begin();

    size_t i = 0;
    for (; itO != itOEnd && i < size(); itO++, i++)
    {
        DVecPtr dataPtr = itO->data[dimension];
        result.push_back(itO->getDeriv(dimension, derivation));
        //        if(dataPtr && dataPtr->size() > deriv)
        //            result.push_back(itMap->getDeriv(dimension, deriv));
        //        else result.push_back(getState(itMap->timestamp, dimension, deriv));
    }
    Eigen::VectorXd resultVec(result.size());
    for(size_t i = 0; i < result.size(); i++)
    {
        resultVec(i) = result[i];
    }
    return resultVec;
}

Eigen::MatrixXd SampledTrajectoryV2::toEigen(unsigned int derivation, double startTime, double endTime) const
{

    Eigen::MatrixXd result(size(), dim());

    const ordered_view& ordv = dataMap.get<TagOrdered>();
    ordered_view::iterator itO = ordv.lower_bound(startTime);
    ordered_view::iterator itOEnd = ordv.upper_bound(endTime);
    //    typename ordered_view::const_iterator itMap = ordv.begin();

    size_t i = 0;
    for (; itO != itOEnd; itO++, i++)
    {
        //        DVecPtr dataPtr = itO->data[dimension];
        for(size_t d = 0; d < itO->data.size(); d++)
            result(i,d) = (itO->getDeriv(d, derivation));
    }

    return result;
}

Eigen::MatrixXd SampledTrajectoryV2::toEigen(unsigned int derivation) const
{
    return toEigen(derivation, begin()->getTimestamp(), rbegin()->getTimestamp());
}

SampledTrajectoryV2 SampledTrajectoryV2::getPart(double startTime, double endTime, unsigned int numberOfDerivations) const
{
    SampledTrajectoryV2 result;

    const ordered_view& ordv = dataMap.get<TagOrdered>();
    ordered_view::iterator itO = ordv.lower_bound(startTime);
    ordered_view::iterator itOEnd = ordv.upper_bound(endTime);
    //    typename ordered_view::const_iterator itMap = ordv.begin();

    size_t i = 0;
    for (; itO != itOEnd; itO++, i++)
    {
        //        DVecPtr dataPtr = itO->data[dimension];
        for(size_t d = 0; d < itO->data.size(); d++)
        {
            DVec derivs;
            for(size_t i = 0; i < numberOfDerivations+1; i++)
            {
                derivs.push_back(itO->getDeriv(d,i));
            }

            result.addDerivationsToDimension(d, itO->getTimestamp(), derivs);
        }
        //            result.addPositionsToDimension(d, itO->getTimestamp(), itO->getDeriv(d, 0));
    }

    return result;
}

unsigned int SampledTrajectoryV2::dim() const
{
    if (dataMap.size() == 0)
    {
        return 0;
    }
    else
    {
        return dataMap.begin()->data.size();
    }
}


//    DVec
//    SampledTrajectoryV2::getDimensionData(unsigned int dimension, unsigned int deriv) const
//    {
//        if(dimension >= dim()){
//            throw Exception("dimension is out of range: ") << dimension << " actual dimensions: " << dim();
//        }

//        DVec result;
//        DVec tempResult = getDimensionData(dimension);
//        typename DVec::iterator it = tempResult.begin();
//        for(; it != tempResult.end(); it++)
//            result.push_back(it->at(deriv));

//        return result;
//    }


double SampledTrajectoryV2::getTimeLength() const
{
    const ordered_view& ordView = dataMap.get<TagOrdered>();

    if (ordView.begin() != ordView.end())
    {
        return ordView.rbegin()->timestamp - ordView.begin()->timestamp;
    }
    else
    {
        return 0;
    }
}

double SampledTrajectoryV2::getSpaceLength(unsigned int dimension, unsigned int stateDim) const
{
    return getSpaceLength(dimension, stateDim, begin()->getTimestamp(), rbegin()->getTimestamp());
}

double SampledTrajectoryV2::getSpaceLength(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const
{
    double length = 0.0;
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    //    timestamp_view::iterator it = dataMap.find(startTime);
    //    DVec data = getDimensionData(0);
    //    timestamp_view::iterator itEnd= dataMap.find(endTime);
    ordered_view::iterator itO = ordv.lower_bound(startTime);
    ordered_view::iterator itOEnd = ordv.upper_bound(endTime);
    if(itO == ordv.end())
        return 0.0;
    double prevValue = itO->getDeriv(dimension,stateDim);

    for (;
         itO != itOEnd;
         itO++
         )
    {
        length += fabs(prevValue - itO->getDeriv(dimension,stateDim));
        prevValue = itO->getDeriv(dimension,stateDim);
    }

    return length;
}

double SampledTrajectoryV2::getSquaredSpaceLength(unsigned int dimension, unsigned int stateDim) const
{
    return getSquaredSpaceLength(dimension, stateDim, begin()->getTimestamp(), rbegin()->getTimestamp());
}

double SampledTrajectoryV2::getSquaredSpaceLength(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const
{
    double length = 0.0;
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    //    timestamp_view::iterator it = dataMap.find(startTime);
    //    DVec data = getDimensionData(0);
    //    timestamp_view::iterator itEnd= dataMap.find(endTime);
    ordered_view::iterator itO = ordv.lower_bound(startTime);
    ordered_view::iterator itOEnd = ordv.upper_bound(endTime);
    if(itO == ordv.end())
        return 0.0;
    //    double prevValue = itO->data[dimension]->at(stateDim);
    double prevValue = itO->getDeriv(dimension,stateDim);

    for (;
         itO != itOEnd;
         itO++
         )
    {
        length += fabs(pow(prevValue, 2.0) - pow(itO->getDeriv(dimension,stateDim), 2.0));
        prevValue = itO->getDeriv(dimension,stateDim);
    }

    return length;
}

double SampledTrajectoryV2::getMax(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const
{
    return getWithFunc(&std::max<double>, -std::numeric_limits<double>::max(), dimension, stateDim, startTime, endTime);
}

double SampledTrajectoryV2::getMin(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const
{
    return getWithFunc(&std::min<double>, std::numeric_limits<double>::max(), dimension, stateDim, startTime, endTime);
}

double SampledTrajectoryV2::getWithFunc(const double &(*foo)(const double&, const double&), double initValue, unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const
{
    double bestValue = initValue;
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    ordered_view::iterator itO = ordv.lower_bound(startTime);
    ordered_view::iterator itOEnd = ordv.upper_bound(endTime);

    if(itO == ordv.end())
        return bestValue;
    for (;
         itO != itOEnd;
         itO++
         )
    {
        bestValue = foo(bestValue, itO->getDeriv(dimension,stateDim));
    }

    return bestValue;
}

double SampledTrajectoryV2::getTrajectorySpace(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const
{
    return getMax(dimension, stateDim, startTime, endTime) - getMin(dimension, stateDim, startTime, endTime);
}

DVec SampledTrajectoryV2::getMinima(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const
{
    DVec result;
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    ordered_view::iterator itO = ordv.lower_bound(startTime);
    ordered_view::iterator itOEnd = ordv.upper_bound(endTime);

    if(itO == ordv.end())
        return result;
    double preValue = itO->getDeriv(dimension,stateDim);
    for (;
         itO != itOEnd;

         )
    {
        double t = itO->getTimestamp();
        double cur = itO->getDeriv(dimension,stateDim);
        itO++;
        if(itO == ordv.end())
            break;
        double next = itO->getDeriv(dimension,stateDim);
        if(cur <= preValue && cur <= next)
            result.push_back(cur);
        preValue = cur;
    }

    return result;
}

DVec SampledTrajectoryV2::getMinimaPositions(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const
{
    DVec result;
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    ordered_view::iterator itO = ordv.lower_bound(startTime);
    ordered_view::iterator itOEnd = ordv.upper_bound(endTime);

    if(itO == ordv.end())
        return result;
    double preValue = itO->getDeriv(dimension,stateDim);
    for (;
         itO != itOEnd;

         )
    {
        double t = itO->getTimestamp();
        double cur = itO->getDeriv(dimension,stateDim);
        itO++;
        if(itO == ordv.end())
            break;
        double next = itO->getDeriv(dimension,stateDim);
        if(cur <= preValue && cur <= next)
            result.push_back(itO->getTimestamp());
        preValue = cur;
    }

    return result;
}

DVec SampledTrajectoryV2::getMaxima(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const
{
    DVec result;
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    ordered_view::iterator itO = ordv.lower_bound(startTime);
    ordered_view::iterator itOEnd = ordv.upper_bound(endTime);

    if(itO == ordv.end())
        return result;
    double preValue = itO->getDeriv(dimension,stateDim);
    for (;
         itO != itOEnd;

         )
    {
        double cur = itO->getDeriv(dimension,stateDim);
        itO++;
        if(itO == ordv.end())
            break;
        double next = itO->getDeriv(dimension,stateDim);
        if(cur >= preValue && cur >= next)
            result.push_back(cur);
        preValue = cur;
    }

    return result;
}

DVec SampledTrajectoryV2::getMaximaPositions(unsigned int dimension, unsigned int stateDim, double startTime, double endTime) const
{
    DVec result;
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    ordered_view::iterator itO = ordv.lower_bound(startTime);
    ordered_view::iterator itOEnd = ordv.upper_bound(endTime);

    if(itO == ordv.end())
        return result;
    double preValue = itO->getDeriv(dimension,stateDim);
    for (;
         itO != itOEnd;

         )
    {
        double cur = itO->getDeriv(dimension,stateDim);
        itO++;
        if(itO == ordv.end())
            break;
        double next = itO->getDeriv(dimension,stateDim);

        if(cur >= preValue && cur >= next)
            result.push_back(itO->getTimestamp());
        preValue = cur;
    }

    return result;
}


Vec<DVecPtr> SampledTrajectoryV2::__calcBaseDataAtTimestamp(const double& t) const
{
    //    typename timestamp_view::const_iterator it = dataMap.find(t);
    //    if(it != dataMap.end())
    //        return it->data;
    Vec<DVecPtr> result;

    for (unsigned int dimension = 0; dimension < dim(); dimension++)
    {
        double newValue = __interpolate(t, dimension, 0);
        checkValue(newValue);
        result.push_back(DVecPtr(new DVec(1, newValue)));

    }

    return result;

}

bool SampledTrajectoryV2::dataExists(double t, unsigned int dimension, unsigned int deriv) const
{
    typename timestamp_view::iterator it = dataMap.find(t);

    if (it == dataMap.end() || !it->data.at(dimension) || it->data.at(dimension)->size() <= deriv)
    {
        return false;
    }
    else
    {
        return true;
    }
}


SampledTrajectoryV2::timestamp_view::iterator SampledTrajectoryV2::__fillBaseDataAtTimestamp(const double& t)
{
    typename timestamp_view::const_iterator it = dataMap.find(t);

    if (it != dataMap.end())
    {
        bool foundEmpty = false;

        for (unsigned int i = 0; i < it->data.size(); i++)
        {
            if (!it->data.at(i))
            {
                foundEmpty = true;
            }
        }

        if (!foundEmpty)
        {
            return it;
        }
    }

    TrajData entry(*this);
    entry.timestamp = t;
    entry.data.resize(dim());
    dataMap.insert(entry);
    it = dataMap.find(t);

    assert(it != dataMap.end());
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    //        typename ordered_view::iterator itOrdered = ordv.iterator_to(*it);
    it->data = __calcBaseDataAtTimestamp(t);

    return it;
}

const Vec<DVecPtr> &SampledTrajectoryV2::getStates(double t)
{
    typename timestamp_view::const_iterator it = dataMap.find(t);

    if (it == dataMap.end())
    {
        // interpolate and insert entry
        it = __fillBaseDataAtTimestamp(t);
    }


    return it->data;
}

Vec<DVecPtr> SampledTrajectoryV2::getStates(double t) const
{
    typename timestamp_view::const_iterator it = dataMap.find(t);

    if (it == dataMap.end())
    {
        // interpolate and return data
        return __calcBaseDataAtTimestamp(t);
    }


    return it->data;
}

DVec SampledTrajectoryV2::getStates(double t, unsigned int derivation) const
{
    DVec result;
    unsigned int dimensions = dim();

    for (unsigned int i = 0; i < dimensions; i++)
    {
        result.push_back(getState(t, i, derivation));
    }

    return result;
}

std::map<double, DVec> SampledTrajectoryV2::getStatesAround(double t, unsigned int derivation, unsigned int extend) const
{

    std::map<double,DVec> res;
    typename timestamp_view::iterator itMap = dataMap.find(t);

    if(itMap != dataMap.end())
    {
        for(size_t i = 0; i < dim(); i++)
            res.insert(std::pair<double, DVec>(t, itMap->data[i]->at(derivation)));

        return res;
    }

    const ordered_view& ordv = dataMap.get<TagOrdered>();

    typename ordered_view::iterator itNext = ordv.upper_bound(t);

    typename ordered_view::iterator itPrev = itNext;

    itPrev--;

    if(itPrev == ordv.end())
    {

        throw Exception("Cannot find value at timestamp ") << t;
    }

    for(size_t i = 0; i < extend; i++)
    {
        DVec preData = getStates(itPrev->timestamp, derivation);
        DVec nexData = getStates(itNext->timestamp, derivation);

        res.insert(std::pair<double, DVec>(itPrev->timestamp, preData));
        res.insert(std::pair<double, DVec>(itNext->timestamp, nexData));

        if(itPrev == ordv.begin() || itNext == ordv.end())
        {
            std::cout << "Warning: the timestamp is out of the range. "<<
                         "The current result will be returned" << std::endl;
            break;
        }
        itPrev--;
        itNext++;
    }

    return res;

}










SampledTrajectoryV2& SampledTrajectoryV2::operator +=(const SampledTrajectoryV2 traj)
{
    int dims = traj.dim();
    DVec timestamps = traj.getTimestamps();

    for (int d = 0; d < dims; ++d)
    {
        addPositionsToDimension(d, timestamps, traj.getDimensionData(d));
    }

    return *this;
}


void SampledTrajectoryV2::__addDimension()
{
    unsigned int newDim = dim() + 1;

    typename timestamp_view::iterator itMap = dataMap.begin();

    for (; itMap != dataMap.end(); itMap++)
    {
        itMap->data.resize(newDim);
    }
}

void SampledTrajectoryV2::setEntries(const double t, const unsigned int dimIndex, const DVec& y)
{
    typename timestamp_view::iterator itMap = dataMap.find(t);

    if (itMap == dataMap.end() || dim() == 0)
    {
        double dura = getTimeLength();
        TrajData newData(*this);
        newData.timestamp = t;
        newData.data =  Vec<DVecPtr>(std::max((unsigned int)1, dim()), DVecPtr());
        newData.data[dimIndex] = DVecPtr(new DVec(y));
        dataMap.insert(newData);
    }
    else
    {
        assert(dim() > 0);

        while (dim() <= dimIndex)
        {
            __addDimension();
        }

        itMap->data.resize(dim());
        itMap->data.at(dimIndex) = DVecPtr(new DVec(y));
    }

}


void SampledTrajectoryV2::setPositionEntry(const double t, const unsigned int dimIndex, const double& y)
{
    typename timestamp_view::iterator itMap = dataMap.find(t);

    if (itMap == dataMap.end() || dim() == 0)
    {
        TrajData newData(*this);
        newData.timestamp = t;
        newData.data =  Vec<DVecPtr>(std::max((unsigned int)1, dim()), DVecPtr());
        newData.data[dimIndex] = DVecPtr(new DVec(1, y));
        dataMap.insert(newData);
    }
    else
    {
        assert(dim() > 0);
        itMap->data.resize(dim());
        itMap->data.at(dimIndex) = DVecPtr(new DVec(1, y));
    }

}


void SampledTrajectoryV2::__fillAllEmptyFields()
{
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    typename ordered_view::const_iterator itMap = ordv.begin();

    for (; itMap != ordv.end(); itMap++)
    {
        for (unsigned int dimension = 0; dimension < dim(); dimension++)
        {
            if (!itMap->data[dimension])
            {
                itMap->data[dimension] = DVecPtr(new DVec(__interpolate(itMap, dimension, 0)));
            }
        }
    }
}




DVec SampledTrajectoryV2::getTimestamps() const
{
    DVec result;

    const ordered_view& ordv = dataMap.get<TagOrdered>();
    typename ordered_view::const_iterator itMap = ordv.begin();

    for (; itMap != ordv.end(); itMap++)
    {
        result.push_back(itMap->timestamp);
    }

    return result;
}



DVec
SampledTrajectoryV2::getDiscretDifferentiationForDim(unsigned int trajDimension, unsigned int derivation) const
{
    if (trajDimension >= dim())
    {
        throw Exception("dimension is out of range: ") << trajDimension << " actual dimensions: " << dim();
    }

    //        if(sourceDimOfSystemState >= double().size()){
    //            throw Exception("sourceDimOfSystemState is out of range: ") << sourceDimOfSystemState << " actual dimensions: " << double().size();
    //        }
    //        if(targetDimOfSystemState >= double().size()){
    //            throw Exception("sourceDimOfSystemState is out of range: ") << targetDimOfSystemState << " actual dimensions: " << double().size();
    //        }


    const ordered_view& ordv = dataMap.get<TagOrdered>();
    typename ordered_view::const_iterator itPrev = ordv.begin();

    typename ordered_view::const_iterator itCurrent = itPrev;
    //        itCurrent++;
    typename ordered_view::const_iterator itNext = itCurrent;
    itNext++;

    DVec result;

    for (; itCurrent != ordv.end();)
    {
        if (itNext == ordv.end()) // last step-> differentiate between current and prev
        {
            itNext = itCurrent;
        }


        // get current data
        DVecPtr prevStatePtr = itPrev->data[trajDimension];
        DVecPtr nextStatePtr = itNext->data[trajDimension];
        //            double nextValue = nextStatePtr->getValues()[sourceDimOfSystemState];
        //            double prevValue = prevStatePtr->getValues()[sourceDimOfSystemState];
        //            double nextTime = itNext->timestamp;
        //            double prevTime = itPrev->timestamp;
        // differentiateDiscretly now
        result.push_back((nextStatePtr->at(derivation) - prevStatePtr->at(derivation)) /
                         (itNext->timestamp - itPrev->timestamp));
        checkValue(*result.rbegin());


        itCurrent++, itPrev++, itNext++;

        if (itPrev == itCurrent)
        {
            // first step-> reestablish that current is > prev
            itPrev--;
        }

    }

    return result;
}


DVec SampledTrajectoryV2::DifferentiateDiscretly(const DVec& timestamps, const DVec& values, int derivationCount)
{
    if (derivationCount < 0)
    {
        throw Exception("Negative derivation value is not allowed!");
    }

    if (derivationCount == 0)
    {
        return values;
    }

    DVec result;
    int size = std::min(timestamps.size(), values.size());

    if (size < 2)
    {
        return values;
    }

    result.resize(size);

    // boundaries
    result[0] = (values.at(1) - values.at(0)) / (timestamps.at(1) - timestamps.at(0)) ;
    result[size - 1] = (values.at(size - 1) - values.at(size - 2)) / (timestamps.at(size - 1) - timestamps.at(size - 2)) ;

    //#pragma omp parallel for
    for (int i = 1; i < size - 1; ++i)
    {
        result[i] = (values.at(i + 1) - values.at(i - 1)) / (timestamps.at(i + 1) - timestamps.at(i - 1)) ;
        checkValue(result[i]);
    }

    if (derivationCount > 1) // recursivly differentiate
    {
        result = DifferentiateDiscretly(timestamps, result, derivationCount - 1);
    }

    return result;

}


double SampledTrajectoryV2::getDiscretDifferentiationForDimAtT(double t, unsigned int trajDimension, unsigned int deriv) const
{
    if (deriv == 0)
    {
        getState(t, trajDimension, deriv);
    }

    typename timestamp_view::iterator it = dataMap.find(t);
    //        if(it == dataMap.end())
    //        {
    //            getStates(t); // interpolate and insert entry
    //            it = dataMap.find(t);
    //        }

    const ordered_view& ordV = dataMap.get<TagOrdered>();
    typename ordered_view::iterator itCurrent = ordV.end();

    if (it != dataMap.end())
    {
        itCurrent = ordV.iterator_to(*it);
    }

    typename ordered_view::iterator itNext = ordV.upper_bound(t); // first element after t

    if (it != dataMap.end() && itNext == ordV.end())
    {
        itNext = itCurrent;    // current item is last, set next to current
    }
    else if (itNext == ordV.end() && it == dataMap.end())
    {
        throw Exception() << "Cannot interpolate for t " << t << " no data in trajectory";
    }

    typename ordered_view::iterator itPrev = itNext;
    itPrev--; // now equal to current (if current exists) or before current

    if (itCurrent != ordV.end()) // check if current item exists
    {
        itPrev--;    //element in front of t
    }

    if (itPrev == ordV.end())
    {
        //element in front of t does not exist
        if (itCurrent != ordV.end())
        {
            itPrev = itCurrent;    // set prev element to current
        }
        else
        {
            throw Exception() << "Cannot interpolate for t " << t;
        }
    }

    if (itNext == ordV.end())
    {
        //element after t does not exist
        if (itCurrent != ordV.end())
        {
            itNext = itCurrent;    // set next element to current
        }
        else
        {
            throw Exception() << "Cannot interpolate for t " << t;
        }
    }

    if (itNext == itPrev)
    {
        throw Exception() << "Interpolation failed: the next data and the previous are missing";
    }

    //        double diff = itNext->data[trajDimension]->at(deriv-1) - itPrev->data[trajDimension]->at(deriv-1);
    double tNext;

    if (dataExists(itNext->timestamp, trajDimension, deriv - 1) || deriv > 1)
    {
        tNext = itNext->timestamp;
    }
    else
    {
        tNext = t;
    }

    double next = getState(tNext, trajDimension, deriv - 1);

    double tBefore;

    if (dataExists(itPrev->timestamp, trajDimension, deriv - 1) || deriv > 1)
    {
        tBefore = itPrev->timestamp;
    }
    else
    {
        tBefore = t;
    }

    double before = getState(tBefore, trajDimension, deriv - 1);

    if (fabs(tNext - tBefore) < 1e-10)
    {
        throw Exception() << "Interpolation failed: the next data and the previous are missing";
    }

    double duration = tNext - tBefore;
    double delta = next - before;
    delta = delta / duration;
    checkValue(delta);
    return delta;
}

void SampledTrajectoryV2::differentiateDiscretly(unsigned int derivation)
{

    for (unsigned int d = 0; d < dim(); d++)
    {
        differentiateDiscretlyForDim(d, derivation);
    }
}

void SampledTrajectoryV2::differentiateDiscretlyForDim(unsigned int trajDimension, unsigned int derivation)
{
    removeDerivation(trajDimension, derivation);
    typename ordered_view::iterator itOrd = begin();

    for (; itOrd != end(); itOrd++)
    {
        getState(itOrd->timestamp, trajDimension, derivation);
    }
}


//    void SampledTrajectoryV2::differentiateDiscretlyForDim(unsigned int trajDimension, unsigned int sourceDimOfSystemState, unsigned int targetDimOfSystemState)
//    {
//        if(trajDimension >= dim()){
//            throw Exception("dimension is out of range: ") << trajDimension << " actual dimensions: " << dim();
//        }
//        if(sourceDimOfSystemState >= double().size()){
//            throw Exception("sourceDimOfSystemState is out of range: ") << sourceDimOfSystemState << " actual dimensions: " << double().size();
//        }
//        if(targetDimOfSystemState >= double().size()){
//            throw Exception("targetDimOfSystemState is out of range: ") << targetDimOfSystemState << " actual dimensions: " << double().size();
//        }


//        const ordered_view & ordv = dataMap.template get<TagOrdered>();
//        typename ordered_view::const_iterator itPrev = ordv.begin();

//        typename ordered_view::const_iterator itCurrent = itPrev;
////        itCurrent++;
//        typename ordered_view::const_iterator itNext = itCurrent;
//        itNext++;

//        for(; itCurrent != ordv.end(); )
//        {
//            if(itNext == ordv.end()) // last step-> differentiate between current and prev
//                itNext = itCurrent;


//            // get current data
//            DVecPtr currentStatePtr = itCurrent->data[trajDimension];
//            DVecPtr prevStatePtr = itPrev->data[trajDimension];
//            DVecPtr nextStatePtr = itNext->data[trajDimension];
//            DVec newStateData = currentStatePtr->getValues();

//            // differentiateDiscretly now
//            newStateData[targetDimOfSystemState] = (nextStatePtr->at(sourceDimOfSystemState) - prevStatePtr->at(sourceDimOfSystemState))/
//                    (itNext->timestamp - itPrev->timestamp);
//            itCurrent->data[trajDimension] = DVecPtr(new DVec(1,newStateData));


//            itCurrent++, itPrev++, itNext++;
//            if(itPrev == itCurrent)
//            {
//                // first step-> reestablish that current is > prev
//                itPrev--;
//            }

//        }
//    }


//    void SampledTrajectoryV2::differentiateDiscretly(unsigned int sourceDimOfSystemState, unsigned int targetDimOfSystemState)
//    {
//        unsigned int size = dim();
//        for(unsigned int i=0; i < size; i++)
//        {
//            differentiateDiscretlyForDim(i, sourceDimOfSystemState, targetDimOfSystemState);
//        }
//    }



void SampledTrajectoryV2::reconstructFromDerivativeForDim(double valueAtFirstTimestamp, unsigned int trajDimension, unsigned int sourceDimOfSystemState, unsigned int targetDimOfSystemState)
{
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    typename ordered_view::iterator it = ordv.begin();

    if (it == ordv.end())
    {
        return;
    }

    it->data[trajDimension]->at(targetDimOfSystemState) = valueAtFirstTimestamp;
    double previousValue = valueAtFirstTimestamp;
    double previousTimestamp = it->timestamp;

    //        unsigned int stateDim = double().size();
    for (it++; it != ordv.end(); it++)
    {
        double slope;
        if(it->data[trajDimension]->size() > sourceDimOfSystemState)
            slope = it->data[trajDimension]->at(sourceDimOfSystemState);
        else
            slope = 0;

        double diff = it->timestamp - previousTimestamp;
        it->data[trajDimension]->at(targetDimOfSystemState) = previousValue + diff * slope;
        previousTimestamp = it->timestamp;
        previousValue = it->data[trajDimension]->at(targetDimOfSystemState);
    }

}


void SampledTrajectoryV2::negateDim(unsigned int trajDimension)
{
    if (trajDimension >= dim())
    {
        throw Exception("dimension is out of range: ") << trajDimension << " actual dimensions: " << dim();
    }

    const ordered_view& ordv = dataMap.get<TagOrdered>();

    SampledTrajectoryV2 filteredTraj;

    for (const auto & it : ordv)
    {
        (*it.data.at(trajDimension)) *= -1;
    }
}





double
SampledTrajectoryV2::__interpolate(double t, unsigned int dimension, unsigned int deriv) const
{
    typename timestamp_view::const_iterator it = dataMap.find(t);

    if (it != dataMap.end() && it->data.size() > dimension && it->data.at(dimension) && it->data.at(dimension)->size() > deriv)
    {
        return it->data.at(dimension)->at(deriv);
    }

    const ordered_view& ordv = dataMap.get<TagOrdered>();

    typename ordered_view::iterator itNext = ordv.upper_bound(t);

    typename ordered_view::iterator itPrev = itNext;

    itPrev--;

    double result = __interpolate(t, itPrev, itNext, dimension, deriv);

    checkValue(result);

    return result;
}

double
SampledTrajectoryV2::__interpolate(typename ordered_view::const_iterator itMap, unsigned int dimension, unsigned int deriv) const
{
    typename ordered_view::iterator itPrev = itMap;
    itPrev--;
    typename ordered_view::iterator itNext = itMap;
    itNext++;
    return __interpolate(itMap->timestamp, itPrev, itNext, dimension, deriv);
}

double
SampledTrajectoryV2::__interpolate(double t, typename ordered_view::const_iterator itPrev, typename ordered_view::const_iterator itNext, unsigned int dimension, unsigned int deriv) const
{
    //    typename ordered_view::const_iterator itCur = itPrev;
    //    itCur++;

    const ordered_view& ordView = dataMap.get<TagOrdered>();
    double previous;
    double next;

    // find previous SystemState that exists for that dimension
    while (itPrev != ordView.end() && (itPrev->data.at(dimension) == nullptr /*|| itPrev->data[dimension]->size() <= deriv*/))
    {
        itPrev--;
    }

    if (itPrev != ordView.end())
    {
        previous = getState(itPrev->timestamp, dimension, deriv);
    }

    // find next SystemState that exists for that dimension
    while (itNext != ordView.end() && (itNext->data.at(dimension) == nullptr /*|| itNext->data[dimension]->size() <= deriv*/))
    {
        itNext++;
    }

    if (itNext != ordView.end())
    {
        next = getState(itNext->timestamp, dimension, deriv);
    }



    if (itNext == ordView.end() && itPrev == ordView.end())
    {
        throw Exception() << "Cannot find next or prev values in dim " << dimension << " at timestamp " << t;
    }

    if (itNext == ordView.end())
    {
        return getState(itPrev->timestamp, dimension, deriv) + getState(itPrev->timestamp, dimension, deriv + 1) * (t - itPrev->timestamp);
    }
    else if (itPrev == ordView.end())
    {
        return getState(itNext->timestamp, dimension, deriv) - getState(itNext->timestamp, dimension, deriv + 1) * (itNext->timestamp - t);
    }
    else
    {
        // linear interpolation

        double distance = fabs(itNext->timestamp - itPrev->timestamp);
        double weightPrev = fabs(t - itNext->timestamp) / distance;
        double weightNext = fabs(itPrev->timestamp - t) / distance;

        return weightNext * next + weightPrev * previous;

    }
}
void SampledTrajectoryV2::gaussianFilter(double filterRadius)
{
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    SampledTrajectoryV2 filteredTraj;
    DVec timestamps = getTimestamps();

    for (unsigned int d = 0; d < dim(); d++)
    {
        DVec entries;

        for (typename ordered_view::iterator it = ordv.begin(); it != ordv.end(); it++)
        {
            entries.push_back(__gaussianFilter(filterRadius, it, d, 0));
        }

        filteredTraj.addDimension(timestamps, entries);
    }

    CopyData(filteredTraj, *this);
}



// gauss macro
#define g(x)  exp(-double((x)*(x))/(2*sigma*sigma)) / ( sigma * sqrt(2*3.14159265))
#define round(x) int(0.5+x)


double
SampledTrajectoryV2::__gaussianFilter(double filterRadiusInTime, typename ordered_view::iterator centerIt, unsigned int trajNum, unsigned int dim)
{
    const ordered_view& ordView = dataMap.get<TagOrdered>();
    //        typename ordered_view::iterator start = centerIt;
    //        while(start != ordView.end() && start->timestamp > centerIt->timestamp - filterRadiusInTime)
    //            start--;
    //        if(start == ordView.end())
    //            start = ordView.begin();

    //        typename ordered_view::iterator end;
    //        while(end != ordView.end() && end->timestamp < centerIt->timestamp + filterRadiusInTime)
    //            end++;

    const double sigma = filterRadiusInTime / 2.5;
    //-log(0.05) / pow(itemsToCheck+1+centerIndex,2) * ( sqrt(1.0/h) * sqrt(2*3.14159265));

    double weightedSum = 0;
    double sumOfWeight = 0;
    ordered_view::iterator start = centerIt;

    const double sqrt2PI = sqrt(2 * M_PI);

    for (int sign = -1; sign < 2; sign += 2)
    {
        for (ordered_view::iterator it = start;
             it != ordView.end()
             && fabs(it->timestamp - centerIt->timestamp) <= fabs(filterRadiusInTime*2);
             )
        {
            if(centerIt->timestamp >= 15.01)
            {
                double t = it->timestamp;
            }
            double value;
            value = it->getDeriv(trajNum, dim);//data[trajNum]->at(dim);
            double diff = (it->timestamp - centerIt->timestamp);
            double squared = diff * diff;
            const double gaussValue = exp(-squared / (2 * sigma * sigma)) / (sigma * sqrt2PI);
            sumOfWeight += gaussValue;
            weightedSum += gaussValue * value;

            if (sign > 0)
            {
                it++;
            }
            else
            {
                it--;
            }

            checkValue(weightedSum);
        }

        start++;// skip center value in second loop iteration
    }

    double result;
    result = weightedSum / sumOfWeight;
    checkValue(result);
    //        centerIt->data[trajNum]->at(dim) = result;
    return result;

}


void SampledTrajectoryV2::CopyData(const SampledTrajectoryV2& source, SampledTrajectoryV2& destination)
{
    if (&source == &destination)
    {
        return;
    }

    destination.clear();
    destination.headerString = source.headerString;
    DVec timestamps = source.getTimestamps();

    for (unsigned int dim = 0; dim < source.dim(); dim++)
    {
        destination.addDimension(timestamps, source.getDimensionData(dim));
    }

    //        typename TrajMap::iterator it = source.begin();
    //        for(; it != source.end(); it++)
    //        {
    //            destination[it->first] = it->second;
    //            Vec< DVecPtr >& vecS = it->second;
    //            Vec< DVecPtr >& vecD = destination[it->first];
    //            for(unsigned int i = 0; i < vecS.size(); i++)
    //            {
    //                vecD[i] = new DVecPtr(*vecS[i]);
    //            }
    //        }
}

void SampledTrajectoryV2::readFromCSVFileWithQuery(const std::string& filepath, DVec& query, bool withHead, int derivations)
{
    clear();
    std::ifstream f(filepath.c_str());

    if (!f.is_open())
    {
        throw Exception("Could not open file: " + filepath);
    }

    using Tokenizer = boost::tokenizer< boost::escaped_list_separator<char> >;

    vector< string > stringVec;
    string line;

    //        unsigned int stateDim = double().size();
    unsigned int i = 1;

    while (getline(f, line))
    {
        if (withHead && i == 2)
        {
            headerString = line;
            i++;
            continue;
        }

        Tokenizer tok(line);
        stringVec.assign(tok.begin(), tok.end());

        if (/*-1+stringVec.size() % stateDim !=0 || */stringVec.size() == 0)
        {
            std::cout << "Line " << i << " is invalid  skipping: " << line << std::endl;
            continue;
        }

        if (i == 1)
        {
            query.clear();
            for(const auto & j : stringVec)
            {
                query.push_back(fromString<double>(j));
            }
            i++;
        }
        else
        {
            TrajData entry(*this);
            entry.timestamp = fromString<double>(stringVec[0]);
            unsigned int j = 1;

            int size = stringVec.size() - 1;
            //            DVec values(int(ceil(size/stateDim)*stateDim), 0.0);
            DVec values(size, 0.0);

            for (unsigned int sd = 0; j < stringVec.size(); sd++, ++j)
            {
                values.at(sd) = (fromString<double>(stringVec[j]));
                checkValue(values.at(sd));
            }

            //            for (DVec::iterator it = values.begin(); it != values.end(); it+=stateDim+columnsToSkip) {
            for (unsigned int d = 0; d < values.size(); d += 1 + derivations)
            {
                DVecPtr statePtr(new DVec(DVec(values.begin() + d, values.begin() + d + 1 + derivations)));
                entry.data.push_back(statePtr);
            }

            dataMap.insert(entry);

            i++;
        }
    }

    std::cout << "file loaded: read " << i << " lines from " << filepath << " with " << dim() << " dimensions" << std::endl;
}

void SampledTrajectoryV2::readFromCSVFile(const string& filepath, bool noTimeStamp, int derivations, int columnsToSkip, bool containsHeader)
{
    clear();
    std::ifstream f(filepath.c_str());

    if (!f.is_open())
    {
        throw Exception("Could not open file: " + filepath);
    }

    using Tokenizer = boost::tokenizer< boost::escaped_list_separator<char> >;

    vector< string > stringVec;
    string line;

    //        unsigned int stateDim = double().size();
    unsigned int i = 1;

    double current_time = 0;
    while (getline(f, line))
    {
        if (containsHeader && i == 1)
        {
            headerString = line;
            Tokenizer tok(line);
            stringVec.assign(tok.begin(), tok.end());

            dimNames.clear();

            for(size_t j = 1; j < stringVec.size(); ++j)
            {
                dimNames.push_back(stringVec.at(j));
            }

            i++;
            continue;
        }

        Tokenizer tok(line);
        stringVec.assign(tok.begin(), tok.end());

        if (/*-1+stringVec.size() % stateDim !=0 || */stringVec.size() == 0)
        {
            std::cout << "Line " << i << " is invalid  skipping: " << line << std::endl;
            continue;
        }

        unsigned int j = 1;
        TrajData entry(*this);
        int size = stringVec.size() - 1;
        if(noTimeStamp)
        {
            j = 0;
            entry.timestamp = current_time;
            current_time += 0.01;
            size = stringVec.size();
        }
        else
        {
            entry.timestamp = fromString<double>(stringVec[0]);
        }


        //            DVec values(int(ceil(size/stateDim)*stateDim), 0.0);
        DVec values(size, 0.0);

        for (unsigned int sd = 0; j < stringVec.size(); sd++, ++j)
        {
            values.at(sd) = (fromString<double>(stringVec[j]));
            checkValue(values.at(sd));
        }

        //            for (DVec::iterator it = values.begin(); it != values.end(); it+=stateDim+columnsToSkip) {
        for (unsigned int d = 0; d < values.size(); d += 1 + derivations + columnsToSkip)
        {
            DVecPtr statePtr(new DVec(DVec(values.begin() + d, values.begin() + d + 1 + derivations)));
            entry.data.push_back(statePtr);
        }

        dataMap.insert(entry);

        i++;
    }

    std::cout << "file loaded: read " << i << " lines from " << filepath << " with " << dim() << " dimensions" << std::endl;
}

void SampledTrajectoryV2::writeToCSVFile(const string &filepath, bool noTimeStamp, char delimiter)
{
//    clear();
    std::ofstream f(filepath.c_str());

    if (!f.is_open())
    {
        throw Exception("Could not open file: " + filepath);
    }
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    typename ordered_view::const_iterator itMap = ordv.begin();

    for(;itMap != ordv.end(); itMap++)
    {
        Vec<DVecPtr > data = itMap->getData();

        if (!noTimeStamp)
        {
            f << itMap->getTimestamp() << delimiter;
        }
        for(size_t i = 0; i < data.size(); ++i)
        {
            DVecPtr cdata = data[i];

            f << cdata->at(0);

            if(i != data.size()-1)
            {
                f << delimiter;
            }
        }
        f << '\n';
    }

    f.close();
}

void SampledTrajectoryV2::writeTrajToCSVFile(const string &filepath, float timestep, float startTime, float endTime)
{
    std::ofstream f(filepath.c_str());

    if (!f.is_open())
    {
        throw Exception("Could not open file: " + filepath);
    }


    for(float t = startTime; t < endTime; t= t+ timestep)
    {
        Vec<DVecPtr > data = getStates(t);

        f << t;
        f << ',';
        for(size_t i = 0; i < data.size(); ++i)
        {
            DVecPtr cdata = data[i];

            f << cdata->at(0);

            if(i != data.size() -1)
            {
                f << ',';
            }
        }
        f << '\n';
    }

    f.close();
}

std::string SampledTrajectoryV2::writeTrajToCSVString(int numOfSamples)
{
    float timestep = 1.0 / (float)numOfSamples;

    float t = 0;
    std::stringstream f;
    for(int counter = 0; counter < numOfSamples; counter++)
    {
        Vec<DVecPtr > data = getStates(t);

        for(size_t i = 0; i < data.size(); ++i)
        {
            DVecPtr cdata = data[i];

            f << cdata->at(0);

            if(i != data.size() -1)
            {
                f << ',';
            }
        }

        if(counter != numOfSamples - 1)
            f << ',';

        t = t + timestep;
    }

    return f.str();
}

DVec SampledTrajectoryV2::generateTimestamps(double startTime, double endTime, double stepSize)
{
    if (startTime >= endTime)
    {
        throw Exception("startTime must be smaller than endTime.");
    }

    DVec result;
    int size = int((endTime - startTime) / stepSize) + 1;
    stepSize = (endTime - startTime) / (size - 1);
    result.reserve(size);

    double currentTimestamp = startTime;
    int i = 0;

    while (i < size)
    {
        result.push_back(currentTimestamp);
        currentTimestamp += stepSize;
        i++;
    }

    //        if(*result.rbegin() != endTime)
    //            result.push_back(endTime);
    return result;
}




DVec SampledTrajectoryV2::normalizeTimestamps(const DVec& timestamps, const double startTime, const double endTime)
{
    DVec normTimestamps;
    normTimestamps.resize(timestamps.size());
    const double minValue = *timestamps.begin();
    const double maxValue = *timestamps.rbegin();
    const double duration = maxValue - minValue;

    for (unsigned int i = 0; i < timestamps.size(); i++)
    {
        normTimestamps[i] = startTime + (timestamps.at(i) - minValue) / duration * (endTime - startTime);
    }

    return normTimestamps;
}


SampledTrajectoryV2 SampledTrajectoryV2::normalizeTimestamps(const SampledTrajectoryV2& traj, const double startTime, const double endTime)
{

    if (traj.size() <= 1 || (traj.begin()->timestamp == startTime && traj.rbegin()->timestamp == endTime) )
    {
        return traj;    // already normalized
    }


    DVec timestamps = traj.getTimestamps();


    DVec normTimestamps = normalizeTimestamps(timestamps, startTime, endTime);
    SampledTrajectoryV2 normExampleTraj;

    for (unsigned int dim = 0; dim < traj.dim(); dim++)
    {
        DVec dimensionData  =  traj.getDimensionData(dim);
        normExampleTraj.addDimension(normTimestamps, dimensionData);
    }

    return normExampleTraj;


}


void SampledTrajectoryV2::plot(unsigned int trajDim, unsigned int derivation, std::string filepathWithoutEnding , bool show) const
{
    DVec timestamps = getTimestamps();
    DVec values = getDimensionData(trajDim, derivation);
    DoubleMap data = DMP::convertArraysToMap(timestamps, values);
    std::stringstream filepath;
    filepath << filepathWithoutEnding << "_dim" << trajDim << "_Der" << derivation << "_%d.gp";
    DMP::plot(data, filepath.str().c_str(), show, 0);
}

void SampledTrajectoryV2::plotAllDimensions(unsigned int stateDim, std::string filepathWithoutEnding , bool show) const
{
    Vec<DVec> plots;
    plots.push_back(getTimestamps());

    for (unsigned int i = 0; i < dim(); i++)
    {
        plots.push_back(getDimensionData(i, stateDim));
    }

    std::stringstream filepath;
    filepath << filepathWithoutEnding << "_dim"  << "_Der" << stateDim << "_%d.gp";
    DMP::plotTogether(plots, filepath.str(), show);
}

void SampledTrajectoryV2::plotAllDimensions(unsigned int stateDim, std::vector<string> dimNameMap, string filepathWithoutEnding, bool show) const
{
    Vec<DVec> plots;
    plots.push_back(getTimestamps());

    for (unsigned int i = 0; i < dim(); i++)
    {
        plots.push_back(getDimensionData(i, stateDim));
    }

    //the first entry in plots is the time axis. In order to get the correct labels we need to add an empty string to the beginning of our dimName vector
    dimNameMap.insert(dimNameMap.begin(), "");

    //take filepath as title of the plot:
    std::stringstream ss;
    ss << "set title '" << filepathWithoutEnding << "'";

    std::stringstream filepath;
    filepath << filepathWithoutEnding << "_dim"  << "_Der" << stateDim << "_%d.gp";
    DMP::plotTogether(plots, dimNameMap, filepath.str(), show, ss.str());
}






unsigned int SampledTrajectoryV2::addDimension(const DVec& x, const DVec& y)
{

    unsigned int newDim = dim() + 1;

    __addDimension();

    addPositionsToDimension(newDim - 1, x, y);
    return newDim - 1;
}

void SampledTrajectoryV2::removeDimension(unsigned int dimension)
{
    typename timestamp_view::iterator itMap = dataMap.begin();

    for (; itMap != dataMap.end(); itMap++)
    {
        Vec< DVecPtr >& data = itMap->data;

        if (dimension < data.size())
        {
            data.erase(data.begin() + dimension);
        }
    }

}

void SampledTrajectoryV2::removeDerivation(unsigned int deriv)
{
    typename timestamp_view::iterator itMap = dataMap.begin();

    for (; itMap != dataMap.end(); itMap++)
    {
        Vec< DVecPtr >& data = itMap->data;

        for (unsigned int i = 0; i < data.size(); i++)
        {
            DVecPtr& vec = data.at(i);

            if (deriv + 1 <  vec->size())
            {
                vec->resize(deriv);
            }
        }
    }
}

void SampledTrajectoryV2::removeDerivation(unsigned int dimension, unsigned int deriv)
{
    typename timestamp_view::iterator itMap = dataMap.begin();

    for (; itMap != dataMap.end(); itMap++)
    {
        Vec< DVecPtr >& data = itMap->data;

        if (data.size() > dimension && deriv + 1 < data.at(dimension)->size())
        {
            data.at(dimension)->resize(deriv);
        }
    }
}

SampledTrajectoryV2 SampledTrajectoryV2::traj2qtraj(int mode) const
{
    typename ordered_view::const_iterator itMap = begin();
    std::map<double, DVec> res;

    for(;itMap != end(); itMap++){
        double curTime = itMap->getTimestamp();
        DVec curData;
        Vec<DVecPtr > data = itMap->getData();

        for(size_t i = 0; i <= data.size() - 3; i += 3){
            double yaw = data[i]->at(0);
            double pitch = data[i+1]->at(0);
            double roll = data[i+2]->at(0);

            Eigen::Quaterniond q = angle2quat(yaw, pitch, roll, mode);
            curData.push_back(q.w());
            curData.push_back(q.x());
            curData.push_back(q.y());
            curData.push_back(q.z());
        }

        res[curTime] = curData;
    }

    return SampledTrajectoryV2(res);

}

SampledTrajectoryV2 SampledTrajectoryV2::qtraj2traj(int mode) const
{
    typename ordered_view::const_iterator itMap = begin();
    std::map<double, DVec> res;

    for(;itMap != end(); itMap++){
        double curTime = itMap->getTimestamp();
        DVec curData;
        Vec<DVecPtr > data = itMap->getData();

        for(size_t i = 0; i <= data.size() - 4; i += 4){
            double w = data[i]->at(0);
            double x = data[i+1]->at(0);
            double y = data[i+2]->at(0);
            double z = data[i+3]->at(0);

            Eigen::Quaterniond q(w,x,y,z);
            Eigen::Vector3d ypr = quat2angle(q, mode);

            curData.push_back(ypr(0));
            curData.push_back(ypr(1));
            curData.push_back(ypr(2));
        }

        res[curTime] = curData;
    }
    return SampledTrajectoryV2(res);
}

SampledTrajectoryV2 SampledTrajectoryV2::traj2dqtraj(int mode) const
{
    typename ordered_view::const_iterator itMap = begin();
    std::map<double, DVec> res;

    for(;itMap != end(); itMap++){
        double curTime = itMap->getTimestamp();
        DVec curData;
        Vec<DVecPtr > data = itMap->getData();

        for(size_t i = 0; i <= data.size() - 6; i += 6){
            double yaw = data[i]->at(0);
            double pitch = data[i+1]->at(0);
            double roll = data[i+2]->at(0);

            double tx = data[i+3]->at(0);
            double ty = data[i+4]->at(0);
            double tz = data[i+5]->at(0);

            Eigen::Quaterniond r = angle2quat(yaw, pitch, roll, mode);
            curData.push_back(r.w());
            curData.push_back(r.x());
            curData.push_back(r.y());
            curData.push_back(r.z());

            Eigen::Quaterniond t = Eigen::Quaterniond(0, tx, ty, tz);
            Eigen::Quaterniond d = 0.5 * t * r;
            curData.push_back(d.w());
            curData.push_back(d.x());
            curData.push_back(d.y());
            curData.push_back(d.z());
        }

        res[curTime] = curData;
    }

    return SampledTrajectoryV2(res);
}

SampledTrajectoryV2 SampledTrajectoryV2::dqtraj2traj(int mode) const
{
    typename ordered_view::const_iterator itMap = begin();
    std::map<double, DVec> res;

    for(;itMap != end(); itMap++){
        double curTime = itMap->getTimestamp();
        DVec curData;
        Vec<DVecPtr > data = itMap->getData();

        for(size_t i = 0; i <= data.size() - 8; i += 8){
            double rw = data[i]->at(0);
            double rx = data[i+1]->at(0);
            double ry = data[i+2]->at(0);
            double rz = data[i+3]->at(0);
            double dw = data[i+4]->at(0);
            double dx = data[i+5]->at(0);
            double dy = data[i+6]->at(0);
            double dz = data[i+7]->at(0);

            Eigen::Quaterniond r(rw,rx,ry,rz);
            Eigen::Quaterniond d(dw,dx,dy,dz);
            Eigen::Vector3d ypr = quat2angle(r, mode);

            Eigen::Quaterniond t = 2 * d * r.conjugate();

            curData.push_back(ypr(0));
            curData.push_back(ypr(1));
            curData.push_back(ypr(2));
            curData.push_back(t.x());
            curData.push_back(t.y());
            curData.push_back(t.z());
        }

        res[curTime] = curData;
    }
    return SampledTrajectoryV2(res);
}


void SampledTrajectoryV2::addPositionsToDimension(unsigned int dimension, const DVec& x, const DVec& y)
{
    if (dimension >= dim() && dim() > 0)
    {
        addDimension(x, y);
    }
    else
    {
        assert(x.size() == y.size());

        for (unsigned int i = 0; i < x.size() && i < y.size(); ++i)
        {
            checkValue(x[i]);
            checkValue(y[i]);
            setPositionEntry(x[i], dimension, y[i]);
        }
    }
}


void SampledTrajectoryV2::addDerivationsToDimension(unsigned int dimension, const double t, const DVec& derivs)
{
    setEntries(t, dimension, derivs);
}

SampledTrajectoryV2::TrajData::TrajData(SampledTrajectoryV2& traj) :
    trajectory(traj)
{

}

void SampledTrajectoryV2::shiftTime(double shift)
{
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    typename ordered_view::const_iterator itMap = ordv.begin();

    for (; itMap != ordv.end(); itMap++)
    {
        //itMap->timestamp -= shift;
    }
}

void SampledTrajectoryV2::shiftValue(const DVec& shift)
{
    if (shift.size() > dim())
    {
        throw Exception("dimension is out of range: ") << shift.size() << " actual dimensions: " << dim();
    }


    for (unsigned int dimension = 0; dimension < dim(); dimension++)
    {
        const ordered_view& ordv = dataMap.get<TagOrdered>();
        typename ordered_view::const_iterator itMap = ordv.begin();

        for (; itMap != ordv.end(); itMap++)
        {
            itMap->data[dimension]->at(0) += shift[dimension];
        }
    }

}

void SampledTrajectoryV2::saveToFile(string filename)
{
    SampledTrajectoryV2::ordered_view::const_iterator itTraj = begin();

    std::ofstream ofs(filename);
    std::cout << "dim: " << dim() << std::endl;
    for(;itTraj != end(); itTraj++)
    {
        ofs << itTraj->getTimestamp();
        ofs << ",";
        for(size_t i = 0; i < dim(); ++i)
        {

            ofs << itTraj->getPosition(i);
            if( i != dim() - 1)
            {
                ofs << ",";
            }
            else
            {
                ofs << "\n";
            }
        }
    }

    ofs.close();

}

DVec SampledTrajectoryV2::calcAnchorPoint()
{
    m_anchorPoint.clear();
    m_anchorPoint.resize(dim());

    for (unsigned int dimension = 0; dimension < dim(); dimension++)
    {

        int startInd = 0;
        double startTime = getTimestamps()[startInd] ;
        double maxi = getMax(dimension, 0, startTime, startTime+getTimeLength());
        double mini = getMin(dimension, 0, startTime, startTime+getTimeLength());

        m_anchorPoint[dimension] = (maxi + mini) / 2;
    }

    return m_anchorPoint;
}

DVec SampledTrajectoryV2::getAnchorPoint() const
{

    return m_anchorPoint;

}

std::map<double, DVec > SampledTrajectoryV2::getPositionData() const
{
    const ordered_view& ordv = dataMap.get<TagOrdered>();
    typename ordered_view::const_iterator itMap = ordv.begin();

    std::map<double, DVec> res;
    for (; itMap != ordv.end(); itMap++)
    {
        DVec dimData;
        dimData.resize(dim());
        for(unsigned int di = 0; di < dim(); di++)
        {
            double cdata = itMap->getPosition(di);
            dimData[di] = cdata;
        }

        res.insert(std::pair<double, DVec>(itMap->getTimestamp(), dimData));
    }

    return res;
}

SampledTrajectoryV2 SampledTrajectoryV2::getSegment(double startTime, double endTime)
{
    double dt = getTimestamps()[1] - getTimestamps()[0];

    std::map<double, DVec> resdata;
    for (double t = startTime;t < endTime; t += dt)
    {
        Vec<DVecPtr> data = getStates(t);
        DVec curData;

        for(size_t i = 0; i < data.size(); ++i)
        {
            curData.push_back(data[i]->at(0));
        }

        resdata.insert(std::pair<double, DVec>(t, curData));
    }

    return SampledTrajectoryV2(resdata);
}

std::vector<SampledTrajectoryV2> SampledTrajectoryV2::getSegments(int num)
{
    std::vector<SampledTrajectoryV2> res;

    DVec timestamps = getTimestamps();
    int nt = timestamps.size();
    double timeLength = getTimeLength()/(double)num;

    for(double t = timestamps[0];
        t < timestamps[nt-1]-timeLength;
        t = t + timeLength)
    {
        SampledTrajectoryV2 cres = getSegment(t, t+timeLength);
        res.push_back(cres);
    }

    return res;


    return res;
}

SampledTrajectoryV2 SampledTrajectoryV2::normalizeTraj(double min, double max)
{
    std::map<double, DVec> res;

    const ordered_view& ordv = dataMap.get<TagOrdered>();
    typename ordered_view::const_iterator itMap = ordv.begin();

    DVec maxData;
    DVec minData;
    for(size_t di = 0; di < dim(); di++)
    {
        double cmax = getMax(di, 0, *(getTimestamps().begin()), *(getTimestamps().rbegin()));
        double cmin = getMin(di, 0, *(getTimestamps().begin()), *(getTimestamps().rbegin()));

        maxData.push_back(cmax);
        minData.push_back(cmin);
    }

    for (; itMap != ordv.end(); itMap++)
    {
        DVec dimData;
        dimData.resize(dim());
        for(size_t di = 0; di < dim(); di++)
        {
            double cdata = itMap->getPosition(di);

            if(maxData[di] != minData[di])
            {
                cdata = min + (max - min) * (cdata - minData[di]) / (maxData[di] - minData[di]);
            }
            else
            {
                cdata = minData[di];
            }
            dimData[di] = cdata;
        }

        res.insert(std::pair<double, DVec>(itMap->getTimestamp(), dimData));
    }


    return SampledTrajectoryV2(res);
}
