#ifndef BASICODE_H
#define BASICODE_H

#include "../general/helpers.h"

namespace DMP
{

    class ODE
    {
    public:
        ODE(unsigned int dim, double epsabs = 1e-15, long maxStepCount = 10000);
        ODE(double epsabs = 1e-15, long maxStepCount = 10000);

        virtual ~ODE() {}

        /**
         * @brief integrateForDim calculates the solution for a specific dimension.
         * @param t
         * @param init
         * @param dim
         * @param project
         * @return
         */
        double integrateForDim(double t, const DVec& init, const unsigned int dim, bool project = true);

        void integrate(double t, double tInit, const DVec& init, DVec& out, bool project = true);
        void integrateWithEulerMethod(double t, double tInit, const DVec& init, DVec& out);
        void integrate(double t, const DVec& init, DVec& out, bool project = true);
        void integrate(const DVec& times, const DVec& init, Vec< DVec > &out, bool project = true);
        void integrateWithEulerMethod(const DVec& times, const DVec& init, Vec< DVec > &out);


        unsigned int dim() const
        {
            return m_dim;
        }
        double epsabs() const
        {
            return m_epsabs;
        }
        void setDim(unsigned int dim)
        {
            m_dim = dim;
        }
        void setEpsabs(double epsabs)
        {
            m_epsabs = epsabs;
        }

        virtual void flow(double t, const DVec& x, DVec& out) = 0;
    protected:
        virtual unsigned int dimValuesOfInterest() const;
        virtual void projectStateToValuesOfInterest(const double* x, DVec& projection) const;
    private:
        unsigned int m_dim;
        double m_epsabs;
        long m_maxStepCount;

    private:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int /*file_version*/)
        {
            ar& boost::serialization::make_nvp("m_dim",m_dim);
            ar& boost::serialization::make_nvp("m_epsabs",m_epsabs);
            ar& boost::serialization::make_nvp("m_maxStepCount",m_maxStepCount);
        }

    };
}


#endif // BASICODE_H
