#include "basicode.h"

#include <gsl/gsl_errno.h>
#include <gsl/gsl_odeiv2.h>

int ode_integrator_flow(double t, const double x[], double f[], void* params);

using namespace DMP;

ODE::ODE(unsigned int dim, double epsabs, long maxStepCount) : m_dim(dim), m_epsabs(epsabs), m_maxStepCount(maxStepCount)
{
}

ODE::ODE(double epsabs,  long maxStepCount) : m_dim(0), m_epsabs(epsabs), m_maxStepCount(maxStepCount)
{
}

unsigned int ODE::dimValuesOfInterest() const
{
    return dim();
}
void ODE::projectStateToValuesOfInterest(const double* x, DVec& projection) const
{
    projection.assign(x, x + dimValuesOfInterest());
}

void ODE::integrate(double t, double tInit, const DVec& init, DVec& out, bool project)
{
    gsl_odeiv2_system sys = {ode_integrator_flow, nullptr, dim(), this};

    double stepSize = m_epsabs;

    if (t < tInit)
    {
        stepSize *= -1;
    }

    gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45,
                                stepSize, m_epsabs, 0.0);
    gsl_odeiv2_driver_set_nmax(driver, m_maxStepCount);

    if (dim() != init.size())
    {
        throw Exception() << "Dimensions of ODE (" << dim() << ") and the initialValue-vector (" << init.size() << ") do not match!";
    }

    double* x = new double[dim()];
    copy(init.begin(), init.end(), x);

    double evolution_time = tInit;
    int status = gsl_odeiv2_driver_apply(driver, &evolution_time, t, x);

    if (status != GSL_SUCCESS)
    {
        std::string error(gsl_strerror(status));
        throw Exception("ODE error: Error message: ") << error;
    }

    //      DVec x_result(x, x+dim());
    //      out = x_result;
    if (project)
    {
        projectStateToValuesOfInterest(x, out);
    }
    else
    {
        out.assign(x, x + dim());    // TODO
    }

    delete[] x;
    gsl_odeiv2_driver_free(driver);
}

void ODE::integrateWithEulerMethod(double t, double tInit, const DVec& init, DVec& out)
{

    double tCurr = tInit;
    out = init;
    while(tCurr <= t)
    {

        DVec deri_out;
        deri_out = out;
        flow(tCurr,out,deri_out);

        for(size_t j = 0; j < out.size(); j++)
        {
            out[j] += m_epsabs * deri_out[j];
        }

        tCurr += m_epsabs;
    }
}

void ODE::integrate(double t, const DVec& init, DVec& out, bool project)
{
    integrate(t, 0, init, out, project);
}

void ODE::integrate(const DVec& times, const DVec& init, Vec< DVec > &out, bool project)
{
    gsl_odeiv2_system sys = {ode_integrator_flow, nullptr, dim(), this};

    double stepSize = m_epsabs;

    if (times.end() < times.begin())
    {
        stepSize *= -1;
    }

    gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&sys, gsl_odeiv2_step_rkf45,
                                stepSize, m_epsabs, 0.0);

    gsl_odeiv2_driver_set_nmax(driver, m_maxStepCount);
    double* x = new double[dim()];
    copy(init.begin(), init.end(), x);

    double evolution_time = times[0];
    int idim = dimValuesOfInterest();
    //    std::cout << "dim: " << idim << std::endl;
    DVec x_result;
    x_result.resize(idim);

    if (project)
    {
        projectStateToValuesOfInterest(&init[0], x_result);
    }
    else
    {
        x_result.assign(init.begin(), init.end());
    }

    out.push_back(x_result);
    int c = 0;

    for (DVec::const_iterator i = times.begin() + 1; i != times.end(); ++i)
    {
        double dt = *i - evolution_time;
        evolution_time = 0;
        int status = gsl_odeiv2_driver_apply(driver, &evolution_time, dt, x);

        if (status != GSL_SUCCESS)
        {
            delete[] x;
            gsl_odeiv2_driver_free(driver);
            throw Exception("ODE error, return value") << status;
        }

        //        DVecx_result(dimValuesOfInterest());

        if (project)
        {
            projectStateToValuesOfInterest(x, x_result);
        }
        else
        {
            x_result.assign(x, x + dim());    // TODO
        }

        out.push_back(x_result);
        //        printf("|");
        //        std::cout.flush();
        c++;
        evolution_time = *i;
    }

    delete[] x;
    gsl_odeiv2_driver_free(driver);
}


void ODE::integrateWithEulerMethod(const DVec &times, const DVec &init, Vec<DVec> &out)
{
    DVec curstate = init;
    out.push_back(curstate);
    for(size_t i= 0; i < times.size()-1; i++)
    {
        double tInit = times.at(i);
        double tEnd = times.at(i+1);

        DVec curout;
        integrateWithEulerMethod(tEnd, tInit,curstate, curout);
        out.push_back(curout);

        curstate = curout;
    }
}

int ode_integrator_flow(double t, const double x[], double f[], void* params)
{
    ODE* ode = (ODE*) params;
    unsigned int dim = ode->dim();

    DVec x_Vec(x, x + dim),
         f_Vec(dim, 0.0);

    ode->flow(t, x_Vec, f_Vec);

    if (f_Vec.size() != dim)
    {
        throw Exception("Output vector of ODE::flow() does not have the correct size:") << f_Vec.size() << ", but should be " << dim;
    }

    copy(f_Vec.begin(), f_Vec.end(), f);

    return GSL_SUCCESS;
}


