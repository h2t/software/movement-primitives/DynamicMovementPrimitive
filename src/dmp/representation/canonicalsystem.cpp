/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "canonicalsystem.h"

#include <gsl/gsl_vector.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_multimin.h>

using namespace DMP;
CanonicalSystem::CanonicalSystem(double tau, double uMin, double initialXValue, double initialYValue) :
    ODE(1, 1e-5),
    phaseStopValue(0),
    uMin(uMin),
    tau(tau),
    alpha(-log(uMin)),
    initialXValue(initialXValue),
    initialYValue(initialYValue)

{

}

void CanonicalSystem::flow(double t, const DVec& x, DVec& out)
{
    const double& u = x[0];

    if (out.size() == 0)
    {
        out.resize(1);
    }

    //    if( u > 10 * uMin)
    //        out[0] = - 0.95 / stopPhaseValuetau;
    //    else
    out[0] = -alpha * u / (tau * (1 + phaseStopValue));
}

DVec CanonicalSystem::getInitValues()
{
    DVec result;
    //     result[0] = initialXValue;
    result.push_back(initialYValue);
    return result;
}

DVec CanonicalSystem::getCanonicalValue(double t)
{
    return exp(-alpha * t);
}





LinearCanonicalSystem::LinearCanonicalSystem(double tau) : CanonicalSystem(tau, 1e-8, 0.0, 0.0)
{
    slowDown = false;
}

void LinearCanonicalSystem::flow(double t, const DVec& x, DVec& out)
{
    float tau = getTau();
    //    std::cout << "x: " << x.at(0) << std::endl;
    //    if(slowDown && x.at(0) >= 0.5 && x.at(0) < 0.54)
    //        out[0] = 1.0/getTau()/5;
    //    else if(slowDown && x.at(0) >= 0.14 && x.at(0) < 0.34)
    //        out[0] = 1.0/getTau()*5;
    //    if(slowDown && t > 0.4 && t < 0.5)
    //        out[0] = 0;
    //    else
    if (out.size() == 0)
    {
        out.resize(1);
    }

    out[0] = 1.0 / tau;
}


LinearDecayCanonicalSystem::LinearDecayCanonicalSystem(double tau, double uMin) : CanonicalSystem(tau, uMin, 0.0, 0.0)
{
}

void LinearDecayCanonicalSystem::flow(double t, const DVec &x, DVec &out)
{
    float tau = getTau();
    if (out.size() == 0)
    {
        out.resize(1);
    }
    out[0] = - 1.0 / tau;
}

double LinearDecayCanonicalSystem::step(double canVal, double deltaT)
{
    return canVal - deltaT * 1.0 / getTau();
}


LinearSigmoidalCanonicalSystem::LinearSigmoidalCanonicalSystem(double tau): CanonicalSystem(tau)
{
}

void LinearSigmoidalCanonicalSystem::flow(double t, const DVec& x, DVec& out)
{
    double u = x.at(0);

    if (u > getInitValues().at(0) * 0.55)
    {
        out[0] = -getTau() / 2;
    }
    else
    {
        //        double steepness = 40;
        //        double sample_rate = 0.55;
        //        double T  = 1;
        //        double e = exp(steepness/sample_rate*(getTau()*T - t));
        //        out[0] = - steepness* e/(pow(1 + e,2));
        out[0] = -getAlpha() * u / getTau();
    }
}




SolvedCanonicalSystem::SolvedCanonicalSystem(double tau, double uMin, double initialXValue, double initialYValue) :
    CanonicalSystem(tau, uMin, initialXValue, initialYValue),
    odeConstant(getInitValues()[1] / exp(getAlpha() / getTau() * getInitValues()[0]))
{

}

void SolvedCanonicalSystem::flow(double t, const DVec& x, DVec& out)
{
    out[0] = odeConstant * exp(getAlpha() / getTau() * t);
}

double SolvedCanonicalSystem::inverse(const double u)
{
    return log(u / odeConstant) * getTau() / getAlpha();
}


void PeriodicCanonicalSystem::flow(double t, const DVec &x, DVec &out)
{
    out[0] = 2 * M_PI / getTau();
}


struct find_r0_params
{
    PeriodicDiscreteCanonicalSystem* osc;
    double T;
    double limit_r;
};

double find_r0_target(double r0, void* params);

struct find_q_params
{
    PeriodicDiscreteCanonicalSystem* osc;
    DVec init;
    DVec p_m1;
    double delta_m1;
};
double find_q_target(double t, void* params);

struct find_T_for_given_r0_params
{
    PeriodicDiscreteCanonicalSystem* osc;
    double limit_r, r0;
};
double find_T_for_given_r0_target(double T, void* params);


double find_r0_target(double r0, void* params)
{
    find_r0_params* p = (find_r0_params*) params;
    DVec init;
    init.resize(2);

    init[0] = 0;
    init[1] = r0;

    double res = p->osc->explicitSolutionForR(p->T, init) - p->limit_r;
    return res;
}


double find_T_for_given_r0_target(double T, void* params)
{
    find_T_for_given_r0_params* p = (find_T_for_given_r0_params*) params;

    DVec init;
    init.resize(2);

    init[0] = 0;
    init[1] = p->r0;

    return p->osc->explicitSolutionForR(T, init) - p->limit_r;
}

double find_q_target(double t, void* params)
{
    find_q_params* p = (find_q_params*) params;
    DVec tmp;
    tmp.resize(2);
    p->osc->integrate(t, p->init, tmp);
    DVec diff;
    diff.resize(2);
    diff = pol2cart(tmp) - pol2cart(p->p_m1);
    return sqrt(diff[0] * diff[0] + diff[1] * diff[1]) - p->delta_m1;
}

PeriodicDiscreteCanonicalSystem::PeriodicDiscreteCanonicalSystem(double Omega, double alpha, double beta,
        double eta, double mu, double mu1)
    :  ODE(1, 1e-5)
{
    m_alpha = alpha;
    m_beta = beta;
    m_eta = eta;

    m_Omega = Omega;
    m_mu = mu;
    m_mu1 = mu1;
}

void PeriodicDiscreteCanonicalSystem::flow(double t, const DVec& x, DVec& out)
{
    double phi = x[0],
           r = x[1];

    out[0] = m_Omega;
    out[1] = m_eta * (pow(m_mu, m_alpha) - pow(r, m_alpha)) * pow(r, m_beta);
}

double PeriodicDiscreteCanonicalSystem::explicitSolutionForR(double t, const DVec& init)
{
    DVec res;
    res.resize(dim());
    integrate(t, init, res);
    return res[1];
}

double PeriodicDiscreteCanonicalSystem::explicitSolutionForPhi(double t, const DVec& init)
{
    DVec res;
    res.resize(dim());
    integrate(t, init, res);
    return res[0];
}

double PeriodicDiscreteCanonicalSystem::getTransientLengthForInitialR(double r0, double upper_limit)
{
    if (r0 <= getMu1())
    {
        return 0.0;
    }

    double T_lo = 0.0, T_hi = upper_limit;
    gsl_function F;
    F.function = find_T_for_given_r0_target;
    find_T_for_given_r0_params params = {this, getMu1(), r0};
    F.params = &params;
    gsl_root_fsolver* s = gsl_root_fsolver_alloc(gsl_root_fsolver_bisection);
    gsl_root_fsolver_set(s, &F, T_lo, T_hi);

    for (int i = 0; i < 100; ++i)
    {
        gsl_root_fsolver_iterate(s);
        int status = gsl_root_test_interval(T_lo, T_hi, 1e-8, 1e-8);

        if (status == GSL_SUCCESS)
        {
            break;
        }
    }

    double T = gsl_root_fsolver_root(s);

    gsl_root_fsolver_free(s);

    return T;
}

void PeriodicDiscreteCanonicalSystem::getInitialConditionsForTransientLength(double T, double phi1, DVec& init,
        double limit_r)
{
    if (limit_r < 0)
    {
        limit_r = this->getMu1();
    }

    if (T > 0)
    {
        double r_lo = m_mu, r_hi = 1000 * m_mu; // TODO: upper limit for r should be configureable
        gsl_function F;
        F.function = find_r0_target;
        find_r0_params params = {this, T, limit_r};
        F.params = &params;
        gsl_root_fsolver* s = gsl_root_fsolver_alloc(gsl_root_fsolver_bisection);
        gsl_root_fsolver_set(s, &F, r_lo, r_hi);

        for (int i = 0; i < 100; ++i)
        {
            gsl_root_fsolver_iterate(s);
            int status = gsl_root_test_interval(r_lo, r_hi, 1e-8, 1e-8);

            if (status == GSL_SUCCESS)
            {
                break;
            }
        }

        init[0] = phi1 - m_Omega * T;
        init[1] = gsl_root_fsolver_root(s);
        gsl_root_fsolver_free(s);
    }
    else
    {
        init[0] = 0.0;
        init[1] = m_mu;
    }
}
