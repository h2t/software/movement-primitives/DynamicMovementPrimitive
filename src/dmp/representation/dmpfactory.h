#ifndef DMPFACTORY_H
#define DMPFACTORY_H

#include <dmp/representation/dmp/dmpinterface.h>

namespace DMP{

class DMPFactory{

public:
    DMPFactory(){}
    ~DMPFactory(){}


    DMP::DMPInterfacePtr getDMP(std::string dmptype, int kernelSize = 100);
};

typedef boost::shared_ptr<DMPFactory> DMPFactoryPtr;

}


#endif
