#ifndef VIAPOINT_H
#define VIAPOINT_H

#include <memory>
#include <armadillo>
#include "../general/vec.h"
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/StdVector>
#include "../general/exception.h"

using namespace arma;
namespace DMP
{

//class SimpleViaPoint
//{
//public:
//    SimpleViaPoint(int dim)
//    {
//        this->dim = dim;
//        pos.zeros(dim);
//        vel.zeros(dim);
//        acc.zeros(dim);
//        indicator.zeros(dim);
//        uncertainty.eye(dim, dim);
//        uncertainty = uncertainty * 1e-5;
//    }
//
//    void setRelevantDim(int dim)
//    {
//        indicator(dim) = 1;
//    }
//
//    void setRelevantDims(const std::vector<int>& dims)
//    {
//        for(size_t i = 0; i < dims.size(); ++i)
//        {
//            setRelevantDim(dims[i]);
//        }
//    }
//
//    void setAllDimRelevant()
//    {
//        indicator.ones();
//    }
//
//    void reset()
//    {
//        indicator.zeros();
//    }
//
//    double canVal;
//    vec pos;
//    vec vel;
//    vec acc;
//private:
//    double dim;
//    vec indicator;
//    mat uncertainty;
//};
//
//typedef boost::shared_ptr<SimpleViaPoint> SimpleViaPointPtr;
//class SimpleViaPointSet
//{
//public:
//    SimpleViaPointSet(){}
//
//    void push_back(const SimpleViaPointPtr& viaPoint)
//    {
//        viaPoints.push_back(viaPoint);
//        canVals.push_back(viaPoint->canVal);
//    }
//
//    bool is_sorted(const char* direction = "descend")
//    {
//        vec canvec(canVals);
//        return canvec.is_sorted(direction);
//    }
//
//    void sort(const char* direction = "descend")
//    {
//        vec canvec(canVals);
//        uvec indices = sort_index(canvec, direction);
//
//        std::vector<SimpleViaPointPtr> sorted;
//        for(size_t i = 0; i < viaPoints.size(); ++i)
//        {
//            sorted.push_back(viaPoints.at(indices[i]));
//            canVals.at(i)=(viaPoints.at(indices[i])->canVal);
//        }
//
//        viaPoints = sorted;
//    }
//
//    void removeViaPointAt(double canVal)
//    {
//        std::vector<double>::iterator it = std::find(canVals.begin(), canVals.end(), canVal);
//        if(it == canVals.end())
//        {
//            std::cout << "No corresponding via-point found";
//        }
//        else
//        {
//            int index = std::distance(canVals.begin(), it);
//            viaPoints.at(index).reset();
//            viaPoints.erase(viaPoints.begin() + index);
//            canVals.erase(canVals.begin() + index);
//        }
//    }
//
//    std::vector<SimpleViaPointPtr> viaPoints;
//    std::vector<double> canVals;
//
//    size_t getNum()
//    {
//        return viaPoints.size();
//    }
//
//};
//typedef boost::shared_ptr<SimpleViaPointSet> SimpleViaPointSetPtr;
//


class ViaPose
{

public:
    ViaPose()
    {
        viaPosition.setZero();
        viaLinearForce.setZero();
        viaOrientation.setIdentity();
        viaLinearForce.setIdentity();
    }

    ViaPose(const DVec& viaPoseVec, const vec& force)
    {
        if(viaPoseVec.size() != 7)
        {
            throw DMP::Exception("Error: ViaPoseVec should have 7 dimensions !!!");
        }

        viaPosition << viaPoseVec.at(0), viaPoseVec.at(1), viaPoseVec.at(2);
        viaLinearForce << force(0), force(1), force(2);

        viaOrientation.w() = viaPoseVec.at(3);
        viaOrientation.x() = viaPoseVec.at(4);
        viaOrientation.y() = viaPoseVec.at(5);
        viaOrientation.z() = viaPoseVec.at(6);

        viaRotationForce.w() = force(3);
        viaRotationForce.x() = force(4);
        viaRotationForce.y() = force(5);
        viaRotationForce.z() = force(6);

        data = viaPoseVec;
    }

    void negateViaOrientation()
    {
        viaOrientation.w() = -viaOrientation.w();
        viaOrientation.x() = -viaOrientation.x();
        viaOrientation.y() = -viaOrientation.y();
        viaOrientation.z() = -viaOrientation.z();

    }

    Eigen::Vector3f viaPosition;
    Eigen::Vector3f viaLinearForce;
    Eigen::Quaterniond viaOrientation;
    Eigen::Quaterniond viaRotationForce;
    DVec data;

private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int /*file_version*/)
    {
        ar& boost::serialization::make_nvp("data",data);

        if(Archive::is_saving::value)
        {
            std::vector<double> vector = {viaPosition.x(), viaPosition.y(), viaPosition.z()};
            ar& boost::serialization::make_nvp("viaPosition",vector);
        }
        else
        {
            std::vector<double> vector;
            ar& boost::serialization::make_nvp("viaPosition",vector);
            viaPosition = Eigen::Vector3f(vector[0], vector[1], vector[2]);
        }

        if(Archive::is_saving::value)
        {
            std::vector<double> vector = {viaLinearForce.x(), viaLinearForce.y(), viaLinearForce.z()};
            ar& boost::serialization::make_nvp("viaLinearForce",vector);
        }
        else
        {
            std::vector<double> vector;
            ar& boost::serialization::make_nvp("viaLinearForce",vector);
            viaLinearForce = Eigen::Vector3f(vector[0], vector[1], vector[2]);
        }

        if(Archive::is_saving::value)
        {
            std::vector<double> vector = {viaOrientation.w(), viaOrientation.x(), viaOrientation.y(), viaOrientation.z()};
            ar& boost::serialization::make_nvp("viaOrientation",vector);
        }
        else
        {
            std::vector<double> vector;
            ar& boost::serialization::make_nvp("viaOrientation",vector);
            viaOrientation = Eigen::Quaterniond(vector[0], vector[1], vector[2], vector[3]);
        }

        if(Archive::is_saving::value)
        {
            std::vector<double> vector = {viaRotationForce.w(), viaRotationForce.x(), viaRotationForce.y(), viaRotationForce.z()};
            ar& boost::serialization::make_nvp("viaRotationForce",vector);
        }
        else
        {
            std::vector<double> vector;
            ar& boost::serialization::make_nvp("viaRotationForce",vector);
            viaRotationForce = Eigen::Quaterniond(vector[0], vector[1], vector[2], vector[3]);
        }
    }

};
typedef std::shared_ptr<ViaPose> ViaPosePtr;

typedef std::pair<double, ViaPosePtr > ViaPoseMap;
typedef std::vector<ViaPoseMap > ViaPoseSet;

typedef std::pair<double, DVec > ViaPointMap;
typedef std::vector<ViaPointMap > ViaPointSet;
}


#endif // VIAPOINT_H
