#include "dmpfactory.h"

#include <representation/dmp/basicdmp.h>
#include <representation/dmp/adaptive3rdorderdmp.h>
#include <representation/dmp/dmp3rdorder.h>
#include <representation/dmp/endvelodmp.h>
#include <representation/dmp/endveloforcefielddmp.h>
#include <representation/dmp/forcefielddmp.h>
#include <representation/dmp/quaterniondmp.h>
#include <representation/dmp/periodicdmp.h>
#include <representation/dmp/taskspacedmp.h>
#include <representation/dmp/umitsmp.h>
#include <representation/dmp/umidmp.h>


DMP::DMPInterfacePtr DMP::DMPFactory::getDMP(std::string dmptype, int kernelSize)
{
    DMP::DMPInterfacePtr dmp;

    if(dmptype == "BasicDMP"){
        dmp.reset(new BasicDMP(kernelSize));
    } else if(dmptype == "DMP3rdOrder") {
        dmp.reset(new DMP3rdOrder(kernelSize));
    } else if(dmptype == "AdaptiveGoal3rdOrderDMP") {
        dmp.reset(new AdaptiveGoal3rdOrderDMP(kernelSize));
    } else if(dmptype == "EndVeloDMP") {
        dmp.reset(new EndVeloDMP(kernelSize));
    } else if(dmptype == "EndVeloForceFieldDMP") {
        dmp.reset(new EndVeloForceFieldDMP(kernelSize));
    } else if(dmptype == "ForceFieldDMP") {
        dmp.reset(new ForceFieldDMP(kernelSize));
    } else if(dmptype == "SimpleEndVeloDMP") {
        dmp.reset(new SimpleEndVeloDMP(kernelSize));
    } else if(dmptype == "QuaternionDMP"){
        dmp.reset(new QuaternionDMP(kernelSize));
    } else if(dmptype == "PeriodicDMP"){
        dmp.reset(new PeriodicDMP(kernelSize));
    } else if(dmptype == "TaskSpaceDMP"){
        dmp.reset(new TaskSpaceDMP(kernelSize));
    } else if(dmptype == "UMITSMP" || dmptype == "TSMP" || dmptype == "tsvmp"){
        dmp.reset(new UMITSMP(kernelSize));
    } else if(dmptype == "UMIDMP" || dmptype == "JSMP" || dmptype == "jsvmp") {
        dmp.reset(new UMIDMP(kernelSize));
    }
    else{
        std::cerr << "unrecognized dmp type " << std::endl;
    }

    return dmp;
}
