#include "dualquaterniondmp.h"
#include <Eigen/Dense>

namespace DMP {



DualQuaternionDMP::DualQuaternionDMP(int kernelSize, double d, double tau):
    BasicDMP(kernelSize)

{
    canonicalSystem.reset(new CanonicalSystem(tau));
    K = (d / 2) * (d / 2);
    D = d;
    ODE::setEpsabs(1e-5);
}

Vec<DoubleMap> DualQuaternionDMP::calcVecPerturbationForceSamples(unsigned int trajDim, const DVec &canonicalValues, const SampledTrajectoryV2 &exampleTraj) //const
{

    Vec<DoubleMap> result;

    if (canonicalValues.size() != exampleTraj.size())
    {
        throw Exception("The amount of the canonicalValues do not match the exampleTrajectory");
    }

    SampledTrajectoryV2 qtraj = exampleTraj;

    DVec::const_iterator itCanon = canonicalValues.begin();
    double tau = canonicalSystem->getTau();


    unsigned int qDim = trajDim * DQ_STATE;

    double grw = qtraj.rbegin()->getPosition(qDim);
    double grx = qtraj.rbegin()->getPosition(qDim + 1);
    double gry = qtraj.rbegin()->getPosition(qDim + 2);
    double grz = qtraj.rbegin()->getPosition(qDim + 3);
    double gtw = qtraj.rbegin()->getPosition(qDim + 4);
    double gtx = qtraj.rbegin()->getPosition(qDim + 5);
    double gty = qtraj.rbegin()->getPosition(qDim + 6);
    double gtz = qtraj.rbegin()->getPosition(qDim + 7);


    DualQuaterniond g0(grw,grx,gry,grz,gtw,gtx,gty,gtz);

    SampledTrajectoryV2::ordered_view::const_iterator itQtraj = qtraj.begin();
    DoubleMap  frw, frx, fry, frz, ftw, ftx, fty, ftz;

    double qrw = itQtraj->getPosition(qDim);
    double qrx = itQtraj->getPosition(qDim + 1);
    double qry = itQtraj->getPosition(qDim + 2);
    double qrz = itQtraj->getPosition(qDim + 3);
    double qtw = itQtraj->getPosition(qDim + 4);
    double qtx = itQtraj->getPosition(qDim + 5);
    double qty = itQtraj->getPosition(qDim + 6);
    double qtz = itQtraj->getPosition(qDim + 7);

    DualQuaterniond q(qrw, qrx, qry, qrz, qtw, qtx, qty, qtz);
    DualQuaterniond q0;
    DualQuaterniond q1;
    q0 = q;
    Eigen::Vector3d w, dw, v, dv;

    // generate first angular velocity
    SampledTrajectoryV2::ordered_view::const_iterator itQtraj1 = qtraj.begin();
    itQtraj1++;
    SampledTrajectoryV2::ordered_view::const_iterator itQtrajend = qtraj.end();
    itQtrajend--;

    Eigen::Quaterniond rot, tran;
    rot.w() = itQtraj1->getPosition(qDim);
    rot.x() = itQtraj1->getPosition(qDim + 1);
    rot.y() = itQtraj1->getPosition(qDim + 2);
    rot.z() = itQtraj1->getPosition(qDim + 3);
    tran.w() = itQtraj1->getPosition(qDim + 4);
    tran.x() = itQtraj1->getPosition(qDim + 5);
    tran.y() = itQtraj1->getPosition(qDim + 6);
    tran.z() = itQtraj1->getPosition(qDim + 7);
    q1 = DualQuaterniond(rot,tran);

    double dt0 = itQtraj1->getTimestamp() - itQtraj->getTimestamp();

    Vec<Eigen::Vector3d> s0 = calcVelocityAndAcceleration(q0, q, q1, dt0);
    w = s0[0];
    v = s0[2];

    double preTime = itQtraj->getTimestamp();
    ++itQtraj;
    ++itQtraj1;

    double dt = itQtraj1->getTimestamp() - itQtraj->getTimestamp();
    q0 = q;
    q = q1;
    rot.w() = itQtraj1->getPosition(qDim);
    rot.x() = itQtraj1->getPosition(qDim + 1);
    rot.y() = itQtraj1->getPosition(qDim + 2);
    rot.z() = itQtraj1->getPosition(qDim + 3);
    tran.w() = itQtraj1->getPosition(qDim + 4);
    tran.x() = itQtraj1->getPosition(qDim + 5);
    tran.y() = itQtraj1->getPosition(qDim + 6);
    tran.z() = itQtraj1->getPosition(qDim + 7);
    q1 = DualQuaterniond(rot,tran);

    Vec<Eigen::Vector3d> s1 = calcVelocityAndAcceleration(q0, q, q1, dt);
    dw = s1[1];
    dv = s1[3];

    w = w - dw * dt0;
    v = v - dv * dt0;

    // twist velocity version
    Eigen::Vector3d tr = v;
    Eigen::Vector3d twistv = v + tr.cross(w);
    Eigen::Vector3d dtwistv = dv + v.cross(w) + tr.cross(dw);
    DualQuaterniond yita =  DualQuaterniond(w,twistv) * tau;
    DualQuaterniond ayita = DualQuaterniond(dw,dtwistv) * tau;

    // normal version
//    DualQuaterniond yita =  DualQuaterniond(w,v) * tau;
//    DualQuaterniond ayita = DualQuaterniond(dw,dv) * tau;


    DualQuaterniond fo = ayita * tau - DualQuaterniond::dqlog(g0 * q0.conjugate()) * K * 2 + yita * D;
//    DualQuaterniond fo = ayita * tau - DualQuaterniond::dqlog(g0 , q0.conjugate()) * K * 2 + yita * D;


    frw[*itCanon] = fo.r.w();
    frx[*itCanon] = fo.r.x();
    fry[*itCanon] = fo.r.y();
    frz[*itCanon] = fo.r.z();
    ftw[*itCanon] = fo.d.w();
    ftx[*itCanon] = fo.d.x();
    fty[*itCanon] = fo.d.y();
    ftz[*itCanon] = fo.d.z();

    ++itCanon;

    for (; itQtraj != qtraj.end(); ++itCanon, ++itQtraj, ++itQtraj1)
    {
        rot.w() = itQtraj->getPosition(qDim);
        rot.x() = itQtraj->getPosition(qDim + 1);
        rot.y() = itQtraj->getPosition(qDim + 2);
        rot.z() = itQtraj->getPosition(qDim + 3);
        tran.w() = itQtraj->getPosition(qDim + 4);
        tran.x() = itQtraj->getPosition(qDim + 5);
        tran.y() = itQtraj->getPosition(qDim + 6);
        tran.z() = itQtraj->getPosition(qDim + 7);
        q = DualQuaterniond(rot,tran);

        dt = itQtraj->getTimestamp() - preTime;

        if(itQtraj == itQtrajend){
            w = w + dw * dt;
            v = v + dv * dt;
        }else{
            rot.w() = itQtraj1->getPosition(qDim);
            rot.x() = itQtraj1->getPosition(qDim + 1);
            rot.y() = itQtraj1->getPosition(qDim + 2);
            rot.z() = itQtraj1->getPosition(qDim + 3);
            tran.w() = itQtraj1->getPosition(qDim + 4);
            tran.x() = itQtraj1->getPosition(qDim + 5);
            tran.y() = itQtraj1->getPosition(qDim + 6);
            tran.z() = itQtraj1->getPosition(qDim + 7);
            q1 = DualQuaterniond(rot,tran);

            Vec<Eigen::Vector3d> res = calcVelocityAndAcceleration(q0, q, q1, dt);
            w = res[0];
            dw = res[1];
            v = res[2];
            dv = res[3];
        }

        // twist velocity version
        tr = v * dt;
        twistv = v + tr.cross(w);
        dtwistv = dv + v.cross(w) + tr.cross(dw);
        yita =  DualQuaterniond(w,twistv) * tau;
        ayita = DualQuaterniond(dw,dtwistv) * tau;

        // normal version
//        yita =  DualQuaterniond(w,v) * tau;
//        ayita =  DualQuaterniond(dw,dv) * tau;


        DVec vel;
        vel << yita.d.x(), yita.d.y(), yita.d.z();
        vels_tr[*itCanon] = vel;

        fo =  ayita * tau - DualQuaterniond::dqlog(g0 * q.conjugate()) * K * 2 + yita * D;
//        fo =  ayita * tau - DualQuaterniond::dqlog(g0 , q.conjugate()) * K * 2 + yita * D;


        frw[*itCanon] = fo.r.w();
        frx[*itCanon] = fo.r.x();
        fry[*itCanon] = fo.r.y();
        frz[*itCanon] = fo.r.z();
        ftw[*itCanon] = fo.d.w();
        ftx[*itCanon] = fo.d.x();
        fty[*itCanon] = fo.d.y();
        ftz[*itCanon] = fo.d.z();

        q0 = q;
        preTime = itQtraj->getTimestamp();

    }


    result.push_back(frw);
    result.push_back(frx);
    result.push_back(fry);
    result.push_back(frz);
    result.push_back(ftw);
    result.push_back(ftx);
    result.push_back(fty);
    result.push_back(ftz);
    return result;
}


void DualQuaternionDMP::learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories)
{
    if (trainingTrajectories.size() > 1)
    {
        throw Exception() << "this DMP can only be trained with one trajectory (but with several dimensions)";
    }

    if (trainingTrajectories.size() == 0)
    {
        return;
    }

    trainingTraj = trainingTrajectories;
    SampledTrajectoryV2& trainingTrajectory = trainingTraj[0];
    __trajDim = trainingTrajectory.dim();

    // normalize timestamps so that they start at 0 and end at 1
    trainingTrajectory = SampledTrajectoryV2::normalizeTimestamps(trainingTrajectory, 0.0, 1.0);
    DVec normTimestamps = trainingTrajectory.getTimestamps();


    Vec<DVec> uValuesOrig;
    canonicalSystem->integrate(normTimestamps, canonicalSystem->getInitValues(), uValuesOrig);
    DVec uValues;
    DoubleMap uValuesPlotMap;

    for (unsigned int i = 0; i < uValuesOrig.size(); ++i)
    {
        uValues.push_back(uValuesOrig[i][0]);
        uValuesPlotMap[normTimestamps.at(i)] = uValuesOrig[i][0];
    }

    unsigned int trajDim = trainingTrajectory.dim() / DQ_STATE;

    for (unsigned int dim = 0; dim < trajDim; dim++)
    {
        Vec<DoubleMap> perturbationForceSamples = calcVecPerturbationForceSamples(dim, uValues, trainingTrajectory);

        for(size_t i = 0; i < perturbationForceSamples.size(); i++){
            _trainFunctionApproximator(dim * perturbationForceSamples.size() + i, perturbationForceSamples[i]);
        }

    }
}


SampledTrajectoryV2 DualQuaternionDMP::calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor)
{
//    qTraj = getTrainingTraj(0);
//    trainingTraj.at(0) = qTraj;
    BasicDMP::setStartPositions(initialStates);

    return DMPInterfaceTemplate<DMPState>::calculateTrajectory(timestamps, goal, initialStates, initialCanonicalValues, temporalFactor);
}

void DualQuaternionDMP::integrate(double t, double tInit, const DVec &init, DVec &out, bool project)
{
    double stepSize = epsabs();

    if (t < tInit)
    {
        stepSize *= -1;
    }

    double tCur = tInit;

    DVec x = init;


    out = x;
    while(tCur < t){
        double u = x.at(0);

        DVec canonicalState(1);
        canonicalState[0] = u;
        canonicalSystem->flow(t, canonicalState, out);

        out[0] = u + stepSize * out[0];

        double tau = canonicalSystem->getTau();

        unsigned int dimensions = __trajDim;

        if (dimensions > goals.size())
        {
            throw Exception("There are less trajectory goals than dimensions");
        }

        if (dimensions > startPositions.size())
        {
            throw Exception("There are less trajectory start positions than dimensions");
        }

        unsigned int offset = canonicalState.size();
        unsigned int curDim = 0;

        while (curDim < dimensions)
        {
            double frw = _getPerturbationForce(curDim, u);
            double frx = _getPerturbationForce(curDim+1, u);
            double fry = _getPerturbationForce(curDim+2, u);
            double frz = _getPerturbationForce(curDim+3, u);
            double ftw = _getPerturbationForce(curDim+4, u);
            double ftx = _getPerturbationForce(curDim+5, u);
            double fty = _getPerturbationForce(curDim+6, u);
            double ftz = _getPerturbationForce(curDim+7, u);
            DualQuaterniond fo(frw,frx,fry,frz,ftw,ftx,fty,ftz);

            //bool scaling = true;
            //double scaleFactor = 1.0;
            //std::map<unsigned int, bool>::iterator it = scalingActivated.find(curDim);

            //if (it != scalingActivated.end() && !it->second)
            //{
               // scaling = false;
            //}

            //if (scaling)
            //{
                //TODO add scale factoring
                //scaleFactor = 1;
            //}

            unsigned int i = curDim * 2 + offset;
            double rw = x[i];
            double ww = x[i + 1];
            double rx = x[i + 2];
            double wx = x[i + 3];
            double ry = x[i + 4];
            double wy = x[i + 5];
            double rz = x[i + 6];
            double wz = x[i + 7];
            double tw = x[i + 8];
            double tvw = x[i + 9];
            double tx = x[i + 10];
            double tvx = x[i + 11];
            double ty = x[i + 12];
            double tvy = x[i + 13];
            double tz = x[i + 14];
            double tvz = x[i + 15];

            DualQuaterniond q(rw,rx,ry,rz,tw,tx,ty,tz);
            q.normalize();

            double grw = goals[curDim];
            double grx = goals[curDim+1];
            double gry = goals[curDim+2];
            double grz = goals[curDim+3];
            double gtw = goals[curDim+4];
            double gtx = goals[curDim+5];
            double gty = goals[curDim+6];
            double gtz = goals[curDim+7];

            DualQuaterniond g0(grw, grx, gry, grz, gtw, gtx, gty, gtz);

            Eigen::Quaterniond w(ww,wx,wy,wz);
            Eigen::Quaterniond tv(tvw,tvx,tvy,tvz);

            DualQuaterniond yita =  DualQuaterniond(w,tv) * tau;


            // calculate angular acceleration
            DualQuaterniond dw = (DualQuaterniond::dqlog(g0 * q.conjugate()) * K * 2  - yita * D + fo) * (1.0 / (tau * tau))  ;
//            DualQuaterniond dw = (DualQuaterniond::dqlog(g0 , q.conjugate()) * K * 2  - yita * D + fo) * (1.0 / (tau * tau))  ;

            // calculate change of quaternion
            DualQuaterniond nq = DualQuaterniond::dqexp(yita * 0.5 * stepSize * (1/tau)) * q;
//            DualQuaterniond nq = DualQuaterniond::dqexp(yita * 0.5 * stepSize * (1/tau), q);

            nq.normalize();

            out[i++] = nq.r.w();
            out[i++] = w.w() + stepSize * dw.r.w();
            out[i++] = nq.r.x();
            out[i++] = w.x() + stepSize * dw.r.x();
            out[i++] = nq.r.y();
            out[i++] = w.y() + stepSize * dw.r.y();
            out[i++] = nq.r.z();
            out[i++] = w.z() + stepSize * dw.r.z();
            out[i++] = nq.d.w();
            out[i++] = tv.w() + stepSize * dw.d.w();
            out[i++] = nq.d.x();
            out[i++] = tv.x() + stepSize * dw.d.x();
            out[i++] = nq.d.y();
            out[i++] = tv.y() + stepSize * dw.d.y();
            out[i++] = nq.d.z();
            out[i++] = tv.z() + stepSize * dw.d.z();
            curDim += DQ_STATE;
        }

        x = out;
        tCur += stepSize;
    }

}

Vec<DMPState> DualQuaternionDMP::calculateTrajectoryPoint(double t, const DVec &goal, double tCurrent, const Vec<DMPState> &currentStates, DVec &canonicalValues, double temporalFactor)
{

    if (startPositions.size() == 0)
    {
        //        throw Exception("There is no start position set. If you use calculateTrajectoryPoint directly, you must set the start position before (setStartPositions()). ");
        std::cout << "No Start Positions set, assuming current state as start position" << std::endl;
        BasicDMP::setStartPositions(currentStates);
    }

    ODE::setDim(1 + __trajDim * 2);

    DVec initialODEConditions = canonicalValues;
    DVec initialStatesVec = SystemStatus::convertStatesToArray(currentStates);
    initialODEConditions.insert(initialODEConditions.end(), initialStatesVec.begin(), initialStatesVec.end());

    setTemporalFactor(temporalFactor);
    setGoals(goal);

    DVec rawResult;
    integrate(t, tCurrent, initialODEConditions, rawResult, false);

    canonicalValues.assign(rawResult.begin(), rawResult.begin() + 1);
    rawResult.erase(rawResult.begin());
    Vec<DMPState> result = SystemStatus::convertArrayToStates<DMPState>(rawResult);

    return result;

}

Vec<Eigen::Vector3d> DualQuaternionDMP::calcVelocityAndAcceleration(DualQuaterniond q0, DualQuaterniond q, DualQuaterniond q1, double dt)
{
    Vec<Eigen::Vector3d> res;
    Eigen::Quaterniond dr, v;
    Eigen::Quaterniond ddr, dv;

    q0.t = DualQuaterniond::getTranslation(q0);
    q1.t = DualQuaterniond::getTranslation(q1);
    q.t = DualQuaterniond::getTranslation(q);
    if(dt != 0){
        dr = (q1.r - q.r) / dt;
        ddr = ((q1.r - q.r) - (q.r - q0.r)) / (dt * dt);
        v = (q1.t - q.t) / dt;
        dv = ((q1.t - q.t) - (q.t - q0.t)) / (dt * dt);
    } else {
        dr = Eigen::Quaterniond::Identity();
        ddr = Eigen::Quaterniond::Identity();
        v = Eigen::Quaterniond::Identity();
        dv = Eigen::Quaterniond::Identity();
    }

    Eigen::Quaterniond qra = 2 * dr * q.r.conjugate();
    Eigen::Quaterniond qrb = 2 * ddr * q.r.conjugate();
    Eigen::Quaterniond qrc = 2 * dr * dr.conjugate();
    Eigen::Quaterniond qrcb = qrb + qrc;
    res.push_back(qra.vec());
    res.push_back(qrcb.vec());

//    qra = 2 * (dd * q.r.conjugate() + q.d * dr.conjugate());
//    qrcb = 2 * (ddd * q.r.conjugate() + 2 * dd * dr.conjugate() + q.d * ddr.conjugate());
//    res.push_back(qra.vec());
//    res.push_back(qrcb.vec());
    res.push_back(v.vec());
    res.push_back(dv.vec());

    return res;
}



}
