/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef DMPINTERFACE_H
#define DMPINTERFACE_H

#include <dmp/representation/trajectory.h>
#include <dmp/representation/basicode.h>
#include <dmp/functionapproximation/functionapproximation.h>
#include <dmp/functionapproximation/locallyweightedlearning.h>
#include <dmp/functionapproximation/radialbasisfunctioninterpolator.h>
#include <dmp/representation/canonicalsystem.h>
#include <dmp/costfunction/costfcn.h>

#include <boost/serialization/shared_ptr.hpp>
#include <boost/variant.hpp>
#include <Eigen/Core>
#include <Eigen/Geometry>
#include <Eigen/StdVector>

#include <random>

#include <boost/tokenizer.hpp>
#include <boost/unordered_map.hpp>

#include <sys/time.h>

namespace DMP
{

// regular factors used in DMP
const std::string SpringFactor = "SpringFactor";
const std::string DampingFactor = "DampingFactor";
const std::string TemporalFactor = "TemporalFactor";
const std::string Goal = "Goal";
const std::string StartPosition = "StartPosition";
const std::string Amplitude = "Amplitude";


typedef Vec<DVec> DVec2d;

class DMPInterface : public ODE
{
public:
    typedef boost::variant<std::string, double, DMP::DVec, Eigen::Quaternionf> paraType;

    DMPInterface() :
        automaticScaling(false),
        __showPlots(false),
        __baseFunctionApproximator(new LWR<GaussKernel>(50))
    {

    }

    virtual void setViaPoint(double u, const DVec& point)
    {
        throw std::logic_error{"Not yet implemented!"};
    }


    virtual void save(const std::string& filename)
    {
        throw std::logic_error{"Not yet implemented!"};
    }

    virtual void load(const std::string& filename)
    {
        throw std::logic_error{"Not yet implemented!"};
    }

    virtual void prepareExecution(const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor)
    {
        throw std::logic_error{"Not yet implemented!"};
    }

    virtual Vec<DMPState> calculateDirectlyVelocity(const Vec<DMPState> &currentState, double canVal, double deltaT, DVec& targetState)
    {
        throw std::logic_error{"Not yet implemented!"};
    }

    virtual SampledTrajectoryV2 calculateTrajectory(const DVec &timestamps, const DVec &goal,
                                                    const Vec<DMPState> &initialStates,
                                                    const DVec &initialCanonicalValues, double temporalFactor)
    {
        throw std::logic_error{"Not yet implemented!"};
    }


    virtual  void learnFromTrajectory(const SampledTrajectoryV2& trainingTrajectory)
    {
        learnFromTrajectories(trainingTrajectory);
    }
    /**
         * @brief learnFromTrajectories is the same as learnFromTrajectory(), but
         * takes a vector of trajectories. The behaviour of the learning depends
         * on the actual dmp-implementation (e.g. average of all trajectories may
         * be learned)
         * @param trainingTrajectories vector of characteristic trajectories for the DMP
         *
         * @see learnFromTrajectory()
         */
    virtual  void learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories) = 0;
    virtual  DVec2d calculateTrajectoryPointBase(
            double /*t*/,
            const DVec& /*goal*/,
            double /*tCurrent*/,
            const DVec2d& /*currentStates*/,
            DVec& /*currentCanonicalValues*/,
            double /*temporalFactor*/)
    {
        throw std::logic_error{"Not yet implemented!"};
    }

    /**
         * @brief convenience overload for calculateTrajectory that reads the initial canonical system value from the canonical system
         * @param timestamps The timestamps for which the trajectory points are calculated.
         * @param goal The goal state of the DMP
         * @param initialStates The initial state at the beginning of the DMP.
         * @param temporalFactor The temporal factor for the duration of the DMP. Below 1 means faster than original trajectory.
         * @return new trajectory
         */
    SampledTrajectoryV2 calculateTrajectory(const DVec& timestamps, const DVec& goal, const DVec2d &initialStates, double temporalFactor)
    {
        return calculateTrajectory(timestamps, goal, initialStates, canonicalSystem->getInitValues(), temporalFactor);
    }

    /**
         * @brief Calculates and return the full trajectory based on trained DMP and given configuration.
         * @param timestamps The timestamps for which the trajectory points are calculated.
         * @param goal The goal state of the DMP
         * @param initialStates The initial state at the beginning of the DMP.
         * @param temporalFactor The temporal factor for the duration of the DMP. Below 1 means faster than original trajectory.
         * @return new trajectory
         */
    virtual  SampledTrajectoryV2 calculateTrajectory(const DVec& timestamps, const DVec& goal, const DVec2d &initialStates, const DVec& initialCanonicalValues, double temporalFactor)
    {

        DVec::const_iterator it = timestamps.begin();
        DVec2d previousState = initialStates;

        std::map<double, DVec > resultingSystemStatesMap;

        DVec canonicalValues = initialCanonicalValues;
        double prevTimestamp = *it;

        for (; it != timestamps.end(); it++)
        {
            previousState = calculateTrajectoryPointBase(*it, goal, prevTimestamp, previousState, canonicalValues, temporalFactor);
            prevTimestamp = *it;
            DVec positions;

            for (unsigned int d = 0; d < previousState.size(); d++)
            {
                positions.push_back(previousState.at(d)[0]);
            }

            resultingSystemStatesMap[prevTimestamp] = positions;
        }

        return SampledTrajectoryV2(resultingSystemStatesMap);
    }

    /**
         * @brief setFunctionApproximator sets a new type of function
         * approximator. All old instances will be deleted and recreated
         * with this type on request. Therefore a retraining is needed!
         * @param newBaseInstance instance of a function approximator,
         * from which all other instances will be cloned.
         */
    void setFunctionApproximator(FunctionApproximationInterfacePtr newBaseInstance)
    {
        __baseFunctionApproximator = newBaseInstance;
        __functionApproximators.clear();
    }

    void setFunctionApproximator(size_t trajDim, FunctionApproximationInterfacePtr newInstance)
    {
        if (trajDim > __functionApproximators.size()
                || !__functionApproximators.at(trajDim))
        {
            throw Exception("OutOfBounds: ") << trajDim << " size: " << __functionApproximators.size();
        }

        if (trajDim == __functionApproximators.size())
        {
            __functionApproximators.push_back(newInstance);
        }

        __functionApproximators.at(trajDim) = newInstance;
    }

    void setShowPlots(bool showPlots)
    {
        __showPlots = showPlots;
    }

    void setodeDim(int dim)
    {
        __trajDim = dim;
    }

    bool getShowPlots()
    {
        return __showPlots;
    }

    int getSizeOfFunctionApproximator(){
        return __functionApproximators.size();
    }

    virtual double _getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const = 0;


    virtual void setStartState(const DVec2d &startpos)
    {
        startState = startpos;
    }

    virtual FunctionApproximationInterfaceCPtr getFunctionApproximator(unsigned int trajDim) const
    {
        if (trajDim >= __functionApproximators.size()
                || !__functionApproximators.at(trajDim))
        {
            return FunctionApproximationInterfaceCPtr();
        }

        return __functionApproximators.at(trajDim);
    }

    virtual FunctionApproximationInterfacePtr getFunctionApproximatorPtr(unsigned int trajDim)
    {
        if (trajDim >= __functionApproximators.size()
                || !__functionApproximators.at(trajDim))
        {
            return FunctionApproximationInterfacePtr();
        }

        return __functionApproximators.at(trajDim);
    }

    virtual void setStartPositions(const DVec& startPositions) = 0;

    virtual DVec getStartPositions(){return DVec(0);}
    virtual void setGoals(const DVec& goals) = 0;
    virtual DVec getGoals(){return DVec(0);}
    virtual void setConfiguration(string ,const paraType&){}
    virtual void showConfiguration(string){}
    virtual double evaluateReproduction(double initialCanonicalValue, double& maxErrorOut, bool showPlot = true){return 0;}
    virtual void setAmpl(const unsigned int i, const double ampl){}
    virtual double getAmpl(const unsigned int i) const{return 0;}
    virtual string getDMPType() {return "UNKNOWN"; }
    virtual string getName() {return "UNKNOWN";}
    virtual const SampledTrajectoryV2& getTrainingTraj(unsigned int trajSampleIndex) const = 0;

    DVec2d getWeights() {
        DVec2d res;
        for(size_t i = 0; i < __functionApproximators.size(); i++)
        {
            FunctionApproximationInterfacePtr funcApprox = __functionApproximators.at(i);
            DVec cur(funcApprox->getWeights());
            res.push_back(cur);
        }
        return res;
    }

    std::vector<std::vector<double> > getWeightsVec2d() {
        std::vector<std::vector<double> > res;
        for(size_t i = 0; i < __functionApproximators.size(); i++)
        {
            FunctionApproximationInterfacePtr funcApprox = __functionApproximators.at(i);
            res.push_back(funcApprox->getWeights());
        }
        return res;
    }

    void saveWeightsToFile(const std::string& fileName)
    {
        DVec2d weights = getWeights();

        std::ofstream ofs(fileName);
        for(size_t i = 0; i < weights.size(); ++i)
        {
            for(size_t j = 0; j < weights.at(i).size(); ++j)
            {
                ofs << weights.at(i).at(j);
                if(j == weights.at(i).size() -1 )
                {
                    ofs << "\n";
                }
                else
                {
                    ofs << ",";
                }
            }
        }

        ofs.close();
    }


    void setWeights(const std::vector<std::vector<double> > & weights)
    {
        for(size_t i = 0; i < weights.size(); i++)
        {
            setWeights(i, weights[i]);
        }
    }

    void setWeights(const DVec2d& weights)
    {
        for(size_t i = 0; i < weights.size(); i++)
        {
            setWeights(i, weights[i]);
        }
    }

    void setKernels(int dim, double xmin=0, double xmax=1)
    {
        for(size_t i = 0; i < (size_t)dim; ++i)
        {
            setupKernels(i, xmin, xmax);
        }
    }

    virtual void setupKernels(int i, double xmin=0, double xmax=1)
    {
        FunctionApproximationInterfacePtr funcApprox = _getFunctionApproximator(i);
        funcApprox->setupKernels(xmin, xmax);
    }

    virtual void setWeights(int i, const std::vector<double>& weights)
    {
        FunctionApproximationInterfacePtr funcApprox = _getFunctionApproximator(i);
        funcApprox->setWeights(weights);
    }

    virtual void setTimeStep(double ts)
    {
        this->setEpsabs(ts);
        canonicalSystem->setEpsabs(ts);
    }

    virtual void setTemporalFactor(const double& tau)
    {
        canonicalSystem->setTau(tau);
    }
    virtual double getTemporalFactor() const
    {
        return canonicalSystem->getTau();
    }

    virtual double getDampingFactor() const { return 0; }
    virtual double getSpringFactor() const { return 0; }


    virtual void setCanonicalSystem(int, CanonicalSystemPtr) {}
    size_t getKernelSize()
    {
        return __baseFunctionApproximator->getKernelSize();
    }

    int getDMPDim(){return __trajDim;}


    void setAutomaticScaling(bool isAutomaticScaling)
    {
        automaticScaling = isAutomaticScaling;
    }

protected:

    bool automaticScaling;
    /**
         * @brief _getPerturbationForce needs to be implemented and should
         * return the perturbation force value at the given canonical state.
         * @param trajDim Dimension (e.g. joint5) for which the perturbation
         * force should be calculated.
         * @param canonicalStates State of the canonical system for which theDVec2d
         * perturbation force should be calculated.
         * @return returns perturbation force for given canonical state.
         */

    virtual void _trainFunctionApproximator(unsigned int trajDim, const DoubleMap& perturbationForceSamples)
    {
        FunctionApproximationInterfacePtr funcApprox = _getFunctionApproximator(trajDim);
        if (!funcApprox)
        {
            throw Exception("function approximator is NULL");
        }

        DoubleMap::const_iterator it = perturbationForceSamples.begin();

        for (; it != perturbationForceSamples.end(); it++)
        {
            funcApprox->learn(DVec(it->first), DVec(it->second));
        }
    }

    /**
         * @brief _getFunctionApproximator returns an instance to a
         * function approximator for a given trajectory dimensions.
         * Each dimension has it's own instance.
         * @param trajDim dimension of the trajectory (e.g. joint5), that this
         * FunctionApproximator should represent.
         * @return shared pointer to the FunctionApproximator
         */
    virtual FunctionApproximationInterfacePtr _getFunctionApproximator(unsigned int trajDim) const
    {
        if (trajDim >= __functionApproximators.size()
                || !__functionApproximators.at(trajDim))
        {
            Vec<FunctionApproximationInterfacePtr>::size_type newSize = trajDim + 1;
            newSize = std::max(newSize, __functionApproximators.size());
            __functionApproximators.resize(newSize);
            __functionApproximators.at(trajDim) = __baseFunctionApproximator->clone();
        }

        return __functionApproximators.at(trajDim);
    }

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int file_version)
    {
        ar.template register_type<LinearDecayCanonicalSystem>();
        ar.template register_type<LWR<GaussKernel>>();

        ar& boost::serialization::make_nvp("__trajDim",__trajDim);
        ar& boost::serialization::make_nvp("dmpName",dmpName);
        ar& boost::serialization::make_nvp("startState",startState);
        ar& boost::serialization::make_nvp("canonicalSystem",canonicalSystem);
        ar& boost::serialization::make_nvp("__functionApproximators",__functionApproximators);
        ar& boost::serialization::make_nvp("__baseFunctionApproximator",__baseFunctionApproximator);
        ar& boost::serialization::make_nvp("__showPlots",__showPlots);
        ar& boost::serialization::make_nvp("automaticScaling",automaticScaling);

        auto& base = boost::serialization::base_object<ODE>(*this);
        ar& boost::serialization::make_nvp("DMPInterface_base",base);
    }

    size_t __trajDim;
    std::string dmpName;

    DVec2d startState;
    CanonicalSystemPtr canonicalSystem;

    mutable Vec<FunctionApproximationInterfacePtr> __functionApproximators;

private:
    bool __showPlots;
    FunctionApproximationInterfacePtr __baseFunctionApproximator;
};

typedef boost::shared_ptr<DMPInterface> DMPInterfacePtr;

/**
     *
     */
template <typename SystemStatusType>
class DMPInterfaceTemplate : public DMPInterface
{
public:
    typedef SystemStatusType SystemType;


    DMPInterfaceTemplate()
    {}

    /**
         * @brief calculateTrajectoryPoint solves the DMP for one specific timepoint.
         * To do so, it needs a initial state vector at a specific time. Calculation
         * takes longer if the difference between t and tInit is big.
         * @param t Point in time for which the DMP should be solved
         * @param goal Goal positions for all dimensions. If the in training
         * trajectory is correctly approximated, the DMP will converge to this
         * point for t -> infinite.
         * @param tCurrent Point in time for which the initialStates are valid.
         * @param currentStates Current state of the DMP at timestamp tCurrent for
         * all dimensions.
         * @param _M_range_checkcurrentCanonicalValues Canonical value(s) at timestamp tCurrent.
         * @param temporalFactor This factor determines the duration of the
         * trajectory. The duration scales linear with this factor.
         * @return returns a vector of DMP states for all dimensions for
         * timestamp t.
         */
    virtual  Vec<SystemStatusType> calculateTrajectoryPoint(double t, const DVec& goal, double tCurrent, const Vec<SystemStatusType>& currentStates, DVec& currentCanonicalValues, double temporalFactor) = 0;


    DVec2d calculateTrajectoryPointBase(double t, const DVec& goal, double tCurrent, const DVec2d& currentStates, DVec& currentCanonicalValues, double temporalFactor) override
    {
        Vec<SystemStatusType> currentStateVec;
        for(const DVec& stateVec : currentStates)
        {
            currentStateVec.push_back(SystemStatusType(stateVec));
        }
        Vec<SystemStatusType> newStates = calculateTrajectoryPoint(t, goal, tCurrent, currentStateVec, currentCanonicalValues, temporalFactor);
        DVec2d result;
        for(const SystemStatusType& state : newStates)
        {
            result.push_back(state.getValues());
        }
        return result;
    }
    virtual SampledTrajectoryV2 calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<SystemStatusType> &initialStates, double temporalFactor)
    {
        return calculateTrajectory(timestamps, goal, initialStates, canonicalSystem->getInitValues(), temporalFactor);
    }
    virtual  SampledTrajectoryV2 calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<SystemStatusType> &initialStates, const DVec& initialCanonicalValues, double temporalFactor);

private:
    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int file_version)
    {
        auto& base = boost::serialization::base_object<DMPInterface>(*this);
        ar& boost::serialization::make_nvp("DMPInterfaceTemplate_base",base);
    }

};

///////////////////////////////////////////////////////////////////////////
// Implementation
///////////////////////////////////////////////////////////////////////////




template <typename SystemStatusType>
SampledTrajectoryV2 DMPInterfaceTemplate<SystemStatusType>::calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<SystemStatusType> &initialStates, const DVec& initialCanonicalValues, double temporalFactor)
{
    DVec::const_iterator it = timestamps.begin();
    Vec<SystemStatusType> previousState = initialStates;
    std::map<double, DVec > resultingSystemStatesMap;

    DVec canonicalValues = initialCanonicalValues;
    double prevTimestamp = *it;

    for (; it != timestamps.end(); it++)
    {
        previousState = calculateTrajectoryPoint(*it, goal, prevTimestamp, previousState, canonicalValues, temporalFactor);
      
        DVec positions;

        for (unsigned int d = 0; d < previousState.size(); d++)
        {
            positions.push_back(previousState.at(d).pos);
        }

        resultingSystemStatesMap[prevTimestamp] = positions;
        prevTimestamp = *it;

    }

    return SampledTrajectoryV2(resultingSystemStatesMap);

}







}



#endif // DMPINTERFACE_H
