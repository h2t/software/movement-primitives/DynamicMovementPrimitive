#ifndef DMP_DUALQUATERNIONDMP_H
#define DMP_DUALQUATERNIONDMP_H

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/math/dualnumber.h>

namespace DMP {

class DualQuaternionDMP : public BasicDMP
{
#define DIM_STATE 3
#define DQ_STATE 8
public:

    DualQuaternionDMP(int kernelSize = 50, double D = 20, double tau = 1.0);
    // ODE interface

    void learnFromTrajectories(const Vec<SampledTrajectoryV2> &trainingTrajectories) override;


    void setGoals(const DVec& goals) override
    {
        this->goals = goals;
    }
    void setStartPositions(const DVec& startPositions) override
    {
        this->startPositions = startPositions;
    }
    void setSpringConstant(const double& K)
    {
        this->K = K;
    }
    void setDampingConstant(const double& D)
    {
        this->D = D;
    }
    void setDampingAndSpringConstant(const double& D)
    {
        this->D = D;
        K = (D / 2) * (D / 2);
    }
//    void setTemporalFactor(const double& tau)
//    {
//        canonicalSystem->setTau(tau);
//    }
//    double getTemporalFactor() const
//    {
//        return canonicalSystem->getTau();
//    }

    SampledTrajectoryV2 getTrainingQTraj() {return qTraj;}
     SampledTrajectoryV2 calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor ) override;

    virtual void integrate(double t, double tInit, const DVec& init, DVec& out, bool project = false);
    Vec<DMPState> calculateTrajectoryPoint(double t, const DVec& goal, double tCurrent, const Vec<DMPState> &currentStates, DVec& canonicalValues, double temporalFactor ) override;

    Vec<Eigen::Vector3d> calcVelocityAndAcceleration(DualQuaterniond q0, DualQuaterniond q, DualQuaterniond q1, double dt);

    std::map<double, DVec> vels_tr;
protected:

    SampledTrajectoryV2 qTraj;
    SampledTrajectoryV2 aTraj;
    Vec<DoubleMap> calcVecPerturbationForceSamples(unsigned int trajDim, const DVec &canonicalValues, const SampledTrajectoryV2 &exampleTraj);

    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int file_version)
    {
        DMPInterface& base = boost::serialization::base_object<DMPInterface>(*this);
        ar& boost::serialization::make_nvp("base",base);

    }


};
}

#endif
