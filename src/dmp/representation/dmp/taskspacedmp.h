#ifndef TASKSPACEDMP_H
#define TASKSPACEDMP_H

#include <dmp/representation/basicode.h>
#include <dmp/functionapproximation/locallyweightedlearning.h>
#include <dmp/functionapproximation/functionapproximation.h>
#include <dmp/representation/systemstatus.h>

#include "dmpinterface.h"

#include <dmp/functionapproximation/lwprwrapper.h>

#include <map>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>
#include <Eigen/Dense>


namespace DMP
{

    #define TASKSPACEDMP_DIM 7
    #define TASKSPACEDMP_POSSTARTDIM 0
    #define TASKSPACEDMP_ORISTARTDIM 3

    class TaskSpaceDMP : public DMPInterface
    {



    public:

        TaskSpaceDMP(int kernelSize = 100, double D = 20, double tau = 1.0, double eps = 1e-5);

        std::string getDMPType() override{return "TaskSpaceDMP";}


        const SampledTrajectoryV2& getTrainingTraj(unsigned int trajSampleIndex) const override
        {
            return trainingTraj[trajSampleIndex];
        }

        void setGoals(const DVec& goals) override
        {
            this->goals = goals;
        }

        DVec getGoals() override
        {
            return goals;
        }

        void setStartPositions(const DVec& startPositions) override
        {
            this->startPositions = startPositions;
        }
        void setSpringConstant(const double& K)
        {
            this->K = K;
        }
        void setDampingConstant(const double& D)
        {
            this->D = D;
        }
        void setDampingAndSpringConstant(const double& D)
        {
            this->D = D;
            K = (D / 2) * (D / 2);
        }

        double getDampingFactor() const override { return D; }
        double getSpringFactor() const override { return K; }

        DVec getStartPositions() override{return startPositions;}

        void setAmpl(const unsigned int i, const double ampl) override
        {
            if(i > amplitudes.size() - 1)
            {
                std::cout << "Warning: out of the range of amplitudes" << std::endl;
                amplitudes.push_back(ampl);
                return;
            }
            amplitudes[i] = ampl;
        }

        double getAmpl(const unsigned int i) const override
        {
            return amplitudes[i];
        }


        Vec<SampledTrajectoryV2 > trainingTraj;

        double _getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const override
        {
            return (*_getFunctionApproximator(trajDim))(canonicalStates.at(0)).at(0);
        }

        void setConfiguration(string paraID,const paraType& para) override;
        void showConfiguration(string paraID) override;

        void learnFromTrajectories(const Vec<SampledTrajectoryV2> &trainingTrajectories) override;

        DVec2d calculateTrajectoryPointBase(double t, const DVec &goal, double tCurrent, const DVec2d &currentStates, DVec &currentCanonicalValues, double temporalFactor) override;

        void setTimeStep(double ts) override
        {
            canonicalSystem->setEpsabs(ts);
        }


        static DMP::Vec<Eigen::Vector3d> calcAngularVelocityAndAcceleration(Eigen::Quaterniond q0, Eigen::Quaterniond q, Eigen::Quaterniond q1, double dt);


    protected:
        // inherited from ODE
        void flow(double t, const DVec& x, DVec& out) override{}

        Vec<DoubleMap > calcPerturbationForceSamples(const DVec& canonicalValues,  const SampledTrajectoryV2& exampleTraj) const;


        //        unsigned int dimValuesOfInterest() const{ return 2;}
        //        void projectStateToValuesOfInterest(const double *x, DVec &projection) const;

        //    private:

        void _trainFunctionApproximator(unsigned int trajDimIndex, const DoubleMap& perturbationForceSamples) override;

        /**
         * @brief goal specifies the desired end position of the trajectory.
         *
         * Must differ from \ref startPosition to have a trajectory.
         */
        DVec goals;

        /**
         * @brief startPosition specifies the start position of the trajectory.
         * @see startVelocity
         */
        DVec startPositions;
        /**
         * @brief K is the spring constant of the damped-spring-mass-system.
         */
        double K;

        /**
         * @brief D is damping of the damped-spring-mass-system.
         */
        double D;


        /**
         * @brief amplitudes are a vector of all amplitudes for different dimension of trajectories
         */
        DVec amplitudes;


        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {

            ar& boost::serialization::make_nvp("K",K);
            ar& boost::serialization::make_nvp("D",D);
            ar& boost::serialization::make_nvp("goals",goals);
            ar& boost::serialization::make_nvp("startPositions", startPositions);

            DMPInterface& base = boost::serialization::base_object<DMPInterface>(*this);
            ar& boost::serialization::make_nvp("base",base);

        }

    };




}



#endif
