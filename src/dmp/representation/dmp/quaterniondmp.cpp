#include "quaterniondmp.h"
#include <Eigen/Dense>

namespace DMP {



QuaternionDMP::QuaternionDMP(int kernelSize, double d, double tau):
    BasicDMP(kernelSize)
{
    canonicalSystem.reset(new CanonicalSystem(tau));
    K = (d / 2) * (d / 2);
    D = d;
    ODE::setEpsabs(1e-5);
}

Vec<DoubleMap> QuaternionDMP::calcVecPerturbationForceSamples(unsigned int trajDim, const DVec &canonicalValues, const SampledTrajectoryV2 &exampleTraj) //const
{
    angular_vels_tr.clear();

    Vec<DoubleMap> result;

    if (canonicalValues.size() != exampleTraj.size())
    {
        throw Exception("The amount of the canonicalValues do not match the exampleTrajectory");
    }

//    SampledTrajectoryV2 qtraj = exampleTraj.traj2qtraj();
    SampledTrajectoryV2 qtraj = exampleTraj;

    DVec::const_iterator itCanon = canonicalValues.begin();

    double tau = canonicalSystem->getTau();

    unsigned int qDim = trajDim * Q_STATE;

    double gw = qtraj.rbegin()->getPosition(qDim);
    double gx = qtraj.rbegin()->getPosition(qDim + 1);
    double gy = qtraj.rbegin()->getPosition(qDim + 2);
    double gz = qtraj.rbegin()->getPosition(qDim + 3);

    Eigen::Quaterniond g0(gw, gx, gy, gz);

    SampledTrajectoryV2::ordered_view::const_iterator itQtraj = qtraj.begin();
    DoubleMap  fw, fx, fy, fz;

    double qw = itQtraj->getPosition(qDim);
    double qx = itQtraj->getPosition(qDim + 1);
    double qy = itQtraj->getPosition(qDim + 2);
    double qz = itQtraj->getPosition(qDim + 3);

    Eigen::Quaterniond q(qw, qx, qy, qz);
    Eigen::Quaterniond q0(qw,qx,qy,qz);
    Eigen::Quaterniond q1(qw,qx,qy,qz);
    Eigen::Vector3d w, dw;

    // generate first angular velocity
    SampledTrajectoryV2::ordered_view::const_iterator itQtraj1 = qtraj.begin();
    itQtraj1++;
    SampledTrajectoryV2::ordered_view::const_iterator itQtrajend = qtraj.end();
    itQtrajend--;

    q1.w() = itQtraj1->getPosition(qDim);
    q1.x() = itQtraj1->getPosition(qDim + 1);
    q1.y() = itQtraj1->getPosition(qDim + 2);
    q1.z() = itQtraj1->getPosition(qDim + 3);

    double dt0 = itQtraj1->getTimestamp() - itQtraj->getTimestamp();

//    q0.w() = q.w() - (q1.w() - q.w());
//    q0.x() = q.x() - (q1.x() - q.x());
//    q0.y() = q.y() - (q1.y() - q.y());
//    q0.z() = q.z() - (q1.z() - q.z());

    Vec<Eigen::Vector3d> s0 = calcAngularVelocityAndAcceleration(q0, q, q1, dt0);
    w = s0[0];

    double preTime = itQtraj->getTimestamp();
    ++itQtraj;
    ++itQtraj1;

    double dt = itQtraj1->getTimestamp() - itQtraj->getTimestamp();
    q0 = q;
    q = q1;
    q1.w() = itQtraj1->getPosition(qDim);
    q1.x() = itQtraj1->getPosition(qDim + 1);
    q1.y() = itQtraj1->getPosition(qDim + 2);
    q1.z() = itQtraj1->getPosition(qDim + 3);

    Vec<Eigen::Vector3d> s1 = calcAngularVelocityAndAcceleration(q0, q, q1, dt);
    dw = s1[1];

    w = w - dw * dt;

    Eigen::Vector3d yita = tau * w;
    Eigen::Vector3d ayita = tau * dw;
    Eigen::Vector3d fo = tau * ayita - K * 2 * qlog(g0 * q0.conjugate()) + D * yita;

    fw[*itCanon] = 0;
    fx[*itCanon] = 0 ;
    fy[*itCanon] = 0;
    fz[*itCanon] = 0 ;
    ++itCanon;

    for (; itQtraj != qtraj.end(); ++itCanon, ++itQtraj, ++itQtraj1)
    {
        q.w() = itQtraj->getPosition(qDim);
        q.x() = itQtraj->getPosition(qDim + 1);
        q.y() = itQtraj->getPosition(qDim + 2);
        q.z() = itQtraj->getPosition(qDim + 3);

        dt = itQtraj->getTimestamp() - preTime;

        if(itQtraj == itQtrajend){
            w = w + dw * dt;
        }else{
            q1.w() = itQtraj1->getPosition(qDim);
            q1.x() = itQtraj1->getPosition(qDim + 1);
            q1.y() = itQtraj1->getPosition(qDim + 2);
            q1.z() = itQtraj1->getPosition(qDim + 3);

            Vec<Eigen::Vector3d> res = calcAngularVelocityAndAcceleration(q0, q, q1, dt);
            w = res[0];
            dw = res[1];
        }

        DVec vw;
        vw << w(0), w(1), w(2);
        angular_vels_tr[*itCanon] = vw;
        yita = tau * w;
        ayita = tau * dw;

        fo = tau * ayita - K * 2 * qlog(g0 * q.conjugate()) + D * yita;

//        double scaleFactor = distance(g0, q);
//        double epsilon = 0.01;
//        fo = fo / (scaleFactor + epsilon);
//        std::cout << "scaleFactor: " << scaleFactor << std::endl;

        fw[*itCanon] = 0;
        fx[*itCanon] = fo(0) ;
        fy[*itCanon] = fo(1) ;
        fz[*itCanon] = fo(2) ;

        q0 = q;
        preTime = itQtraj->getTimestamp();
    }

    result.push_back(fw);
    result.push_back(fx);
    result.push_back(fy);
    result.push_back(fz);
    return result;
}


void QuaternionDMP::learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories)
{
    if (trainingTrajectories.size() > 1)
    {
        throw Exception() << "this DMP can only be trained with one trajectory (but with several dimensions)";
    }

    if (trainingTrajectories.size() == 0)
    {
        return;
    }

    trainingTraj = trainingTrajectories;
    SampledTrajectoryV2& trainingTrajectory = trainingTraj[0];
    __trajDim = trainingTrajectory.dim();

    // normalize timestamps so that they start at 0 and end at 1
    trainingTrajectory = SampledTrajectoryV2::normalizeTimestamps(trainingTrajectory, 0.0, 1.0);
    DVec normTimestamps = trainingTrajectory.getTimestamps();


    Vec<DVec> uValuesOrig;
    canonicalSystem->integrate(normTimestamps, canonicalSystem->getInitValues(), uValuesOrig);
    DVec uValues;
    DoubleMap uValuesPlotMap;

    for (unsigned int i = 0; i < uValuesOrig.size(); ++i)
    {
        uValues.push_back(uValuesOrig[i][0]);
        uValuesPlotMap[normTimestamps.at(i)] = uValuesOrig[i][0];
    }

    unsigned int trajDim = trainingTrajectory.dim() / DIM_STATE;

    for (unsigned int dim = 0; dim < trajDim; dim++)
    {
        Vec<DoubleMap> perturbationForceSamples = calcVecPerturbationForceSamples(dim, uValues, trainingTrajectory);

        for(size_t i = 0; i < perturbationForceSamples.size(); i++){
            _trainFunctionApproximator(dim * perturbationForceSamples.size() + i, perturbationForceSamples[i]);
        }

    }
}


SampledTrajectoryV2 QuaternionDMP::calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor)
{
//    aTraj = getTrainingTraj(0);
//    qTraj = aTraj.traj2qtraj();
//    trainingTraj.at(0) = qTraj;
    angular_vels_in.clear();
    BasicDMP::setStartPositions(initialStates);

    return DMPInterfaceTemplate<DMPState>::calculateTrajectory(timestamps, goal, initialStates, initialCanonicalValues, temporalFactor);
}

void QuaternionDMP::integrate(double t, double tInit, const DVec &init, DVec &out, bool project)
{
    double stepSize = epsabs();

    if (t < tInit)
    {
        stepSize *= -1;
    }

    double tCur = tInit;

    DVec x = init;


    out = x;
    while(tCur < t){
        double u = x.at(0);

        DVec canonicalState(1);
        canonicalState[0] = u;
        canonicalSystem->flow(tCur, canonicalState, out);

        out[0] = u + stepSize * out[0];

        double tau = canonicalSystem->getTau();

        unsigned int dimensions = __trajDim;

        if (dimensions > goals.size())
        {
            throw Exception("There are less trajectory goals than dimensions");
        }

        if (dimensions > startPositions.size())
        {
            throw Exception("There are less trajectory start positions than dimensions");
        }

        unsigned int offset = canonicalState.size();
        unsigned int curDim = 0;

        while (curDim < dimensions)
        {
            double fx = _getPerturbationForce(curDim+1, u);
            double fy = _getPerturbationForce(curDim+2, u);
            double fz = _getPerturbationForce(curDim+3, u);
            Eigen::Vector3d fo;
            fo << fx, fy, fz;

            //bool scaling = true;
            //double scaleFactor = 1.0;
            //std::map<unsigned int, bool>::iterator it = scalingActivated.find(curDim);

            //if (it != scalingActivated.end() && !it->second)
            //{
                //scaling = false;
            //}

            //if (scaling)
            //{
                //TODO add scale factoring
                //scaleFactor = 1;
            //}

            unsigned int i = curDim * 2 + offset;
            double qw = x[i];
            double qx = x[i + 2];
            double dwx = x[i + 3];
            double qy = x[i + 4];
            double dwy = x[i + 5];
            double qz = x[i + 6];
            double dwz = x[i + 7];

            Eigen::Quaterniond q(qw, qx, qy, qz);
//            q.normalize();

            double gw = goals[curDim];
            double gx = goals[curDim+1];
            double gy = goals[curDim+2];
            double gz = goals[curDim+3];

            Eigen::Quaterniond g0(gw, gx, gy, gz);

            Eigen::Vector3d w;
            w << dwx, dwy, dwz;
            DVec vw;
            vw << dwx, dwy, dwz;
            angular_vels_in[u] = vw;

            Eigen::Vector3d yita = tau * w;

            // calculate angular acceleration
            Eigen::Vector3d dw = 1.0 / (tau * tau) * ( K * 2 * qlog(g0 * q.conjugate()) - D * yita + fo);

//            std::cout << "fo: " << fo << '\t' << "scaleFactor: " << scaleFactor << std::endl;
            // calculate change of quaternion
            Eigen::Quaterniond nq = qexp(stepSize * w / 2) * q;
//            nq.normalize();

            out[i++] = nq.w();
            out[i++] = 0;
            out[i++] = nq.x();
            out[i++] = w(0) + stepSize * dw(0);
            out[i++] = nq.y();
            out[i++] = w(1) + stepSize * dw(1);
            out[i++] = nq.z();
            out[i++] = w(2) + stepSize * dw(2);
            curDim += Q_STATE;
        }

        x = out;
        tCur += stepSize;
    }

}

Vec<DMPState> QuaternionDMP::calculateTrajectoryPoint(double t, const DVec &goal, double tCurrent, const Vec<DMPState> &currentStates, DVec &canonicalValues, double temporalFactor)
{

    if (startPositions.size() == 0)
    {
        //        throw Exception("There is no start position set. If you use calculateTrajectoryPoint directly, you must set the start position before (setStartPositions()). ");
        std::cout << "No Start Positions set, assuming current state as start position" << std::endl;
        BasicDMP::setStartPositions(currentStates);
    }

    ODE::setDim(1 + __trajDim * 2);

    DVec initialODEConditions = canonicalValues;
    DVec initialStatesVec = SystemStatus::convertStatesToArray(currentStates);
    initialODEConditions.insert(initialODEConditions.end(), initialStatesVec.begin(), initialStatesVec.end());

    setTemporalFactor(temporalFactor);
    setGoals(goal);

    DVec rawResult;
    integrate(t, tCurrent, initialODEConditions, rawResult, false);

    canonicalValues.assign(rawResult.begin(), rawResult.begin() + 1);
    rawResult.erase(rawResult.begin());
    Vec<DMPState> result = SystemStatus::convertArrayToStates<DMPState>(rawResult);

    return result;

}

Vec<Eigen::Vector3d> QuaternionDMP::calcAngularVelocityAndAcceleration(Eigen::Quaterniond q0, Eigen::Quaterniond q, Eigen::Quaterniond q1, double dt)
{
    Vec<Eigen::Vector3d> res;
    Eigen::Quaterniond dq;
    Eigen::Quaterniond ddq;

    if(dt != 0){
        dq = (q1 - q) / dt;
        ddq = ((q1 - q) - (q - q0)) / (dt * dt);
    } else {
        dq = Eigen::Quaterniond(0,0,0,0);
        ddq = Eigen::Quaterniond(0,0,0,0);
    }

    Eigen::Quaterniond qa = 2 * dq * q.conjugate();
    Eigen::Quaterniond qb = 2 * ddq * q.conjugate();
    Eigen::Quaterniond qc = 2 * dq * dq.conjugate();
    Eigen::Quaterniond qcb = qb + qc;
    res.push_back(qa.vec());
    res.push_back(qcb.vec());

    return res;
}


double QuaternionDMP::evaluateReproduction(double initialCanonicalValue, double& maxErrorOut, bool showPlot)
{
    DVec timestamps;
    double meanError = 0.0;
    timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + getTrainingTraj(0).size()));

    // set initial states and final goals;
    Vec<DMPState> initialState;
    DVec goal;

    double qw = getTrainingTraj(0).begin()->getPosition(0);
    double qx = getTrainingTraj(0).begin()->getPosition(1);
    double qy = getTrainingTraj(0).begin()->getPosition(2);
    double qz = getTrainingTraj(0).begin()->getPosition(3);

    typename SampledTrajectoryV2::ordered_view::const_iterator itqtraj = getTrainingTraj(0).begin();
    itqtraj++;

    Eigen::Quaterniond startq(qw,qx,qy,qz);
    Eigen::Quaterniond nextq(itqtraj->getPosition(0),
                             itqtraj->getPosition(1),
                             itqtraj->getPosition(2),
                             itqtraj->getPosition(3));

    double dt = itqtraj->getTimestamp() - getTrainingTraj(0).begin()->getTimestamp();
    Eigen::Quaterniond dq = (nextq - startq) / dt;
    Eigen::Quaterniond wq = 2 * dq * nextq.conjugate();

    initialState.push_back(DMPState(qw, wq.w()));
    initialState.push_back(DMPState(qx, wq.x()));
    initialState.push_back(DMPState(qy, wq.y()));
    initialState.push_back(DMPState(qz, wq.z()));

    goal.push_back(getTrainingTraj(0).rbegin()->getPosition(0));
    goal.push_back(getTrainingTraj(0).rbegin()->getPosition(1));
    goal.push_back(getTrainingTraj(0).rbegin()->getPosition(2));
    goal.push_back(getTrainingTraj(0).rbegin()->getPosition(3));

    // Learning new trajectory
    SampledTrajectoryV2 newTraj = calculateTrajectory(
                timestamps,
                goal,
                initialState,
                DVec(initialCanonicalValue),
                1);
    std::cout << "start: ["
              << getTrainingTraj(0).begin()->getPosition(0) << " , "
              << getTrainingTraj(0).begin()->getPosition(1) << " , "
              << getTrainingTraj(0).begin()->getPosition(2) << " , "
              << getTrainingTraj(0).begin()->getPosition(3)
              << "] goal: ["
              << goal[0] << " , "
              << goal[1] << " , "
              << goal[2] << " , "
              << goal[3] << "] " << std::endl;

    DVec statesGenW = newTraj.getDimensionData(0, 0);
    DVec statesGenX = newTraj.getDimensionData(0, 0);
    DVec statesGenY = newTraj.getDimensionData(0, 0);
    DVec statesGenZ = newTraj.getDimensionData(0, 0);

    DVec statesOrigW = getTrainingTraj(0).getDimensionData(0, 0);
    DVec statesOrigX = getTrainingTraj(0).getDimensionData(0, 0);
    DVec statesOrigY = getTrainingTraj(0).getDimensionData(0, 0);
    DVec statesOrigZ = getTrainingTraj(0).getDimensionData(0, 0);


    DoubleMap errorMap;
    double minTrajValue = std::numeric_limits<double>::max();
    double maxTrajValue = std::numeric_limits<double>::min();
    maxErrorOut = 0.0;

    for (unsigned int i = 0; i < statesGenW.size() && i < statesOrigW.size(); ++i)
    {
        Eigen::Quaterniond quatGen(statesGenW[i], statesGenX[i], statesGenY[i], statesGenZ[i]);
        Eigen::Quaterniond quatOrig(statesOrigW[i], statesOrigX[i], statesOrigY[i], statesOrigZ[i]);
        Eigen::Quaterniond dquat = quatOrig * quatGen.conjugate();
        Eigen::AngleAxisd aad(dquat);

        double error = aad.angle();

        if (error > maxErrorOut)
        {
            maxErrorOut = error;
        }

        errorMap[timestamps[i]] = error;
        meanError += error;

    }

    if (showPlot)
    {
        Vec< SampledTrajectoryV2 > trajectories;

        trajectories.push_back(SampledTrajectoryV2::normalizeTimestamps(getTrainingTraj(0)));
        trajectories.push_back(newTraj);
        //        SampledTrajectory::plot(trajectories,"plots/trajectories_%d_dim%d.gp", true);
        plot(errorMap, "plots/traj_reproduction_error_%d.gp", true, 0);
        DVec timestamps = newTraj.getTimestamps();

        Vec<DVec> plots;
        plots.push_back(timestamps);
        plots.push_back(getTrainingTraj(0).getDimensionData(0, 0));
        plots.push_back(getTrainingTraj(0).getDimensionData(1, 0));
        plots.push_back(getTrainingTraj(0).getDimensionData(2, 0));
        plots.push_back(getTrainingTraj(0).getDimensionData(3, 0));
        plots.push_back(newTraj.getDimensionData(0, 0));
        plots.push_back(newTraj.getDimensionData(1, 0));
        plots.push_back(newTraj.getDimensionData(2, 0));
        plots.push_back(newTraj.getDimensionData(3, 0));

        plotTogether(plots, "plots/trajComparison_%d.gp");
//        plots.clear();
//        plots.push_back(timestamps);
//        plots.push_back(getTrainingTraj(0).getDimensionData(0, 1));
//        plots.push_back(newTraj.getDimensionData(0, 1));
//        plotTogether(plots, "plots/trajComparison_vel_%d.gp");
//        plots.clear();
//        plots.push_back(timestamps);
//        plots.push_back(getTrainingTraj(0).getDimensionData(0, 2));
//        plots.push_back(newTraj.getDimensionData(0, 2));
//        plotTogether(plots, "plots/trajComparison_acc_%d.gp",true);
    }

    meanError /= statesOrigW.size();
    return meanError;
}

}
