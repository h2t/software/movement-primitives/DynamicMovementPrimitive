#include "endveloforcefieldwithobjrepulsiondmp.h"

#include <limits>

using namespace DMP;

EndVeloForceFieldWithObjRepulsionDMP::EndVeloForceFieldWithObjRepulsionDMP(double tau) :
    EndVeloForceFieldDMP(tau)
{
    VirtualRobot::SceneObjectSetPtr obs(new VirtualRobot::SceneObjectSet);
    obstacles = obs;
}


void EndVeloForceFieldWithObjRepulsionDMP::flow(double t, const DMP::DVec& x, DMP::DVec& out)
{
    double u = x.at(0);

    DVec canonicalState(1);
    canonicalState[0] = u;
    canonicalSystem->flow(t, canonicalState, out);

    double tau = canonicalSystem->getTau();


    unsigned int dimensions = getTrainingTraj(0).dim();

    if (dimensions > goals.size())
    {
        throw Exception("There are less trajectory goals than dimensions");
    }

    if (dimensions > startPositions.size())
    {
        throw Exception("There are less trajectory start positions than dimensions");
    }


    unsigned int offset = canonicalState.size();
    unsigned int curDim = 0;

    while (curDim < dimensions)
    {
        double curGoal = _getPerturbationForce(curDim, u);
        unsigned int i = curDim * 2 + offset;
        const double pos =  x[i];
        const double vel = x[i + 1];
        const double goal = goals[curDim];
        const double start = startPositions[curDim];

        out.at(i++) = 1.0 / tau * (vel);
        checkValue(out.at(i-1));
        const double x0 = getTrainingTraj(0).begin()->getPosition(curDim);
        const double xT = getTrainingTraj(0).rbegin()->getPosition(curDim);
        double goalOffset = fabs(goal - start) - fabs(xT - x0);

        double scaleFactor;
        if(maxAbsDisplacement.at(curDim) != 0)
            scaleFactor = goalOffset / (maxAbsDisplacement.at(curDim));
        else
            scaleFactor = 0;
        curGoal *= 1 + (scaleFactor);

        out.at(i)   = 1.0 / tau * (K * (goal + curGoal - pos) - (D * vel));
        checkValue(out.at(i));
        curDim++;
    }

//    curDim = 0;
//    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet("TorsoBothArms");
//    while (curDim < dimensions) {
//        unsigned int i = curDim * 2 + offset;
//        rns->getNode(curDim)->setJointValue(out.at(i));
//        curDim++;
//    }
}

double
EndVeloForceFieldWithObjRepulsionDMP::_getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const
{

    if (objects.size() > 0 && robotNS) {
        VirtualRobot::CollisionCheckerPtr collisionChecker = VirtualRobot::CollisionChecker::getGlobalCollisionChecker();


//        cout <<" PertubationForce distance, DIM:" << trajDim << endl;
//        VirtualRobot::CollisionModelPtr colModel1 = objects.begin()->second->getCollisionModel();
//        VirtualRobot::CollisionModelPtr colModel2 = objects.rbegin()->second->getCollisionModel();

//        if (!colModel1 || !colModel2) {
//            VR_ERROR << "No collision Modell" << endl;
//        }

        Eigen::Vector3f surface1,surface2, forceDir;


        float dist = collisionChecker->calculateDistance(robotNS, obstacles, surface1,surface2);
        if (dist < 0.1) {
//            cout << "Distance smaller than 0.1" << endl;
            dist = 0.1;
        }

        forceDir = surface1 - surface2;
        forceDir.normalize();

//        string log;

//        std::ostringstream log;

//        log << "Timestep: " << canonicalStates.at(0) << " \n";
//        log << "Dimension: " << trajDim << "\n";
//        log << "Distance: " << dist << "\n";

//        cout << "dist: " << dist << endl;

//        cout << "forceDir         " << forceDir << endl;

        VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet("TorsoBothArms");
        Eigen::Matrix4f targetPose = rns->getNode(10)->getGlobalPose();

        // Version A

//        forceDir = 100 / dist * forceDir ;

        //Version B : Gauss Bell

//        double x = dist/200;
//        double sigma = 0.5;

//        double g = exp(-double((x)*(x))/(2*sigma*sigma)) / ( sigma * sqrt(2*3.14159265));

//        forceDir = 200 * g * forceDir;

//        Version C : static Force Field

        double u_static = 0.0;
        double force = 200.0;

        if (dist < 100.0) {
            u_static = force * ((1.0 / dist) - (1.0 / 100.0));
        }

        forceDir = u_static * forceDir;


//        cout << "g: " << g << endl;
//        cout << "forceDir faktor: "<< endl << forceDir << endl;

//        log << "Gauss g: " << g << "\n";
//        log << "ForceDir: " << forceDir << "\n";



        targetPose(0,3) += forceDir(0);
        targetPose(1,3) += forceDir(1);
        targetPose(2,3) += forceDir(2);
        VirtualRobot::DifferentialIKPtr j(new VirtualRobot::DifferentialIK(rns));
        j->setGoal(targetPose,rns->getNode(10) ,VirtualRobot::IKSolver::All);
        Eigen::VectorXf thetas = j->computeStep();
//        Eigen::VectorXf thetas = j->getError();
//        thetas.normalize();

//        log << "Thetas: " << thetas(trajDim) << "\n";
//        log << "Approx: " << (*_getFunctionApproximator(trajDim))(canonicalStates.at(0)).at(0) << "\n\n";

//        write_text_to_log_file(log.str());

        return (*_getFunctionApproximator(trajDim))(canonicalStates.at(0)).at(0) + sqrt(1-canonicalStates.at(0)) * thetas(trajDim);// +  thetas(trajDim) / dist * 20;
        //TODO: inverser Vektor als Kraft aktuelle Position. IK berechnen, normieren, Distanz einberechnen
    }



    //    return 0;
    double steepness = 200;
    //    double sample_rate = 0.55;
    double T  = 0.90;// point a which the sigmoid reaches 0.5
    //    double e = exp(steepness/sample_rate*(T - canonicalStates.at(0)));

    double sigmoidal_weight = exp(steepness * T) / (exp(steepness * T) + exp(steepness * canonicalStates.at(0)));
    sigmoidal_weight = 1;
    //    std::cout << "canonicalState: " << canonicalStates.at(0) << " weight: " << sigmoidal_weight << std::endl;
    return (*_getFunctionApproximator(trajDim))(canonicalStates.at(0)).at(0) * sigmoidal_weight;
}


SampledTrajectoryV2 EndVeloForceFieldWithObjRepulsionDMP::calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMP::DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor)
{

    setStartPositions(initialStates);

    DVec::const_iterator it = timestamps.begin();
    Vec<DMP::DMPState> previousState = initialStates;

    std::map<double, DVec > resultingSystemStatesMap;

    DVec canonicalValues = initialCanonicalValues;
    double prevTimestamp = *it;

    //TODO nicht hard coden
    VirtualRobot::RobotNodeSetPtr rns = robot->getRobotNodeSet("TorsoBothArms");

    for (; it != timestamps.end(); it++)
    {
        previousState = calculateTrajectoryPoint(*it, goal, prevTimestamp, previousState, canonicalValues, temporalFactor);
        prevTimestamp = *it;
        DVec positions;
//            std::cout << canonicalValues.at(0) << " Vel: " << previousState.at(0).vel / temporalFactor << std::endl;

        for (unsigned int d = 0; d < previousState.size(); d++)
        {
            positions.push_back(previousState.at(d).pos);


//            rns->getNode(d)->setJointValue(previousState.at(d).pos);

        }

        std:vector<float> jointValues;
        for (int i = 0;  i < positions.size(); i++) {
            jointValues.push_back(positions.at(i));
        }
        rns->setJointValues(jointValues);
        resultingSystemStatesMap[prevTimestamp] = positions;
    }

    return SampledTrajectoryV2(resultingSystemStatesMap);
}

void EndVeloForceFieldWithObjRepulsionDMP::loadObject(string name, VirtualRobot::ObstaclePtr object)
{
    objects[name] = object;
    obstacles->addSceneObject(object);
}

void EndVeloForceFieldWithObjRepulsionDMP::loadRobotNS(string name, VirtualRobot::RobotNodeSetPtr robot) {

    robotNS = robot;

}

void EndVeloForceFieldWithObjRepulsionDMP::loadRobot(string name, VirtualRobot::RobotPtr _robot) {

    robot = _robot;

}

void EndVeloForceFieldWithObjRepulsionDMP::write_text_to_log_file( const std::string &text ) const
{
    std::ofstream log_file(
        "log_file.txt", std::ios_base::out | std::ios_base::app );
    log_file << text << std::endl;
}
