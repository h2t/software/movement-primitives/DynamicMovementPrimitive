#ifndef __DMP_MULTIPHASEDMP_H
#define __DMP_MULTIPHASEDMP_H

#include <dmp/representation/basicode.h>
#include <dmp/functionapproximation/functionapproximation.h>
#include <dmp/representation/systemstatus.h>

#include "dmpinterface.h"

#include <dmp/functionapproximation/lwprwrapper.h>

#include <map>
#include <boost/serialization/map.hpp>
#include <boost/serialization/nvp.hpp>


namespace DMP
{


    class MultiPhaseDMP : public DMPInterfaceTemplate<DMPState>
    {
    public:
        /**
         * @brief MultiPhaseDMP
         * @param kernelSize is the kernel sizes of different dimension
         * @param D is the damping factor
         * @param tau is the temporal length factor used for training
         */
        MultiPhaseDMP(unsigned int dim, double dt = 1e-3, double D = 20, double tau = 1.0);

        std::string getDMPType() override{return "MultiPhaseDMP";}

        void learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories) override ;

        Vec<DMPState> calculateTrajectoryPoint(double t, const DVec& goal, double tCurrent, const Vec<DMPState> &currentStates, DVec& canonicalValues, double temporalFactor = 1) override;
         SampledTrajectoryV2 calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor = 1) override;

        /**
         * @brief evaluateReproduction evaluates the accuracy of the reproduction
         * of an example trajectory with same startPosition, startVelocity and
         * goalPosition.
         * @return returns the mean error of the trajectory scaled to the range
         * of the trajectory values
         * @pre An example trajectory must have been learned in with
         * learnFromTrajectory()
         * @see learnFromTrajectory()
         */
        double evaluateReproduction(DVec initialCanonicalValue, double& maxErrorOut, bool showPlot = true);


        const SampledTrajectoryV2& getTrainingTraj(unsigned int trajSampleIndex) const override;

        void setGoals(const DVec& goals) override
        {
            this->goals = goals;
        }

        DVec getGoals() override
        {
            return goals;
        }

        void setStartPositions(const DVec& startPositions) override
        {
            this->startPositions = startPositions;
        }
        void setSpringConstant(const double& K)
        {
            this->K = K;
        }
        void setDampingConstant(const double& D)
        {
            this->D = D;
        }
        void setDampingAndSpringConstant(const double& D)
        {
            this->D = D;
            K = (D / 2) * (D / 2);
        }

        void setTimeStep(double ts) override
        {
            this->setEpsabs(ts);
            for(size_t i = 0; i < canonicalSystemList.size(); ++i)
            {
                canonicalSystemList[i]->setEpsabs(ts);
            }
        }

        void setTemporalFactor(const double& tau) override
        {
            for(size_t i = 0; i < canonicalSystemList.size(); ++i)
            {
                canonicalSystemList[i]->setTau(tau);
            }
        }

        void setCanonicalSystem(int dim, CanonicalSystemPtr canSys) override
        {
            canonicalSystemList[dim] = canSys;
        }

        double getDampingFactor() const override { return D; }
        double getSpringFactor() const override { return K; }
        void setStartPositions(const Vec<DMPState> &initialStates);

        DVec getStartPositions() override{return startPositions;}
        void setConfiguration(string paraID,const paraType& para) override;

//        virtual void showdmpDataMemoryPrxConfiguration(int paraID);

        void setAmpl(const unsigned int i, const double ampl) override
        {
            if(i > amplitudes.size() - 1)
            {
                std::cout << "Warning: out of the range of amplitudes" << std::endl;
                amplitudes.push_back(ampl);
                return;
            }
            amplitudes[i] = ampl;
        }

        double getAmpl(const unsigned int i) const override
        {
            return amplitudes[i];
        }

//        virtual void showdmpDataMemoryPrxConfiguration(int paraID);


        Vec<SampledTrajectoryV2 > trainingTraj;

        // inherited from DMPInterfaceTemplate
        double _getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const override;

        void showConfiguration(string paraID) override;

        CanonicalSystemList canonicalSystemList;
    protected:
        // inherited from ODE
        void flow(double t, const DVec& x, DVec& out) override;

        /**
         * @brief calcPerturbationForceSamples calculates the perturbation function dependent on the canonical system
         * that determines the characteristic shape of the result trajectory.
         * @param t
         * @return
         * @see learnFromTrajectory(), flow()
         */
        virtual DVecMap calcPerturbationForceSamples(unsigned int trajDim, const Vec<DVec>& canonicalValueList,  const SampledTrajectoryV2& exampleTraj) const;



        //        unsigned int dimValuesOfInterest() const{ return 2;}
        //        void projectStateToValuesOfInterest(const double *x, DVec &projection) const;

        //    private:

        virtual void _trainFunctionApproximator(unsigned int trajDimIndex, const DVecMap &perturbationForceSamples);

        /**
         * @brief goal specifies the desired end position of the trajectory.
         *
         * Must differ from \ref startPosition to have a trajectory.
         */
        DVec goals;

        /**
         * @brief startPosition specifies the start position of the trajectory.
         * @see startVelocity
         */
        DVec startPositions;
        /**
         * @brief K is the spring constant of the damped-spring-mass-system.
         */
        double K;

        /**
         * @brief D is damping of the damped-spring-mass-system.
         */
        double D;


        /**
         * @brief amplitudes are a vector of all amplitudes for different dimension of trajectories
         */
        DVec amplitudes;



        //        DoubleMap perturbationForceSamples;



        /**
         * @brief normalizedLearnedTraj contains the learnedTraj, where the timestamps
         * a normalized to go from 0 to 1.
         */
        //        SampledTrajectory normalizedLearnedTraj;
        //        mutable Vec<FunctionApproximationInterfacePtr> __functionApproximators;

        mutable std::map<unsigned int, bool> scalingActivated;

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {

            ar& boost::serialization::make_nvp("K",K);
            ar& boost::serialization::make_nvp("D",D);
            ar& boost::serialization::make_nvp("goals",goals);
            ar& boost::serialization::make_nvp("startPositions", startPositions);
            ar& boost::serialization::make_nvp("scalingActivated",scalingActivated);

            DMPInterface& base = boost::serialization::base_object<DMPInterface>(*this);
            ar& boost::serialization::make_nvp("base",base);

        }
    };




}




#endif
