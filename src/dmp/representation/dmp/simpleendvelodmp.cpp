/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "simpleendvelodmp.h"

using namespace DMP;

SimpleEndVeloDMP::SimpleEndVeloDMP(int kernelSize, double tau, bool stopAtEnd) :
    BasicDMP(kernelSize),
    stopAtEnd(stopAtEnd)
{
    canonicalSystem = CanonicalSystemPtr(new LinearCanonicalSystem(tau));
}

void SimpleEndVeloDMP::flow(double t, const DVec& x, DVec& out)
{

    double u = x.at(0);

    DVec canonicalState(1);
    canonicalState[0] = u;
    boost::dynamic_pointer_cast<LinearCanonicalSystem>(canonicalSystem)->setSlowDown(true);
    canonicalSystem->flow(t, canonicalState, out);
    boost::dynamic_pointer_cast<LinearCanonicalSystem>(canonicalSystem)->setSlowDown(false);

    double tau = canonicalSystem->getTau();


    unsigned int dimensions = __trajDim;

    if (dimensions > goals.size())
    {
        throw Exception("There are less trajectory goals than dimensions");
    }

    if (dimensions > startPositions.size())
    {
        throw Exception("There are less trajectory start positions than dimensions");
    }

    // calculate the sigmoidal weight to ensure stopping before reaching the target
    double steepness = 100;
    //    double sample_rate = 0.55;
    double T  = 0.90;// point a which the sigmoid reaches 0.5
    //    double e = exp(steepness/sample_rate*(T - canonicalStates.at(0)));

    double slowDownWeight = exp(steepness * T) / (exp(steepness * T) + exp(steepness * u));


    unsigned int offset = canonicalState.size();
    unsigned int curDim = 0;

    while (curDim < dimensions)
    {
        double force = _getPerturbationForce(curDim, u);
        unsigned int i = curDim * 2 + offset;
        double pos = x[i];
        double vel = x[i + 1];
        double scaleFactor = fabs(goals[curDim] - startPositions[curDim]);
        double scaledForce =  scaleFactor * force; // fabs for no mirroring

        if (stopAtEnd && scaledForce * vel > 0) // only slow down if force is not slowing down actively (unchanged force might be a stronger slowdown)
        {
            //            std::cout << "weakening force: " << scaledForce << " with " << slowDownWeight << std::endl;
            scaledForce *= slowDownWeight;
        }

        out.at(i++) = 1.0 / tau * (vel);
        out.at(i)   = 1.0 / tau * scaleFactor * (K * (goals[curDim] - pos) - (D * vel) + force);
//        std::cout << "t: " << t << " u: " << u << " pos: " << pos << " vel: " << vel << " scaledForce: " << scaledForce << " resulting Acc: " << out[i] << std::endl;
        curDim++;
    }
}





DoubleMap SimpleEndVeloDMP::calcPerturbationForceSamples(unsigned int trajDim, const DVec& canonicalValues, const SampledTrajectoryV2& exampleTraj) const
{
    DoubleMap result;


    if (exampleTraj.size() == 0)
    {
        throw Exception("Sample traj is empty");
    }

    if (canonicalValues.size() != exampleTraj.size())
    {
        throw Exception("The amount of the canonicalValues do not match the exampleTrajectory");
    }

    // start position of example traj
    double x0 = exampleTraj.begin()->getPosition(trajDim);
    // end position of example traj
    double xT = exampleTraj.rbegin()->getPosition(trajDim);

    double tau = canonicalSystem->getTau();
    double scaleFactor =  (xT - x0);
    //    exampleTraj.plot();
    DVec::const_iterator itCanon = canonicalValues.begin();
    SampledTrajectoryV2::ordered_view::const_iterator itTraj = exampleTraj.begin();
    //    DVec accelerations = exampleTraj.differentiateDiscretlyForDim(trajDim, 1);
    int i = 0;

    for (; itCanon != canonicalValues.end(); ++itCanon, ++i, itTraj++)
    {

        const SampledTrajectoryV2::TrajData& entry = *itTraj;
        const double& pos = entry.getPosition(trajDim);
        const double& vel = entry.getDeriv(trajDim, 1);
        const double& acc = entry.getDeriv(trajDim, 2);

        double value;

        if (xT == x0)
        {
            value = 0;
        }
        else
        {
            value = (-K * (xT - pos) +  D * vel  + tau * acc / scaleFactor);
            // value /= fabs(xT - x0) ;    // fabs for no mirroring
        }

        checkValue(value);
        result[*itCanon] = value;

        //        if(result[*itCanon] != result[*itCanon])
        //            throw Exception("Nan!");
        //        std::cout << *itCanon << "[" << result[*itCanon] << "] calc. from ( -" << K << " * ("<<xT<<" - " << pos<<" ) +  "<<D<<  "*" << vel << " + "<< tau <<" * "<<acc<<" ) / ( "<< xT <<" - "<< x0 << ")" << std::endl;
        //        std::cout << *itCanon << ": " << result[*itCanon] << std::endl;
    }



    return result;
}

void SimpleEndVeloDMP::_trainFunctionApproximator(unsigned int trajDim, const DoubleMap& perturbationForceSamples)
{
    //    boost::shared_ptr<LWR<GaussKernel> >  approximator = boost::dynamic_pointer_cast<LWR<GaussKernel> >(_getFunctionApproximator(trajDim));
    FunctionApproximationInterfacePtr  approximator = boost::dynamic_pointer_cast<FunctionApproximationInterface >(_getFunctionApproximator(trajDim));
    //            LWPRWrapperPtr  approximator = boost::dynamic_pointer_cast<LWPRWrapper >(_getFunctionApproximator(trajDim));
    //    LWPRPtr lwpr = approximator->getLWPR();

    DoubleMap::const_iterator it = perturbationForceSamples.begin();
    int oldCount = 0;
    DVec timestamps;
    DVec values;

    //    timestamps.push_back( perturbationForceSamples.begin()->first - (perturbationForceSamples.rbegin()->first - perturbationForceSamples.begin()->first) * 0.01);
    //    values.push_back(0);
    for (it = perturbationForceSamples.begin(); it != perturbationForceSamples.end(); ++it)
    {

        timestamps.push_back(it->first);
        values.push_back(it->second);
        //        if(oldCount != lwpr->numRFS(0))
        //            std::cout << "new RF at " << lwpr->getRF(0, lwpr->numRFS(0)-1).center().at(0) << std::endl;
        //        oldCount =lwpr->numRFS(0);
        //        std::cout << "x: " <<  it->first << " value: " << it->second << " prediction: " << lwpr->predict(doubleVec(1,it->first), 0.0001).at(0) << std::endl;
    }

    //    timestamps.push_back( perturbationForceSamples.rbegin()->first + (perturbationForceSamples.rbegin()->first - perturbationForceSamples.begin()->first) * 0.01);
    //    timestamps.push_back( perturbationForceSamples.rbegin()->first + (perturbationForceSamples.rbegin()->first - perturbationForceSamples.begin()->first) * 0.1);
    //    values.push_back(0);
    //    values.push_back(0);

    approximator->learn(timestamps, values);
    //    std::cout << *approximator;
    //    std::cout << "approx at 1.0001: " << (*approximator)(1.0001) << std::endl;
    //    std::cout << "approx at 1.001: " << (*approximator)(1.001) << std::endl;
    //    std::cout << "mean error: " << approximator->evaluate(timestamps, values) << std::endl;
    //    wrapper->update(doubleVec(1,perturbationForceSamples.begin()->first - 0.001), 0);
    //    wrapper->update(doubleVec(1,perturbationForceSamples.rbegin()->first + 0.001), 0);




    // evaluate quality of training data
    it = perturbationForceSamples.begin();
    DoubleMap errors;

    for (; it != perturbationForceSamples.end(); it++)
    {
        errors[it->first] = it->second - _getPerturbationForce(trajDim, it->first) ;
        //        std::cout << "x: " <<  it->first << " value: " << it->second << " prediction: " << lwpr->predict(doubleVec(1,it->first), 0.0001).at(0) << std::endl;
    }

    if (getShowPlots())
    {
        plot(errors, "learningError.gp", true, 0.0);
    }

    //    std::cout << "number of basis functions: " << lwpr->numRFS(0) << std::endl;
    //    for(int i = 0; i < lwpr->numRFS(0); i++)
    //    {
    //        LWPR_ReceptiveFieldObject rf = lwpr->getRF(0, i);

    ////        std::cout << "Center: " << rf.center() << " beta: " << rf.beta().at(0) << " mean: " << rf.meanX() << " thrustworthy: " << ((rf.trustworthy())?"yes":"no")  << " number of samples seen: " << rf.numData().at(0) << std::endl;
    //    }
}

double
SimpleEndVeloDMP::_getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const
{
    //    return 0;
    double steepness = 200;
    //    double sample_rate = 0.55;
    double T  = 0.90;// point a which the sigmoid reaches 0.5
    //    double e = exp(steepness/sample_rate*(T - canonicalStates.at(0)));

    double sigmoidal_weight = exp(steepness * T) / (exp(steepness * T) + exp(steepness * canonicalStates.at(0)));
    sigmoidal_weight = 1;
    //    std::cout << "canonicalState: " << canonicalStates.at(0) << " weight: " << sigmoidal_weight << std::endl;
    return (*_getFunctionApproximator(trajDim))(canonicalStates.at(0)).at(0) * sigmoidal_weight;
}


