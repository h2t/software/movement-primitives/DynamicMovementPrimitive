#include "taskspacedmp.h"




DMP::TaskSpaceDMP::TaskSpaceDMP(int kernelSize, double D, double tau, double eps)
{
    this->D = D;
    this->K = (D/2) * (D/2);

    FunctionApproximationInterfacePtr approximater(new LWR<GaussKernel>(kernelSize));
    setFunctionApproximator(approximater);
    canonicalSystem.reset(new CanonicalSystem(tau));

    canonicalSystem->setEpsabs(eps);
    amplitudes.push_back(1.0);
}

DMP::Vec<Eigen::Vector3d> DMP::TaskSpaceDMP::calcAngularVelocityAndAcceleration(Eigen::Quaterniond q0, Eigen::Quaterniond q, Eigen::Quaterniond q1, double dt)
{
    Vec<Eigen::Vector3d> res;
    Eigen::Quaterniond dq;
    Eigen::Quaterniond ddq;

    if(dt != 0){
        dq = (q1 - q) / dt;
        ddq = ((q1 - q) - (q - q0)) / (dt * dt);
    } else {
        dq = Eigen::Quaterniond(0,0,0,0);
        ddq = Eigen::Quaterniond(0,0,0,0);
    }

    Eigen::Quaterniond qa = 2 * dq * q.conjugate();
    Eigen::Quaterniond qb = 2 * ddq * q.conjugate();
    Eigen::Quaterniond qc = 2 * dq * dq.conjugate();
    Eigen::Quaterniond qcb = qb + qc;
    res.push_back(qa.vec());
    res.push_back(qcb.vec());

    return res;
}

DMP::Vec<DMP::DoubleMap > DMP::TaskSpaceDMP::calcPerturbationForceSamples(const DMP::DVec &canonicalValues, const DMP::SampledTrajectoryV2 &exampleTraj) const
{
    DMP::Vec<DMP::DoubleMap > result;

    if (exampleTraj.size() == 0)
    {
        throw Exception("Sample traj is empty");
    }


    DVec::const_iterator itCanon = canonicalValues.begin();
    SampledTrajectoryV2::ordered_view::const_iterator itTraj = exampleTraj.begin();

    double tau = canonicalSystem->getTau();
    int pDim = 0;
    // handle quaternion
    int qDim = 3;
    double gw = exampleTraj.rbegin()->getPosition(qDim);
    double gx = exampleTraj.rbegin()->getPosition(qDim + 1);
    double gy = exampleTraj.rbegin()->getPosition(qDim + 2);
    double gz = exampleTraj.rbegin()->getPosition(qDim + 3);

    Eigen::Quaterniond g0(gw, gx, gy, gz);

    SampledTrajectoryV2::ordered_view::const_iterator itQtraj = exampleTraj.begin();
    DoubleMap fqw, fqx, fqy, fqz;
    Vec<DoubleMap > fpos;
    fpos.resize(3);

    double qw = itQtraj->getPosition(qDim);
    double qx = itQtraj->getPosition(qDim + 1);
    double qy = itQtraj->getPosition(qDim + 2);
    double qz = itQtraj->getPosition(qDim + 3);

    Eigen::Quaterniond q(qw, qx, qy, qz);
    Eigen::Quaterniond q0(qw,qx,qy,qz);
    Eigen::Quaterniond q1(qw,qx,qy,qz);
    Eigen::Vector3d w, dw;

    // generate first angular velocity
    SampledTrajectoryV2::ordered_view::const_iterator itQtraj1 = exampleTraj.begin();
    itQtraj1++;
    SampledTrajectoryV2::ordered_view::const_iterator itQtrajend = exampleTraj.end();
    itQtrajend--;

    q1.w() = itQtraj1->getPosition(qDim);
    q1.x() = itQtraj1->getPosition(qDim + 1);
    q1.y() = itQtraj1->getPosition(qDim + 2);
    q1.z() = itQtraj1->getPosition(qDim + 3);

    double dt0 = itQtraj1->getTimestamp() - itQtraj->getTimestamp();

    Vec<Eigen::Vector3d> s0 = TaskSpaceDMP::calcAngularVelocityAndAcceleration(q0, q, q1, dt0);
    w = s0[0];

    double preTime = itQtraj->getTimestamp();
    ++itQtraj;
    ++itQtraj1;

    double dt1 = itQtraj1->getTimestamp() - itQtraj->getTimestamp();
    q0 = q;
    q = q1;
    q1.w() = itQtraj1->getPosition(qDim);
    q1.x() = itQtraj1->getPosition(qDim + 1);
    q1.y() = itQtraj1->getPosition(qDim + 2);
    q1.z() = itQtraj1->getPosition(qDim + 3);

    Vec<Eigen::Vector3d> s1 = TaskSpaceDMP::calcAngularVelocityAndAcceleration(q0, q, q1, dt1);
    dw = s1[1];

    w = w - dw * dt1;

    Eigen::Vector3d yita = tau * w;
    Eigen::Vector3d ayita = tau * dw;
    Eigen::Vector3d fo = tau * ayita - K * 2 * qlog(g0 * q0.conjugate()) + D * yita;

    fqw[*itCanon] = 0;
    fqx[*itCanon] = 0;
    fqy[*itCanon] = 0;
    fqz[*itCanon] = 0;

    int i = 0;


    for (; itCanon != canonicalValues.end(); ++itCanon, ++i, itTraj++)
    {
        // Handle Position DMP
        for(int j = 0; j < 3; ++j)
        {
            const double& pos = itTraj->getPosition(pDim + j);
            const double& vel = itTraj->getDeriv(pDim + j, 1);
            const double& acc = itTraj->getDeriv(pDim + j, 2);

            double xT = exampleTraj.rbegin()->getPosition(pDim + j);

            double value = -K  * (xT - pos) +   D * vel  + tau * acc;
            value /= amplitudes[pDim + j];
            checkValue(value);
            fpos[j][*itCanon] =  value;


        }

        if(itCanon == canonicalValues.begin()) continue;

        // Handle Orientation Quaternion DMP
        q.w() = itQtraj->getPosition(qDim);
        q.x() = itQtraj->getPosition(qDim + 1);
        q.y() = itQtraj->getPosition(qDim + 2);
        q.z() = itQtraj->getPosition(qDim + 3);

        dt1 = itQtraj->getTimestamp() - preTime;

        if(itQtraj == itQtrajend){
            w = w + dw * dt1;
        }else{
            q1.w() = itQtraj1->getPosition(qDim);
            q1.x() = itQtraj1->getPosition(qDim + 1);
            q1.y() = itQtraj1->getPosition(qDim + 2);
            q1.z() = itQtraj1->getPosition(qDim + 3);

            Vec<Eigen::Vector3d> res = TaskSpaceDMP::calcAngularVelocityAndAcceleration(q0, q, q1, dt1);
            w = res[0];
            dw = res[1];
        }

        DVec vw;
        vw << w(0), w(1), w(2);

        yita = tau * w;
        ayita = tau * dw;

        fo = tau * ayita - K * 2 * qlog(g0 * q.conjugate()) + D * yita;

        fqw[*itCanon] = 0/amplitudes[qDim];
        fqx[*itCanon] = fo(0) / amplitudes[qDim+1];
        fqy[*itCanon] = fo(1) / amplitudes[qDim+2];
        fqz[*itCanon] = fo(2) / amplitudes[qDim+3];

        q0 = q;
        preTime = itQtraj->getTimestamp();
        ++itQtraj;
        ++itQtraj1;
    }

    result.resize(7);
    result[0] = fpos[0];
    result[1] = fpos[1];
    result[2] = fpos[2];
    result[3] = fqw;
    result[4] = fqx;
    result[5] = fqy;
    result[6] = fqz;


    return result;
}



void DMP::TaskSpaceDMP::_trainFunctionApproximator(unsigned int trajDimIndex, const DMP::DoubleMap &perturbationForceSamples)
{
    FunctionApproximationInterfacePtr approximator = _getFunctionApproximator(trajDimIndex);

    DVec2d x;
    DVec2d y;

    DVec canVals;
    DVec vals;

    DoubleMap::const_iterator it = perturbationForceSamples.begin();

    for (it = perturbationForceSamples.begin(); it != perturbationForceSamples.end(); ++it)
    {
        canVals.push_back(it->first);
        vals.push_back(it->second);
    }

    x.push_back(canVals);
    y.push_back(vals);

    approximator->learn(x, y);


}


void DMP::TaskSpaceDMP::learnFromTrajectories(const Vec<DMP::SampledTrajectoryV2> &trainingTrajectories)
{
    if (trainingTrajectories.size() != 1)
    {
        return;
    }

    trainingTraj = trainingTrajectories;

    __trajDim = trainingTraj.at(0).dim();
    SampledTrajectoryV2& trainingTrajectory = trainingTraj[0];

    // normalize timestamps so that they start at 0 and end at 1
    trainingTrajectory = SampledTrajectoryV2::normalizeTimestamps(trainingTrajectory, 0.0, 1.0);
    trainingTrajectory.differentiateDiscretly(2);
    DVec normTimestamps = trainingTrajectory.getTimestamps();

    Vec<DVec> uValuesOrig;
    canonicalSystem->integrate(normTimestamps, canonicalSystem->getInitValues(), uValuesOrig);
    DVec uValues;
    DoubleMap uValuesPlotMap;

    for (unsigned int i = 0; i < uValuesOrig.size(); ++i)
    {
        uValues.push_back(uValuesOrig[i][0]);
        uValuesPlotMap[normTimestamps.at(i)] = uValuesOrig[i][0];
    }

    unsigned int trajDim = trainingTrajectory.dim();

    // guarantee the size of amplitudes
    while(amplitudes.size() < trajDim)
        amplitudes.push_back(1.0);


    Vec<DoubleMap> perturbationForceSamples = calcPerturbationForceSamples(uValues, trainingTrajectory);


    for (unsigned int dim = 0; dim < perturbationForceSamples.size(); dim++)
    {
        _trainFunctionApproximator(dim, perturbationForceSamples[dim]);
    }
}

DMP::DVec2d DMP::TaskSpaceDMP::calculateTrajectoryPointBase(double t, const DMP::DVec &goal, double tCurrent, const DMP::DVec2d &currentStates, DMP::DVec &currentCanonicalValues, double temporalFactor)
{
    if(goal.size() != 7)
    {
        throw DMP::Exception("The goal's dimension is not correct. (it should be 7, 3 for position, 4 for quaternion)");
    }

    if(currentStates.size() != 7)
    {
        throw DMP::Exception("The current states' dimension is not correct.");
    }

    setGoals(goal);

    // use explicit eular method to integral differential equation
    double tau = temporalFactor;
    double dt = canonicalSystem->epsabs();

    double tCur = tCurrent;
    DVec2d curState = currentStates;

    DVec2d nextState;
    nextState = curState;
    double uCur = currentCanonicalValues.at(0);

    while(tCur < t)
    {
        double tNext = tCur + dt;

        // step 1: Get next position state
        for(unsigned int i = 0; i < 3; ++i)
        {
            int dim = TASKSPACEDMP_POSSTARTDIM + i;
            double force = _getPerturbationForce(dim, uCur);


            double pos = curState.at(dim)[0];
            double vel = curState.at(dim)[1];

            double acc =  1.0 / tau * (K  * (goal[dim] - pos) - (D  * vel) + amplitudes[dim] * force);
            double nvel = vel + dt * acc;
            double npos = pos + 1.0 / tau * vel * dt;

            DVec state;
            state.push_back(npos);
            state.push_back(nvel);
//            state.push_back(acc);
            nextState[dim] = state;
        }

        // step 2: Get next orientation state

        double fx = _getPerturbationForce(TASKSPACEDMP_ORISTARTDIM+1, uCur);
        double fy = _getPerturbationForce(TASKSPACEDMP_ORISTARTDIM+2, uCur);
        double fz = _getPerturbationForce(TASKSPACEDMP_ORISTARTDIM+3, uCur);
        Eigen::Vector3d fo;

        fo << fx, fy, fz;

        int oridim = TASKSPACEDMP_ORISTARTDIM;
        double qw = curState[oridim][0];
        double qx = curState[oridim+1][0];
        double dwx = curState[oridim+1][1];
        double qy = curState[oridim+2][0];
        double dwy = curState[oridim+2][1];
        double qz = curState[oridim+3][0];
        double dwz = curState[oridim+3][1];

        Eigen::Quaterniond q(qw, qx, qy, qz);

        double gw = goal[oridim];
        double gx = goal[oridim+1];
        double gy = goal[oridim+2];
        double gz = goal[oridim+3];

        Eigen::Quaterniond g0(gw, gx, gy, gz);

        Eigen::Vector3d w;
        w << dwx, dwy, dwz;
        DVec vw;
        vw << dwx, dwy, dwz;

        Eigen::Vector3d yita = tau * w;

        // calculate angular acceleration
        Eigen::Vector3d dw = 1.0 / (tau * tau) * ( K * 2 * qlog(g0 * q.conjugate()) - D * yita + fo);

        Eigen::Quaterniond nq = qexp(dt * w / 2) * q;

        DVec dimState;
        dimState.resize(2);
        dimState[0] = nq.w();
        dimState[1] = 0;
        nextState[oridim] = dimState;

        dimState[0] = nq.x();
        dimState[1] = w(0) + dt * dw(0);
        nextState[oridim+1] = dimState;

        dimState[0] = nq.y();
        dimState[1] = w(1) + dt * dw(1);
        nextState[oridim+2] = dimState;

        dimState[0] = nq.z();
        dimState[1] = w(2) + dt * dw(2);
        nextState[oridim+3] = dimState;


        // step 3: Get next canonical values
        DVec canonicalState(1);
        canonicalState[0] = uCur;
        DVec out;
        canonicalSystem->integrate(tNext, tCur, canonicalState, out);
        uCur = out[0];
        tCur = tNext;

        curState = nextState;
    }

    currentCanonicalValues[0] = uCur;



    return nextState;
}


void DMP::TaskSpaceDMP::setConfiguration(string paraID, const paraType& para){
    if (paraID == DMP::SpringFactor)
    {
        setSpringConstant(boost::get<double>(para));
    }
    else if (paraID == DMP::DampingFactor)
    {
        setDampingConstant(boost::get<double>(para));
    }
    else if (paraID == DMP::TemporalFactor)
    {
        setTemporalFactor(boost::get<double>(para));
    }
    else if (paraID == DMP::Goal)
    {
        setGoals(boost::get<DMP::DVec >(para));
    }
    else if (paraID == DMP::StartPosition)
    {
        setStartPositions(boost::get<DMP::DVec>(para));
    }
    else
    {
        std::cout << "Warning: it is not a valid parameter ID" << std::endl;
    }
}



void DMP::TaskSpaceDMP::showConfiguration(string paraID){
    if (paraID == DMP::SpringFactor)
    {
        std::cout << "K: " << K << std::endl;
    }
    else if (paraID == DMP::DampingFactor)
    {
        std::cout << "D: " << D << std::endl;
    }
    else if (paraID == DMP::TemporalFactor)
    {
        std::cout << "tau: " << canonicalSystem->getTau() << std::endl;
    }
    else if (paraID == DMP::Goal)
    {
        std::cout << "goals: " << goals << std::endl;
    }
    else if (paraID == DMP::StartPosition)
    {
        std::cout << "startPositions: " << startPositions << std::endl;
    }
    else
    {
        std::cout << "Warning: it is not a valid parameter ID" << std::endl;
    }
}
