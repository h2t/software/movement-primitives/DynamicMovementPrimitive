#ifndef DMP_QUATERNIONDMP_H
#define DMP_QUATERNIONDMP_H

#include <dmp/representation/dmp/basicdmp.h>
namespace DMP {

class QuaternionDMP : public BasicDMP
{
#define DIM_STATE 3
#define Q_STATE 4
public:

    QuaternionDMP(int kernelSize = 50, double D = 20, double tau = 1.0);
    // ODE interface

    void learnFromTrajectories(const Vec<SampledTrajectoryV2> &trainingTrajectories) override;


    void setGoals(const DVec& goals) override
    {
        this->goals = goals;
    }
    void setStartPositions(const DVec& startPositions) override
    {
        this->startPositions = startPositions;
    }
    void setSpringConstant(const double& K)
    {
        this->K = K;
    }
    void setDampingConstant(const double& D)
    {
        this->D = D;
    }
    void setDampingAndSpringConstant(const double& D)
    {
        this->D = D;
        K = (D / 2) * (D / 2);
    }
//    void setTemporalFactor(const double& tau)
//    {
//        canonicalSystem->setTau(tau);
//    }
//    double getTemporalFactor() const
//    {
//        return canonicalSystem->getTau();
//    }

    SampledTrajectoryV2 getTrainingQTraj() {return qTraj;}
     SampledTrajectoryV2 calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor ) override;

    virtual void integrate(double t, double tInit, const DVec& init, DVec& out, bool project = false);
    Vec<DMPState> calculateTrajectoryPoint(double t, const DVec& goal, double tCurrent, const Vec<DMPState> &currentStates, DVec& canonicalValues, double temporalFactor ) override;

    Vec<Eigen::Vector3d> calcAngularVelocityAndAcceleration(Eigen::Quaterniond q0, Eigen::Quaterniond q, Eigen::Quaterniond q1, double dt);

    std::map<double, DVec> angular_vels_in;
    std::map<double, DVec> angular_vels_tr;

    friend class CCDMP;

    /**
     * @brief evaluateReproduction evaluates the accuracy of the reproduction
     * of an example trajectory with same startPosition, startVelocity and
     * goalPosition.
     * @return returns the mean error of the trajectory scaled to the range
     * of the trajectory values
     * @pre An example trajectory must have been learned in with
     * learnFromTrajectory()
     * @see learnFromTrajectory()
     */
    double evaluateReproduction(double initialCanonicalValue, double& maxErrorOut, bool showPlot = true) override;

protected:

    SampledTrajectoryV2 qTraj;
    SampledTrajectoryV2 aTraj;
    Vec<DoubleMap> calcVecPerturbationForceSamples(unsigned int trajDim, const DVec &canonicalValues, const SampledTrajectoryV2 &exampleTraj);


    friend class boost::serialization::access;
    template<class Archive>
    void serialize(Archive& ar, const unsigned int file_version)
    {
        DMPInterface& base = boost::serialization::base_object<DMPInterface >(*this);
        ar& boost::serialization::make_nvp("base",base);

    }



};

typedef boost::shared_ptr<QuaternionDMP> QuaternionDMPPtr;
}

#endif
