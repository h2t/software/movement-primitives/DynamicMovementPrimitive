#include "getlearningerrordmp.h"

using namespace DMP;

GetLearningErrorDMP::GetLearningErrorDMP()
= default;

std::vector<std::pair<DoubleMap, DoubleMap> > GetLearningErrorDMP::getLearningApproximationError(const SampledTrajectoryV2 &trainingTrajectory)
{
    // normalize timestamps so that they start at 0 and end at 1
    SampledTrajectoryV2 normalizedTrajectory = SampledTrajectoryV2::normalizeTimestamps(trainingTrajectory, 0.0, 1.0);
    normalizedTrajectory.differentiateDiscretly(2);
    DVec normTimestamps = normalizedTrajectory.getTimestamps();


    Vec<DVec> uValuesOrig;
    canonicalSystem->integrate(normTimestamps, canonicalSystem->getInitValues(), uValuesOrig);
    DVec uValues;
    DoubleMap uValuesPlotMap;

    for (unsigned int i = 0; i < uValuesOrig.size(); ++i)
    {
        uValues.push_back(uValuesOrig[i][0]);
        uValuesPlotMap[normTimestamps.at(i)] = uValuesOrig[i][0];
    }

    if (getShowPlots())
    {
        plot(uValuesPlotMap, "plots/canonical_system_%d.gp", true, 0);
    }

    unsigned int trajDim = normalizedTrajectory.dim();
    SampledTrajectoryV2 traj;

    std::vector<std::pair<DoubleMap, DoubleMap>> result;

    for (unsigned int dim = 0; dim < trajDim; dim++)
    {
        DVec values = normalizedTrajectory.getDimensionData(dim, 0);
        DoubleMap perturbationForceSamples = calcPerturbationForceSamples(dim, uValues, normalizedTrajectory);
        _trainFunctionApproximator(dim, perturbationForceSamples);

        DoubleMap learnedData;
        DVec perturbationSamples;
        DVec timestamps;
        for(auto & perturbationForceSample : perturbationForceSamples)
        {
            timestamps.push_back(perturbationForceSample.first);
            perturbationSamples.push_back(perturbationForceSample.second);
        }
        for (unsigned int i = 0; i < uValues.size(); i++)
        {
            learnedData[uValues[i]] = _getPerturbationForce(dim, uValues[i]);

        }
        traj.addDimension(timestamps, perturbationSamples);

        std::pair<DoubleMap, DoubleMap> resultForDimension;
        for(auto & perturbationForceSample : perturbationForceSamples)
        {
            resultForDimension.first[perturbationForceSample.first] = perturbationForceSample.second;
        }
        for (unsigned int i = 0; i < uValues.size(); i++)
        {
            resultForDimension.second[uValues[i]] = _getPerturbationForce(dim, uValues[i]);
        }
        result.push_back(resultForDimension);

        if (getShowPlots())
            {
            Vec<DoubleMap> curves;
            curves.push_back(perturbationForceSamples);
            curves.push_back(learnedData);
            plot(curves, "plots/learningData_%d.gp", true);
        }
    }

    return result;
}
