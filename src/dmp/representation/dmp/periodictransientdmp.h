/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef PERIODICTRANSIENTDMP_H
#define PERIODICTRANSIENTDMP_H
#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/general/helpers.h>
#include <dmp/general/vec.h>
#include <dmp/representation/basicode.h>
#include <dmp/functionapproximation/locallyweightedlearning.h>
#include <dmp/representation/canonicalsystem.h>
#include <iostream>
class DMP2DPerturbationApproximation;

namespace DMP
{


    class TransientBasisFunctionsDescription
    {
    public:
        double m_T, m_phi_0, m_r0, m_mu1, m_mu2, m_tf, m_N;
        DVec m_deltaj, m_tj;
        Vec< DVec > m_pj;

        TransientBasisFunctionsDescription() {}
        TransientBasisFunctionsDescription(double T, double phi0, double r0,
                                           double mu1, double mu2,
                                           const Vec< DVec > &pj, const DVec& deltaj,
                                           const DVec& tj, double tf);

        Vec< LWLPhaseSpaceBasisFunction* > getFunctionList() const;

        unsigned int N() const
        {
            return (int) m_N;
        }
        double mu1() const
        {
            return m_mu1;
        }
        double mu2() const
        {
            return m_mu2;
        }
        double tf() const
        {
            return m_tf;
        }
        const DVec& tj() const
        {
            return m_tj;
        }
    };

    class PeriodicBasisFunctionsDescription
    {
    public:
        unsigned int m_N;
        double m_mu, m_mu1, m_mu2, m_width, m_phiOffset;
        unsigned int m_k;

        PeriodicBasisFunctionsDescription() {}
        PeriodicBasisFunctionsDescription(unsigned int N, double mu, double mu1, double mu2,
                                          double width, unsigned int k = 4);

        Vec< LWLPhaseSpaceBasisFunction* > getFunctionList() const;

        unsigned int N() const
        {
            return m_N;
        }
    };



    class PeriodicTransientDMP : public BasicDMP
    {

    public:
        double m_alphaZ, m_betaZ;
        DVec m_g;
        unsigned int m_output_dim;
        int m_input_dim;
        bool firstIteration;
        DVec m_currentState;
        PeriodicDiscreteCanonicalSystem m_basisOSC;
        Vec< DMP2DPerturbationApproximation* > m_perturbationApproximation;
        Vec< Vec< DVec > > m_known_initial_states;
        Vec< DVec > m_original_osc_initial_conditions;

        PeriodicTransientDMP();
        PeriodicTransientDMP(const PeriodicDiscreteCanonicalSystem& basisOSC, unsigned int output_dim = 1, unsigned int input_dim = 0, double alpha_z = 2.0, double D = 20, double tau = 1.0);
        ~PeriodicTransientDMP();

        void perturbation(double t, const DVec& x, DVec& force_out);
        void getPerturbationForceSamples(const SampledTrajectory& input_trajectory, Vec<DVec> &ftarg,
                                         unsigned int smooth = 0);
        void getPerturbationForceSamplesV2(const SampledTrajectoryV2& input_trajectory,
                Vec< DVec > &ftarg, unsigned int smooth = 0);
        void flow(double t, const DVec& x, DVec& out);
        unsigned int dimValuesOfInterest() const;
        void projectStateToValuesOfInterest(const double* x, DVec& projection) const;
        TransientBasisFunctionsDescription findSupportCentersAndDistancesForPsii(
            double T, unsigned int K, unsigned int M = 40, double phi1 = 0);
        //        SampledTrajectory normalizedModifiedLearnedTraj;
        void findBestTrajectoryAndInitialPhiAndR(const DVec& initial_vel_and_pos,
                unsigned int& transient_index, double& phi0, double& r0);

        void learnFromTrajectory(const SampledComposedPeriodicTrajectory& input_trajectory,
                                 unsigned int M = 40, unsigned int N = 40);
        void learnFromTrajectoryV2(const SampledTrajectoryV2& periodic_trajectory, const SampledTrajectoryV2& transient_trajectory,
                                 unsigned int M = 40, unsigned int N = 40);
        void learnFromTrajectories(const Vec< const SampledComposedPeriodicTrajectory* > &input_trajectories,
                                   const Vec<unsigned int> &M, unsigned int N = 40);

        void learnFromTrajectories(const Vec< const SampledComposedPeriodicTrajectory* > &input_trajectories,
                                   unsigned int M_per_time = 20, unsigned int N = 40);
        void learnFromTrajectories(
            const Vec< const SampledTrajectoryV2* > &input_trajectories,
            const Vec<unsigned int> &Ms, unsigned int N);
        void learnFromTrajectories(const Vec< SampledTrajectoryV2 > &input_trajectories);
//learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories) ;
        void learnWithZ(SampledComposedPeriodicTrajectory &orignal_trajectory,
                                          SampledTrajectory &z_trajectory, double z_init);
        //    void learnFromPeriodicTrajectory(const SampledComposedPeriodicTrajectory &input_trajectory,
        //                                     unsigned int N=40);

        void play(const DVec& initial_vel_and_pos, const DVec& times, SampledTrajectory& out,
                  int transient_nr = -1);

        void playV2(const DVec& initial_vel_and_pos, const DVec& times, SampledTrajectoryV2& out,
                  int transient_nr = -1);

        unsigned int outputDim() const
        {
            return m_output_dim;
        }

        void writeToFile(const char* filename) const;
        void readFromFile(const char* filename);
        void initializePlaying(Vec< DMPState > &state, const DVec &initial_vel_and_pos, int transient_nr);
        friend ostream& operator << (ostream& out, const PeriodicTransientDMP& dmp);

        void bla(unsigned int traj_count);
        Vec<DMPState> calculateTrajectoryPoint(double t, const DVec& goal, double tCurrent, const Vec<DMPState> &currentStates, DVec& canonicalValues, double temporalFactor );
        SampledTrajectoryV2 calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor );
    protected:
        void reorderAndOptimizePhi1AndT(Vec< SampledComposedPeriodicTrajectory > &trajectories) const;
        void reorderAndOptimizePhi1AndTSimple(Vec< SampledComposedPeriodicTrajectory > &trajectories) const;
    };

    //    public:
    //        PeriodicTransientDMP(double D = 20, double tau = 1.0);
    //        void learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories);
    //        Vec<DMPState> calculateTrajectoryPoint(double t, const DVec& goal, double tInit, const Vec<DMPState> &initialStates, DVec &canonicalValues, double temporalFactor=1);

    //        const SampledTrajectoryV2 & getTrainingTraj(unsigned int i) const{ return originalTrainingTraj;}
    //        SampledTrajectoryV2 calculateTrajectory(const DVec &timestamps, const DVec &goal, const Vec<DMPState> &initialStates, const DVec &initialCanonicalValues, double temporalFactor=1);
    //         SampledTrajectoryV2 reducedGeneratedTraj;
    //    protected:

    //        SampledTrajectoryV2 removeEndVelocityFromTrajectory(const SampledTrajectoryV2 &trajectory);
    //        void calcVelocityReductionFunctionParameters(SampledTrajectoryV2 &traj, unsigned int trajDimension, double &b, double & c);
    //    private:
    //        /**
    //         * @brief trajOffset is the offset of the complete trajectory, so that
    //         * the end velocity and acceleration is zero.
    //         *
    //         */
    //        SampledTrajectoryV2 trajOffset;
    //        SampledTrajectoryV2 originalTrainingTraj;

    //        DVec offsetTrajScalingFactor;


    //    };
}

#endif // ENDVELODMP_H
