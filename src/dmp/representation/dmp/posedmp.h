#ifndef ORIENTATIONDMP_H
#define ORIENTATIONDMP_H

#include <dmp/general/vec.h>
#include <dmp/general/exception.h>
#include "dmpinterface.h"

#include <boost/shared_ptr.hpp>

#include <Eigen/Core>
#include <Eigen/Geometry>

namespace DMP
{
    template <typename DMPType>
    class PoseDMP
    {
    public:
        typedef boost::shared_ptr<DMPType> DMPTypePtr;
        PoseDMP(DMPTypePtr __dmp, const SampledTrajectoryV2&  trainingTrajectory);
        PoseDMP(DMPTypePtr __dmp);

        DMPTypePtr getDMP()
        {
            return __dmp;
        }

        void setStartPose(const DVec& startPose);
        static bool ContinuousAngles(const Eigen::AngleAxisf& oldOrientation, Eigen::AngleAxisf& newOrientation, double& offset);
        DVec calculatePoseDelta(double t, const DVec& goal, double tCurrent, Vec<typename DMPType::SystemType> currentStates, double tDeltaSinceLastCalc, DVec& canonicalValues, double temporalFactor);
        Vec<typename DMPType::SystemType> calculatePose(double t, const DVec& goal, double tCurrent, Vec<typename DMPType::SystemType> currentStates, double tDeltaSinceLastCalc, DVec& canonicalValues, double temporalFactor);
    private:
        void __fixAngles(SampledTrajectoryV2& traj);

        boost::shared_ptr<DMPInterfaceTemplate<typename DMPType::SystemType> > __dmp;
        double __angleOffset;
        Eigen::AngleAxisf __oldOrientation;
        Vec<typename DMPType::SystemType> __oldState;
        static bool verbose;
    };
    template <typename DMPType>
    bool PoseDMP<DMPType>::verbose = false;

    template <typename DMPType>
    PoseDMP<DMPType>::PoseDMP(DMPTypePtr dmp, const SampledTrajectoryV2& trainingTrajectory) :
        __dmp(dmp)
    {
        verbose = false;
        __angleOffset = 0.0;

        if (!this->__dmp)
        {
            throw Exception("dmp pointer must not be null");
        }

        if (trainingTrajectory.dim() != 4)
        {
            throw Exception("Dimension of trajectory must be 4: axisangle(alpha,x,y,z)") << " Dimension is:" << trainingTrajectory.dim();
        }

        SampledTrajectoryV2 trainingTraj(trainingTrajectory);
        trainingTraj.plotAllDimensions(0, "plots/orientationBefore");
        __fixAngles(trainingTraj);
        trainingTraj.plotAllDimensions(0);
//        trainingTraj.plotAllDimensions(1);
        __dmp->setFunctionApproximator(FunctionApproximationInterfacePtr(new ExactReproduction()));
        this->__dmp->learnFromTrajectory(trainingTraj);

    }

    template <typename DMPType>
    PoseDMP<DMPType>::PoseDMP(PoseDMP::DMPTypePtr dmp) :
        __dmp(dmp),
        __angleOffset(0.0)
    {

    }

    template <typename DMPType>
    void PoseDMP<DMPType>::setStartPose(const DVec& startPose)
    {
        //        dmp->setStartPosition();
    }

    template <typename DMPType>
    DVec PoseDMP<DMPType>::calculatePoseDelta(double t, const DVec& goal, double tCurrent, Vec<typename DMPType::SystemType> currentStates, double tDeltaSinceLastCalc, DVec& canonicalValues, double temporalFactor)
    {
        if (currentStates.size() != 4)
        {
            throw Exception("The currentStates variable must have a dimension of 4");
        }

        if (__oldState.size() != 0 && false)
        {
            Eigen::Vector3f newAxis;
            newAxis << currentStates.at(1).pos, currentStates.at(2).pos, currentStates.at(3).pos;
            Eigen::AngleAxisf newOrientation(currentStates.at(0).pos, newAxis);
            Eigen::Vector3f oldAxis;
            oldAxis << __oldState.at(1).pos, __oldState.at(2).pos, __oldState.at(3).pos;

            if (ContinuousAngles(Eigen::AngleAxisf(__oldState.at(0).pos, oldAxis),
                                 newOrientation,
                                 __angleOffset))
            {
                currentStates.at(0).pos = newOrientation.angle();
                currentStates.at(1).pos = newOrientation.axis()[0];
                currentStates.at(2).pos = newOrientation.axis()[1];
                currentStates.at(3).pos = newOrientation.axis()[2];
                currentStates.at(0).vel = (currentStates.at(0).pos - __oldState.at(0).pos) / tDeltaSinceLastCalc ;
                currentStates.at(1).vel = (currentStates.at(1).pos - __oldState.at(1).pos) / tDeltaSinceLastCalc ;
                currentStates.at(2).vel = (currentStates.at(2).pos - __oldState.at(2).pos) / tDeltaSinceLastCalc ;
                currentStates.at(3).vel = (currentStates.at(3).pos - __oldState.at(3).pos) / tDeltaSinceLastCalc ;
            }


        }

        __oldState =  currentStates;

        Vec<typename DMPType::SystemType> newStates = __dmp->calculateTrajectoryPoint(t, goal, tCurrent, currentStates, canonicalValues, temporalFactor);
        int size = newStates.size();
        DVec deltas(size, 0.0);

        for (int i = 0; i < size; ++i)
        {
            deltas.at(i) = newStates.at(i).getValue(0) - currentStates.at(i).getValue(0);
        }

        using namespace Eigen;
        AngleAxisf AAPrev(currentStates.at(0).pos, Vector3f(currentStates.at(1).getValue(0), currentStates.at(2).getValue(0), currentStates.at(3).getValue(0)));
        AngleAxisf AANext(newStates.at(0).pos, Vector3f(newStates.at(1).getValue(0), newStates.at(2).getValue(0), newStates.at(3).getValue(0)));

        Quaternionf quatPrev(AAPrev);
        Quaternionf quatNext(AANext);
        quatNext.normalize();
        quatPrev.normalize();
        Quaternionf delta = quatPrev.inverse() * quatNext;
        deltas.at(0) = delta.w();
        deltas.at(1) = delta.x();
        deltas.at(2) = delta.y();
        deltas.at(3) = delta.z();

        //        Quaternionf quatPrev(currentStates.at(0).getValue(0), currentStates.at(1).getValue(0), currentStates.at(2).getValue(0), currentStates.at(3).getValue(0));
        //        Quaternionf quatNext(newStates.at(0).getValue(0), newStates.at(1).getValue(0), newStates.at(2).getValue(0), newStates.at(3).getValue(0));
        //        quatNext.normalize();
        //        Quaternionf delta = quatPrev.inverse() * quatNext;
        //        deltas.at(0) = delta.w();
        //        deltas.at(1) = delta.x();
        //        deltas.at(2) = delta.y();
        //        deltas.at(3) = delta.z();
        return deltas;
    }

    template <typename DMPType>
    Vec<typename DMPType::SystemType> PoseDMP<DMPType>::calculatePose(double t, const DVec& goal, double tCurrent, Vec<typename DMPType::SystemType> currentStates, double tDeltaSinceLastCalc, DVec& canonicalValues, double temporalFactor)
    {
        if (currentStates.size() != 4)
        {
            throw Exception("The currentStates variable must have a dimension of 4");
        }

        if (__oldState.size() != 0)
        {
            Eigen::Vector3f newAxis;
            newAxis << currentStates.at(1).pos, currentStates.at(2).pos, currentStates.at(3).pos;
            Eigen::AngleAxisf newOrientation(currentStates.at(0).pos, newAxis);
            Eigen::Vector3f oldAxis;
            oldAxis << __oldState.at(1).pos, __oldState.at(2).pos, __oldState.at(3).pos;

            if (!ContinuousAngles(Eigen::AngleAxisf(__oldState.at(0).pos, oldAxis),
                                  newOrientation,
                                  __angleOffset))
            {
                currentStates.at(0).pos = newOrientation.angle();
                currentStates.at(1).pos = newOrientation.axis()[0];
                currentStates.at(2).pos = newOrientation.axis()[1];
                currentStates.at(3).pos = newOrientation.axis()[2];
                currentStates.at(0).vel = (currentStates.at(0).pos - __oldState.at(0).pos) / tDeltaSinceLastCalc ;
                currentStates.at(1).vel = (currentStates.at(1).pos - __oldState.at(1).pos) / tDeltaSinceLastCalc ;
                currentStates.at(2).vel = (currentStates.at(2).pos - __oldState.at(2).pos) / tDeltaSinceLastCalc ;
                currentStates.at(3).vel = (currentStates.at(3).pos - __oldState.at(3).pos) / tDeltaSinceLastCalc ;
            }


        }

        __oldState =  currentStates;

        Vec<typename DMPType::SystemType> newStates = __dmp->calculateTrajectoryPoint(t, goal, tCurrent, currentStates, canonicalValues, temporalFactor);

        return newStates;
    }



    template <typename DMPType>
    void PoseDMP<DMPType>::__fixAngles(SampledTrajectoryV2& traj)
    {
        SampledTrajectoryV2::ordered_view::const_iterator it = traj.begin();
        Eigen::Vector3f axis;
        axis << it->getPosition(1), it->getPosition(2), it->getPosition(3);
        Eigen::AngleAxisf prevPose(it->getPosition(0), axis);
        double offset = 0.0;

        for (it++; it != traj.end(); it++)
        {
            int t = it->getTimestamp() ;
            if(t > 1510)
                verbose = true;
            Eigen::Vector3f axis;
            axis << it->getPosition(1), it->getPosition(2), it->getPosition(3);
            Eigen::AngleAxisf curPose(it->getPosition(0), axis);
            ContinuousAngles(prevPose, curPose, offset);
            it->getData().at(0)->at(0) = curPose.angle();
            it->getData().at(1)->at(0) = curPose.axis()[0];
            it->getData().at(2)->at(0) = curPose.axis()[1];
            it->getData().at(3)->at(0) = curPose.axis()[2];
            prevPose = curPose;
        }

    }

    template <typename DMPType>
    bool PoseDMP<DMPType>::ContinuousAngles(const Eigen::AngleAxisf& oldOrientation, Eigen::AngleAxisf& newAngle, double& offset)
    {
        bool result = true;
        //    if(oldAngle.axis().isApprox(newAngle.axis()*-1))
        const Eigen::Vector3f& v1 = oldOrientation.axis();
        const Eigen::Vector3f& v2 = newAngle.axis();
        Eigen::Vector3f v2i = newAngle.axis() * -1;
        double value = v1.dot(v2) / (v1.norm() * v2.norm());
        if(value < -1 )
            value = -1;
        if(value > 1 )
            value = 1;
        double angle = acos(value);
        value = v1.dot(v2i) / (v1.norm() * v2i.norm());
        if(value < -1 )
            value = -1;
        if(value > 1 )
            value = 1;
        double angleInv = acos(value);


        //        Eigen::AngleAxisf result;
        if (angle > angleInv)
        {
            if(verbose)
            {
                std::cout << "inversion needed" << std::endl;
                std::cout << "angle1: " << angle << std::endl;
                std::cout << "angleInv: " << angleInv << std::endl;
            }
            newAngle = Eigen::AngleAxisf(2.0 * M_PIl - newAngle.angle(), newAngle.axis() * -1);
            result = false;
        }

        //        else newAngle = newAngle;

        if (fabs(newAngle.angle() + offset - oldOrientation.angle()) > fabs(newAngle.angle() + offset - (oldOrientation.angle() + M_PIl * 2)))
        {
            offset -= M_PIl * 2;
        }
        else if (fabs(newAngle.angle() + offset - oldOrientation.angle()) > fabs((newAngle.angle() + M_PIl * 2 + offset) - oldOrientation.angle()))
        {
            offset += M_PIl * 2;
        }

        newAngle.angle() += offset;
        return result;

    }


}

#endif
