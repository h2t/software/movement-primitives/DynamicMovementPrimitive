/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "dmp3rdorder.h"


#include <dmp/general/exception.h>
#include <dmp/general/helpers.h>
#include <dmp/functionapproximation/locallyweightedlearning.h>
#include <dmp/functionapproximation/lwprwrapper.h>

//#include <omp.h>

#include <limits>

using namespace DMP;

///////////////////////////////////////////////////////////////////////////
// Implementation
///////////////////////////////////////////////////////////////////////////


DMP3rdOrder::DMP3rdOrder(int kernelSize, double H, double D, double tau, bool stopAtEnd) :
    stopAtEnd(stopAtEnd),
    K((D / 2) * (D / 2)),
    D(D),
    H(H)
{
    canonicalSystem.reset(new LinearCanonicalSystem(tau));
    ODE::setEpsabs(1e-2);
}


void DMP3rdOrder::learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories)
{
    if (trainingTrajectories.size() > 1)
    {
        throw Exception() << "this DMP can only be trained with one trajectory (but with several dimensions)";
    }

    if (trainingTrajectories.size() == 0)
    {
        return;
    }

    trainingTraj = trainingTrajectories;
    SampledTrajectoryV2& trainingTrajectory = trainingTraj[0];
    //            DVec timestamps = trainingTrajectory.getTimestamps();
    __trajDim = trainingTrajectory.dim();

    // normalize timestamps so that they start at 0 and end at 1
    trainingTrajectory = SampledTrajectoryV2::normalizeTimestamps(trainingTrajectory, 0.0, 1.0);

    trainingTrajectory.differentiateDiscretly(3);
    DVec normTimestamps = trainingTrajectory.getTimestamps();


    Vec<DVec> uValuesOrig;
    canonicalSystem->integrate(normTimestamps, canonicalSystem->getInitValues(), uValuesOrig);
    DVec uValues;
    DoubleMap uValuesPlotMap;

    for (unsigned int i = 0; i < uValuesOrig.size(); ++i)
    {
        uValues.push_back(uValuesOrig[i][0]);
        uValuesPlotMap[normTimestamps.at(i)] = uValuesOrig[i][0];
    }

    if (getShowPlots())
    {
        plot(uValuesPlotMap, "plots/canonical_system_%d.gp", true, 0);
    }

    for (unsigned int dim = 0; dim < trainingTrajectory.dim(); dim++)
    {

        DoubleMap perturbationForceSamples = calcPerturbationForceSamples(dim, uValues, trainingTrajectory);
        _trainFunctionApproximator(dim, perturbationForceSamples);

        DoubleMap learnedData;

        if (getShowPlots())
        {
            for (unsigned int i = 0; i < uValues.size(); i++)
            {
                learnedData[uValues[i]] = _getPerturbationForce(dim, uValues[i]);
                //        std::cout << uValues[i] << ", " << learnedData[uValues[i]] << std::endl;
            }

            //        for(double i= 1; i < 2; i+=0.001)
            //        {
            //            learnedData[i] = _getPerturbationForce(dim, i);
            //            //        std::cout << uValues[i] << ", " << learnedData[uValues[i]] << std::endl;
            //        }
            Vec<DoubleMap> curves;
            curves.push_back(perturbationForceSamples);
            curves.push_back(learnedData);
            std::stringstream file;
            file << "plots/learningData_" << dim << "_%d.gp";
            plot(curves, file.str().c_str(), true);
        }
    }


    std::cout << "Learning Done" << std::endl;

}






void DMP3rdOrder::_trainFunctionApproximator(unsigned int trajDim, const DoubleMap& perturbationForceSamples)
{
    FunctionApproximationInterfacePtr  approximator = _getFunctionApproximator(trajDim);
    approximator->reset();
    DoubleMap::const_iterator it = perturbationForceSamples.begin();
    DVec timestamps;
    DVec values;

    for (it = perturbationForceSamples.begin(); it != perturbationForceSamples.end(); ++it)
    {

        timestamps.push_back(it->first);
        values.push_back(it->second);
    }


    approximator->learn(timestamps, values);


    // evaluate quality of training data
    if (getShowPlots())
    {
        it = perturbationForceSamples.begin();
        DoubleMap errors;

        for (; it != perturbationForceSamples.end(); it++)
        {
            errors[it->first] = it->second - _getPerturbationForce(trajDim, it->first) ;
        }

        plot(errors, "learningError.gp", true, 0.0);
    }
}




const SampledTrajectoryV2& DMP3rdOrder::getTrainingTraj(unsigned int trajDim) const
{
    return trainingTraj.at(trajDim);
}

_3rdOrderDMP DMP3rdOrder::convertStateTo3rdOrderDMPState(const _3rdOrderDMP& realState, float filterConstant)
{
    _3rdOrderDMP result;
    result.acc = realState.vel + realState.acc / filterConstant;
    result.vel = realState.pos + realState.vel / filterConstant;
    result.pos = realState.pos;
    return result;
}



void DMP3rdOrder::flow(double t, const DVec& x, DVec& out)
{

    double u = x.at(0);

    DVec canonicalState(1);
    canonicalState[0] = u;
    canonicalSystem->flow(t, canonicalState, out);

    double tau = canonicalSystem->getTau();


    unsigned int dimensions = __trajDim;

    if (dimensions > goals.size())
    {
        throw Exception("There are less trajectory goals than dimensions");
    }

    if (dimensions > startPositions.size())
    {
        throw Exception("There are less trajectory start positions than dimensions");
    }

    // calculate the sigmoidal weight to ensure stopping before reaching the target
    double steepness = 100;
    //    double sample_rate = 0.55;
    double T  = 0.90;// point a which the sigmoid reaches 0.5
    //    double e = exp(steepness/sample_rate*(T - canonicalStates.at(0)));
    double slowDownWeight = exp(steepness * T) / (exp(steepness * T) + exp(steepness * u));



    unsigned int offset = canonicalState.size();

    //#pragma omp parallel for
    for (unsigned int curDim = 0; curDim < dimensions; ++curDim)
    {
        double force = _getPerturbationForce(curDim, u);
        bool scaling = true;
        std::map<unsigned int, bool>::iterator it = scalingActivated.find(curDim);

        if (it != scalingActivated.end() && !it->second)
        {
            scaling = false;
        }

        if (scaling)
        {
            force *= (goals[curDim] - startPositions[curDim]);
        }

        unsigned int i = curDim * SystemType().size() + offset;
        double pos = x[i];
        double vel = x[i + 1];
        double acc = x[i + 2];

        if (stopAtEnd)
        {
            force *= slowDownWeight;
        }

        out[i++] = 1.0 / tau * H * (vel - pos);
        out[i++] = 1.0 / tau * (acc);
        out[i++] = 1.0 / tau * (K * (goals[curDim] - vel) - (D * acc) + force);
        //        std::cout << "D: " << curDim << " t: " << t << " u: " << u << " vel: " << vel << " acc: " << acc << " force: " << force << " resulting Jerk: " << (out[i-1]*H)  << " resulting acc: " << (out[i-2]*H) << " filtered Vel: " << out[i-3] << std::endl;
    }
}





DoubleMap DMP3rdOrder::calcPerturbationForceSamples(unsigned int trajDim, const DVec& canonicalValues, const SampledTrajectoryV2& exampleTraj) const
{
    DoubleMap result;


    if (exampleTraj.size() == 0)
    {
        throw Exception("Sample traj is empty");
    }

    if (canonicalValues.size() != exampleTraj.size())
    {
        throw Exception("The amount of the canonicalValues do not match the exampleTrajectory");
    }

    // start position of example traj
    double x0 = exampleTraj.begin()->getPosition(trajDim);
    // end position of example traj
    double xT = exampleTraj.rbegin()->getPosition(trajDim);
    bool scaling = true;

    if (xT == x0)
    {
        scaling = scalingActivated[trajDim] = false;
    }

    double tau = canonicalSystem->getTau();

    DVec::const_iterator itCanon = canonicalValues.begin();
    SampledTrajectoryV2::ordered_view::const_iterator itTraj = exampleTraj.begin();
    int i = 0;

    for (; itCanon != canonicalValues.end(); ++itCanon, ++i, itTraj++)
    {
        // first value is time value, so skip it
        const SampledTrajectoryV2::TrajData& state = *itTraj;
        const double& pos = state.getPosition(trajDim);
        const double& vel = state.getDeriv(trajDim, 1);
        const double& acc = state.getDeriv(trajDim, 2);
        const double& jerk = state.getDeriv(trajDim, 3);


        result[*itCanon] =
            (
                pow(tau, 3) / H * jerk +
                pow(tau, 2) * (1 + D / H) * acc +
                tau * (K / H + D) * vel  +
                K * (pos - xT)) ;

        if (scaling)
        {
            result[*itCanon] /= (xT - x0);
        }

        //        std::cout << *itCanon << "[" << result[*itCanon] << "] calc. from ( -" << K << " * ("<<xT<<" - " << pos<<" ) +  "<<D<<  "*" << vel << " + "<< tau <<" * "<<acc<<" ) / ( "<< xT <<" - "<< x0 << ")" << "jerk: " << jerk << " H: " << H << std::endl;
        //        std::cout << *itCanon << ": " << result[*itCanon] << std::endl;
    }



    return result;
}


double
DMP3rdOrder::_getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const
{
    //    return 0;
    return (*_getFunctionApproximator(trajDim))(canonicalStates.at(0)).at(0);
}



Vec<_3rdOrderDMP>
DMP3rdOrder::calculateTrajectoryPoint(double t, const DVec& goal, double tInit, const Vec<_3rdOrderDMP> &initialStates, DVec& canonicalValues, double temporalFactor)
{
    if (startPositions.size() == 0)
    {
        throw Exception("There is no start position set. If you use calculateTrajectoryPoint directly, you must set the start position before (setStartPositions()). ");
    }

    ODE::setDim(1 + __trajDim * SystemType().size());

    DVec initialODEConditions = canonicalValues;
    DVec initialStatesVec = SystemStatus::convertStatesToArray(initialStates);
    initialODEConditions.insert(initialODEConditions.end(), initialStatesVec.begin(), initialStatesVec.end());

    setTemporalFactor(temporalFactor);
    setGoals(goal);

    DVec rawResult;
    ODE::integrate(t, tInit, initialODEConditions, rawResult, false);


    canonicalValues.assign(rawResult.begin(), rawResult.begin() + 1);
    rawResult.erase(rawResult.begin());
    Vec<_3rdOrderDMP> result = SystemStatus::convertArrayToStates<_3rdOrderDMP>(rawResult);
    //    std::cout << "T: " << t << " pos: " << result.at(0).acc << " vel: " << result.at(0).pos << " acc: " << result.at(0).vel << std::endl;

    return result;

}

SampledTrajectoryV2 DMP3rdOrder::calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<_3rdOrderDMP> &initialStates, const DVec& initialCanonicalValues, double temporalFactor)
{

    Vec<DVec> uValuesOrig;
    //    boost::dynamic_pointer_cast<LinearCanonicalSystem>(canonicalSystem)->setSlowDown(true);
    canonicalSystem->integrate(timestamps, canonicalSystem->getInitValues(), uValuesOrig);
    //    boost::dynamic_pointer_cast<LinearCanonicalSystem>(canonicalSystem)->setSlowDown(false);

    //    DVec uValues;
    //    DoubleMap uValuesPlotMap;
    //    for (unsigned int i = 0; i < uValuesOrig.size(); ++i) {
    //        uValues.push_back(uValuesOrig[i][0]);
    //        uValuesPlotMap[timestamps.at(i)] = uValuesOrig[i][0];
    //    }
    //    plot(uValuesPlotMap,"plots/canonical_system_sloweddown%d.gp", true, 0);


    Vec<_3rdOrderDMP> initialStatesCopy(initialStates);

    DVec tempStartPositions;

    for (unsigned int i = 0; i < initialStatesCopy.size(); i++)
    {
        initialStatesCopy[i] = convertStateTo3rdOrderDMPState(initialStatesCopy[i], getFilterConstant());

        tempStartPositions.push_back(initialStatesCopy[i].pos);
    }

    setStartPositions(tempStartPositions);

    SampledTrajectoryV2 result = DMPInterfaceTemplate<_3rdOrderDMP>::calculateTrajectory(timestamps, goal, initialStatesCopy, initialCanonicalValues, temporalFactor);
    result.differentiateDiscretly(2);
    return result;

}





double DMP3rdOrder::evaluateReproduction(double initialCanonicalValue, double& maxErrorOut, bool showPlot)
{
    //    DVec timestamps;
    double meanError = 0.0;
    //    timestamps = SampledTrajectoryV2::generateTimestamps(0,1.0,1.0/(-0.5+getTrainingTraj(0).size()));
    DVec trainTimestamps = getTrainingTraj(0).getTimestamps();
    double goal = getTrainingTraj(0).rbegin()->getPosition(0);
    _3rdOrderDMP initialState;
    initialState.pos = getTrainingTraj(0).begin()->getPosition(0);
    initialState.vel = getTrainingTraj(0).begin()->getDeriv(0, 1);
    initialState.acc = getTrainingTraj(0).begin()->getDeriv(0, 2);

    SampledTrajectoryV2 newTraj = calculateTrajectory(
                                      trainTimestamps,
                                      goal,
                                      initialState,
                                      initialCanonicalValue,
                1);
    std::cout << "start: " << getTrainingTraj(0).begin()->getPosition(0) << " startVel: " << getTrainingTraj(0).begin()->getPosition(0) << " goal: " << goal << std::endl;

    DVec statesGen = newTraj.getDimensionData(0, 0);
    DVec statesOrig = getTrainingTraj(0).getDimensionData(0, 0);

    //    if(statesGen.size() != statesOrig.size())
    //        throw Exception("generated and original trajectories do not have the same sample count") << statesGen.size() << " vs. " << statesOrig.size();
    //    if(statesGen.size() != timestamps.size())
    //        throw Exception("trajectory timestamps and positions sample count do not match:") ;
    DoubleMap errorMap;
    double minTrajValue = std::numeric_limits<double>::max();
    double maxTrajValue = std::numeric_limits<double>::min();
    maxErrorOut = 0.0;

    for (unsigned int i = 0; i < statesGen.size() && i < statesOrig.size(); ++i)
    {
        const double error = fabs(statesGen[i] - statesOrig[i]);

        if (error > maxErrorOut)
        {
            maxErrorOut = error;
        }

        errorMap[trainTimestamps[i]] = error;
        meanError += error;

        if (minTrajValue > statesOrig[i])
        {
            minTrajValue = statesOrig[i];
        }

        if (maxTrajValue < statesOrig[i])
        {
            maxTrajValue = statesOrig[i];
        }

    }

    if (showPlot)
    {
        Vec< SampledTrajectoryV2 > trajectories;

        trajectories.push_back(SampledTrajectoryV2::normalizeTimestamps(getTrainingTraj(0)));
        trajectories.push_back(newTraj);
        //        SampledTrajectory::plot(trajectories,"plots/trajectories_%d_dim%d.gp", true);
        plot(errorMap, "plots/traj_reproduction_error_%d.gp", true, 0);
        DoubleMap newTrajData;
        //        DVec timestamps = newTraj.getTimestamps();
        //        DVec positions = newTraj.getDimensionData(0,0);

        Vec<DVec> plots;
        plots.push_back(trainTimestamps);
        plots.push_back(getTrainingTraj(0).getDimensionData(0, 0));
        plots.push_back(newTraj.getDimensionData(0, 2));
        plotTogether(plots, "plots/trajComparison_%d.gp");
        plots.clear();
        plots.push_back(trainTimestamps);
        plots.push_back(getTrainingTraj(0).getDimensionData(0, 1));
        plots.push_back(newTraj.getDiscretDifferentiationForDim(0, 1));
        plotTogether(plots, "plots/trajComparison_vel_%d.gp");
        plots.clear();
        plots.push_back(trainTimestamps);
        plots.push_back(getTrainingTraj(0).getDiscretDifferentiationForDim(0, 2));
        plots.push_back(SampledTrajectoryV2::DifferentiateDiscretly(trainTimestamps, newTraj.getDimensionData(0, 2), 2));
        plotTogether(plots, "plots/trajComparison_acc_%d.gp");
    }

    meanError /= statesOrig.size();
    meanError /= maxTrajValue - minTrajValue;
    maxErrorOut /= maxTrajValue - minTrajValue;
    return meanError;
}

void DMP3rdOrder::setConfiguration(string paraID, const paraType& para){
    if (paraID == DMP::SpringFactor)
    {
        setSpringConstant(boost::get<double>(para));
    }
    else if (paraID == DMP::DampingFactor)
    {
        setDampingConstant(boost::get<double>(para));
    }
    else if (paraID == DMP::TemporalFactor)
    {
        setTemporalFactor(boost::get<double>(para));
    }
    else if (paraID == DMP::Goal)
    {
        setGoals(boost::get<DMP::DVec >(para));
    }
    else if (paraID == DMP::StartPosition)
    {
        setStartPositions(boost::get<DMP::DVec>(para));
    }
    else
    {
        std::cout << "Warning: it is not a valid parameter ID" << std::endl;
    }
//    switch(paraID){
//    case DMP_PARAMETERS_SPRINGCONSTANT: setSpringConstant(boost::get<double>(para)); break;
//    case DMP_PARAMETERS_DAMPINGFACTOR: setDampingConstant(boost::get<double>(para)); break;
//    case DMP_PARAMETERS_TEMPORALFACTOR: setTemporalFactor(boost::get<double>(para)); break;
//    case DMP_PARAMETERS_GOAL: setGoals(boost::get<DMP::DVec >(para)); break;
//    case DMP_PARAMETERS_STARTPOSITION: setStartPositions(boost::get<DMP::DVec>(para)); break;
//    default: std::cout << "Warning: it is not a valid parameter ID" << std::endl;
//    }
}

void DMP3rdOrder::showConfiguration(string paraID){
    if (paraID == DMP::SpringFactor)
    {
        std::cout << "K: " << K << std::endl;
    }
    else if (paraID == DMP::DampingFactor)
    {
        std::cout << "D: " << D << std::endl;
    }
    else if (paraID == DMP::TemporalFactor)
    {
        std::cout << "tau: " << canonicalSystem->getTau() << std::endl;
    }
    else if (paraID == DMP::Goal)
    {
        std::cout << "goals: " << goals << std::endl;
    }
    else if (paraID == DMP::StartPosition)
    {
        std::cout << "startPositions: " << startPositions << std::endl;
    }
    else
    {
        std::cout << "Warning: it is not a valid parameter ID" << std::endl;
    }
//    switch(paraID){
//    case DMP_PARAMETERS_SPRINGCONSTANT: std::cout << "K: " << K << std::endl; break;
//    case DMP_PARAMETERS_DAMPINGFACTOR: std::cout << "D: " << D << std::endl; break;
//    case DMP_PARAMETERS_TEMPORALFACTOR: std::cout << "tau: " << canonicalSystem->getTau() << std::endl; break;
//    case DMP_PARAMETERS_GOAL: std::cout << "goals: " << goals << std::endl; break;
//    case DMP_PARAMETERS_STARTPOSITION: std::cout << "startPositions: " << startPositions << std::endl; break;
//    default: std::cout << "Warning: it is not a valid parameter ID" << std::endl;
//    }
}




