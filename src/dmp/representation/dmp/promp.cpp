#include "promp.h"
#include <Eigen/Dense>
#include "general/helpers.h"
#include <representation/mathtools.h>

DMP::ProMP::ProMP(size_t kernelSize)
{
    canSys.reset(new LinearDecayCanonicalSystem(1.0));
    this->tau = 1.0;
    kerSize = kernelSize;
}

void DMP::ProMP::learnFromTrajectories(const Vec<DMP::SampledTrajectoryV2> &trainingTrajectories)
{
    if (trainingTrajectories.size() == 0)
    {
        throw Exception() << "The training trajectories is empty ... ";
    }

    trainingTrajs = trainingTrajectories;
    dimOfTraj = trainingTrajs.at(0).dim();;
    numOfDemos = trainingTrajs.size();
    for(size_t i = 0; i < numOfDemos; ++i)
    {
        trainingTrajs.at(i) = SampledTrajectoryV2::normalizeTimestamps(trainingTrajs.at(i), 0, 1);
    }

    DVec normTimestamps = trainingTrajs.at(0).getTimestamps();
    dtime = normTimestamps[1] - normTimestamps[0];
    Vec<DVec> uValuesOrig;
    canSys->integrate(normTimestamps, 1.0, uValuesOrig);
    uValues.clear();
    for (size_t i = 0; i < uValuesOrig.size(); ++i)
    {
        uValues.push_back(uValuesOrig[i][0]);
    }

    int trajDim = 0;
    posLWmu.clear();
    posLWsigma.clear();
    velLWmu.clear();
    velLWsigma.clear();
    for(size_t dim = 0; dim < dimOfTraj; ++dim)
    {
        std::vector<vec > posWeights;
        std::vector<vec > velWeights;
        vec posAvgWeight(kerSize,fill::zeros);
        vec velAvgWeight(kerSize,fill::zeros);
        for(size_t demoId = 0; demoId < numOfDemos; demoId++)
        {
            DVec posTraj = trainingTrajs.at(demoId).getDimensionData(dim);
            DoubleMap posSamples = createDoubleMap(uValues, posTraj);
            _trainFunctionApproximator(trajDim, posSamples);
            vec posWeight(fapprox.at(trajDim)->getWeights());

            posAvgWeight = posAvgWeight + posWeight;
            posWeights.push_back(posWeight);

            DVec velTraj = trainingTrajs.at(demoId).getDimensionData(dim, 1.0);
            DoubleMap velSamples = createDoubleMap(uValues, velTraj);
            _trainFunctionApproximator(dimOfTraj + trajDim, velSamples);
            vec velWeight(fapprox.at(dimOfTraj + trajDim)->getWeights());

            velAvgWeight = velAvgWeight + velWeight;
            velWeights.push_back(velWeight);

        }

        posAvgWeight = posAvgWeight / numOfDemos;
        posLWmu.push_back(posAvgWeight);
        velAvgWeight = velAvgWeight / numOfDemos;
        velLWmu.push_back(velAvgWeight);

        mat posCovWeight(kerSize, kerSize, fill::zeros);
        mat velCovWeight(kerSize, kerSize, fill::zeros);
        for(size_t i = 0; i < posWeights.size(); ++i)
        {
            posCovWeight = posCovWeight + (posWeights.at(i) - posAvgWeight) * (posWeights.at(i) - posAvgWeight).t();
            velCovWeight = velCovWeight + (velWeights.at(i) - velAvgWeight) * (velWeights.at(i) - velAvgWeight).t();
        }
        posCovWeight = posCovWeight / numOfDemos;
        posLWsigma.push_back(posCovWeight);
        velCovWeight = velCovWeight / numOfDemos;
        velLWsigma.push_back(velCovWeight);

        fapprox.at(trajDim)->setWeights(MathTools::vec2dvec(posAvgWeight));
        fapprox.at(dimOfTraj + trajDim)->setWeights(MathTools::vec2dvec(velAvgWeight));
        trajDim++;
    }

    reset();
}

DMP::DVec2d DMP::ProMP::sampleWeight(bool isPosition)
{
    DVec2d sampledWeights;

    std::vector<vec > avgWeights;
    std::vector<mat > covWeights;

    if(isPosition)
    {
        avgWeights = posWmu;
        covWeights = posWsigma;
    }
    else
    {
        avgWeights = velWmu;
        covWeights = velWsigma;
    }

    for(size_t i = 0; i < dimOfTraj; ++i)
    {
        vec sampledWeight = MathTools::mvrnorm(avgWeights.at(i), covWeights.at(i));
        sampledWeights.push_back(MathTools::vec2dvec(sampledWeight));
    }
    return sampledWeights;
}

DMP::SampledTrajectoryV2 DMP::ProMP::generateTrajectory(const DVec& timestamps, const DVec2d &weight, bool isPosition)
{
    DMP::SampledTrajectoryV2 resTraj;
    DVec canVals;
    DVec2d values;

    int startId = 0;
    if(!isPosition)
    {
        startId = dimOfTraj;
    }

    for(size_t i = 0; i < dimOfTraj; ++i)
    {
        fapprox.at(startId + i)->setWeights(weight.at(i));
        DVec val;
        val.push_back((*fapprox.at(startId + i))(1.0).at(0));
        values.push_back(val);
    }

    canVals.push_back(1.0);
    for(size_t ti = 1; ti < timestamps.size(); ++ti)
    {
        double deltaT = timestamps[ti] - timestamps[ti-1];
        for(size_t i = 0; i < dimOfTraj; ++i)
        {
            values.at(i).push_back((*fapprox.at(startId + i))(canVals[ti-1]).at(0));
        }
        canVals.push_back(canSys->step(canVals[ti-1], deltaT));
    }

    for(size_t i = 0; i < dimOfTraj; ++i)
    {
        resTraj.addDimension(timestamps, values.at(i));
    }

    return resTraj;
}

void DMP::ProMP::updateWeights()
{
    for(size_t i = 0; i < viaPointSet->getNum(); ++i)
    {
        // update weights according to the via-point
        SimpleViaPointPtr viaPoint = viaPointSet->viaPoints.at(i);
        double xstar = viaPoint->canVal;
        vec ypos = viaPoint->pos;
        vec yvel = viaPoint->vel;


    }
}

DMP::Vec<DMP::DMPState> DMP::ProMP::step(double& canVal, double deltaT)
{
    DMP::Vec<DMP::DMPState> res;
    for(size_t i = 0; i < dimOfTraj; ++i)
    {
       DMP::DMPState cres;
       cres.pos = (*fapprox.at(i))(canVal).at(0);
       cres.vel = (*fapprox.at(dimOfTraj+i))(canVal).at(0);
       res.push_back(cres);
    }
    canVal = canSys->step(canVal, deltaT);
    return res;
}

void DMP::ProMP::reset()
{
    posWmu = posLWmu;
    posWsigma = posLWsigma;
    velWmu = velLWmu;
    velWsigma = velLWsigma;
}

void DMP::ProMP::_trainFunctionApproximator(unsigned int funcID, const DMP::DoubleMap &forceSamples)
{
    LWRPtr approximator = _getFunctionApproximator(funcID);
    DVec timestamps;
    DVec values;
    for (auto it = forceSamples.begin(); it != forceSamples.end(); ++it)
    {
        timestamps.push_back(it->first);
        values.push_back(it->second);
    }
    approximator->learn(timestamps, values);
}

DMP::LWRPtr DMP::ProMP::_getFunctionApproximator(unsigned int trajDim)
{
    if (trajDim >= fapprox.size()
            || !fapprox.at(trajDim))
    {
        Vec<LWRPtr>::size_type newSize = trajDim + 1;
        newSize = std::max(newSize, fapprox.size());
        fapprox.resize(newSize);
        fapprox.at(trajDim).reset(new LWR<GaussKernel>(kerSize));
    }

    return fapprox.at(trajDim);
}

DMP::DoubleMap DMP::ProMP::createDoubleMap(const DMP::DVec &canonicalVals, const DMP::DVec &sampleVec)
{
    DoubleMap result;
    for(size_t i = 0; i < canonicalVals.size(); ++i)
    {
        result[canonicalVals[i]] = sampleVec[i];
    }
    return result;
}
