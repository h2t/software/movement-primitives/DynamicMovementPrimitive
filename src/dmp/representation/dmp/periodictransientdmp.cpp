/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "periodictransientdmp.h"
#include <dmp/functionapproximation/locallyweightedlearning.h>
#include <algorithm>

#include <dmp/general/helpers.h>
#include <dmp/representation/trajectory.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_roots.h>
#include <gsl/gsl_multimin.h>

#define EPSABS_DMP 0.0001

using namespace DMP;


TransientBasisFunctionsDescription::TransientBasisFunctionsDescription(
    double T, double phi0, double r0,
    double mu1, double mu2,
    const Vec< DVec > &pj, const DVec& deltaj,
    const DVec& tj, double tf)
{
    m_T = T;
    m_phi_0 = phi0;
    m_r0 = r0;
    m_mu1 = mu1;
    m_mu2 = mu2;
    m_pj = pj;
    m_deltaj = deltaj;
    m_tj = tj;
    m_N = pj.size();
    m_tf = tf;
    cout << "Kons Trnsient\n";
    cout << m_T << endl;
    cout << m_phi_0 << endl;
    cout << m_r0 << endl;
    cout << m_mu1 << endl;
    cout << m_mu2 << endl;
    cout << m_pj << endl;
    cout << m_deltaj << endl;
    cout << m_tj << endl;
    cout << m_N << endl;
    cout << m_tf << endl;
    cout << "\n";
}

Vec< LWLPhaseSpaceBasisFunction* > TransientBasisFunctionsDescription::getFunctionList() const
{
    Vec< LWLPhaseSpaceBasisFunction* > res;

    for (unsigned int i = 0; i < N(); ++i)
    {
        DVec center(2);
        center = pol2cart(m_pj[i]);
        res.push_back(new JDistanceAndSeparationBasedPhaseSpaceBasisFunction(
                          center, m_deltaj[i], m_mu1, m_mu2
                      ));
    }

    return res;
}

PeriodicBasisFunctionsDescription::PeriodicBasisFunctionsDescription(
    unsigned int N, double mu, double mu1, double mu2,
    double width, unsigned int k)
{
    m_N = N;
    m_mu = mu;
    m_mu1 = mu1;
    m_mu2 = mu2;
    m_width = width;
    m_phiOffset = 0;
    m_k = k;
}

Vec< LWLPhaseSpaceBasisFunction* > PeriodicBasisFunctionsDescription::getFunctionList() const
{
    Vec< LWLPhaseSpaceBasisFunction* > res;

    for (unsigned int i = 0; i < N(); ++i)
    {
        double center = 2 * M_PI / N() * i;
        res.push_back(new JGAMSCombinedWithGaussAndIndicator(center, m_width,
                      m_mu, m_mu1, m_mu2, m_k));
    }

    return res;
}
//TODO structs missing

TransientBasisFunctionsDescription PeriodicTransientDMP::findSupportCentersAndDistancesForPsii(double T, int unsigned K,
        unsigned int M, double phi1)
{
    double mu = m_basisOSC.getMu(),
           mu1 = m_basisOSC.getMu1(),
           mu2 = 1.2 * mu1, // TODO: is that a good standard value?
           Omega = m_basisOSC.getOmega();

    if (T == 0 || M == 0)
    {
        Vec< DVec > pj;
        DVec deltaj;
        DVec tj;
        return TransientBasisFunctionsDescription(T, 0.0, mu, mu1, mu2, pj, deltaj, tj, 0.0);
    }

    double delta_t = T / (M - 1);
    double tf = delta_t;
    //  cout << "    T=" << T << endl;
    //  cout << "    M=" << M << endl;
    //  cout << "    delta_t= " << delta_t << endl;
    //  cout << "    tf=" << tf << endl;

    // find r0
    DVec init;
    init.resize(2);
    m_basisOSC.getInitialConditionsForTransientLength(T, phi1, init);
    //  cout << "phi0=" << init[0] << ", r0=" << init[1] << endl;
    //  cout << "phi1=" << phi1 << endl;

    //  cout << "    r(T)=" << explicitSolutionForR(T, init) << endl;

    DVec init_tmp;
    init_tmp.resize(2);
    m_basisOSC.getInitialConditionsForTransientLength(tf, 0.0, init_tmp);
    mu2 = init_tmp[1];
    //  cout << "mu2=" << mu2 << endl;

    //  double delta_m1 = (mu2-mu1)*delta;
    //  cout << "delta_m1=" << delta_m1 << endl;

    // find p_m1
    //  DVec p_m1;
    //  integrate (T, init, p_m1);
    //  cout << "p_m1=" << p_m1 << endl;

    // find q
    //  double t_lo = 0, t_hi = T;
    //  gsl_function F;
    //  F.function = find_q_target;
    //  find_q_params params = {this, init, p_m1, delta_m1};
    //  F.params = &params;
    //  gsl_root_fsolver *s = gsl_root_fsolver_alloc(gsl_root_fsolver_bisection);
    //  gsl_root_fsolver_set (s, &F, t_lo, t_hi);

    //  for (int i=0; i<100; ++i) {
    //      gsl_root_fsolver_iterate (s);
    //      int status = gsl_root_test_interval (t_lo, t_hi, 1e-20, 1e-20);
    //      if (status == GSL_SUCCESS)
    //          break;
    //  }
    //  double tq = gsl_root_fsolver_root (s);
    //  gsl_root_fsolver_free (s);
    //  cout << "tq=" << tq << endl;

    //  DVec q(2);
    //  integrate (tq, init, q);
    //  cout << "Q = " << q << endl;

    //  cout << "    K=" << K << endl;


    // OLD (BUT WELL-WORKING) WAY
    //      cout << "K=" << K << endl;
    //      double delta_t = (T - tq)/K;
    //      cout << "delta_t= " << delta_t << endl;
    //      unsigned int M = ceil(T/delta_t);
    //      cout << "M=" << M << endl;

    // calculate tj
    DVec tj;
    tj.resize(M + K);

    //  tj[0] = 0;
    for (unsigned int i = 0; i < M + K; ++i)
    {
        tj[i] = i * delta_t;
    }

    //cout << "tj=" << tj << endl;

    // find pj
    Vec< DVec > pj, cpj;
    m_basisOSC.integrate(tj, init, pj);
    //  cout << "pj=" << pj << endl;

    for (unsigned int j = 0; j < pj.size(); ++j)
    {
        cpj.push_back(pol2cart(pj[j]));
    }

    //  cout << "cpj=" << cpj << endl;

    // TODO: calculate correct deltas by 1/2*delta_t

    // find deltaj
    DVec deltaj;
    deltaj.resize(M);

    for (unsigned int j = 0; j < M; ++j)
    {
        //unsigned int k = min(M-1, (unsigned int) ceil(j+K));
        unsigned int k = j + K;
        DVec diff;
        diff.resize(2);
        diff = pol2cart(pj[j]) - pol2cart(pj[k]);
        double dj = sqrt(diff[0] * diff[0] + diff[1] * diff[1]);

        //          if (pj[j][1] - dj < mu1)
        //              dj = pj[j][1] - mu1;

        //if (dj < delta_m1)
        //  dj = delta_m1;

        // DEBUG
        deltaj[j] = dj;
    }

    //deltaj[M-1] = delta_m1;

    // delete unneccessary K points/times
    for (unsigned int i = 0; i < K; ++i)
    {
        tj.pop_back();
        pj.pop_back();
    }

    //  cout << "tj=" << tj << endl;
    //  cout << "pj=" << pj << endl;
    //  cout << "deltaj=" << deltaj << endl;

    return TransientBasisFunctionsDescription(T, init[0], init[1], mu1, mu2, pj, deltaj, tj, tf);
}



//double m_alpha, m_beta, m_eta;
//double m_Omega, m_mu, m_mu1;

ostream& DMP::operator << (ostream& out, const DMP::PeriodicDiscreteCanonicalSystem& osc)
{
    out << "DMP2DCanonicalSystem (" << osc.m_Omega << ", " << osc.m_alpha << ", " << osc.m_beta << ", "
        << osc.m_eta << ", " << osc.m_mu << ", " << osc.m_mu1 << ")";
    return out;
}



PeriodicTransientDMP::PeriodicTransientDMP() : BasicDMP()
{
}

PeriodicTransientDMP::PeriodicTransientDMP(const PeriodicDiscreteCanonicalSystem& basisOSC, unsigned int output_dim, unsigned int input_dim,
        double alpha_z, double D, double tau)
    : BasicDMP(D, tau)
    //      ,m_todo_phi0(100), m_todo_r0(100)
{
    m_g.assign(input_dim, 0.0);
    m_alphaZ = alpha_z; // 8
    m_betaZ = m_alphaZ / 4;
    m_basisOSC = basisOSC;
    m_output_dim = output_dim;
    if (input_dim == 0)
        m_input_dim = output_dim;
    else
        m_input_dim = input_dim;
    ODE::setDim(2 + 2 * output_dim);
    ODE::setEpsabs(EPSABS_DMP);
    m_perturbationApproximation.assign(output_dim, NULL);
}

PeriodicTransientDMP::~PeriodicTransientDMP()
{
    for (unsigned int i = 0; i < m_output_dim; ++i)
    {
        delete m_perturbationApproximation[i];
    }
}

void PeriodicTransientDMP::perturbation(double t, const DVec& x, DVec& force_out)
{
    double phi = x[0],
           r = x[1];

    for (unsigned int i = 0; i < m_input_dim; ++i)
    {
        force_out[i] = (*m_perturbationApproximation[i])(phi, r);
    }

    /*double phi = x[0],
                    r = x[1];

    //        double fading = exp(0.2*(-r+m_basisOSC.mu1()));
    //        double fading = (r-m_chosen_r0)/(m_basisOSC.mu1()-m_chosen_r0);
            double fading = linearInterpolation(m_chosen_r0,0, m_basisOSC.mu1(),1, r);
            fading = max(0.0, min(1.0, pow(fading, 2)));

            for (unsigned int i=0; i<inputDim(); ++i)
                force_out[i] += force_out[i]*fading*(m_amplitude_scaling[i]-1);*/
}

void PeriodicTransientDMP::getPerturbationForceSamples(const SampledTrajectory& input_trajectory,
        Vec< DVec > &ftarg, unsigned int smooth)
{
    unsigned int N = input_trajectory.size(),
                 dim = input_trajectory[0].size() - 1;

    if (m_input_dim != dim)
    {
        cout << "ERROR: DMPTransientPeriodic::getPerturbationForceSamples: dimension missmatch!" << " " << m_input_dim << " " << dim << endl;
        exit(1);
    }

    SampledTrajectory yd, ydp, ydpp;
    input_trajectory.getPositions(yd);
    input_trajectory.getVelocities(ydp);
    input_trajectory.getAccelerations(ydpp);

    double Omega = m_basisOSC.getOmega();

    ftarg.resize(N);

    for (unsigned int i = 0; i < N; ++i)
    {
        ftarg[i].resize(dim);

        for (unsigned int j = 0; j < m_input_dim; ++j)
        {
            ftarg[i][j] = ydpp[i][1 + j] / (Omega * Omega) - m_alphaZ * (m_betaZ * (m_g[j] - yd[i][1 + j]) - ydp[i][1 + j] / Omega);
        }
    }

    Vec< DVec > ftarg_tmp = ftarg;

    for (unsigned int s = 0; s < smooth; ++s)
    {
        for (unsigned int j = 0; j < dim; ++j)
        {
            ftarg_tmp[0][j] = (2 * ftarg[0][j] + ftarg[1][j]) / 3.0;
            ftarg_tmp[ftarg.size() - 1][j] = (2 * ftarg[ftarg.size() - 1][j] + ftarg[ftarg.size() - 2][j]) / 3.0;

            for (unsigned int i = 1; i < ftarg.size() - 1; ++i)
            {
                ftarg_tmp[i][j] = (ftarg[i - 1][j] + 2 * ftarg[i][j] + ftarg[i + 1][j]) / 4.0;
            }
        }

        ftarg = ftarg_tmp;
    }
}

void PeriodicTransientDMP::getPerturbationForceSamplesV2(const SampledTrajectoryV2& input_trajectory,
        Vec< DVec > &ftarg, unsigned int smooth)
{
    unsigned int N = input_trajectory.size(),
                 dim = input_trajectory.dim();

    if (m_input_dim != dim)
    {
        cout << "ERROR: DMPTransientPeriodic::getPerturbationForceSamples: dimension missmatch!" << endl;
        exit(1);
    }
    //TODO change
    //SampledTrajectoryV2 yd, ydp, ydpp;
    //input_trajectory.getPositions(yd);
    //input_trajectory.getVelocities(ydp);
    //input_trajectory.getAccelerations(ydpp);

    double Omega = m_basisOSC.getOmega();

    ftarg.resize(N);



    for (unsigned int i = 0; i < N; ++i)
        ftarg[i].resize(dim);

    for (unsigned int j = 0; j < m_input_dim; ++j)
    {
        SampledTrajectoryV2::ordered_view& ordv = ((SampledTrajectoryV2)input_trajectory).data().get<SampledTrajectoryV2::TagOrdered>();
        typename SampledTrajectoryV2::ordered_view::const_iterator itMap = ordv.begin();
        int i = 0;
        for (; itMap != ordv.end(); itMap++)
        {
            //DVecPtr dataPtr = itMap->data[j];
            double yd = itMap->getDeriv(j, 0);
            double ydp = itMap->getDeriv(j, 1);
            double ydpp = itMap->getDeriv(j, 2);
            ftarg[i][j] = ydpp / (Omega * Omega) - m_alphaZ * (m_betaZ * (m_g[j] - yd) - ydp / Omega);
            i++;
        }

    }

    Vec< DVec > ftarg_tmp = ftarg;

    for (unsigned int s = 0; s < smooth; ++s)
    {
        for (unsigned int j = 0; j < dim; ++j)
        {
            ftarg_tmp[0][j] = (2 * ftarg[0][j] + ftarg[1][j]) / 3.0;
            ftarg_tmp[ftarg.size() - 1][j] = (2 * ftarg[ftarg.size() - 1][j] + ftarg[ftarg.size() - 2][j]) / 3.0;

            for (unsigned int i = 1; i < ftarg.size() - 1; ++i)
            {
                ftarg_tmp[i][j] = (ftarg[i - 1][j] + 2 * ftarg[i][j] + ftarg[i + 1][j]) / 4.0;
            }
        }

        ftarg = ftarg_tmp;
    }
}
void PeriodicTransientDMP::flow(double t, const DVec& x, DVec& out)
{
    double phi = x[0],
           r = x[1];
    double Omega = m_basisOSC.getOmega();

    DVec z, y;
    z.resize(m_input_dim);
    y.resize(m_input_dim);

    for (unsigned int i = 0; i < m_input_dim; ++i)
    {
        z[i] = x[2 + 2 * i];
        y[i] = x[3 + 2 * i];
    }

    DVec canonical_state;
    canonical_state.resize(2);
    canonical_state[0] = phi;
    canonical_state[1] = r;

    m_basisOSC.flow(t, canonical_state, out);

    DVec perturbation_force;
    perturbation_force.resize(m_input_dim);
    perturbation(t, x, perturbation_force);

    for (unsigned int i = 0; i < m_input_dim; ++i)
    {
        out[2 + 2 * i] = Omega * (m_alphaZ * (m_betaZ * (m_g[i] - y[i]) - z[i]) + perturbation_force[i]);
        out[3 + 2 * i] = Omega * z[i];
    }
}

unsigned int PeriodicTransientDMP::dimValuesOfInterest() const
{
    return m_output_dim;
}

void PeriodicTransientDMP::projectStateToValuesOfInterest(const double* x, DVec& projection) const
{
    //for (unsigned int i = 0; i < m_input_dim; ++i)
    //{
    //    projection[i] = x[3 + 2 * i];
    //}
    for (unsigned int i = 0; i < m_output_dim; ++i)
    {
        projection[i] = x[2 * i];
    }
}


bool compByPhi1(const SampledComposedPeriodicTrajectory& t1, const SampledComposedPeriodicTrajectory& t2)
{
    return t1.phi1() < t2.phi1();
}

struct find_phi1_min_params
{
    double c, max_T, Omega, *lower_bounds, *upper_bounds, lambda;
    Vec< SampledComposedPeriodicTrajectory > *trajectories;
};


// TODO: neither working nor done...
void encodeParam(const gsl_vector* v, double* new_v, double* lb, double* ub)
{
    for (unsigned int i = 0; i < v->size; ++i)
    {
        new_v[i] = -log((ub[i] - gsl_vector_get(v, i)) / (gsl_vector_get(v, i) - ub[i]));
    }
}

// TODO: neither working nor done...
inline double decodeValue(double t, double lb, double ub)
{
    return lb + (ub - lb) / (1 + exp(-t));
}

// TODO: neither working nor done...
inline void decodeParam(const gsl_vector* v, double* new_v, double* lb, double* ub)
{
    for (unsigned int i = 0; i < v->size; ++i)
    {
        new_v[i] = decodeValue(gsl_vector_get(v, i), lb[i], ub[i]);
    }
}

// TODO: neither working nor done...
double find_phi1_min_target(const gsl_vector* v, void* params)
{
    find_phi1_min_params* p = (find_phi1_min_params*) params;
    Vec< SampledComposedPeriodicTrajectory > *trajectories = p->trajectories;

    unsigned int dim = trajectories->size();

    double* new_v = new double[dim];
    decodeParam(v, new_v, p->lower_bounds, p->upper_bounds);

    double res = 0.0;

    for (unsigned int j = 0; j < dim - 1; ++j)
        res += abs(trajectories->at(j + 1).phi1() + p->Omega * new_v[j + 1]
                   - (trajectories->at(j).phi1() + p->Omega * new_v[j]) - p->c);

    res += abs(2 * M_PI + trajectories->at(0).phi1() + p->Omega * new_v[0]
               - (trajectories->at(dim - 1).phi1() + p->Omega * new_v[dim - 1]) - p->c);

    double penalty = 0.0;

    for (unsigned int j = 0; j < dim; ++j)
    {
        penalty += new_v[j] * trajectories->at(j).T();
    }

    delete[] new_v;

    return res + p->lambda * penalty / p->max_T;
}

// TODO: neither working nor done...
void PeriodicTransientDMP::reorderAndOptimizePhi1AndT(Vec< SampledComposedPeriodicTrajectory > &trajectories) const
{
    double max_T = 0.0;

    for (unsigned int i = 0; i < trajectories.size(); ++i)
    {
        trajectories[i].setPhi1(fmod(trajectories[i].phi1(), 2 * M_PI));
        max_T = std::max(trajectories.at(i).T(), max_T);
    }

    sort(trajectories.begin(), trajectories.end(), compByPhi1);

    size_t dim = trajectories.size();

    const gsl_multimin_fminimizer_type* T = gsl_multimin_fminimizer_nmsimplex2;
    gsl_multimin_fminimizer* s = NULL;
    gsl_vector* ss, *x;
    gsl_multimin_function minex_func;

    size_t iter = 0;
    int status;
    double size;

    /* Starting point */
    x = gsl_vector_alloc(dim);
    gsl_vector_set_all(x, 0.0);

    /* Set initial step sizes to 1 */
    ss = gsl_vector_alloc(dim);
    gsl_vector_set_all(ss, 1.0);

    /* Initialize method and iterate */
    minex_func.n = dim;
    minex_func.f = find_phi1_min_target;

    find_phi1_min_params params;
    params.c = 2 * M_PI / dim;
    params.Omega = m_basisOSC.getOmega();
    params.max_T = max_T;
    params.trajectories = &trajectories;
    params.lower_bounds = new double[dim];
    params.upper_bounds = new double[dim];
    params.lambda = 1;

    for (unsigned int i = 0; i < dim; ++i)
    {
        params.lower_bounds[i] = 0.0;
        params.upper_bounds[i] = (1 + 0.3) * max_T - trajectories.at(i).T(); // TODO: magic constant
    }

    minex_func.params = &params;

    s = gsl_multimin_fminimizer_alloc(T, dim);
    gsl_multimin_fminimizer_set(s, &minex_func, x, ss);

    do
    {
        iter++;
        status = gsl_multimin_fminimizer_iterate(s);

        if (status)
        {
            break;
        }

        size = gsl_multimin_fminimizer_size(s);
        status = gsl_multimin_test_size(size, 1e-2);

        if (status == GSL_SUCCESS)
        {
            printf("converged to minimum at:\n");

            for (unsigned int i = 0; i < dim; ++i)
            {
                cout << "  " << gsl_vector_get(s->x, i);
            }

            cout << endl;
        }

        //        cout << "size:" << size << ", current min: " << gsl_multimin_fminimizer_minimum(s) << endl;
    }
    while (status == GSL_CONTINUE && iter < 1000);

    double* delta_T = new double[dim];
    double* PHI1 = new double[dim];

    for (unsigned int i = 0; i < dim; ++i)
    {
        delta_T[i] = decodeValue(gsl_vector_get(s->x, i),
                                 params.lower_bounds[i], params.upper_bounds[i]);
        PHI1[i] =  fmod(trajectories.at(i).phi1() + params.Omega * delta_T[i], 2 * M_PI);
    }

    cout << "best found after " << iter << " iterations" << endl;

    cout << "delta_T: ";

    for (unsigned int i = 0; i < dim; ++i)
    {
        cout << "  " << delta_T[i];
    }

    cout << endl;


    cout << "PHI1: ";

    for (unsigned int i = 0; i < dim; ++i)
    {
        cout << "  " << PHI1[i];
    }

    cout << endl;

    delete[] delta_T;
    delete[] PHI1;

    delete[] params.upper_bounds;
    delete[] params.lower_bounds;

    gsl_vector_free(x);
    gsl_vector_free(ss);
    gsl_multimin_fminimizer_free(s);
}


// TODO: not working...
void PeriodicTransientDMP::reorderAndOptimizePhi1AndTSimple(Vec< SampledComposedPeriodicTrajectory > &trajectories) const
{
    unsigned int count = trajectories.size();

    //    sort(trajectories.begin(), trajectories.end(), compByPhi1);

    unsigned int index_of_longest_transient;
    double longest_transient = 0.0;
    double phi1_of_longest_transient = 0.0;

    for (unsigned int i = 0; i < count; ++i)
    {
        if (trajectories.at(i).T() > longest_transient)
        {
            index_of_longest_transient = i;
            longest_transient = trajectories.at(i).T();
            phi1_of_longest_transient = trajectories.at(i).phi1();
        }
    }

    cout << "index_of_longest_transient: " << index_of_longest_transient << endl;

    for (unsigned int i = 0; i < count; ++i)
    {
        trajectories.at(i).setPhi1(trajectories.at(i).phi1() - phi1_of_longest_transient);
    }

    unsigned int i = 0; //index_of_longest_transient;

    for (unsigned int runs = 0; runs < count; ++runs)
    {
        double phi1 = 2 * M_PI / count * runs;
        SampledComposedPeriodicTrajectory& traj = trajectories[i];
        //        cout << "d_phi1: " << fmod(phi1 - traj.phi1(), 2*M_PI) << endl;
        double d_phi1 = fmod(200 * M_PI + traj.phi1(), 2 * M_PI);
        cout << "wished phi1: " << phi1 << ", true phi1: " << d_phi1 << endl;
        //        cout << "d_T: " << d_T << endl;

        //        double d_T = fmod(200*M_PI+phi1-traj.phi1(), 2*M_PI)/m_basisOSC.Omega();
        //        traj.setT(traj.T()+d_T);
        //        traj.setPhi1(phi1);

        i = (i + 1) % count;
    }

    int a = 0;
    //    cout << 1/a;
}


void PeriodicTransientDMP::learnFromTrajectories(
    const Vec< const SampledComposedPeriodicTrajectory* > &input_trajectories,
    unsigned int M_per_time, unsigned int N)
{
    Vec< unsigned int > Ms;
    Ms.resize(input_trajectories.size());

    for (unsigned int i = 0; i < input_trajectories.size(); ++i)
    {
        Ms[i] = ceil(M_per_time * input_trajectories.at(i)->T());
        //        cout << "Ms[" << i << "] = " << Ms[i] << endl;
    }
    printf("++++++++++++++++++++++++++Before norn\n");
    return learnFromTrajectories(input_trajectories, Ms, N);
}


void PeriodicTransientDMP::learnFromTrajectories(
    const Vec< const SampledComposedPeriodicTrajectory* > &input_trajectories,
    const Vec<unsigned int> &Ms, unsigned int N)
{

    unsigned int traj_count = input_trajectories.size();

    Vec < SampledComposedPeriodicTrajectory > trajectories;

    for (unsigned int i = 0; i < traj_count; ++i)
    {
        trajectories.push_back(*input_trajectories[i]);
    }

    trajectories[0].setPhi1(0.0);
    trajectories[0].shiftTime(trajectories[0].T());

    cout << "Aligning trajectories... ";
    flush(cout);
    #pragma omp parallel for

    for (unsigned int i = 1; i < traj_count; ++i)
    {
        cout << "Aligning trajectory " << i << endl;
        trajectories[i].align(trajectories[0]);
        trajectories[i].setPhi1((trajectories[i][0][0] + trajectories[i].T())*m_basisOSC.getOmega());
    }

    cout << "done" << endl;
    flush(cout);

    //    reorderAndOptimizePhi1AndTSimple(trajectories);
    // DEBUG
    //    exit(11);

    unsigned int all_samples_count = 0;
    Vec< unsigned int > previous_sample_count_for_traj(traj_count);

    for (unsigned int i = 0; i < traj_count; ++i)
    {
        previous_sample_count_for_traj[i] = all_samples_count;
        all_samples_count += trajectories[i].size();
    }
    cout << "all samples " << all_samples_count  << endl;
    flush(cout);
    Vec< DVec > ftarg_all;
    ftarg_all.resize(all_samples_count);
    Vec< Vec< LWLPhaseSpaceBasisFunction* > > basis_functions_all;
    Vec< DVec > phi_r_samples_all;
    phi_r_samples_all.resize(all_samples_count);

    Vec< TransientBasisFunctionsDescription > transients_basis_function_descs(traj_count, TransientBasisFunctionsDescription());
    Vec< DVec > initial_conditions_all;
    initial_conditions_all.resize(traj_count);
    cout << "Transients reset "  << endl;
    flush(cout);
    m_g.assign(m_input_dim, 0.0);

    for (unsigned int i = 0; i < traj_count; ++i)
    {
        cout << trajectories[i].anchorPoint()  << endl;
        m_g += trajectories[i].anchorPoint();
    }

    m_g /= traj_count;
    cout << "Anchor point calculated " << endl;
    flush(cout);
    for (unsigned int i = 0; i < m_output_dim; ++i)
    {
        basis_functions_all.push_back(Vec< LWLPhaseSpaceBasisFunction* >());
    }

    m_original_osc_initial_conditions.resize(traj_count);

    cout << "Collecting learning information... ";
    flush(cout);
    #pragma omp parallel for

    for (unsigned int i = 0; i < traj_count; ++i)
    {
        const SampledComposedPeriodicTrajectory* input_trajectory = &trajectories[i];
        unsigned int M = Ms[i];

        double T = input_trajectory->T(),
               phi1 = input_trajectory->phi1();

               cout << "    T=" << T << endl;
                cout << "    phi1=" << phi1 << endl;

        DVec initial_value_osc;
        initial_value_osc.resize(2);
        m_basisOSC.getInitialConditionsForTransientLength(T, phi1, initial_value_osc);

        m_original_osc_initial_conditions[i] = initial_value_osc;

                cout << "    learn phi0=" << initial_value_osc[0] << endl;
                cout << "    learn r0=" << initial_value_osc[1] << endl;

        // TODO: get rid of m_todo_phi0, m_todo_r0
        //        m_todo_phi0[i] = initial_value_osc[0];
        //        m_todo_r0[i] = initial_value_osc[1];

        DVec times;
        input_trajectory->timeSamples(times);

        Vec< DVec > phi_r_samples;
        m_basisOSC.integrate(times, initial_value_osc, phi_r_samples);
        copy(phi_r_samples.begin(), phi_r_samples.end(), phi_r_samples_all.begin() + previous_sample_count_for_traj[i]);

        Vec< DVec > ftarg;
        getPerturbationForceSamples(*input_trajectory, ftarg);
        copy(ftarg.begin(), ftarg.end(), ftarg_all.begin() + previous_sample_count_for_traj[i]);

        // TODO: Should not be constants
        int K = 4;
        double width = 4 * (2 * M_PI / (N - 1)),
               mu = m_basisOSC.getMu();

        transients_basis_function_descs[i] = findSupportCentersAndDistancesForPsii(T, K, M, phi1);
        TransientBasisFunctionsDescription* trans_desc = &transients_basis_function_descs.at(i);
        PeriodicBasisFunctionsDescription per_desc(N, mu, trans_desc->mu1(), trans_desc->mu2(), width);

        DVec initial_conditions;
        initial_conditions.push_back(initial_value_osc[0]);
        initial_conditions.push_back(initial_value_osc[1]);
        initial_conditions_all[i] = initial_conditions;

        for (unsigned int i = 0; i < m_output_dim; ++i)
        {
            Vec< LWLPhaseSpaceBasisFunction* > trans_functions = trans_desc->getFunctionList();
            Vec< LWLPhaseSpaceBasisFunction* > per_functions = per_desc.getFunctionList();
            basis_functions_all[i].insert(basis_functions_all[i].end(), trans_functions.begin(), trans_functions.end());
            basis_functions_all[i].insert(basis_functions_all[i].end(), per_functions.begin(), per_functions.end());
        }
    }

    cout << "done" << endl;
    flush(cout);

    cout << "Calculating weights... ";
    flush(cout);

    //#pragma omp parallel for
    for (unsigned int i = 0; i < m_input_dim; ++i)
    {
        delete m_perturbationApproximation[i];
        m_perturbationApproximation[i] = new DMP2DPerturbationApproximation(basis_functions_all[i]);

        DVec ftarg_i;
        ftarg_i.resize(ftarg_all.size());

        for (unsigned int j = 0; j < ftarg_all.size(); j++)
        {
            ftarg_i[j] = ftarg_all[j][i];
        }

        m_perturbationApproximation[i]->learn(phi_r_samples_all, ftarg_i);
    }

    cout << "done" << endl;
    flush(cout);

    // calculate map of known initial states
    DVec G = m_g;
    m_g = 0 * m_g;

    m_known_initial_states.clear();

    cout << "Creating map of known initial conditions... ";
    flush(cout);
    #pragma omp parallel for

    for (unsigned int i = 0; i < traj_count; ++i)
    {
        DVec initial_conditions = initial_conditions_all[i];
        DVec y0 = trajectories[i].getVelocity(0);
        DVec z0 = trajectories[i].getPosition(0) - G;

        for (unsigned int k = 0; k < z0.size(); ++k)
        {
            initial_conditions.push_back(y0[k]);
            initial_conditions.push_back(z0[k]);
        }

        Vec< DVec > known_states;
        TransientBasisFunctionsDescription* trans_desc = &transients_basis_function_descs.at(i);
        integrate(trans_desc->tj(), initial_conditions, known_states, false);

        m_known_initial_states.push_back(known_states);
    }

    m_g = G;
    cout << "done" << endl;
    flush(cout);
}


void PeriodicTransientDMP::learnFromTrajectories(
    const Vec< const SampledTrajectoryV2* > &input_trajectories,
    const Vec<unsigned int> &Ms, unsigned int N)
{

    unsigned int traj_count = 1;
    //TODO change
    //adress later
    //input_trajectories.size();

    Vec < SampledTrajectoryV2 > trajectories;

    for (unsigned int i = 0; i < traj_count; ++i)
    {
        trajectories.push_back(*input_trajectories[i]);
    }

    //TODO change
    trajectories[0].setPhi1(0.0);
    double phi1 = 0.0;
    trajectories[0].shiftTime(trajectories[0]. getTimeLength());

    cout << "Aligning trajectories... ";
    flush(cout);
    //#pragma omp parallel for

    //TODO change if multpiple transients
    //adress later
    //for (unsigned int i = 1; i < traj_count; ++i)
    //{
    //    trajectories[i].align(trajectories[0]);
    //    trajectories[i].setPhi1((trajectories[i][0][0] + trajectories[i].T())*m_basisOSC.getOmega());
    //}

    cout << "done" << endl;
    flush(cout);

    //    reorderAndOptimizePhi1AndTSimple(trajectories);
    // DEBUG
    //    exit(11);

    unsigned int all_samples_count = 0;
    Vec< unsigned int > previous_sample_count_for_traj(traj_count);
//TODO change
    for (unsigned int i = 0; i < traj_count; ++i)
    {
        previous_sample_count_for_traj[i] = all_samples_count;
        all_samples_count += trajectories[i].size();
    }

    Vec< DVec > ftarg_all;
    ftarg_all.resize(all_samples_count);
    Vec< Vec< LWLPhaseSpaceBasisFunction* > > basis_functions_all;
    Vec< DVec > phi_r_samples_all;
    phi_r_samples_all.resize(all_samples_count);

    Vec< TransientBasisFunctionsDescription > transients_basis_function_descs(traj_count, TransientBasisFunctionsDescription());
    Vec< DVec > initial_conditions_all;
    initial_conditions_all.resize(traj_count);

    m_g.assign(m_input_dim, 0.0);
//TODO change
    for (unsigned int i = 0; i < traj_count; ++i)
    {
        m_g += trajectories[i].getAnchorPoint();
    }

    m_g /= traj_count;

    for (unsigned int i = 0; i < m_output_dim; ++i)
    {
        basis_functions_all.push_back(Vec< LWLPhaseSpaceBasisFunction* >());
    }

    m_original_osc_initial_conditions.resize(traj_count);

    cout << "Collecting learning information... ";
    flush(cout);
    #pragma omp parallel for

    for (unsigned int i = 0; i < traj_count; ++i)
    {
        SampledTrajectoryV2* input_trajectory = &trajectories[i];
        unsigned int M = Ms[i];
        //TODO change
        double T = input_trajectory->getTimeLength();//,
        double phi1 = input_trajectory->getPhi1();

        //        cout << "    T=" << T << endl;
        //        cout << "    phi1=" << phi1 << endl;

        DVec initial_value_osc;
        initial_value_osc.resize(2);
        m_basisOSC.getInitialConditionsForTransientLength(T, phi1, initial_value_osc);

        m_original_osc_initial_conditions[i] = initial_value_osc;

        //        cout << "    learn phi0=" << initial_value_osc[0] << endl;
        //        cout << "    learn r0=" << initial_value_osc[1] << endl;

        // TODO: get rid of m_todo_phi0, m_todo_r0
        //        m_todo_phi0[i] = initial_value_osc[0];
        //        m_todo_r0[i] = initial_value_osc[1];

        DVec times = input_trajectory->getTimestamps();

        Vec< DVec > phi_r_samples;
        m_basisOSC.integrate(times, initial_value_osc, phi_r_samples);
        copy(phi_r_samples.begin(), phi_r_samples.end(), phi_r_samples_all.begin() + previous_sample_count_for_traj[i]);

        Vec< DVec > ftarg;
        getPerturbationForceSamplesV2(*input_trajectory, ftarg);
        copy(ftarg.begin(), ftarg.end(), ftarg_all.begin() + previous_sample_count_for_traj[i]);

        // TODO: Should not be constants
        int K = 4;
        double width = 4 * (2 * M_PI / (N - 1)),
               mu = m_basisOSC.getMu();

        transients_basis_function_descs[i] = findSupportCentersAndDistancesForPsii(T, K, M, phi1);
        TransientBasisFunctionsDescription* trans_desc = &transients_basis_function_descs.at(i);
        PeriodicBasisFunctionsDescription per_desc(N, mu, trans_desc->mu1(), trans_desc->mu2(), width);

        DVec initial_conditions;
        initial_conditions.push_back(initial_value_osc[0]);
        initial_conditions.push_back(initial_value_osc[1]);
        initial_conditions_all[i] = initial_conditions;

        for (unsigned int i = 0; i < m_output_dim; ++i)
        {
            Vec< LWLPhaseSpaceBasisFunction* > trans_functions = trans_desc->getFunctionList();
            Vec< LWLPhaseSpaceBasisFunction* > per_functions = per_desc.getFunctionList();
            basis_functions_all[i].insert(basis_functions_all[i].end(), trans_functions.begin(), trans_functions.end());
            basis_functions_all[i].insert(basis_functions_all[i].end(), per_functions.begin(), per_functions.end());
        }
    }

    cout << "done" << endl;
    flush(cout);

    cout << "Calculating weights... ";
    flush(cout);

    //#pragma omp parallel for
    for (unsigned int i = 0; i < m_output_dim; ++i)
    {
        delete m_perturbationApproximation[i];
        m_perturbationApproximation[i] = new DMP2DPerturbationApproximation(basis_functions_all[i]);

        DVec ftarg_i;
        ftarg_i.resize(ftarg_all.size());

        for (unsigned int j = 0; j < ftarg_all.size(); j++)
        {
            ftarg_i[j] = ftarg_all[j][i];
        }

        m_perturbationApproximation[i]->learn(phi_r_samples_all, ftarg_i);
    }

    cout << "done" << endl;
    flush(cout);

    // calculate map of known initial states
    DVec G = m_g;
    m_g = 0 * m_g;

    m_known_initial_states.clear();

    cout << "Creating map of known initial conditions... ";
    flush(cout);
    #pragma omp parallel for
//TODO change
    for (unsigned int i = 0; i < traj_count; ++i)
    {
        DVec initial_conditions = initial_conditions_all[i];

        DVec y0 = trajectories[i].getStates(0.0, 1);//->begin()->getDeriv(0, 1);
        DVec z0 = trajectories[i].getStates(0.0, 0) - G;

        for (unsigned int k = 0; k < z0.size(); ++k)
        {
            initial_conditions.push_back(y0[k]);
            initial_conditions.push_back(z0[k]);
        }

        Vec< DVec > known_states;
        TransientBasisFunctionsDescription* trans_desc = &transients_basis_function_descs.at(i);
        integrate(trans_desc->tj(), initial_conditions, known_states, false);

        m_known_initial_states.push_back(known_states);
    }

    m_g = G;
    cout << "done" << endl;
    flush(cout);
}


void PeriodicTransientDMP::learnFromTrajectory(
    const SampledComposedPeriodicTrajectory& input_trajectory, unsigned int M, unsigned int N)
{
    Vec< const SampledComposedPeriodicTrajectory* > trajectories;
    Vec< unsigned int > Ms;

    trajectories.push_back(&input_trajectory);
    Ms.push_back(M);

    learnFromTrajectories(trajectories, Ms, N);
}


void PeriodicTransientDMP::learnFromTrajectoryV2(
    const SampledTrajectoryV2& periodic_trajectory, const SampledTrajectoryV2& transient_trajectory, unsigned int M, unsigned int N)
{
    Vec< const SampledTrajectoryV2* > trajectories;
    Vec< unsigned int > Ms;

    trajectories.push_back(&periodic_trajectory);
    trajectories.push_back(&transient_trajectory);
    //ToDo Change
    Ms.push_back(M);
    Ms.push_back(M);

    learnFromTrajectories(trajectories, Ms, N);
}



void PeriodicTransientDMP::findBestTrajectoryAndInitialPhiAndR(const DVec& initial_vel_and_pos,
        unsigned int& transient_index, double& phi0, double& r0)
{
    DVec weights;
    weights.resize(initial_vel_and_pos.size());

    // TODO: how to choose weights?
    for (unsigned int i = 0; i < weights.size(); ++i)
    {
        if (i % 2)
        {
            weights[i] = 100.0;    // position
        }
        else
        {
            weights[i] = 1.0;    // velocitiy
        }
    }

    DVec centered_initial_vel_and_pos = initial_vel_and_pos;

    for (unsigned int i = 0; i < outputDim(); ++i)
    {
        centered_initial_vel_and_pos[1 + 2 * i] -= m_g[i];
    }

    double min_error = INFINITY;

    for (unsigned int i = 0; i < m_known_initial_states.size(); ++i)
    {
        Vec< DVec >& cur_transient_states = m_known_initial_states.at(i);

        for (unsigned int k = 0; k < cur_transient_states.size(); ++k)
        {
            const DVec& cur_state = cur_transient_states.at(k);
            DVec cur_vel_pos;

            cur_vel_pos.assign(cur_state.begin() + 2, cur_state.end());
            double error = weightedNorm2(cur_vel_pos - centered_initial_vel_and_pos, weights);

            if (error < min_error)
            {
                transient_index = i;
                phi0 = cur_state[0];
                r0 = cur_state[1];
                min_error = error;
            }
        }
    }

    cout << "selected transient: " << transient_index << endl;
    cout << "selected phi0: " << phi0 <<  endl; //" (old:" << m_todo_phi0[transient_index] << ")" << endl;
    cout << "selected r0: " << r0 << endl; //"(old:" << m_todo_r0[transient_index] << ")" <<  endl;

    // DEBUG
    //    phi0 = m_todo_phi0[transient_index];
    //    r0 = m_todo_r0[transient_index];
}
void PeriodicTransientDMP::initializePlaying(Vec< DMPState > &state,
        const DVec &initial_vel_and_pos, int transient_nr) {

    DVec z0_y0 = initial_vel_and_pos;

    for(unsigned int i=0; i<m_input_dim; ++i)
        z0_y0[2*i] /= m_basisOSC.getOmega();

    unsigned int transient_index;
    DVec canonical_init(2);

    if (transient_nr < 0)
        findBestTrajectoryAndInitialPhiAndR(initial_vel_and_pos, transient_index, canonical_init[0], canonical_init[1]);
    else
        canonical_init = m_original_osc_initial_conditions[transient_nr];

    DVec initial;
    initial.resize(2+2*m_input_dim);
    initial[0] = canonical_init[0];
    initial[1] = canonical_init[1];
    printf("INPUT_dim %d\n",m_input_dim);
    for(unsigned int i=0; i<m_input_dim; ++i) {
        initial[2+2*i] = z0_y0[2*i];
        initial[3+2*i] = z0_y0[2*i+1];
        state.push_back(DMPState(z0_y0[2*i+1],z0_y0[2*i]));
    }
    m_currentState = initial;

}
void PeriodicTransientDMP::play(const DVec& initial_vel_and_pos, const DVec& times,
                                SampledTrajectory& out, int transient_nr)
{

    //    cout << "++++++++++++++++++++ DMP ++++++++++++++++" << endl;
    //    cout << "initial_vel_pos: " << initial_vel_and_pos << endl;
    //    cout << "m_g: " << m_g << endl;
    //    cout << "times: " << times << endl;

    DVec z0_y0 = initial_vel_and_pos;

    for (unsigned int i = 0; i < m_output_dim; ++i)
    {
        z0_y0[2 * i] /= m_basisOSC.getOmega();
    }

    unsigned int transient_index;
    DVec canonical_init;
    canonical_init.resize(2);

    if (transient_nr < 0)
    {
        findBestTrajectoryAndInitialPhiAndR(initial_vel_and_pos, transient_index, canonical_init[0], canonical_init[1]);
    }
    else
    {
        canonical_init = m_original_osc_initial_conditions[transient_nr];
    }

    double T = m_basisOSC.getTransientLengthForInitialR(canonical_init[1]);
    double phi1 = m_basisOSC.explicitSolutionForPhi(T, canonical_init);
    //  cout << "Expected transient duration T=" << T << endl;
    //  cout << "Expected entrance phase phi1=" << phi1 << endl;

    DVec initial;
    initial.resize(2 + 2 * m_output_dim);
    initial[0] = canonical_init[0];
    initial[1] = canonical_init[1];

    for (unsigned int i = 0; i < m_output_dim; ++i)
    {
        initial[2 + 2 * i] = z0_y0[2 * i];
        initial[3 + 2 * i] = z0_y0[2 * i + 1];
    }

    Vec< DVec > tmp_traj;
    integrate(times, initial, tmp_traj);

    for (unsigned int i = 0; i < tmp_traj.size(); ++i)
    {
        DVec sample;
        sample.push_back(times[i]);
        sample.insert(sample.end(), tmp_traj[i].begin(), tmp_traj[i].end());
        out.push_back(sample);
    }
}
void PeriodicTransientDMP::playV2(const DVec& initial_vel_and_pos, const DVec& times,
                                SampledTrajectoryV2& out, int transient_nr)
{

    //    cout << "++++++++++++++++++++ DMP ++++++++++++++++" << endl;
    //    cout << "initial_vel_pos: " << initial_vel_and_pos << endl;
    //    cout << "m_g: " << m_g << endl;
    //    cout << "times: " << times << endl;
/*
    DVec z0_y0 = initial_vel_and_pos;

    for (unsigned int i = 0; i < m_input_dim; ++i)
    {
        z0_y0[2 * i] /= m_basisOSC.getOmega();
    }

    unsigned int transient_index;

    DVec canonical_init;
    canonical_init.resize(2);

    if (transient_nr < 0)
    {
        findBestTrajectoryAndInitialPhiAndR(initial_vel_and_pos, transient_index, canonical_init[0], canonical_init[1]);
    }
    else
    {
        canonical_init = m_original_osc_initial_conditions[transient_nr];
    }

    //double T = m_basisOSC.getTransientLengthForInitialR(canonical_init[1]);
    //double phi1 = m_basisOSC.explicitSolutionForPhi(T, canonical_init);
    //  cout << "Expected transient duration T=" << T << endl;
    //  cout << "Expected entrance phase phi1=" << phi1 << endl;

    DVec initial;
    initial.resize(2 + 2 * m_input_dim);
    initial[0] = canonical_init[0];
    initial[1] = canonical_init[1];

    for (unsigned int i = 0; i < m_input_dim; ++i)
    {
        initial[2 + 2 * i] = z0_y0[2 * i];
        initial[3 + 2 * i] = z0_y0[2 * i + 1];
    }

*/
    Vec< DVec > tmp_traj;
    //DVec testVec;
    //for (int k = 0; k < 6; k++)
    //    testVec.push_back(1.0);
    //tmp_traj.push_back(testVec);

    integrate(times, m_currentState, tmp_traj,false);

    for (unsigned int i = 0; i < m_input_dim; ++i)
        {

            for (unsigned int j = 1; j < tmp_traj.size(); ++j)
            {
                DVec derivations;
                derivations.push_back(tmp_traj[j][2+2*i+1]);
                derivations.push_back(tmp_traj[j][2+2*i]);
                out.addDerivationsToDimension(i,times[j],derivations);
            }

        }
    for (unsigned int i = 0; i < m_currentState.size(); ++i)
        m_currentState[i] = tmp_traj[tmp_traj.size()-1][i];
}

//Vec< Vec< DVec > > m_known_initial_states;

// TODO: works for 2D only
ostream& DMP::operator << (ostream& out, const PeriodicTransientDMP& dmp)
{
    out << ";==========dmp==========" << endl;
    out << "odeDim: " << dmp.dim() << endl;
    out << "odeEpsabs: " << dmp.epsabs() << endl;
    out << "alphaZ: " << dmp.m_alphaZ << endl;
    out << "betaZ: " << dmp.m_betaZ << endl;
    out << "outputDim: " << dmp.m_output_dim << endl;
    out << "transientCount: " << dmp.m_known_initial_states.size() << endl;

    out << endl;

    out << ";Anchor Point" << endl;
    out << dmp.m_g << endl << endl;

    out << ";BasisOSC" << endl;
    out << dmp.m_basisOSC << endl << endl;

    out << ";PerturbationApproximation" << endl;

    for (unsigned int i = 0; i < dmp.m_output_dim; ++i)
    {
        out << *dmp.m_perturbationApproximation.at(i) << endl;
    }

    out << endl;

    out << ";KnownInitialStates" << endl;

    for (unsigned int i = 0; i < dmp.m_known_initial_states.size(); ++i)
    {
        out << "count: " << dmp.m_known_initial_states.at(i).size() << endl;

        for (unsigned int j = 0; j < dmp.m_known_initial_states.at(i).size(); ++j)
        {
            out << dmp.m_known_initial_states.at(i).at(j) << endl;
        }
    }
    return out;
}


void PeriodicTransientDMP::writeToFile(const char* filename) const
{
    ofstream f;
    f.open(filename);
    f << *this;
    f.close();
}

void PeriodicTransientDMP::readFromFile(const char* filename)
{
#ifdef _WIN32
    system("cd");
#endif
    FILE* fp = fopen(filename, "r");

    if (fp == 0)
    {
        std::cerr << "File [" << filename << "] not found!" << std::endl;
        std::cout << "File [" << filename << "] not found!" << std::endl;
        return;
    }

    unsigned int transient_count = 0;
    char buf[J_LINE_BUF_SIZE];

    while (!feof(fp))
    {
        getNextLine(fp, buf);

        if (strcmp(buf, ";==========dmp==========\n") == 0)
        {
            unsigned int ode_dim;
            fscanf(fp, "odeDim: %d\n", &ode_dim);
            setDim(ode_dim);

            double epsabs;
            fscanf(fp, "odeEpsabs: %lf\n", &epsabs);
            setEpsabs(epsabs);

            fscanf(fp, "alphaZ: %lf\n", &m_alphaZ);
            fscanf(fp, "betaZ: %lf\n", &m_betaZ);
            fscanf(fp, "outputDim: %d\n", &m_output_dim);
            fscanf(fp, "transientCount: %d\n", &transient_count);
            m_known_initial_states.resize(transient_count);
        }
        else if (strcmp(buf, ";Anchor Point\n") == 0)
        {
            m_g = readVectorFromFile(fp);
        }
        else if (strcmp(buf, ";BasisOSC\n") == 0)
        {
            double alpha, beta, eta, Omega, mu, mu1;
            fscanf(fp, "DMP2DCanonicalSystem (%lf, %lf, %lf, %lf, %lf, %lf)\n",
                   &Omega, &alpha, &beta, &eta, &mu, &mu1);
            m_basisOSC = PeriodicDiscreteCanonicalSystem(Omega, alpha, beta, eta, mu, mu1);
        }
        else if (strcmp(buf, ";PerturbationApproximation\n") == 0)
        {
            m_perturbationApproximation.clear();
            m_perturbationApproximation.resize(m_output_dim);

            for (unsigned int i = 0; i < m_output_dim; ++i)
            {
                DMP2DPerturbationApproximation* appr = new DMP2DPerturbationApproximation;
                appr->readFromFile(fp);
                m_perturbationApproximation[i] = appr;
            }
        }
        else if (strcmp(buf, ";KnownInitialStates\n") == 0)
        {
            for (unsigned int i = 0; i < transient_count; ++i)
            {
                getNextLine(fp, buf);
                unsigned int count;
                sscanf(buf, "count: %d\n", &count);

                m_known_initial_states[i].resize(count);

                for (unsigned int j = 0; j < count; ++j)
                {
                    getNextLine(fp, buf);
                    extractVectorFromString(buf, m_known_initial_states[i][j]);
                }
            }
        }

    }
}

Vec<DMPState> PeriodicTransientDMP::calculateTrajectoryPoint(double t, const DVec& goal, double tCurrent, const Vec<DMPState> &currentStates, DVec& canonicalValues, double temporalFactor)
{
    this->m_g = goal;
    if (firstIteration)
    {
        firstIteration = false;
        DVec initialPosVel;
        Vec<DMPState> initStates;
        initialPosVel.resize(2*currentStates.size());
        for (int i = 0; i < currentStates.size(); i++)
        {
           initialPosVel[i*2] = currentStates[i].vel;
           initialPosVel[i*2+1] = currentStates[i].pos;
        }
           int transient_to_use = 0;
        initializePlaying(initStates, initialPosVel, transient_to_use);

    }

    Vec<DMPState> result;// = SystemStatus::convertArrayToStates<DMPState>(rawResult);
    SampledTrajectoryV2 output;

    DVec current_pos_vel;
    current_pos_vel.resize(currentStates.size()*2);
    for (int i = 0; i < currentStates.size(); i++)
    {
        current_pos_vel[2*i] = currentStates[i].pos;
        current_pos_vel[2*i+1] = currentStates[i].vel;
    }

    DVec times;
     times.push_back(0.0);
    times.push_back(t-tCurrent); //Should delta_t

    this->playV2(current_pos_vel,times,output,0);
    for (int i = 0; i < output.dim(); i++)
        result.push_back(DMPState(output.begin()->getPosition(i),output.begin()->getDeriv(i,1)));//output.begin()->getDeriv(i,1)));
    return result;
}

SampledTrajectoryV2 PeriodicTransientDMP::calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor)
{
    return DMPInterfaceTemplate<DMPState>::calculateTrajectory(timestamps, goal, initialStates, initialCanonicalValues, temporalFactor);
}

//Needed for FT feedback learning
void PeriodicTransientDMP::learnWithZ(SampledComposedPeriodicTrajectory &orignal_trajectory,
                                  SampledTrajectory &z_trajectory, double z_init) {
    double z_average = 0;
    for (unsigned int i=0; i<z_trajectory.size(); ++i)
        z_average += z_trajectory[i][1];
    z_average /= z_trajectory.size();

//    cout << "z_average = " << z_average << endl;

    DVec anchor_point = orignal_trajectory.anchorPoint();
    anchor_point.push_back(z_average);

    SampledComposedPeriodicTrajectory new_trajectory_with_z(
                orignal_trajectory.T(), orignal_trajectory.phi1(),
                orignal_trajectory.periodLength(),
                &anchor_point
                );

    // merge the trajectories into new_trajectory_with_z
    // -> do this by interpolating z at all time-positions in orignal_trajectory
    double t1 = (*(z_trajectory.end()-1))[0],
           p = orignal_trajectory.periodLength();
//ToDo Change
    double T = orignal_trajectory.T();



        double t2 = T+floor((t1-T)/p)*p;
        unsigned int it1 = 1;
        while (it1<z_trajectory.size()-1 && z_trajectory[it1][0]<t2)
            ++it1;

        double z_T = SampledComposedPeriodicTrajectory::linearInterpolationBase(z_trajectory[it1-1], z_trajectory[it1], t2)[0];

    for (unsigned int i=0; i<orignal_trajectory.size(); ++i) {
        DVec sample = orignal_trajectory[i];
        double t = sample[0];

        double new_t = t + floor((t1-t)/p)*p;
        //ToDo Change

        it1 = 1;
        while (it1<z_trajectory.size()-1 && z_trajectory[it1][0]<new_t)
            ++it1;

        DVec new_z = SampledComposedPeriodicTrajectory::linearInterpolationBase(z_trajectory[it1-1], z_trajectory[it1], new_t);


        // fading (sensible?!)
        if (t <=T) {
            //ToDo Change
            double z_transient = SampledComposedPeriodicTrajectory::linearInterpolationSingle(0,z_init, T,z_T, t);
            double fade_out = SampledComposedPeriodicTrajectory::linearInterpolationSingle(0,1, T,0, t);
            double fade_in = pow(1.0 - fade_out, 4);
            new_z[0] = fade_out*(z_transient-z_average) + z_average+ fade_in*(new_z[0]-z_average);
        }

        sample.push_back(new_z[0]);
        new_trajectory_with_z.push_back(sample);
    }
    m_basisOSC = PeriodicDiscreteCanonicalSystem(new_trajectory_with_z.omega());
    int output_dim = new_trajectory_with_z.dim()*2;
    int input_dim = new_trajectory_with_z.dim();
    double alpha_z = 4.0;
    m_g.assign(input_dim, 0.0);
    m_alphaZ = alpha_z; // 8
    m_betaZ = m_alphaZ / 4;

    m_output_dim = output_dim;
    if (input_dim == 0)
        m_input_dim = output_dim;
    else
        m_input_dim = input_dim;
    ODE::setDim(2 + 2 * output_dim);
    ODE::setEpsabs(EPSABS_DMP);

    this->learnFromTrajectory(new_trajectory_with_z);

    /*m_original_initial_vel_and_pos = new_trajectory_with_z.getInitialVelocityAndPosition();
    m_set_initial_pos_and_vel = m_original_initial_vel_and_pos;

    m_orignal_anchor_point = new_trajectory_with_z.anchorPoint();
    m_achor_point = m_orignal_anchor_point;
    m_transient_to_use = 0;*/
    //z_trajectory.plot(); new_trajectory_with_z.plot(false, false); //exit(1);


}

void PeriodicTransientDMP::learnFromTrajectories(const Vec < SampledTrajectoryV2 > &trajectories)
{
    int M = 30;
    int N = 40;

    Vec< unsigned int > Ms;

    Ms.push_back(M);
    printf("a %e\n",trajectories[0].getPeriodLength());
    cout << "Anchor in learning " << trajectories[0].getAnchorPoint() << endl;
    Vec< const SampledComposedPeriodicTrajectory* > input_trajectories;
    const Vec < const SampledComposedPeriodicTrajectory* > *c_input_trajectories;

    for (int i = 0; i < trajectories.size(); i++)
    {
        SampledComposedPeriodicTrajectory *input_traj = new SampledComposedPeriodicTrajectory(trajectories[i]);
        const SampledComposedPeriodicTrajectory *c_input_traj = input_traj;
        input_trajectories.push_back(c_input_traj);
    }
    printf("b\n");
    double omega = 2*M_PI/input_trajectories[0]->periodLength();
    PeriodicDiscreteCanonicalSystem tempOSC(omega);
    printf("c\n");

    m_alphaZ = 4.0; // 8
    m_betaZ = m_alphaZ / 4;
    m_basisOSC = tempOSC;
    m_output_dim = 2*input_trajectories[0]->dim();

    m_input_dim = input_trajectories[0]->dim();
    m_g.assign(m_input_dim, 0.0);
    ODE::setDim(2 + 2 * m_output_dim);
    ODE::setEpsabs(EPSABS_DMP);
    printf("d\n");
    m_perturbationApproximation.assign(m_output_dim, NULL);
    c_input_trajectories = &input_trajectories;
    learnFromTrajectories(*c_input_trajectories, Ms, N);
    firstIteration = true;
    printf("e\n");
}


