/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "forcefielddmp.h"

#include <limits>

using namespace DMP;

ForceFieldDMP::ForceFieldDMP(int kernelSize, double tau) :
    SimpleEndVeloDMP(kernelSize, tau, false)
{
}



void ForceFieldDMP::flow(double t, const DMP::DVec& x, DMP::DVec& out)
{
    double u = x.at(0);

    DVec canonicalState(1);
    canonicalState[0] = u;
    canonicalSystem->flow(t, canonicalState, out);

    double tau = canonicalSystem->getTau();


    unsigned int dimensions = __trajDim;

    if (dimensions > goals.size())
    {
        throw Exception("There are less trajectory goals than dimensions");
    }

    if (dimensions > startPositions.size())
    {
        throw Exception("There are less trajectory start positions than dimensions");
    }


    unsigned int offset = canonicalState.size();
    unsigned int curDim = 0;

    while (curDim < dimensions)
    {
        double curGoal = _getPerturbationForce(curDim, u);
        unsigned int i = curDim * 2 + offset;
        double pos =  x[i];
        double vel = x[i + 1];


        out.at(i++) = 1.0 / tau * (vel);
        // Original ForceField
        out.at(i)   = 1.0/tau * (K * (goals[curDim] - pos) - (D*vel) - K*(goals[curDim] - startPositions[curDim])*std::max(1-u,0.0) + K*curGoal);

//        std::cout << "t: " << t << " u: " << u << " pos: " << pos << " vel: " << vel << " Current goal: " << curGoal << " resulting Acc: " << out[i] << std::endl;
        curDim++;
    }
}


DoubleMap ForceFieldDMP::calcPerturbationForceSamples(unsigned int trajDim, const DVec& canonicalValues, const SampledTrajectoryV2& exampleTraj) const
{
    DoubleMap result;


    if (exampleTraj.size() == 0)
    {
        throw Exception("Sample traj is empty");
    }

    if (canonicalValues.size() != exampleTraj.size())
    {
        throw Exception("The amount of the canonicalValues do not match the exampleTrajectory");
    }

    // start position of example traj
    double x0 = exampleTraj.begin()->getPosition(trajDim);
    // end position of example traj
    double xT = exampleTraj.rbegin()->getPosition(trajDim);

    double tau = canonicalSystem->getTau();

    //    exampleTraj.plot();

        DVec::const_iterator itCanon = canonicalValues.begin();
//    int i = 0;
    SampledTrajectoryV2::ordered_view::const_iterator itTraj = exampleTraj.begin();
    for ( itCanon = canonicalValues.begin(), itTraj = exampleTraj.begin(); itCanon != canonicalValues.end(); ++itCanon,  itTraj++)
    {
        // first value is time value, so skip it
        const SampledTrajectoryV2::TrajData& state = *itTraj;
        const double& pos = state.getPosition(trajDim);
        const double& vel = state.getDeriv(trajDim, 1);
        const double& acc = state.getDeriv(trajDim, 2);

        // Original ForceField
        result[*itCanon] = (tau * acc + D * vel) / K - (xT - pos) + (xT-x0)*(1-*itCanon);


    }



    return result;
}



