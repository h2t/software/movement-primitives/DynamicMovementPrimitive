/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef ENDVELODMP_H
#define ENDVELODMP_H
#include "basicdmp.h"

namespace DMP
{
    class EndVeloDMP : public BasicDMP
    {
    public:
        EndVeloDMP(int kernelSize = 50, double D = 20, double tau = 1.0);
        void learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories) override;
        Vec<DMPState> calculateTrajectoryPoint(double t, const DVec& goal, double tInit, const Vec<DMPState> &initialStates, DVec& canonicalValues, double temporalFactor ) override;

        const SampledTrajectoryV2& getTrainingTraj(unsigned int i) const override
        {
            return originalTrainingTraj;
        }
        SampledTrajectoryV2 calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor ) override;
        SampledTrajectoryV2 reducedGeneratedTraj;
    protected:

        SampledTrajectoryV2 removeEndVelocityFromTrajectory(const SampledTrajectoryV2& trajectory);
        void calcVelocityReductionFunctionParameters(SampledTrajectoryV2& traj, unsigned int trajDimension, double& b, double& c);


        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {

            ar& boost::serialization::make_nvp("trajOffset",trajOffset);
            ar& boost::serialization::make_nvp("offsetTrajScalingFactor",offsetTrajScalingFactor);
            ar& boost::serialization::make_nvp("originalTrainingTraj",originalTrainingTraj);


            DMPInterface& base = boost::serialization::base_object<DMPInterface>(*this);
            ar& boost::serialization::make_nvp("base",base);

        }

    private:
        /**
         * @brief trajOffset is the offset of the complete trajectory, so that
         * the end velocity and acceleration is zero.
         *
         */
        SampledTrajectoryV2 trajOffset;
        SampledTrajectoryV2 originalTrainingTraj;

        DVec offsetTrajScalingFactor;


        //        SampledTrajectory normalizedModifiedLearnedTraj;
    };
}

#endif // ENDVELODMP_H
