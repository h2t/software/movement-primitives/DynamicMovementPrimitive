#include "umitsmp.h"
#include <Eigen/Dense>
#include "general/helpers.h"


DMP::UMITSMP::UMITSMP(int kernelSize, int baseMode , double tau, double eps):
    baseMode(baseMode)
{
    FunctionApproximationInterfacePtr approximater(new LWR<GaussKernel>(kernelSize));
    setFunctionApproximator(approximater);
    canonicalSystem.reset(new LinearDecayCanonicalSystem(tau));

    ODE::setEpsabs(1e-3);
    canonicalSystem->setEpsabs(1e-3);
    this->tau = tau;
    numOfDemos = 1;

    overFlyFactor = 0;
}

void DMP::UMITSMP::learnFromTrajectories(const Vec<DMP::SampledTrajectoryV2> &trainingTrajectories)
{
    if (trainingTrajectories.size() == 0)
    {
        throw Exception() << "The training trajectories is empty ... ";
    }

    trainingTrajs = trainingTrajectories;
    DVec normTimestamps = trainingTrajs.at(0).getTimestamps();
    Vec<DVec> uValuesOrig;
    canonicalSystem->integrate(normTimestamps, 1.0, uValuesOrig);
    DVec uValues;
    for (size_t i = 0; i < uValuesOrig.size(); ++i)
    {
        uValues.push_back(uValuesOrig[i][0]);
    }

    dimOfTraj = trainingTrajs.at(0).dim();;
    numOfDemos = trainingTrajs.size();

    canVals = uValues;

    {
         DMP::SampledTrajectoryV2 trainingTrajectory = trainingTrajs[0];
         defaultGoal.clear();
         for(size_t dim = 0; dim < 7; ++dim)
         {
             defaultGoal.push_back(trainingTrajectory.rbegin()->getPosition(dim));
         }

    }

    DMP::Vec<mat > fMatList;
    fMatList.reserve(numOfDemos);
    mat avgfMat(dimOfTraj, uValues.size(), fill::zeros);
    for(size_t i = 0; i < numOfDemos; ++i)
    {
        DMP::SampledTrajectoryV2 trainingTrajectory = trainingTrajs[i];
        //        trainingTrajectory.differentiateDiscretly(2);
        trainingTrajectory = SampledTrajectoryV2::normalizeTimestamps(trainingTrajectory, 0, 1);
        trainingTrajs[i] = trainingTrajectory;


        mat fMat(dimOfTraj, uValues.size()); // forceMat: D x T (D: dimension, T: sample points)
        for(size_t dim = 0; dim < 3; ++dim)
        {

            vec force = getForce(dim, normTimestamps, uValues, trainingTrajectory);
            fMat.row(dim) = force.t();
        }

        mat forceMat = getQuatForce(3, normTimestamps, uValues, trainingTrajectory);
        fMat.rows(3, 6) = forceMat.t();


        fMatList.push_back(fMat);
        avgfMat = avgfMat + fMat;
    }


    avgfMat /= float(numOfDemos);
    unsigned int funcId = 0;

    for(size_t dim = 0; dim < dimOfTraj; ++dim)
    {
        DoubleMap forceSamples = createDoubleMap(uValues, avgfMat.row(dim).t());
        _trainFunctionApproximator(funcId, forceSamples);
        funcId++;
    }

    scoreMatList.clear();

    if(numOfDemos > 1)
    {
        std::cout << "More than one trajectories are found, PCA is now being called ..." << std::endl;
        mat G(uValues.size(), uValues.size(), fill::zeros);
        for(size_t i = 0; i < numOfDemos; ++i)
        {
            mat fMat = fMatList[i] - avgfMat;
            fMatList[i] = fMat;
            G = G + fMat.t() * fMat;
        }

        G /= double(numOfDemos);

        mat U;
        vec SVec;
        mat V;

        std::cout << "start svd ... " << std::endl;
        bool svdsuccess = svd(U,SVec,V,G, "std");
        std::cout << "end svd ... " << std::endl;

        std::cout << "svd success: " << (svdsuccess?"true":"false") << std::endl;

        double totalsVal = sum(SVec);

        double ratio = 0;
        double sVal = 0;
        int idOfPCs = 0;

        while(totalsVal != 0.0 && ratio < 0.99)
        {
            sVal += SVec(idOfPCs);
            ratio = sVal / totalsVal;

            int colId = idOfPCs;
            mat sMat(dimOfTraj, numOfDemos);
            for(size_t i = 0; i < numOfDemos; ++i)
            {
                sMat.col(i) = fMatList[i] * V.col(colId);
            }

            if(sMat.has_inf() || sMat.has_nan())
            {
                throw DMP::Exception("pca error: score matrix is nan or inf");
            }
            scoreMatList.push_back(sMat);

            DoubleMap forceSamples = createDoubleMap(uValues, V.col(colId));
            _trainFunctionApproximator(funcId, forceSamples);
            funcId++;
            idOfPCs++;
        }
        std::cout << "number of PCs: " << idOfPCs << std::endl;

    }

}

//#define uInv(x) -log(x)*6.90776
#define uInv(x) x
void DMP::UMITSMP::_trainFunctionApproximator(unsigned int funcID, const DMP::DoubleMap &forceSamples)
{
    FunctionApproximationInterfacePtr approximator = _getFunctionApproximator(funcID);
    DoubleMap forceSamplesInv;
    DoubleMap::const_iterator it = forceSamples.begin();

    for (; it != forceSamples.end(); ++it)
    {
        forceSamplesInv[uInv(it->first)] = it->second;
    }

    DVec timestamps;
    DVec values;

    for (it = forceSamplesInv.begin(); it != forceSamplesInv.end(); ++it)
    {
        timestamps.push_back(it->first);
        values.push_back(it->second);
    }

    approximator->learn(timestamps, values);
}

vec DMP::UMITSMP::getForce(int dim, const DVec& timestamps, const DMP::DVec &canonicalValues, const DMP::SampledTrajectoryV2 &exampleTraj)
{
    double x0 = exampleTraj.begin()->getPosition(dim);
    double xT = exampleTraj.rbegin()->getPosition(dim);

    DVec::const_iterator itCanon = canonicalValues.begin();
    //    SampledTrajectoryV2::ordered_view::const_iterator itTraj = exampleTraj.begin();

    vec result(canonicalValues.size());
    int i = 0;

    vec mjcoef_t(6, fill::zeros);
    if(baseMode == UMITSMP_BASE_MINJERK)
    {
        vec cond(6);
        cond(0) = xT;
        cond(1) = 0;
        cond(2) = 0;
        cond(3) = x0;
        cond(4) = 0;
        cond(5) = 0;

        mjcoef_t = getMinJerkCoef(canonicalValues[0], cond);
    }

    for (; itCanon != canonicalValues.end(); ++itCanon, ++i)
    {
        const double& pos = exampleTraj.getState(timestamps[i],dim);

        double u = *itCanon;
        double lpart;

        if(baseMode == UMITSMP_BASE_MINJERK)
        {
            vec u_vec = {1, u, pow(u,2), pow(u,3), pow(u,4), pow(u,5)};
            lpart = dot(u_vec, mjcoef_t);
        }
        else
        {
            lpart = - (xT - x0) * u + xT;
        }

        double value = (pos - lpart);
        checkValue(value);
        result(i) = value;
    }

    return result;
}

Eigen::Quaterniond DMP::UMITSMP::quatSlerp(double t, const Eigen::Quaterniond& q0, const Eigen::Quaterniond& q1)
{
    double cosHalfTheta = q0.w() * q1.w() + q0.x() * q1.x() + q0.y() * q1.y() + q0.z() * q1.z();


    Eigen::Quaterniond q1x = q1;
    if(cosHalfTheta < 0)
    {
        q1x.w() = -q1.w();
        q1x.x() = -q1.x();
        q1x.y() = -q1.y();
        q1x.z() = -q1.z();
    }


    if(fabs(cosHalfTheta) >= 1.0)
    {
        return q0;
    }

    double halfTheta = acos(cosHalfTheta);
    double sinHalfTheta = sqrt(1.0 - cosHalfTheta * cosHalfTheta);


    Eigen::Quaterniond result;
    if (fabs(sinHalfTheta) < 0.001)
    {
        result.w() = (1 - t) * q0.w() + t * q1x.w();
        result.x() = (1 - t) * q0.x() + t * q1x.x();
        result.y() = (1 - t) * q0.y() + t * q1x.y();
        result.z() = (1 - t) * q0.z() + t * q1x.z();

    }
    else
    {
        double ratioA = sin((1 - t) * halfTheta) / sinHalfTheta;
        double ratioB = sin(t * halfTheta) / sinHalfTheta;

        result.w() = ratioA * q0.w() + ratioB * q1x.w();
        result.x() = ratioA * q0.x() + ratioB * q1x.x();
        result.y() = ratioA * q0.y() + ratioB * q1x.y();
        result.z() = ratioA * q0.z() + ratioB * q1x.z();
    }

    return result;
}

Eigen::Quaterniond DMP::UMITSMP::dquatSlerp(double t, const Eigen::Quaterniond& q0, const Eigen::Quaterniond& q1)
{
    double cosHalfTheta = q0.w() * q1.w() + q0.x() * q1.x() + q0.y() * q1.y() + q0.z() * q1.z();

    Eigen::Quaterniond q1x = q1;
    if(cosHalfTheta < 0)
    {
        q1x.w() = -q1.w();
        q1x.x() = -q1.x();
        q1x.y() = -q1.y();
        q1x.z() = -q1.z();
    }



    Eigen::Quaterniond result;

    if(fabs(cosHalfTheta) >= 1.0)
    {
        result.w() = 0;
        result.x() = 0;
        result.y() = 0;
        result.z() = 0;
        return result;
    }

    double halfTheta = acos(cosHalfTheta);
    double sinHalfTheta = sqrt(1.0 - cosHalfTheta * cosHalfTheta);


    if (fabs(sinHalfTheta) < 0.001)
    {
        result.w() = - q0.w() + q1x.w();
        result.x() = - q0.x() + q1x.x();
        result.y() = - q0.y() + q1x.y();
        result.z() = - q0.z() + q1x.z();

    }
    else
    {
        double ratioA = -halfTheta * cos((1 - t) * halfTheta) / sinHalfTheta;
        double ratioB = halfTheta * cos(t * halfTheta) / sinHalfTheta;

        result.w() = ratioA * q0.w() + ratioB * q1x.w();
        result.x() = ratioA * q0.x() + ratioB * q1x.x();
        result.y() = ratioA * q0.y() + ratioB * q1x.y();
        result.z() = ratioA * q0.z() + ratioB * q1x.z();
    }

    return result;
}



mat DMP::UMITSMP::getQuatForce(int dim, const DVec& timestamps, const DMP::DVec &canonicalValues, const DMP::SampledTrajectoryV2 &exampleTraj)
{
    Eigen::Quaterniond q0, qT;
    q0.w() = exampleTraj.begin()->getPosition(dim);
    q0.x() = exampleTraj.begin()->getPosition(dim + 1);
    q0.y() = exampleTraj.begin()->getPosition(dim + 2);
    q0.z() = exampleTraj.begin()->getPosition(dim + 3);
    qT.w() = exampleTraj.rbegin()->getPosition(dim);
    qT.x() = exampleTraj.rbegin()->getPosition(dim + 1);
    qT.y() = exampleTraj.rbegin()->getPosition(dim + 2);
    qT.z() = exampleTraj.rbegin()->getPosition(dim + 3);

    DVec::const_iterator itCanon = canonicalValues.begin();

    mat result(canonicalValues.size(), 4);
    int i = 0;

    vec mjcoef_t(6, fill::zeros);
    if(baseMode == UMITSMP_BASE_MINJERK)
    {
        vec cond(6);
        cond(0) = 1;
        cond(1) = 0;
        cond(2) = 0;
        cond(3) = 0;
        cond(4) = 0;
        cond(5) = 0;

        mjcoef_t = getMinJerkCoef(canonicalValues[0], cond);
    }

    for (; itCanon != canonicalValues.end(); ++itCanon, ++i)
    {
        Eigen::Quaterniond currentQ;
        currentQ.w() = exampleTraj.getState(timestamps.at(i),dim);
        currentQ.x() = exampleTraj.getState(timestamps.at(i),dim+1);
        currentQ.y() = exampleTraj.getState(timestamps.at(i),dim+2);
        currentQ.z() = exampleTraj.getState(timestamps.at(i),dim+3);

        double u = *itCanon;

        Eigen::Quaterniond lpart;

        if(baseMode == UMITSMP_BASE_MINJERK)
        {
            vec u_vec = {1, u, pow(u,2), pow(u,3), pow(u,4), pow(u,5)};
            double tratio = dot(u_vec, mjcoef_t);
            lpart = quatSlerp(tratio, q0, qT);
        }
        else
        {

            double tratio = timestamps.at(i) / timestamps.at(timestamps.size()-1);
            lpart = quatSlerp(tratio, q0, qT);
        }


        Eigen::Quaterniond value = lpart.inverse() * currentQ;
        result(i,0) = value.w();
        result(i,1) = value.x();
        result(i,2) = value.y();
        result(i,3) = value.z();
    }

    if(result.has_nan() || result.has_inf())
    {
        throw DMP::Exception("calculated quaternion force is nan or inf");
    }

    return result;
}


DMP::DoubleMap DMP::UMITSMP::createDoubleMap(const DMP::DVec &canonicalVals, const vec &sampleVec)
{
    DoubleMap result;
    for(size_t i = 0; i < canonicalVals.size(); ++i)
    {
        result[canonicalVals[i]] = sampleVec(i);
    }
    return result;
}

vec DMP::UMITSMP::calcForce(double u)
{
    vec fVec(dimOfTraj);
    unsigned int funcId = 0;
    for(size_t dim = 0; dim < dimOfTraj; ++dim)
    {
        double avgForce = _getPerturbationForce(funcId, u);
        funcId++;
        fVec(dim) = avgForce;
    }

    if(numOfDemos == 1)
    {
        if(fVec.has_nan() || fVec.has_inf())
        {
            throw DMP::Exception("calculated force contains nan or inf");
        }
        return fVec;
    }
    else
    {

        if(styleParas.size() == 0)
        {
            DMP::DVec ratios;
            for(size_t i = 0; i < numOfDemos; ++i)
            {
                ratios.push_back(1/(float)numOfDemos);
            }
            styleParas = getStyleParasWithRatio(ratios);
        }

        size_t numOfPCs = scoreMatList.size();
        for(size_t i = 0; i < numOfPCs; ++i)
        {
            double pcForce = _getPerturbationForce(funcId,u);
            funcId++;

            fVec += styleParas[i] * pcForce;
        }

        if(fVec.has_nan() || fVec.has_inf())
        {
            throw DMP::Exception("calculated force contains nan or inf");
        }
        return fVec;
    }
}

vec DMP::UMITSMP::calcForceWithDeri(double u, vec &deriForce)
{
    vec fVec(dimOfTraj);
    deriForce.clear();
    deriForce.resize(dimOfTraj);
    unsigned int funcId = 0;
    for(size_t dim = 0; dim < dimOfTraj; ++dim)
    {
        double avgForce = _getPerturbationForce(funcId, u);
        double avgDerivForce = _getPerturbationForceDeriv(funcId, u);

        funcId++;
        fVec(dim) = avgForce;
        deriForce(dim) = avgDerivForce;
    }

    if(numOfDemos == 1)
    {
        if(fVec.has_nan() || fVec.has_inf())
        {
            throw DMP::Exception("calculated force contains nan or inf");
        }
        return fVec;
    }
    else
    {
        if(styleParas.size() == 0)
        {
            DMP::DVec ratios;
            for(size_t i = 0; i < numOfDemos; ++i)
            {
                ratios.push_back(1/(float)numOfDemos);
            }
            styleParas = getStyleParasWithRatio(ratios);
        }

        size_t numOfPCs = scoreMatList.size();
        for(size_t i = 0; i < numOfPCs; ++i)
        {
            double pcForce = _getPerturbationForce(funcId,u);
            double pcForceDeriv = _getPerturbationForceDeriv(funcId, u);
            funcId++;

            fVec += styleParas[i] * pcForce;
            deriForce += styleParas[i] * pcForceDeriv;
        }

        if(fVec.has_nan() || fVec.has_inf())
        {
            throw DMP::Exception("calculated force contains nan or inf");
        }
        return fVec;
    }
}

vec DMP::UMITSMP::getMinJerkCoef(double terminalTime, const vec &condition)
{
    double T = terminalTime;

    mat A(6,6,fill::zeros);

    A(0,0) = 1;
    A(1,1) = 1;
    A(2,2) = 2;

    for(size_t i = 0; i < 6; ++i)
    {
        A(3,i) = pow(T,i);
        A(4,i) = i * pow(T,i-1);
        A(5,i) = i * (i-1) * pow(T, i-2);
    }
    vec a = inv(A) * condition;

    if(a.has_inf() || a.has_nan())
    {
        throw DMP::Exception("minimum jerk coefficient is nan or inf");
    }

    return a;
}


DMP::Vec<vec> DMP::UMITSMP::getStyleParasWithRatio(const DMP::DVec &ratios)
{
    if(ratios.size() != numOfDemos)
    {
        throw Exception("getStyeParasWithRatio: ratios size should be the same as number of demos");
    }

    DMP::Vec<vec> result;
    for(size_t i = 0; i < scoreMatList.size(); ++i)
    {
        mat sMat = scoreMatList[i];

        vec coef(dimOfTraj, fill::zeros);
        for(size_t j = 0; j < numOfDemos; ++j)
        {
            coef += ratios[j] * sMat.col(j);
        }
        result.push_back(coef);
    }

    return result;
}


double DMP::UMITSMP::_getPerturbationForce(unsigned int funcID, DMP::DVec canonicalStates) const
{
    if (canonicalStates.at(0) == 0)
    {
        throw Exception("The canonical state must not be 0!");
    }

    return (*_getFunctionApproximator(funcID))(uInv(canonicalStates.at(0))).at(0);
}

double DMP::UMITSMP::_getPerturbationForceDeriv(unsigned int funcID, DMP::DVec canonicalStates) const
{
    if (canonicalStates.at(0) == 0)
    {
        throw Exception("The canonical state must not be 0!");
    }

    return (*_getFunctionApproximator(funcID)).getDeriv(uInv(canonicalStates.at(0))).at(0);
}


const DMP::SampledTrajectoryV2& DMP::UMITSMP::getTrainingTraj(unsigned int trajSampleIndex) const
{
    return trainingTrajs.at(trajSampleIndex);

}

void DMP::UMITSMP::setViaPoint(double u, const DVec &viaPoseVec)
{
    if(viaPoseVec.size() != 7)
    {
        throw DMP::Exception("Error: viapoint should be a 7 dim vector (position + quaternion) !!");
    }

    vec force = calcForce(u);

    if(viaPoseVec.size() != force.n_rows)
    {
        throw DMP::Exception("Error: setViaPoint does not allow insertion of a point with different dimensions !!!");
    }



    ViaPosePtr viaPose(new ViaPose(viaPoseVec, force));

    //    Eigen::Quaterniond qpoint;
    //    qpoint.w() = point[3];
    //    qpoint.x() = point[4];
    //    qpoint.y() = point[5];
    //    qpoint.z() = point[6];
    //    Eigen::Quaterniond qforce;
    //    qforce.w() = force(3);
    //    qforce.x() = force(4);
    //    qforce.y() = force(5);
    //    qforce.z() = force(6);
    //    Eigen::Quaterniond qdiff = qpoint * qforce.inverse();
    //    point[3] = qdiff.w();
    //    point[4] = qdiff.x();
    //    point[5] = qdiff.y();
    //    point[6] = qdiff.z();

    bool isExist = false;
    for(auto & viaPoint : viaPoints)
    {
        if(viaPoint.first == u)
        {
            viaPoint.second = viaPose;
            isExist = true;
            break;
        }
    }


    if(!isExist)
    {
        viaPoints.push_back(std::make_pair(u, viaPose));
    }


    sort(viaPoints.begin(), viaPoints.end(),
         [](const ViaPoseMap& lhs, const ViaPoseMap& rhs) {return lhs.first < rhs.first;});



    for(ViaPoseSet::iterator itStart = viaPoints.end() - 1; itStart != viaPoints.begin(); itStart--)
    {
        ViaPoseSet::iterator itGoal = itStart - 1;

        Eigen::Quaterniond oriGoal = itGoal->second->viaOrientation;
        Eigen::Quaterniond oriStart = itStart->second->viaOrientation;

        double cosHalfTheta = oriGoal.dot(oriStart);

        if(cosHalfTheta < 0)
        {
            itGoal->second->negateViaOrientation();
        }
    }


}

void DMP::UMITSMP::removeViaPoints()
{
    viaPoints.clear();
}


void DMP::UMITSMP::prepareExecution(const DMP::DVec &goal, const Vec<DMP::DMPState> &initialStates, const DMP::DVec &initialCanonicalValues, double temporalFactor)
{
    setTemporalFactor(temporalFactor);
    dimOfTraj = getWeights().size();
    if(goal.size() != dimOfTraj)
    {
        throw DMP::Exception("The goal's dimension is not correct");
    }

    if(initialStates.size() != dimOfTraj)
    {
        throw DMP::Exception("The current states' dimension is not correct.");
    }

    // check orientation
    DVec startPositions;
    for(size_t i = 0; i < initialStates.size(); ++i)
    {
        startPositions.push_back(initialStates[i].pos);
    }

    setGoals(goal);

    double max_u = initialCanonicalValues[0];
    setViaPoint(max_u, startPositions);

    sort(viaPoints.begin(), viaPoints.end(),
         [](const ViaPoseMap& lhs, const ViaPoseMap& rhs) {return lhs.first < rhs.first;});

    switch(baseMode)
    {
    case UMITSMP_BASE_LINEAR:
        break;


    case UMITSMP_BASE_MINJERK:
    {

        break;
    }
    case UMITSMP_BASE_OPTIMIZED:
    {
        learnBaseFunction();
    }
    }

}


DMP::Vec<DMP::DMPState> DMP::UMITSMP::calculateDirectlyVelocity(const DMP::Vec<DMP::DMPState> &currentState, double canVal, double deltaT, DMP::DVec &targetState)
{
    DMP::Vec<DMP::DMPState> nextState = currentState;
    targetState.clear();
    targetState.resize(7);

    double u = canVal;
    unsigned int dimensions = dimOfTraj;


    double u0, u1;

    if(u < canonicalSystem->getUMin())
    {
        u = canonicalSystem->getUMin() + 1e-6;
    }
    ViaPoseSet::iterator it_next = std::lower_bound(viaPoints.begin(), viaPoints.end(), u,
                                                      [](const ViaPoseMap& element, double uval){ return element.first < uval;} );

    ViaPoseSet::iterator it_begin = std::lower_bound(viaPoints.begin(), viaPoints.end(), u,
                                                       [](const ViaPoseMap& element, double uval){ return element.first < uval;} );

    it_begin++;

    if(it_next == viaPoints.end())
    {
        return currentState;
    }

    ViaPosePtr cinits;
    ViaPosePtr cgoals;

    cinits = it_next->second;
    u0 = it_next->first;
    cgoals = (--it_next)->second;
    u1 = it_next->first;

    vec dfVec;
    vec fVec = calcForceWithDeri(u, dfVec);

    int curDim = 0;
    while (curDim < 3 && ((baseMode == UMITSMP_BASE_LINEAR && overFlyFactor == 0 )|| baseMode == UMITSMP_BASE_MINJERK))
    {
        double force = fVec(curDim);
        double dforce = dfVec(curDim);
        double vel = currentState[curDim].vel;

        // calculate lpart:
        double x0 = cinits->viaPosition(curDim) - cinits->viaLinearForce(curDim);
        double x1 = cgoals->viaPosition(curDim) - cgoals->viaLinearForce(curDim);
        double lpart,ctarget, cdtarget;

        if(baseMode == UMITSMP_BASE_MINJERK)
        {
            vec cond(6);
            cond(0) = x1;
            cond(1) = 0;
            cond(2) = 0;
            cond(3) = x0;
            cond(4) = 0;
            cond(5) = 0;

            vec mjcoef_t = getMinJerkCoef(u0 - u1, cond);
            double cu = u - u1;
            vec u_vec = {1, cu, pow(cu,2), pow(cu,3), pow(cu,4), pow(cu,5)};
            lpart = dot(u_vec, mjcoef_t);
            ctarget = lpart + force;

            vec u_dvec = {0, 1, 2*cu, 3*pow(cu,2), 4*pow(cu,3), 5*pow(cu,4)};
            cdtarget = - dot(u_dvec, mjcoef_t) - dforce;
        }
        else
        {
            double b = (x1 * u0 - x0 * u1) / (u0 - u1);
            double a;
            if(u0 != 0)
                a = (x0 - b) / u0;
            else
                a = (x1 - b) / u1;

            lpart = a * u + b;
            ctarget = lpart + force;
            cdtarget = -a - dforce;
        }

        nextState[curDim].pos = currentState[curDim].pos + vel * deltaT;
        nextState[curDim].vel = cdtarget;

        targetState[curDim] = ctarget;

        curDim++;
    }

    if(baseMode == UMITSMP_BASE_LINEAR && overFlyFactor != 0)
    {

        Eigen::Vector3f x0 = cinits->viaPosition - cinits->viaLinearForce;
        Eigen::Vector3f x1 = cgoals->viaPosition - cgoals->viaLinearForce;

        bool existPrevious = true;
        bool existNext = true;
        if(it_next == viaPoints.begin())
        {
            existNext = false;

        }
        else
        {
            it_next--;
        }

        if(it_begin == viaPoints.end())
        {
            existPrevious = false;
        }

        Eigen::Vector3f vel = (x1 - x0) / (u1 - u0);
        Eigen::Vector3f lpartvec = (u - u0) * vel + x0;

        if(u > u0 - overFlyFactor && existPrevious)
        {
            double upre = it_begin->first;
            ViaPosePtr prePoint = it_begin->second;
            Eigen::Vector3f xpre = prePoint->viaPosition - prePoint->viaLinearForce;

            Eigen::Vector3f vel0 = (x0 - xpre) / (u0 - upre);
            Eigen::Vector3f vel1 = (x1 - x0) / (u1 - u0);

            double a = (u0 + overFlyFactor - u) / (2 * overFlyFactor);
            vel = a * vel1 + (1 - a) * vel0;

            double um = u0 + overFlyFactor;

            Eigen::Vector3f linearPart = (um * vel1 + 2 * um * vel0 - 2 * u0 * vel0 - um * vel0) * (u - um);
            Eigen::Vector3f quadPart = 0.5 * (vel0 - vel1) * (u - um) * (u + um);
            Eigen::Vector3f integralPart = (linearPart + quadPart) /(2*overFlyFactor);

            lpartvec = xpre + (um - upre) * vel0 + integralPart;
        }

        if(u < u1 + overFlyFactor && existNext)
        {
            ViaPosePtr nextPoint = it_next->second;
            Eigen::Vector3f x2 = nextPoint->viaPosition - nextPoint->viaLinearForce;
            double u2 = it_next->first;

            Eigen::Vector3f vel0 = (x1 - x0) / (u1 - u0);
            Eigen::Vector3f vel1 = (x2 - x1) / (u2 - u1);

            double a = (u1 + overFlyFactor - u) / (2 * overFlyFactor);
            vel = a * vel1 + (1 - a) * vel0;

            double um = u1 + overFlyFactor;

            Eigen::Vector3f linearPart = (um * vel1 + 2 * um * vel0 - 2 * u1 * vel0 - um * vel0) * (u - um) ;
            Eigen::Vector3f quadPart = 0.5 * (vel0 - vel1) * (u - um) * (u + um);
            Eigen::Vector3f integralPart = (linearPart + quadPart) /(2*overFlyFactor);

            lpartvec = x0 + (um - u0) * vel0 + integralPart;
        }

        curDim = 0;
        for(size_t i = 0; i < 3; ++curDim, ++i)
        {
            targetState[curDim] = lpartvec(i) + fVec(i);
            nextState[curDim].pos = currentState[curDim].pos + currentState[curDim].vel * deltaT;
            nextState[curDim].vel = -vel(i) - dfVec(i);
        }

    }


    Eigen::Quaterniond quatForce;
    Eigen::Quaterniond dquatForce;
    Eigen::Quaterniond q0 = cinits->viaOrientation * cinits->viaRotationForce.inverse();
    Eigen::Quaterniond q1 = cgoals->viaOrientation * cgoals->viaRotationForce.inverse();

    curDim = 3;
    quatForce.w() = fVec(curDim);
    dquatForce.w() = dfVec(curDim);
    curDim++;

    quatForce.x() = fVec(curDim);
    dquatForce.x() = dfVec(curDim);
    curDim++;

    quatForce.y() = fVec(curDim);
    dquatForce.y() = dfVec(curDim);
    curDim++;

    quatForce.z() = fVec(curDim);
    dquatForce.z() = dfVec(curDim);

    double tratio;
    double dtratio;

    if(baseMode == UMITSMP_BASE_MINJERK)
    {
        vec cond(6);
        cond(0) = 1;
        cond(1) = 0;
        cond(2) = 0;
        cond(3) = 0;
        cond(4) = 0;
        cond(5) = 0;

        vec mjcoef_t = getMinJerkCoef(u0 - u1, cond);
        double cu = u - u1;
        vec u_vec = {1, cu, pow(cu,2), pow(cu,3), pow(cu,4), pow(cu,5)};
        tratio = dot(u_vec, mjcoef_t);

        vec u_dvec = {0, 1, 2*cu, 3*pow(cu,2), 4*pow(cu,3), 5*pow(cu,4)};
        dtratio = - dot(u_dvec, mjcoef_t);
    }
    else
    {
        tratio = (u - u0) / (u1 - u0);
        dtratio = - 1 / (u1 - u0);
    }

    //    q0.normalize();
    //    q1.normalize();

    Eigen::Quaterniond dqslerp = dtratio * dquatSlerp(tratio, q0, q1);
    Eigen::Quaterniond qslerp = quatSlerp(tratio, q0, q1);

    Eigen::Quaterniond qderi = dqslerp * quatForce +  qslerp * dquatForce;
    Eigen::Quaterniond qtarget = qslerp * quatForce;

    curDim = 3;
    nextState[curDim].pos = currentState[curDim].pos + currentState[curDim].vel * deltaT;
    nextState[curDim].vel = qderi.w();
    curDim++;
    nextState[curDim].pos = currentState[curDim].pos + currentState[curDim].vel * deltaT;
    nextState[curDim].vel = qderi.x();
    curDim++;
    nextState[curDim].pos = currentState[curDim].pos + currentState[curDim].vel * deltaT;
    nextState[curDim].vel = qderi.y();
    curDim++;
    nextState[curDim].pos = currentState[curDim].pos + currentState[curDim].vel * deltaT;
    nextState[curDim].vel = qderi.z();

    targetState[3] = qtarget.w();
    targetState[4] = qtarget.x();
    targetState[5] = qtarget.y();
    targetState[6] = qtarget.z();

    return nextState;
}

void DMP::UMITSMP::learnBaseFunction()
{
    throw DMP::Exception("learBaseFunciton: not implemented yet ");
    int len = 1000;
    mat mjMat = getMinJerkQuadTerm(len);

}

mat DMP::UMITSMP::getMinJerkQuadTerm(int len)
{
    //    vec v1 = ones<vec>(len-1);
    //    vec v2 = ones<vec>(len-2);

    //    mat mat1 = diagmat(-0.5 * v2, 2).t();
    //    mat mat2 = diagmat(v1, 1).t();
    //    mat mat3 = diagmat(-1*v1, 1);
    //    mat mat4 = diagmat(0.5*v2, 2);

    //    mat J = mat1 + mat2 + mat3 + mat4;
    //    J(1,1) = -0.5;
    //    J(1,2) = 0.5;
    //    J(1,3) = 0;
    //    J(2,1) = 0.5;
    //    J(2,2) = -1;
    //    J(2,3) = 0.5;
    //    J(2,4) = 0;

    //    int end = J.n_rows - 1;
    //    J(end,end) = 0.5;
    //    J(end,end-1) = -0.5;
    //    J(end,end-2) = 0;
    //    J(end-1,end) = 0.5;
    //    J(end-1,end-1) = -1;
    //    J(end-1,end-2) = 0.5;
    //    J(end-1,end-3) = 0;
    mat J;
    return J;
}

//real_2d_array DMP::UMITSMP::mat2real2d(const mat& m)
//{
//    std::string source = "[[";


//    for(size_t i = 0; i < m.n_rows; ++i)
//    {
//        for(size_t j = 0; j < m.n_cols; ++j)
//        {
//            std::stringstream ss;
//            ss << mat(i,j);
//            source = source + ss.str();

//            if (j != m.n_cols - 1)
//            {
//                source = source + ",";
//            }
//        }
//        source = source + "]";

//        if (i != m.n_rows - 1)
//        {
//            source = source + ",";
//        }
//    }
//    source = source + "]]";

//    return real_2d_array(source.c_str());
//}

//real_1d_array DMP::UMITSMP::vec2real1d(const vec& v)
//{
//    std::string source = "[";
//    for(size_t i = 0; i < v.size(); ++i)
//    {
//        std::stringstream ss;
//        ss << v(i);
//        source = source + ss.str();

//        if (i != v.size() - 1)
//        {
//            source = source + ",";
//        }
//    }
//    source = source + "]";

//    return real_1d_array(source.c_str());
//}

Eigen::Quaterniond DMP::UMITSMP::quatMultiply(const Eigen::Quaterniond &q2, const Eigen::Quaterniond &q1)
{
    Eigen::Quaterniond qres;
    qres.w() = q1.w() * q2.w() - q1.x() * q1.x() - q1.y() * q2.y() - q1.z() - q2.z();
    qres.x() = q1.w() * q2.x() + q1.x() * q2.w() - q1.y() * q2.z() + q1.z() * q2.y();
    qres.y() = q1.w() * q2.y() + q1.x() * q2.z() + q1.y() * q2.w() - q1.z() * q2.x();
    qres.z() = q1.w() * q2.z() - q1.x() * q2.y() + q1.y() * q2.x() + q1.z() * q2.w();

    return qres;
}

DMP::SampledTrajectoryV2 DMP::UMITSMP::calculateTrajectory(const DMP::DVec &timestamps, const DMP::DVec &goal, const Vec<DMP::DMPState> &initialStates, const DMP::DVec &initialCanonicalValues, double temporalFactor)
{

    Vec<DMPState> previousState = initialStates;
    std::map<double, DVec > resultingSystemStatesMap;

    double canVal = initialCanonicalValues[0];

    {
        DVec positions;
        for (unsigned int d = 0; d < previousState.size(); d++)
        {
            positions.push_back(previousState.at(d).pos);
        }

        resultingSystemStatesMap[0] = positions;
    }

    double deltaT = 0;
    DVec targetState;
    for (size_t i = 1; i < timestamps.size(); i++)
    {
        deltaT = timestamps[i] - timestamps[i-1];

        struct timeval start, end;
        gettimeofday(&start, nullptr);

        previousState = calculateDirectlyVelocity(previousState, canVal, deltaT, targetState);


        gettimeofday(&end, nullptr);
        double delta = ((end.tv_sec  - start.tv_sec) * 1000000u +
                        end.tv_usec - start.tv_usec) / 1.e6;
        //        std::cout << "time for one step: " << delta << std::endl;

        canVal -= 1.0/tau * deltaT;

        DVec positions;
        //        for (unsigned int d = 0; d < previousState.size(); d++)
        //        {
        //            positions.push_back(previousState.at(d).pos);
        //        }


        for (unsigned int d = 0; d < targetState.size(); d++)
        {
            positions.push_back(targetState.at(d));
        }
        resultingSystemStatesMap[timestamps[i]] = positions;
    }
    return SampledTrajectoryV2(resultingSystemStatesMap);

}


void DMP::UMITSMP::loadWeightsFromFile(const std::string &fileName)
{
    using Tokenizer = boost::tokenizer< boost::escaped_list_separator<char> >;
    std::vector< std::string > stringVec;
    std::string line;
    std::ifstream ifs(fileName);

    int i = 0;
    while (getline(ifs, line))
    {
        Tokenizer tok(line);
        stringVec.assign(tok.begin(), tok.end());
        std::vector<double> weights;
        for(const auto & j : stringVec)
        {
            weights.push_back(fromString<double>(j));
        }

        setWeights(i, weights);
        i++;
    }

    ifs.close();
}

void DMP::UMITSMP::setupWithWeights(const std::vector<std::vector<double> > &weights)
{
    if(weights.size() != 7)
    {
        throw Exception("the weights vector should have 7 dimensions;");
    }

    dimOfTraj = weights.size();
    setKernels(dimOfTraj);
    setWeights(weights);
}
