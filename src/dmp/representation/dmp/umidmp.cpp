#include "umidmp.h"
#include <Eigen/Dense>
#include "general/helpers.h"


DMP::UMIDMP::UMIDMP(int kernelSize, int baseMode , double tau, double eps):
    baseMode(baseMode)
{
    FunctionApproximationInterfacePtr approximater(new LWR<GaussKernel>(kernelSize));
    setFunctionApproximator(approximater);
    canonicalSystem.reset(new LinearDecayCanonicalSystem(tau));

    ODE::setEpsabs(1e-3);
    canonicalSystem->setEpsabs(1e-3);
    this->tau = tau;

    numOfDemos = 1;

}

void DMP::UMIDMP::learnFromTrajectories(const Vec<DMP::SampledTrajectoryV2> &trainingTrajectories)
{
    if (trainingTrajectories.size() == 0)
    {
        throw Exception() << "The training trajectories is empty ... ";
    }


    trainingTrajs = trainingTrajectories;


    DVec normTimestamps = trainingTrajs.at(0).getTimestamps();
    Vec<DVec> uValuesOrig;
    canonicalSystem->integrate(normTimestamps, 1.0, uValuesOrig);
    DVec uValues;
    for (size_t i = 0; i < uValuesOrig.size(); ++i)
    {
        uValues.push_back(uValuesOrig[i][0]);
    }


    dimOfTraj = trainingTrajs.at(0).dim();;
    numOfDemos = trainingTrajs.size();

    canVals = uValues;

    {
         DMP::SampledTrajectoryV2 trainingTrajectory = trainingTrajs[0];
         defaultGoal.clear();
         for(size_t dim = 0; dim < dimOfTraj; ++dim)
         {
             defaultGoal.push_back(trainingTrajectory.rbegin()->getPosition(dim));
         }

    }

    // calculate forces for each demonstrations and each dimensions
    DMP::Vec<mat > fMatList;
    mat avgfMat(dimOfTraj, uValues.size(), fill::zeros);
    for(size_t i = 0; i < numOfDemos; ++i)
    {
        DMP::SampledTrajectoryV2 trainingTrajectory = trainingTrajs[i];
        //        trainingTrajectory.differentiateDiscretly(2);
        trainingTrajectory = SampledTrajectoryV2::normalizeTimestamps(trainingTrajectory, 0, 1);
        trainingTrajs[i] = trainingTrajectory;

        DVec training_goal;
        DVec training_start;
        mat fMat(dimOfTraj, uValues.size()); // forceMat: D x T (D: dimension, T: sample points)
        for(size_t dim = 0; dim < dimOfTraj; ++dim)
        {
            training_start.push_back(trainingTrajectory.begin()->getPosition(dim));
            training_goal.push_back(trainingTrajectory.rbegin()->getPosition(dim));

            vec force = getForce(dim, normTimestamps, uValues, trainingTrajectory);
            fMat.row(dim) = force.t();
        }
        fMatList.push_back(fMat);
        avgfMat = avgfMat + fMat;
    }

    avgfMat /= float(numOfDemos);
    unsigned int funcId = 0;

    for(size_t dim = 0; dim < dimOfTraj; ++dim)
    {
        DoubleMap forceSamples = createDoubleMap(uValues, avgfMat.row(dim).t());
        _trainFunctionApproximator(funcId, forceSamples);
        funcId++;
    }

    if(numOfDemos > 1)
    {
        std::cout << "More than one trajectories are found, PCA is now being called ..." << std::endl;

        mat G(uValues.size(), uValues.size(), fill::zeros);
        for(size_t i = 0; i < numOfDemos; ++i)
        {
            mat fMat = fMatList[i] - avgfMat;
            fMatList[i] = fMat;
            G = G + fMat.t() * fMat;
        }
        G /= double(numOfDemos);

        mat U;
        vec SVec;
        mat V;
        svd(U,SVec,V,G);
        double totalsVal = sum(SVec);

        double ratio = 0;
        double sVal = 0;
        int idOfPCs = 0;


        scoreMatList.clear();
        while(ratio < 0.99)
        {
            sVal += SVec(idOfPCs);
            ratio = sVal / totalsVal;

            int colId = idOfPCs;
            mat sMat(dimOfTraj, numOfDemos);
            for(size_t i = 0; i < numOfDemos; ++i)
            {
                sMat.col(i) = fMatList[i] * V.col(colId);
            }
            scoreMatList.push_back(sMat);

            DoubleMap forceSamples = createDoubleMap(uValues, V.col(colId));
            _trainFunctionApproximator(funcId, forceSamples);
            funcId++;
            idOfPCs++;
        }

        std::cout << "number of PCs: " << idOfPCs << std::endl;
    }
}

//#define uInv(x) -log(x)*6.90776
#define uInv(x) x
void DMP::UMIDMP::_trainFunctionApproximator(unsigned int funcID, const DMP::DoubleMap &forceSamples)
{
    FunctionApproximationInterfacePtr approximator = _getFunctionApproximator(funcID);
    DoubleMap forceSamplesInv;
    DoubleMap::const_iterator it = forceSamples.begin();

    for (; it != forceSamples.end(); ++it)
    {
        forceSamplesInv[uInv(it->first)] = it->second;
    }

    DVec timestamps;
    DVec values;

    for (it = forceSamplesInv.begin(); it != forceSamplesInv.end(); ++it)
    {
        timestamps.push_back(it->first);
        values.push_back(it->second);
    }

    approximator->learn(timestamps, values);
}

vec DMP::UMIDMP::getForce(int dim, const DVec& timestamps, const DMP::DVec &canonicalValues, const DMP::SampledTrajectoryV2 &exampleTraj)
{
    double x0 = exampleTraj.begin()->getPosition(dim);
    double xT = exampleTraj.rbegin()->getPosition(dim);

    DVec::const_iterator itCanon = canonicalValues.begin();
    //    SampledTrajectoryV2::ordered_view::const_iterator itTraj = exampleTraj.begin();

    vec result(canonicalValues.size());
    int i = 0;

    vec mjcoef_t(6, fill::zeros);
    if(baseMode == UMIDMP_BASE_MINJERK)
    {
        vec cond(6);
        cond(0) = xT;
        cond(1) = 0;
        cond(2) = 0;
        cond(3) = x0;
        cond(4) = 0;
        cond(5) = 0;

        mjcoef_t = getMinJerkCoef(canonicalValues[0], cond);
    }

    for (; itCanon != canonicalValues.end(); ++itCanon, ++i)
    {
        const double& pos = exampleTraj.getState(timestamps[i],dim);

        double u = *itCanon;
        double lpart;

        if(baseMode == UMIDMP_BASE_MINJERK)
        {
            vec u_vec = {1, u, pow(u,2), pow(u,3), pow(u,4), pow(u,5)};
            lpart = dot(u_vec, mjcoef_t);
        }
        else
        {
            lpart = - (xT - x0) * u + xT;
        }

        double value = (pos - lpart);
        checkValue(value);
        result(i) = value;
    }

    return result;
}

DMP::DoubleMap DMP::UMIDMP::createDoubleMap(const DMP::DVec &canonicalVals, const vec &sampleVec)
{
    DoubleMap result;
    for(size_t i = 0; i < canonicalVals.size(); ++i)
    {
        result[canonicalVals[i]] = sampleVec(i);
    }
    return result;
}


vec DMP::UMIDMP::calcForce(double u)
{
    vec fVec(dimOfTraj);
    unsigned int funcId = 0;
    for(size_t dim = 0; dim < dimOfTraj; ++dim)
    {
        double avgForce = _getPerturbationForce(funcId, u);
        funcId++;
        fVec(dim) = avgForce;
    }

    if(numOfDemos == 1)
    {
        return fVec;
    }
    else
    {

        if(styleParas.size() == 0)
        {
            DMP::DVec ratios;
            for(size_t i = 0; i < numOfDemos; ++i)
            {
                ratios.push_back(1/(float)numOfDemos);
            }
            styleParas = getStyleParasWithRatio(ratios);
        }

        size_t numOfPCs = scoreMatList.size();
        for(size_t i = 0; i < numOfPCs; ++i)
        {
            double pcForce = _getPerturbationForce(funcId,u);
            funcId++;

            fVec += styleParas[i] * pcForce;
        }
        if(fVec.has_nan() || fVec.has_inf())
        {
            throw DMP::Exception("calculated force contains nan or inf");
        }
        return fVec;
    }
}

vec DMP::UMIDMP::calcForceWithDeri(double u, vec &deriForce)
{
    vec fVec(dimOfTraj);
    deriForce.clear();
    deriForce.resize(dimOfTraj);
    unsigned int funcId = 0;
    for(size_t dim = 0; dim < dimOfTraj; ++dim)
    {
        double avgForce = _getPerturbationForce(funcId, u);
        double avgDerivForce = _getPerturbationForceDeriv(funcId, u);

        funcId++;
        fVec(dim) = avgForce;
        deriForce(dim) = avgDerivForce;
    }

    if(numOfDemos == 1)
    {
//        if(fVec.has_nan() || fVec.has_inf())
//        {
//            throw DMP::Exception("calculated force contains nan or inf");
//        }
        return fVec;
    }
    else
    {

        if(styleParas.size() == 0)
        {
            DMP::DVec ratios;
            for(size_t i = 0; i < numOfDemos; ++i)
            {
                ratios.push_back(1/(float)numOfDemos);
            }
            styleParas = getStyleParasWithRatio(ratios);
        }

        size_t numOfPCs = scoreMatList.size();
        for(size_t i = 0; i < numOfPCs; ++i)
        {
            double pcForce = _getPerturbationForce(funcId,u);
            double pcForceDeriv = _getPerturbationForceDeriv(funcId, u);
            funcId++;

            fVec += styleParas[i] * pcForce;
            deriForce += styleParas[i] * pcForceDeriv;
        }
//        if(fVec.has_nan() || fVec.has_inf())
//        {
//            throw DMP::Exception("calculated force contains nan or inf");
//        }
        return fVec;
    }
}

vec DMP::UMIDMP::getMinJerkCoef(double terminalTime, const vec &condition)
{
    double T = terminalTime;

    mat A(6,6,fill::zeros);

    A(0,0) = 1;
    A(1,1) = 1;
    A(2,2) = 2;

    for(size_t i = 0; i < 6; ++i)
    {
        A(3,i) = pow(T,i);
        A(4,i) = i * pow(T,i-1);
        A(5,i) = i * (i-1) * pow(T, i-2);
    }

    vec a = inv(A) * condition;
    return a;
}


DMP::Vec<vec> DMP::UMIDMP::getStyleParasWithRatio(const DMP::DVec &ratios)
{
    if(ratios.size() != numOfDemos)
    {
        throw Exception("getStyeParasWithRatio: ratios size should be the same as number of demos");
    }

    DMP::Vec<vec> result;
    for(size_t i = 0; i < scoreMatList.size(); ++i)
    {
        mat sMat = scoreMatList[i];

        vec coef(dimOfTraj, fill::zeros);
        for(size_t j = 0; j < numOfDemos; ++j)
        {
            coef += ratios[j] * sMat.col(j);
        }
        result.push_back(coef);
    }

    return result;
}


DMP::Vec<DMP::DMPState> DMP::UMIDMP::calculateTrajectoryPoint(double t, const DVec& goal, double tInit, const Vec<DMPState> &currentStates, DVec& canonicalValues, double temporalFactor)
{
    if(goal.size() != dimOfTraj)
    {
        throw DMP::Exception("The goal's dimension is not correct");
    }

    if(currentStates.size() != dimOfTraj)
    {
        throw DMP::Exception("The startPositions' dimension is not correct.");
    }


    ODE::setDim(1 + dimOfTraj * 2);
    DVec initialODEConditions = canonicalValues;
    DVec initialStatesVec = SystemStatus::convertStatesToArray(currentStates);
    initialODEConditions.insert(initialODEConditions.end(), initialStatesVec.begin(), initialStatesVec.end());
    setTemporalFactor(temporalFactor);

    DVec rawResult;
    ODE::integrate(t, tInit, initialODEConditions, rawResult, false);

    canonicalValues.assign(rawResult.begin(), rawResult.begin() + 1);
    rawResult.erase(rawResult.begin());
    Vec<DMPState> result = SystemStatus::convertArrayToStates<DMPState>(rawResult);

    return result;
}

DMP::SampledTrajectoryV2 DMP::UMIDMP::calculateTrajectory(const DMP::DVec &timestamps, const DMP::DVec &goal, const Vec<DMP::DMPState> &initialStates, const DMP::DVec &initialCanonicalValues, double temporalFactor)
{
    prepareExecution(goal, initialStates, initialCanonicalValues, temporalFactor);

    Vec<DMPState> previousState = initialStates;
    std::map<double, DVec > resultingSystemStatesMap;

    double canVal = initialCanonicalValues[0];

    {
        DVec positions;
        for (unsigned int d = 0; d < previousState.size(); d++)
        {
            positions.push_back(previousState.at(d).pos);
        }

        resultingSystemStatesMap[0] = positions;
    }

    double deltaT = 0;
    DVec targetState;
    for (size_t i = 1; i < timestamps.size(); i++)
    {
        deltaT = timestamps[i] - timestamps[i-1];

        struct timeval start, end;
        gettimeofday(&start, nullptr);

        previousState = calculateDirectlyVelocity(previousState, canVal, deltaT, targetState);

        gettimeofday(&end, nullptr);
        double delta = ((end.tv_sec  - start.tv_sec) * 1000000u +
                        end.tv_usec - start.tv_usec) / 1.e6;
        //        std::cout << "time for one step: " << delta << std::endl;

        canVal -= 1.0/tau * deltaT;

        DVec positions;
        for (unsigned int d = 0; d < previousState.size(); d++)
        {
            positions.push_back(previousState.at(d).pos);
        }

        resultingSystemStatesMap[timestamps[i]] = positions;
    }
    return SampledTrajectoryV2(resultingSystemStatesMap);

}

double DMP::UMIDMP::_getPerturbationForce(unsigned int funcID, DMP::DVec canonicalStates) const
{
    if (canonicalStates.at(0) == 0)
    {
        throw Exception("The canonical state must not be 0!");
    }

    return (*_getFunctionApproximator(funcID))(uInv(canonicalStates.at(0))).at(0);
}

double DMP::UMIDMP::_getPerturbationForceDeriv(unsigned int funcID, DMP::DVec canonicalStates) const
{
    if (canonicalStates.at(0) == 0)
    {
        throw Exception("The canonical state must not be 0!");
    }

    return (*_getFunctionApproximator(funcID)).getDeriv(uInv(canonicalStates.at(0))).at(0);
}


void DMP::UMIDMP::setViaPoint(const ViaPointMap &viapoint, bool CanonicalValueAsKey)
{
    double u;
    if(CanonicalValueAsKey)
    {
        u = viapoint.first;
    }
    else
    {
        double t = viapoint.first;
        u = canonicalSystem->getCanonicalValue(t)[0];
    }

    DVec point = viapoint.second;
    vec force = calcForce(u);

    if(point.size() != force.n_rows)
    {
        throw DMP::Exception("Error: setViaPoint does not allow insertion of a point with different dimensions !!!");
    }

    for(size_t i = 0; i < point.size(); ++i)
    {
        point[i] -=  force(i);
    }

    // check whether viapoint is already there

    bool isExist = false;
    for(auto & viaPoint : viaPoints)
    {
        if(viaPoint.first == u)
        {
            viaPoint.second = point;
            isExist = true;
            break;
        }
    }


    if(!isExist)
    {
        viaPoints.push_back(std::make_pair(u, point));
    }


    sort(viaPoints.begin(), viaPoints.end(),
         [](const ViaPointMap& lhs, const ViaPointMap& rhs) {return lhs.first < rhs.first;});
}


void DMP::UMIDMP::prepareExecution(const DMP::DVec &goal, const Vec<DMP::DMPState> &initialStates, const DMP::DVec &initialCanonicalValues, double temporalFactor)
{
    setTemporalFactor(temporalFactor);
//    dimOfTraj = getWeights().size();
    if(goal.size() != dimOfTraj)
    {
        throw DMP::Exception("The goal's dimension is not correct: goal.size: " + std::to_string(goal.size()) + " expected size:" + std::to_string(dimOfTraj));
    }

    if(initialStates.size() != dimOfTraj)
    {
        throw DMP::Exception("The current states' dimension is not correct. initialState.size: " + std::to_string(initialStates.size()) + " expected size: " + std::to_string(dimOfTraj));
    }

    setGoals(goal);
    DMP::DVec starts;
    for(size_t i = 0; i < initialStates.size(); ++i)
    {
        starts.push_back(initialStates[i].pos);
    }
    setStartPositions(starts);


    //    double min_u = canonicalSystem->getUMin();
    double max_u = initialCanonicalValues[0];

    ViaPointMap viapoint0(canonicalSystem->getUMin(), goal);
    ViaPointMap viapoint1(max_u, starts);
    setViaPoint(viapoint0);
    setViaPoint(viapoint1);

    sort(viaPoints.begin(), viaPoints.end(),
         [](const ViaPointMap& lhs, const ViaPointMap& rhs) {return lhs.first < rhs.first;});

    switch(baseMode)
    {
    case UMIDMP_BASE_LINEAR:
        break;


    case UMIDMP_BASE_MINJERK:
    {
        for(size_t i = 0; i < viaPoints.size() - 1; ++i)
        {
            mat mjcoef_m = mat(dimOfTraj, 6, fill::zeros);

            double cval_d = viaPoints[i+1].first - viaPoints[i].first;
            DVec cgoal = viaPoints[i].second;
            DVec cinit = viaPoints[i+1].second;
            for(size_t dim = 0; dim < dimOfTraj; ++dim)
            {
                vec mjcoef_t(6, fill::zeros);
                vec cond(6);
                cond(0) = cgoal[dim];
                cond(1) = 0;
                cond(2) = 0;
                cond(3) = cinit[dim];
                cond(4) = 0;
                cond(5) = 0;

                mjcoef_t = getMinJerkCoef(cval_d, cond);
                mjcoef_m.row(dim) = mjcoef_t.t();
            }

        }
        break;
    }
    case UMIDMP_BASE_OPTIMIZED:
    {
        throw DMP::Exception("not implemented");
    }
    }

}

DMP::Vec<DMP::DMPState> DMP::UMIDMP::calculateDirectlyVelocity(const Vec<DMP::DMPState> &currentState, double canVal, double deltaT, DMP::DVec &targetState)
{
    Vec<DMP::DMPState> nextState = currentState;
    double u = canVal;
    unsigned int dimensions = dimOfTraj;

    DVec cinits;
    DVec cgoals;

    double u0, u1;

    if(u < canonicalSystem->getUMin())
    {
        u = canonicalSystem->getUMin() + 1e-6;
    }
    ViaPointSet::iterator it_next = std::lower_bound(viaPoints.begin(), viaPoints.end(), u,
                                                      [](const ViaPointMap& element, double uval){ return element.first < uval;} );

    if(it_next == viaPoints.end())
    {
        return currentState;
    }
    cinits = it_next->second;
    u0 = it_next->first;
    cgoals = (--it_next)->second;
    u1 = it_next->first;

    vec dfVec;
    vec fVec = calcForceWithDeri(u, dfVec);

    // model predictive control to get extra_term
    double curDim = 0;
    while (curDim < dimensions)
    {
        double force = fVec(curDim);
        double dforce = dfVec(curDim);
        double vel = currentState[curDim].vel;

        // calculate lpart:
        double x0 = cinits[curDim];
        double x1 = cgoals[curDim];
        double b = (x1 * u0 - x0 * u1) / (u0 - u1);
        double a;
        if(u0 != 0)
            a = (x0 - b) / u0;
        else
            a = (x1 - b) / u1;
        double lpart = a * u + b;

        // calculate target
        double ctarget = lpart + force;
        double cdtarget = -a - dforce;

        nextState[curDim].vel = cdtarget;
        nextState[curDim].pos += vel * deltaT;


        if(targetState.size() < curDim+1)
        {
            targetState.push_back(ctarget);
        }
        else
        {
            targetState[curDim] = ctarget;
        }

        curDim++;
    }

    return nextState;
}


void DMP::UMIDMP::loadWeightsFromFile(const std::string &fileName)
{
    using Tokenizer = boost::tokenizer< boost::escaped_list_separator<char> >;
    std::vector< std::string > stringVec;
    std::string line;
    std::ifstream ifs(fileName);

    int i = 0;
    while (getline(ifs, line))
    {
        Tokenizer tok(line);
        stringVec.assign(tok.begin(), tok.end());
        std::vector<double> weights;
        for(const auto & j : stringVec)
        {
            weights.push_back(fromString<double>(j));
        }

        setWeights(i, weights);
        i++;
    }

    ifs.close();
}

void DMP::UMIDMP::setupWithWeights(const std::vector<std::vector<double> > &weights)
{
    dimOfTraj = weights.size();
    setKernels(dimOfTraj);
    setWeights(weights);
}




//real_2d_array DMP::UMIDMP::mat2real2d(const mat& m)
//{
//    std::string source = "[[";


//    for(size_t i = 0; i < m.n_rows; ++i)
//    {
//        for(size_t j = 0; j < m.n_cols; ++j)
//        {
//            std::stringstream ss;
//            ss << mat(i,j);
//            source = source + ss.str();

//            if (j != m.n_cols - 1)
//            {
//                source = source + ",";
//            }
//        }
//        source = source + "]";

//        if (i != m.n_rows - 1)
//        {
//            source = source + ",";
//        }
//    }
//    source = source + "]]";

//    return real_2d_array(source.c_str());
//}

//real_1d_array DMP::UMIDMP::vec2real1d(const vec& v)
//{
//    std::string source = "[";
//    for(size_t i = 0; i < v.size(); ++i)
//    {
//        std::stringstream ss;
//        ss << v(i);
//        source = source + ss.str();

//        if (i != v.size() - 1)
//        {
//            source = source + ",";
//        }
//    }
//    source = source + "]";

//    return real_1d_array(source.c_str());
//}
