/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "periodicdmp.h"

#include <dmp/general/exception.h>
#include <dmp/general/helpers.h>

#include <limits>



using namespace DMP;




///////////////////////////////////////////////////////////////////////////
// Implementation
///////////////////////////////////////////////////////////////////////////


PeriodicDMP::PeriodicDMP(int kernelSize, double D, double tau, double r):
    K((D / 2) * (D / 2)),
    D(D)
{
    FunctionApproximationInterfacePtr approximater(new LWR<LWLPeriodicGaussGAMS>(kernelSize));
    setFunctionApproximator(approximater);
    canonicalSystem.reset(new PeriodicCanonicalSystem(tau, r));
    ODE::setEpsabs(1e-3);
    canonicalSystem->setEpsabs(1e-3);
    amplitudes.push_back(1.0);
}


void PeriodicDMP::learnFromTrajectories(const Vec<SampledTrajectoryV2 > & trainingTrajectories)
{
    std::cout << "PeriodicDMP: Learning From Trajectories" << std::endl;
    if (trainingTrajectories.size() > 1)
    {
        throw Exception() << "this DMP can only be trained with one trajectory (but with several dimensions)";
    }

    if (trainingTrajectories.size() == 0)
    {

        throw Exception() << "There are no trajectories to learn from";
    }

    trainingTraj = trainingTrajectories;
    SampledTrajectoryV2& trainingTrajectory = trainingTraj[0];
    __trajDim = trainingTrajectory.dim();

    // normalize timestamps so that they start at 0 and end at 1
//    trainingTrajectory = SampledTrajectoryV2::normalizeTimestamps(trainingTrajectory, 0.0, 1.0);
    trainingTrajectory.differentiateDiscretly(2);
    DVec normTimestamps = trainingTrajectory.getTimestamps();


    Vec<DVec> uValuesOrig;
    canonicalSystem->integrate(normTimestamps, canonicalSystem->getInitValues(), uValuesOrig);
    DVec uValues;
    DoubleMap uValuesPlotMap;

    for (unsigned int i = 0; i < uValuesOrig.size(); ++i)
    {
        uValues.push_back(uValuesOrig[i][0]);
        uValuesPlotMap[normTimestamps.at(i)] = uValuesOrig[i][0] * 2 * M_PI;
    }

    if (getShowPlots())
    {
        plot(uValuesPlotMap, "plots/canonical_system_%d.gp", true, 0);
    }

    unsigned int trajDim = trainingTrajectory.dim();
    SampledTrajectoryV2 traj;

    if(goals.size() < trainingTrajectory.dim())
    {
        goals = trainingTrajectory.calcAnchorPoint();
        for(size_t i = 0; i < trainingTrajectory.dim(); ++i)
        {
            if(i >= goals.size())
                goals.push_back(0.0);
        }
    }

    if(amplitudes.size() < trainingTrajectory.dim())
    {
        for(size_t i = amplitudes.size(); i < trainingTrajectory.dim(); i++)
            amplitudes.push_back(1.0);
    }

    std::cout << "goals size: " << goals.size() << std::endl;

    std::cout << "goals: ";
    for(size_t i = 0; i < goals.size(); ++i)
        std::cout << goals[i] << " ";

    std::cout << std::endl;

    for (unsigned int dim = 0; dim < trajDim; dim++)
    {

        DoubleMap perturbationForceSamples = calcPerturbationForceSamples(dim, uValues, trainingTrajectory);
        _trainFunctionApproximator(dim, perturbationForceSamples);

        if (getShowPlots())
        {
            DoubleMap learnedData;
            DVec perturbationSamples;
            DVec timestamps;
            for(auto & perturbationForceSample : perturbationForceSamples)
            {
                timestamps.push_back(perturbationForceSample.first);
                perturbationSamples.push_back(perturbationForceSample.second);
            }
            for (unsigned int i = 0; i < uValues.size(); i++)
            {
                learnedData[uValues[i]] = _getPerturbationForce(dim, uValues[i]);

            }
            traj.addDimension(timestamps, perturbationSamples);
            Vec<DoubleMap> curves;
            curves.push_back(perturbationForceSamples);
            curves.push_back(learnedData);
            plot(curves, "plots/learningData_%d.gp", true);
        }
    }

}


void PeriodicDMP::_trainFunctionApproximator(unsigned int trajDimIndex, const DoubleMap& perturbationForceSamples)
{
    FunctionApproximationInterfacePtr approximator = _getFunctionApproximator(trajDimIndex);
    DoubleMap perturbationForceSamplesInv;
    DoubleMap::const_iterator it = perturbationForceSamples.begin();

    // u(t) has a lot of values near uMin (e.g. 0.001) -> baaad for lwpr ->
    // use inverse of u(t) to distribute them regularly
    for (; it != perturbationForceSamples.end(); ++it)
    {
        perturbationForceSamplesInv[it->first] = it->second;
    }

    DVec phases;
    DVec values;

    for (it = perturbationForceSamplesInv.begin(); it != perturbationForceSamplesInv.end(); ++it)
    {
        phases.push_back(it->first);
        values.push_back(it->second);
    }

    approximator->learn(phases, values);



    if (getShowPlots())
    {
        // evaluate quality of training data
        DoubleMap::iterator itInv = perturbationForceSamplesInv.begin();
        it = perturbationForceSamples.begin();
        DoubleMap errors;

        for (; it != perturbationForceSamples.end(); ++itInv, it++)
        {
            errors[it->first] = it->second - _getPerturbationForce(trajDimIndex, it->first) ;
        }

        plot(errors, "learningError.gp", true, 0.0);
    }
}




const SampledTrajectoryV2& PeriodicDMP::getTrainingTraj(unsigned int trajSampleIndex) const
{
    return trainingTraj.at(trajSampleIndex);

}



void PeriodicDMP::flow(double t, const DVec& x, DVec& out)
{

    double u = x.at(0);

    DVec canonicalState(1);
    canonicalState[0] = u;
    canonicalSystem->flow(t, canonicalState, out);

    double tau = canonicalSystem->getTau();


    unsigned int dimensions =  __trajDim;

    if (dimensions > goals.size())
    {
        throw Exception("There are less trajectory goals than dimensions");
    }

    if (dimensions > startPositions.size())
    {
        throw Exception("There are less trajectory start positions than dimensions");
    }



    unsigned int offset = canonicalState.size();
    unsigned int curDim = 0;

    while (curDim < dimensions)
    {
        double force = _getPerturbationForce(curDim, u);

        unsigned int i = curDim * 2 + offset;
        double pos = x[i];
        double vel = x[i + 1];

        double dpos = 1.0 / tau * (vel);
        double dvel = 1.0 / tau * (K  * (goals[curDim] - pos) - (D  * vel) + getAmpl(curDim) * force);
        double dt = this->epsabs();

        out[i++] = 1.0 / tau * (vel);
        out[i++] = 1.0 / tau * (K  * (goals[curDim] - pos) - (D  * vel) + getAmpl(curDim) * force);
        curDim++;
    }

//    std::cout << "=============== " << std::endl;
}


double
PeriodicDMP::_getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const
{
    //    return 0;
//    if (canonicalStates.at(0) == 0)
//    {
//        throw Exception("The canonical state must not be 0!");
//    }
    double phi = canonicalStates.at(0);
    phi -= floor(phi / (2*M_PI)) * 2 * M_PI;
    return (*_getFunctionApproximator(trajDim))(phi).at(0);
}





DoubleMap PeriodicDMP::calcPerturbationForceSamples(unsigned int trajDim, const DVec& canonicalValues, const SampledTrajectoryV2& exampleTraj) const
{


    DoubleMap result;


    if (exampleTraj.size() == 0)
    {
        throw Exception("Sample traj is empty");
    }

    if (canonicalValues.size() != exampleTraj.size())
    {
        throw Exception("The amount of the canonicalValues do not match the exampleTrajectory");
    }

    double tau = canonicalSystem->getTau();

    DVec::const_iterator itCanon = canonicalValues.begin();
    SampledTrajectoryV2::ordered_view::const_iterator itTraj = exampleTraj.begin();

    int i = 0;

    for (; itCanon != canonicalValues.end(); ++itCanon, ++i, itTraj++)
    {
        const double& pos = itTraj->getPosition(trajDim);
        const double& vel = itTraj->getDeriv(trajDim, 1);
        const double& acc = itTraj->getDeriv(trajDim, 2);

//        std::cout << "K: " << K << " D: " << D << " goal: " << goals[trajDim] << " tau: " << tau << " ampl: " << getAmpl(trajDim) << std::endl;
//        std::cout << "pos: " << pos << " vel: " << vel << " acc: " << acc << std::endl;

        double value = (-K  * (goals[trajDim] - pos) +   D * vel  + tau * acc) / getAmpl(trajDim);
        checkValue(value);

//        cout << *itCanon << '\t' << value << std::endl;
        result[*itCanon] =  value;
    }

    return result;
}



Vec<DMPState>
PeriodicDMP::calculateTrajectoryPoint(double t, const DVec& goal, double tInit, const Vec<DMPState> &currentStates, DVec& canonicalValues, double temporalFactor)
{

    if (startPositions.size() == 0)
    {
        std::cout << "No Start Positions set, assuming current state as start position" << std::endl;
        setStartPositions(currentStates);
    }

    ODE::setDim(1 + __trajDim * 2);

    DVec initialODEConditions = canonicalValues;
    DVec initialStatesVec = SystemStatus::convertStatesToArray(currentStates);
    initialODEConditions.insert(initialODEConditions.end(), initialStatesVec.begin(), initialStatesVec.end());

    setTemporalFactor(temporalFactor);
    setGoals(goal);

    DVec rawResult;
    ODE::integrate(t, tInit, initialODEConditions, rawResult, false);


    canonicalValues.assign(rawResult.begin(), rawResult.begin() + 1);
    rawResult.erase(rawResult.begin());
    Vec<DMPState> result = SystemStatus::convertArrayToStates<DMPState>(rawResult);

//    std::cout << rawResult[0] << std::endl;
    return result;

}


void PeriodicDMP::setStartPositions(const Vec<DMPState> &initialStates)
{
    DVec tempStartPositions;

    for (unsigned int i = 0; i < initialStates.size(); i++)
    {
        tempStartPositions.push_back(initialStates[i].pos);
    }

    setStartPositions(tempStartPositions);
}


SampledTrajectoryV2 PeriodicDMP::calculateTrajectory(const DVec& timestamps, const DVec& goal, const Vec<DMPState> &initialStates, const DVec& initialCanonicalValues, double temporalFactor)
{
    if (getShowPlots())
    {
        Vec<DVec> uValuesOrig;
        canonicalSystem->setTau(temporalFactor);
        canonicalSystem->integrate(timestamps, canonicalSystem->getInitValues(), uValuesOrig);
        DoubleMap uValuesPlotMap;

        for (unsigned int i = 0; i < uValuesOrig.size(); ++i)
        {
            uValuesPlotMap[timestamps.at(i)] = uValuesOrig[i][0];
        }

        plot(uValuesPlotMap, "plots/canonical_system_sloweddown%d.gp", true, 0);
    }

    setStartPositions(initialStates);

    return DMPInterfaceTemplate<DMPState>::calculateTrajectory(timestamps, goal, initialStates, initialCanonicalValues, temporalFactor);
}





double PeriodicDMP::evaluateReproduction(double initialCanonicalValue, double& maxErrorOut, bool showPlot)
{
    DVec timestamps;
    timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + getTrainingTraj(0).size()));

    Vec<DMPState> initialState;
    for(size_t i = 0; i < getTrainingTraj(0).dim(); i++)
        initialState.push_back(DMPState(getTrainingTraj(0).begin()->getPosition(i),
                                        getTrainingTraj(0).begin()->getDeriv(i, 1)));

    SampledTrajectoryV2 newTraj = calculateTrajectory(
                timestamps,
                goals,
                initialState,
                initialCanonicalValue,
                1);

    std::cout << "start: [";
    for(size_t i = 0; i < getTrainingTraj(0).dim(); i++)
    {
            std::cout << getTrainingTraj(0).begin()->getPosition(i);
            if(i != getTrainingTraj(0).dim() - 1)
                std::cout << " , ";
    }
    std::cout << "]\n startVel: [";
    for(size_t i = 0; i < getTrainingTraj(0).dim(); i++)
    {
            std::cout << getTrainingTraj(0).begin()->getDeriv(0, 1);
            if(i != getTrainingTraj(0).dim() - 1)
                std::cout << " , ";
    }
    std::cout << "]\n goal: [";
    for(size_t i = 0; i < getTrainingTraj(0).dim(); i++)
    {
        std::cout << goals[i];
        if(i != getTrainingTraj(0).dim() - 1)
            std::cout << " , ";
    }
    std::cout << "]" << std::endl;

    DVec2d statesGen;
    DVec2d statesOrig;
    for(size_t i = 0; i < getTrainingTraj(0).dim(); i++){
        statesGen.push_back(newTraj.getDimensionData(0, i));
        statesOrig.push_back(getTrainingTraj(0).getDimensionData(0, 0));
    }


    Vec<DoubleMap> errorMaps;
    DVec meanErrors;

    for(unsigned int j = 0; j < statesGen.size() && j < statesOrig.size(); j++)
    {
        double meanError = 0.0;
        maxErrorOut = 0.0;
        DoubleMap errorMap;
        double minTrajValue = std::numeric_limits<double>::max();
        double maxTrajValue = std::numeric_limits<double>::min();

        for (unsigned int i = 0; i < statesGen[j].size() && i < statesOrig[j].size(); ++i)
        {
            const double error = fabs(statesGen[j][i] - statesOrig[j][i]);

            if (error > maxErrorOut)
            {
                maxErrorOut = error;
            }

            errorMap[timestamps[i]] = error;
            meanError += error;

            if (minTrajValue > statesOrig[j][i])
            {
                minTrajValue = statesOrig[j][i];
            }

            if (maxTrajValue < statesOrig[j][i])
            {
                maxTrajValue = statesOrig[j][i];
            }

        }

        meanError /= statesOrig[j].size();
        meanError /= maxTrajValue - minTrajValue;
        maxErrorOut /= maxTrajValue - minTrajValue;

        meanErrors.push_back(meanError);
        errorMaps.push_back(errorMap);
    }

    if (showPlot)
    {
        Vec< SampledTrajectoryV2 > trajectories;

        trajectories.push_back(SampledTrajectoryV2::normalizeTimestamps(getTrainingTraj(0)));
        trajectories.push_back(newTraj);
        for(size_t i = 0; i < getTrainingTraj(0).dim(); i++)
        {
            std::stringstream ss;
            ss << i;
            std::string filename = "plots/traj_reproduction_error_"+ss.str()+"_%d.gp";
            plot(errorMaps[i], filename.c_str(), true, 0);
        }
        DVec timestamps = newTraj.getTimestamps();

        Vec<DVec> plots;
        plots.push_back(timestamps);
        for(size_t i = 0; i < getTrainingTraj(0).dim(); i++)
        {
            plots.push_back(getTrainingTraj(0).getDimensionData(i, 0));
            plots.push_back(newTraj.getDimensionData(i, 0));
        }
        plotTogether(plots, "plots/trajComparison_%d.gp");      
        plots.clear();

        plots.push_back(timestamps);
        for(size_t i = 0; i < getTrainingTraj(0).dim(); i++)
        {
            plots.push_back(getTrainingTraj(0).getDimensionData(i, 1));
            plots.push_back(newTraj.getDimensionData(i, 1));
        }
        plotTogether(plots, "plots/trajComparison_vel_%d.gp");
        plots.clear();

        plots.push_back(timestamps);
        for(size_t i = 0; i < getTrainingTraj(0).dim(); i++)
        {
            plots.push_back(getTrainingTraj(0).getDimensionData(i, 2));
            plots.push_back(newTraj.getDimensionData(i, 2));
        }
        plotTogether(plots, "plots/trajComparison_acc_%d.gp",true);
    }

    double sumError = 0.0;
    for(size_t i = 0; i < meanErrors.size(); i++)
        sumError += meanErrors[i];


    return sumError/meanErrors.size();
}


void PeriodicDMP::setConfiguration(string paraID, const paraType& para){
    if (paraID == DMP::SpringFactor)
    {
        setSpringConstant(boost::get<double>(para));
    }
    else if (paraID == DMP::DampingFactor)
    {
        setDampingConstant(boost::get<double>(para));
    }
    else if (paraID == DMP::TemporalFactor)
    {
        setTemporalFactor(boost::get<double>(para));
    }
    else if (paraID == DMP::Goal)
    {
        setGoals(boost::get<DMP::DVec >(para));
    }
    else if (paraID == DMP::StartPosition)
    {
        setStartPositions(boost::get<DMP::DVec>(para));
    }
    else
    {
        std::cout << "Warning: it is not a valid parameter ID" << std::endl;
    }
}



void PeriodicDMP::showConfiguration(string paraID){
    if (paraID == DMP::SpringFactor)
    {
        std::cout << "K: " << K << std::endl;
    }
    else if (paraID == DMP::DampingFactor)
    {
        std::cout << "D: " << D << std::endl;
    }
    else if (paraID == DMP::TemporalFactor)
    {
        std::cout << "tau: " << canonicalSystem->getTau() << std::endl;
    }
    else if (paraID == DMP::Goal)
    {
        std::cout << "goals: " << goals << std::endl;
    }
    else if (paraID == DMP::StartPosition)
    {
        std::cout << "startPositions: " << startPositions << std::endl;
    }
    else
    {
        std::cout << "Warning: it is not a valid parameter ID" << std::endl;
    }
}



