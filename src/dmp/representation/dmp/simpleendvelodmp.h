/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef SIMPLEENDVELODMP_H
#define SIMPLEENDVELODMP_H

#include "basicdmp.h"

namespace DMP
{

    class SimpleEndVeloDMP : public BasicDMP
    {
    public:
        SimpleEndVeloDMP(int kernelSize = 50, double tau = 1, bool stopAtEnd = true);
        void setStopAtEnd(bool stop)
        {
            stopAtEnd = stop;
        }
    protected:
        DoubleMap calcPerturbationForceSamples(unsigned int trajDim, const DVec& canonicalValues, const SampledTrajectoryV2& exampleTraj) const override;
        void _trainFunctionApproximator(unsigned int trajDim, const DoubleMap& perturbationForceSamples) override;
        double _getPerturbationForce(unsigned int trajDim, DVec canonicalStates) const override;
        void flow(double t, const DVec& x, DVec& out) override;

        bool stopAtEnd;


        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {

            ar& boost::serialization::make_nvp("stopAtEnd",stopAtEnd);

            BasicDMP& base = boost::serialization::base_object<BasicDMP>(*this);
            ar& boost::serialization::make_nvp("base",base);

        }
    };
}
#endif // SIMPLEENDVELODMP_H
