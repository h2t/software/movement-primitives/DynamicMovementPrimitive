/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "dmp3rdorderforcefield.h"

using namespace DMP;



DMP3rdOrderForceField::DMP3rdOrderForceField(int kernelSize, double H, double D, double tau) :
    DMP3rdOrder(kernelSize, H, D, tau)
{
}

void DMP3rdOrderForceField::flow(double t, const DVec& x, DVec& out)
{
    double u = x.at(0);

    DVec canonicalState(1);
    canonicalState[0] = u;
    canonicalSystem->flow(t, canonicalState, out);

    double tau = canonicalSystem->getTau();


    unsigned int dimensions = __trajDim;

    if (dimensions > goals.size())
    {
        throw Exception("There are less trajectory goals than dimensions");
    }

    if (dimensions > startPositions.size())
    {
        throw Exception("There are less trajectory start positions than dimensions");
    }



    unsigned int offset = canonicalState.size();
    unsigned int curDim = 0;

    while (curDim < dimensions)
    {
        double force = _getPerturbationForce(curDim, u);
        bool scaling = true;
        std::map<unsigned int, bool>::iterator it = scalingActivated.find(curDim);

        if (it != scalingActivated.end() && !it->second)
        {
            scaling = false;
        }

        if (scaling && goals[curDim] != startPositions[curDim])
        {
            force *= (goals[curDim] - startPositions[curDim]);
        }

        unsigned int i = curDim * SystemType().size() + offset;
        double pos = x[i];
        double vel = x[i + 1];
        double acc = x[i + 2];

        out[i++] = 1.0 / tau * H * (vel - pos);
        out[i++] = 1.0 / tau * (acc);
        out[i++] = 1.0 / tau * (K * (force + startPositions[curDim] - vel) - (D * acc));
        //        std::cout << "t: " << t << " u: " << u << " vel: " << pos << " acc: " << vel << " force: " << force << " resulting Jerk: " << (out[i-2]*H)  << " resulting acc: " << (out[i-3]*H) << " filtered Vel: " << out[i-1] << std::endl;
        curDim++;
    }
}

DoubleMap DMP3rdOrderForceField::calcPerturbationForceSamples(unsigned int trajDim, const DVec& canonicalValues, const SampledTrajectoryV2& exampleTraj) const
{
    DoubleMap result;


    if (exampleTraj.size() == 0)
    {
        throw Exception("Sample traj is empty");
    }

    if (canonicalValues.size() != exampleTraj.size())
    {
        throw Exception("The amount of the canonicalValues do not match the exampleTrajectory");
    }

    // start position of example traj
    double x0 = exampleTraj.begin()->getPosition(trajDim);
    // end position of example traj
    double xT = exampleTraj.rbegin()->getPosition(trajDim);
    bool scaling = true;

    if (xT == x0)
    {
        scaling = scalingActivated[trajDim] = false;
    }

    double tau = canonicalSystem->getTau();
    //    if(scalingActivated.find(trajDim) != scalingActivated.end() && !scalingActivated.at(trajDim))
    //        scaling = false;

    //    exampleTraj.differentiateDiscretlyForDim(trajDim,2);

    //    exampleTraj.plot();
    DVec::const_iterator itCanon = canonicalValues.begin();
    SampledTrajectoryV2::ordered_view::const_iterator itTraj = exampleTraj.begin();
    //    DVec accelerations = exampleTraj.differentiateDiscretlyForDim(trajDim, 1);
    //    DVec jerks = SampledTrajectoryV2::DifferentiateDiscretly(exampleTraj.getTimestamps(), accelerations);
    int i = 0;

    for (; itCanon != canonicalValues.end(); ++itCanon, ++i, itTraj++)
    {
        // first value is time value, so skip it
        const SampledTrajectoryV2::TrajData& state = *itTraj;
        const double& pos = state.getPosition(trajDim);
        const double& vel = state.getDeriv(trajDim, 1);
        const double& acc = state.getDeriv(trajDim, 2);
        const double& jerk = state.getDeriv(trajDim, 3);


        //        result[*itCanon] = ( -K * (xT - pos) +  D * vel  + tau * acc ) / ( xT - x0);

        result[*itCanon] =
            pow(tau, 2) / H * jerk +
            tau * (1 + D / H) * acc +
            (K / H * tau + D) * vel  +
            K * pos;
        result[*itCanon] /= K;
        result[*itCanon] -= x0;

        if (scaling)
        {
            result[*itCanon] /= (xT - x0);
        }

        checkValue(result[*itCanon]);
        //        std::cout << *itCanon << "[" << result[*itCanon] << "] calc. from ( -" << K << " * ("<<xT<<" - " << pos<<" ) +  "<<D<<  "*" << vel << " + "<< tau <<" * "<<acc<<" ) / ( "<< xT <<" - "<< x0 << ")" << std::endl;
        //        std::cout << *itCanon << ": " << result[*itCanon] << std::endl;
    }



    return result;
}
