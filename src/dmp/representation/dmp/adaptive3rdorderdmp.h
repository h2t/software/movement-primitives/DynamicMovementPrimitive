/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef ADAPTIVE3RDORDERDMP_H
#define ADAPTIVE3RDORDERDMP_H

#include "dmp3rdorder.h"
#include "dmp3rdorderforcefield.h"

namespace DMP
{
    class AdaptiveGoal : public ODE
    {

    public:
        AdaptiveGoal(const DVec& initalGoals, float goalChangeFactor = 1.0f);
        void flow(double t, const DVec& x, DVec& out) override;

        void setNewGoals(const DVec& newGoals);
        const DVec& getCurrentGoals() const
        {
            return goals;
        }

    protected:

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            DMPInterface& base = boost::serialization::base_object<DMPInterface>(*this);
            ar& boost::serialization::make_nvp("base",base);

        }

    private:
        float goalChangeFactor;
        DVec goals;

    };

    class AdaptiveGoal3rdOrderDMP : public DMP3rdOrderForceField
    {
    public:
        AdaptiveGoal3rdOrderDMP(int kernelSize = 50):
            DMP3rdOrderForceField(kernelSize)
        {}
        ~AdaptiveGoal3rdOrderDMP() override {}
        Vec<_3rdOrderDMP> calculateTrajectoryPoint(double t, const DVec& goal, double tInit, const Vec<_3rdOrderDMP> &initialStates, DVec& canonicalValues, double temporalFactor) override;

    private:
        boost::shared_ptr<AdaptiveGoal> adaptiveGoal;
        DVec currentGoals;
    };


}
#endif // ADAPTIVE3RDORDERDMP_H
