/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef DMPREGISTRATION_H
#define DMPREGISTRATION_H

#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/export.hpp>
#include <boost/serialization/base_object.hpp>
#include <boost/serialization/serialization.hpp>
#include <boost/archive/xml_iarchive.hpp>
#include <boost/archive/xml_oarchive.hpp>

#include <dmp/functionapproximation/functionapproximationregistration.h>
#include <dmp/math/measure.h>
#include "basicdmp.h"
#include "simpleendvelodmp.h"
#include "dmp3rdorder.h"
#include "endvelodmp.h"
#include "forcefielddmp.h"
#include "quaterniondmp.h"
#include "dualquaterniondmp.h"
#include "endveloforcefielddmp.h"
#include "adaptive3rdorderdmp.h"
//#include "periodictransientdmp.h"

#include "periodicdmp.h"
#include "taskspacedmp.h"
#include "umidmp.h"
#include "umitsmp.h"

BOOST_CLASS_EXPORT_KEY(DMP::ODE)
BOOST_CLASS_EXPORT_KEY(DMP::DMPInterface)
BOOST_CLASS_EXPORT_KEY(DMP::Distance)
BOOST_CLASS_EXPORT_KEY(DMP::PoseDistance)
BOOST_CLASS_EXPORT_KEY(DMP::DistancePtr)
BOOST_CLASS_EXPORT_KEY(DMP::DMPState)
BOOST_CLASS_EXPORT_KEY(DMP::DMPInterfaceTemplate<DMP::DMPState>)
BOOST_CLASS_EXPORT_KEY(DMP::BasicDMP)
BOOST_CLASS_EXPORT_KEY(DMP::SimpleEndVeloDMP)
BOOST_CLASS_EXPORT_KEY(DMP::DMP3rdOrder)
BOOST_CLASS_EXPORT_KEY(DMP::EndVeloDMP)
BOOST_CLASS_EXPORT_KEY(DMP::ForceFieldDMP)
BOOST_CLASS_EXPORT_KEY(DMP::QuaternionDMP)
BOOST_CLASS_EXPORT_KEY(DMP::DualQuaternionDMP)
BOOST_CLASS_EXPORT_KEY(DMP::EndVeloForceFieldDMP)
BOOST_CLASS_EXPORT_KEY(DMP::AdaptiveGoal3rdOrderDMP)
//BOOST_CLASS_EXPORT_KEY(DMP::PeriodicTransientDMP)
BOOST_CLASS_EXPORT_KEY(DMP::PeriodicDMP)
BOOST_CLASS_EXPORT_KEY(DMP::TaskSpaceDMP)
BOOST_CLASS_EXPORT_KEY(DMP::UMIDMP)
BOOST_CLASS_EXPORT_KEY(DMP::UMITSMP)

BOOST_CLASS_EXPORT_KEY(DMP::CanonicalSystem)
BOOST_CLASS_EXPORT_KEY(DMP::LinearCanonicalSystem)
BOOST_CLASS_EXPORT_KEY(DMP::LinearDecayCanonicalSystem)
BOOST_CLASS_EXPORT_KEY(DMP::LinearSigmoidalCanonicalSystem)
BOOST_CLASS_EXPORT_KEY(DMP::SolvedCanonicalSystem)
BOOST_CLASS_EXPORT_KEY(DMP::PeriodicCanonicalSystem)


#define FIXED_BOOST_SERIALIZATION_FACTORY_0(T)                 \
		namespace boost {                                        \
			namespace serialization {                                \
				    template<>                                           \
				    inline T * factory<T, 0>(std::va_list){                     \
					            return new T();                                  \
					        }                                                    \
			}                                                        \
		} 

//FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::DMPInterface)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::BasicDMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::SimpleEndVeloDMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::DMP3rdOrder)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::EndVeloDMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::ForceFieldDMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::QuaternionDMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::DualQuaternionDMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::EndVeloForceFieldDMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::AdaptiveGoal3rdOrderDMP)
//BOOST_SERIALIZATION_FACTORY_0(DMP::PeriodicTransientDMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::PeriodicDMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::TaskSpaceDMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::UMIDMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::UMITSMP)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::CanonicalSystem)
FIXED_BOOST_SERIALIZATION_FACTORY_0(DMP::LinearDecayCanonicalSystem)

#endif
