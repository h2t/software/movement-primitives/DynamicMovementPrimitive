#ifndef MATHTOOLS_H
#define MATHTOOLS_H

#include <armadillo>
#include "general/vec.h"

using namespace arma;
namespace DMP
{
    class MathTools
    {
    public:
        static vec mvrnorm(const vec& mu, const mat& sigma);
        static DVec vec2dvec(const vec& v);
    };

}


#endif // MATHTOOLS_H
