/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2013
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef DMP_DATABASE_H
#define DMP_DATABASE_H

#include <dmp/representation/dmp/dmpinterface.h>
#include <boost/serialization/map.hpp>
#include <boost/serialization/shared_ptr.hpp>
#include <dmp/representation/dmp/basicdmp.h>

namespace DMP
{

    template <typename DMPBaseType>
    class Database
    {
    public:
        typedef boost::shared_ptr<DMPBaseType > DMPTypePtr ;
        typedef std::map<std::string, DMPTypePtr > DMPMap;

        Database() {}

        void addDMP(const std::string& dmpName,  DMPTypePtr dmp);
        template <typename DMPType>
        void addDMP(const std::string& dmpName, const std::string&   fileName)
        {
            SampledTrajectoryV2 traj;
            traj.readFromCSVFile(fileName);
            DMPTypePtr newDMP = new DMPType();
            newDMP->learnFromTrajectory(traj);
            addDMP(dmpName, newDMP);
        }

        DMPTypePtr getDMP(const std::string& dmpName)
        {
            return dmps[dmpName];
        }
        std::vector<std::string> getNames() const;

    private:

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {

            ar& BOOST_SERIALIZATION_NVP(dmps);
        }

        DMPMap dmps;
    };

    template <typename DMPBaseType>
    void Database<DMPBaseType>::addDMP(const std::string& dmpName, Database::DMPTypePtr dmp)
    {
        if (dmpName.empty())
        {
            throw Exception("The dmpName must not be empty!");
        }

        typename DMPMap::iterator it = dmps.find(dmpName);

        if (it == dmps.end() || !dmps[dmpName])
        {
            dmps[dmpName] = dmp;
        }
        else
        {
            throw Exception("DMP with name '") << dmpName << "' already exists!";
        }
    }

    template <typename DMPBaseType>
    std::vector<string> Database<DMPBaseType>::getNames() const
    {
        std::vector<string> result;

        for (typename DMPMap::const_iterator it = dmps.begin(); it != dmps.end(); it++)
        {
            result.push_back(it->first);
        }

        return result;
    }

}

#endif
