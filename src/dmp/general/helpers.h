#ifndef HELPERS_H
#define HELPERS_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cstring>
#include <limits>

#include <cstdio>
#include <cstdlib>
#include <map>

#include "defines.h"
#include "exception.h"

#include "vec.h"

#include <Eigen/Geometry>
#include <boost/tokenizer.hpp>

namespace DMP
{
    typedef std::map<double, double > DoubleMap;
    typedef std::map<DVec, double > DVecMap;

    using namespace std;

    static const double J_EPS = 1.2; // TODO: get rid of this!
    static const int J_LINE_BUF_SIZE = 1024 * 1024;

    inline Eigen::VectorXf vec2eigen(const DVec& x)
    {
        Eigen::VectorXf res(x.size());
        int i = 0;
        for(DVec::const_iterator it = x.begin(); it != x.end(); it++, ++i)
        {
            res(i) = *it;

        }
        return res;
    }

    template<class T>
    inline void checkValue(const T& value)
    {
        if (std::numeric_limits<T>::infinity() == value)
        {
            throw DMP::Exception("result value is inf");
        }

        if (value != value)
        {
            throw DMP::Exception("result value is nan");
        }
    }

    template<class T>
    inline void checkValues(const Vec<T >& values)
    {
        for(size_t i = 0; i < values.size(); ++i)
        {
            checkValue(values[i]);
        }
    }


    template<class T> T fromString(const std::string& s)
    {
        std::istringstream stream(s);
        T t;
        stream >> t;
        return t;
    }

    template <class T> ostream& operator << (ostream& out, const std::vector<T>& v)
    {
        out << "VECTOR (" << v.size() << "): ";

        for (typename std::vector<T>::const_iterator i = v.begin(); i != v.end(); ++i)
        {
            out << *i;

            if (i + 1 != v.end())
            {
                out << ", ";
            }
        }

        out << ";";
        return out;
    }

    template <class T> Vec<T> operator -(const Vec<T> &v1, const Vec<T> &v2)
    {
        Vec<T> res(std::min(v1.size(), v2.size()), T());
        int j = 0;

        for (typename Vec<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v1[j] - v2[j];
            ++j;
        }

        return res;
    }

    template <class T> Vec<T> operator +(const Vec<T> &v1, const Vec<T> &v2)
    {
        Vec<T> res(std::min(v1.size(), v2.size()), T());
        int j = 0;

        for (typename Vec<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v1[j] + v2[j];
            ++j;
        }

        return res;
    }

    template <class T> Vec<T>& operator +=(Vec<T> &v1, const Vec<T> &v2)
    {
        int j = 0;

        for (typename Vec<T>::iterator i = v1.begin(); i != v1.end(); ++i)
        {
            *i += v2[j];
            ++j;
        }

        return v1;
    }


    template <class T> Vec<T>& operator -=(Vec<T> &v1, const Vec<T> &v2)
    {
        int j = 0;

        for (typename Vec<T>::iterator i = v1.begin(); i != v1.end(); ++i)
        {
            *i -= v2[j];
            ++j;
        }

        return v1;
    }

    template <class T> Vec<T>& operator *=(Vec<T> &v1, double c)
    {
        int j = 0;

        for (typename Vec<T>::iterator i = v1.begin(); i != v1.end(); ++i)
        {
            *i *= c;
            ++j;
        }

        return v1;
    }



    template <class T> Vec<T>& operator /=(Vec<T> &v1, double c)
    {
        int j = 0;

        for (typename Vec<T>::iterator i = v1.begin(); i != v1.end(); ++i)
        {
            *i /= c;
            ++j;
        }

        return v1;
    }

    template <class T> Vec<T> operator *(double c, const Vec<T> &v)
    {
        Vec<T> res(v.size(), T());
        int j = 0;

        for (typename Vec<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = c * v[j];
            ++j;
        }

        return res;
    }

    template <class T> double operator *(const Vec<T> &v, const Vec<T> &v2)
    {
        double res = 0;

        if (v.size() != v2.size())
        {
            throw Exception("vectors do not match in size: ") << v.size() << " vs. " << v2.size();
        }

        int j = 0;

        for (typename Vec<T>::const_iterator i = v.begin(); i != v.end(); ++i)
        {
            res += *i * v2[j];
            ++j;
        }

        return res;
    }


    template <class T> Vec<T> operator +(const Vec<T> &v, double s)
    {
        Vec<T> res(v.size(), T());
        int j = 0;

        for (typename Vec<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v[j] + s;
            ++j;
        }

        return res;
    }

    template <class T> Vec<T> operator -(const Vec<T> &v, double s)
    {
        Vec<T> res(v.size(), T());
        int j = 0;

        for (typename Vec<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v[j] - s;
            ++j;
        }

        return res;
    }

    template <class T> double vecLength(const Vec<T> &v)
    {
        double result = 0.0;

        for (typename Vec<T>::const_iterator it = v.begin(); it != v.end(); it++)
        {
            result += *it * *it;
        }

        return sqrt(result);
    }

    template <class T> double vecSum(const Vec<T> &v)
    {
        double result = 0.0;

        for (typename Vec<T>::const_iterator it = v.begin(); it != v.end(); it++)
        {
            result += *it;
        }

        return result;
    }

    template <class T> Vec<T> normalizedVec(const Vec<T> &v)
    {
        double length = vecLength(v);
        Vec<T> result = v;
        result /= length;
        return result;
    }

    template <class T> Vec<T> normalize(const Vec<T> &v, double & max_val, double & min_val)
    {
        max_val = v.at(0);
        min_val = v.at(0);

        for(size_t i = 0; i < v.size(); ++i)
        {
            if(v.at(i) < min_val)
            {
                min_val = v.at(i);
            }
            if(v.at(i) > max_val)
            {
                max_val = v.at(i);
            }
        }

        if(max_val == min_val)
        {
            return v;
        }
        else
        {
            Vec<T> vnew;

            for(size_t i = 0; i < v.size(); ++i)
            {
                vnew.push_back(v.at(i) - min_val / (max_val - min_val));
            }
            return vnew;
        }
    }

    template <class T> void getNormalizedVal(Vec<T>& v, double max_val, double min_val)
    {
        for(size_t i = 0; i < v.size(); ++i)
        {
            v.at(i) = (v.at(i) - min_val / (max_val - min_val));
        }
    }

    template <class T> void getUnNormalizedVal(Vec<T>& v, double max_val, double min_val)
    {
        for(size_t i = 0; i < v.size(); ++i)
        {
            v.at(i) = min_val + v.at(i) * (max_val - min_val);
        }

    }

    template <class T> Vec<T> operator /(const Vec<T> &v, double c)
    {
        Vec<T> res(v.size(),  T());
        int j = 0;

        for (typename Vec<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = v[j] / c;
            ++j;
        }

        return res;
    }

    template <class T> Vec<T> abs(const Vec<T> &v)
    {
        Vec<T> res(v.size(), T());
        int j = 0;

        for (typename Vec<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = fabs(v[j]);
            ++j;
        }

        return res;
    }

    template <class T> Vec<T> max(const Vec<T> &v1, const Vec<T> &v2)
    {
        Vec<T> res(std::min(v1.size(), v2.size()), T());
        int j = 0;

        for (typename Vec<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = std::max(v1[j], v2[j]);
            ++j;
        }

        return res;
    }

    template <class T> T max(const Vec<T> &v1)
    {
        T maxValue = std::numeric_limits<T>::min();

        for (typename Vec<T>::const_iterator i = v1.begin(); i != v1.end(); ++i)
        {
            maxValue = std::max(maxValue, *i);

        }

        return maxValue;
    }

    template <class T> Vec<T> max(const Vec<Vec<T> > &v)
    {
        Vec<T> res;
        res.resize(v.size());

        for(size_t i = 0; i < res.size(); ++i)
        {
            res[i] = std::numeric_limits<T>::min();
        }

        for(size_t i = 0; i < v.size(); ++i)
        {
            for(size_t j = 0; j < res.size(); ++j)
            {
                res[i] = std::max(res[i], v[i][j]);
            }
        }

        return res;
    }

    template <class T> Vec<T> min(const Vec<Vec<T> > &v)
    {
        Vec<T> res;
        res.resize(v.size());

        for(size_t i = 0; i < res.size(); ++i)
        {
            res[i] = std::numeric_limits<T>::max();
        }

        for(size_t i = 0; i < v.size(); ++i)
        {
            for(size_t j = 0; j < res.size(); ++j)
            {
                res[i] = std::min(res[i], v[i][j]);
            }
        }

        return res;
    }

    template <class T> Vec<T> min(const Vec<T> &v1, const Vec<T> &v2)
    {
        Vec<T> res(std::min(v1.size(), v2.size()));
        int j = 0;

        for (typename Vec<T>::iterator i = res.begin(); i != res.end(); ++i)
        {
            *i = std::min(v1[j], v2[j]);
            ++j;
        }

        return res;
    }

    template <class T> T min(const Vec<T> &v1)
    {
        T minValue = std::numeric_limits<T>::max();

        for (typename Vec<T>::const_iterator i = v1.begin(); i != v1.end(); ++i)
        {
            minValue = std::min(minValue, *i);

        }

        return minValue;
    }


    inline DVec turn2DVector(const DVec& v, double alpha)
    {
        DVec res;
        res.resize(2);
        res[0] = cos(alpha) * v[0] - sin(alpha) * v[1];
        res[1] = sin(alpha) * v[0] + cos(alpha) * v[1];
        return res;
    }


    inline void pol2cart(double phi, double r, double& x, double& y)
    {
        x = r * cos(phi);
        y = r * sin(phi);
    }

    inline DVec pol2cart(const DVec& pol)
    {
        DVec res;
        res.resize(2);
        pol2cart(pol[0], pol[1], res[0], res[1]);
        return res;
    }


    inline DVec readVectorFromFile(FILE* fp)
    {
        int size;

        fscanf(fp, "VECTOR (%d):\n", &size);

        DVec res;
        res.resize(size);

        double k;

        for (int i = 0; i < size; i++)
        {
            fscanf(fp, "%lf,", &k);
            res[i] = k;
        }

        fscanf(fp, ";\n");

        return res;
    }

    inline string extractVectorFromString(const string& str, DVec& res)
    {
        int size, pos;

        sscanf(str.c_str(), "VECTOR (%d):", &size);
        pos = str.find(":") + 1;

        res.resize(size);

        double k;

        for (int i = 0; i < size; i++)
        {
            sscanf(str.c_str() + pos, "%lf,", &k);
            res[i] = k;
            pos = str.find_first_of(",;", pos) + 1;
        }

        return str.substr(pos);
    }

    inline void freeDoubleArray(double** &a, unsigned int size)
    {
        for (unsigned int i = 0; i < size; ++i)
        {
            delete[] a[i];
        }

        delete[] a;
    }

    inline void writeGNUPlotFile(double** &a, unsigned int size, unsigned int dim,
                                 const char* filename = "plots/tmp_double_array_traj_%d.gp",
                                 double delta_t = 1.0 / 100)
    {
        for (unsigned int d = 0; d < dim; ++d)
        {
            char buf[255];
            sprintf(buf, filename, d);
            FILE* fp = fopen(buf, "w");

            for (unsigned int i = 0; i < size; ++i)
            {
                fprintf(fp, "%lf\t%lf\n", i * delta_t, a[i][d]);
            }

            fclose(fp);
        }
    }



    inline void writeGNUPlotFileWithXAxis(const Vec<DVec>&a, unsigned int size, unsigned int dim,
                                          const char* filename = "plots/tmp_double_array_traj_%d.gp")
    {
        if (dim < 2)
        {
            throw DMP::Exception("Dimension must be atleast 2 for plotting");
        }

        for (unsigned int d = 1; d < dim; ++d)
        {
            char buf[255];
            sprintf(buf, filename, d);
            FILE* fp = fopen(buf, "w");

            for (unsigned int i = 0; i < size; ++i)
            {
                fprintf(fp, "%lf\t%lf\n", a[i][0], a[i][d]);
            }

            fclose(fp);
        }
    }
    inline void writeGNUPlotFileWithXAxis(double** &a, unsigned int size, unsigned int dim,
                                          const char* filename = "plots/tmp_double_array_traj_%d.gp")
    {

        if (dim < 2)
        {
            throw DMP::Exception("Dimension must be atleast 2 for plotting");
        }

        for (unsigned int d = 1; d < dim; ++d)
        {
            char buf[255];
            sprintf(buf, filename, d);
            FILE* fp = fopen(buf, "w");

            if (!fp)
            {
                throw Exception("Could not open file '") << buf  << "'!";
            }

            for (unsigned int i = 0; i < size; ++i)
            {
                fprintf(fp, "%lf\t%lf\n", a[i][0], a[i][d]);
            }

            fclose(fp);
        }

    }

    inline void writeGNUPlotFileWithXAxis(const DVec& x, const DVec& y,
                                          const char* filename = "plots/tmp_double_array_traj.gp")
    {
        FILE* fp = fopen(filename, "w");

        if (!fp)
        {
            throw Exception("Could not open file '") << filename << "'!";
        }

        for (unsigned int i = 0; i < x.size() && i < y.size(); ++i)
        {
            fprintf(fp, "%lf\t%lf\n", x[i], y[i]);
        }

        fclose(fp);

    }

    inline void writeGNUPlotFileWithXAxis(const DoubleMap& a,
                                          const char* filename = "plots/tmp_double_array_traj_%d.gp")
    {
        DVec xValues;
        DVec yValues;

        for (DoubleMap::const_iterator it = a.begin(); it != a.end(); ++it)
        {
            xValues.push_back(it->first);
            yValues.push_back(it->second);
        }

        writeGNUPlotFileWithXAxis(xValues, yValues, filename);
    }

    inline void plotTogether(const Vec<DVec> &data,
                             const Vec<std::string> &titles,
                             const std::string& tmp_filename = "plots/tmp_double_array_traj_%d.gp",
                             bool show = true,
                             std::string additionalplotCommand = "")
    {
        if (data.size() < 2)
        {
            throw DMP::Exception("Input dimensions must be atleast 2 to plot.");
        }

        for (unsigned int i = 1; i < data.size(); i++)
        {
            char specificFilename[1024];
            sprintf(specificFilename, tmp_filename.c_str(), i);

            writeGNUPlotFileWithXAxis(data[0], data[i], specificFilename);
        }

        if (show)
        {
            char scriptBuff[10240];
            std::fstream file;
            sprintf(scriptBuff, tmp_filename.c_str(), 0);
            std::stringstream scriptfile;
            scriptfile << scriptBuff << "s";
            file.open(scriptfile.str().c_str(), std::fstream::out);

            if (!file.is_open())
            {
                throw Exception("Could not open file: " + scriptfile.str());
            }

            std::cout << " file:" << scriptfile.str() << std::endl;
            char buf[10240];
            stringstream plots;

            if (!additionalplotCommand.empty())
            {
                plots <<  additionalplotCommand << "\n";
            }

            plots << " plot " ;

            for (unsigned int i = 1; i < data.size(); i++)
            {
                char buf_filename[255];
                sprintf(buf_filename, tmp_filename.c_str(), i);
                plots << "\"" << buf_filename << "\" with lines ls " << i; //every ::0::m

                if (i < titles.size())
                {
                    plots << " title '" << titles.at(i) << "'";
                }

                if (i != data.size() - 1)
                {
                    plots << ", ";
                }
            }

            file << plots.str();
            file.close();
            sprintf(buf, GNUPLOT_COMMAND_FROM_FILE, scriptfile.str().c_str());
            std::cout << buf << std::endl;

            int res = system(buf);
            if(res)
            {
                throw std::logic_error{"Command '" + std::string{buf} + "' returned " + std::to_string(res)};
            }
        }
    }


    inline void plot2DTogether(const Vec<DVec> &data,
                               const Vec<std::string> &titles,
                             const std::string& tmp_filename = "plots/tmp_double_array_traj_%d.gp",
                             bool show = true)
    {
        if (data.size() < 2)
        {
            throw DMP::Exception("Input dimensions must be at least 2 to plot.");
        }

        unsigned int trajNum = 0;
        for (unsigned int i = 0; i < data.size(); i=i+2)
        {
            char specificFilename[1024];
            sprintf(specificFilename, tmp_filename.c_str(), trajNum);
            writeGNUPlotFileWithXAxis(data[i], data[i+1], specificFilename);
            trajNum++;
        }

        if (show)
        {
            char buf[10240];
            stringstream plots;
            for (unsigned int i = 0; i < trajNum; i++)
            {
                char buf_filename[255];
                sprintf(buf_filename, tmp_filename.c_str(), i);
                plots << "\\\"" << buf_filename << "\\\" using " << 1 << ":" << 2; //every ::0::m

                if (i < titles.size())
                {
                    plots << " title '" << titles.at(i) << "'";
                }
                plots << " with lines";

                if (i != data.size() - 1)
                {
                    plots << ", ";
                }
            }

            sprintf(buf, GNUPLOT_COMMAND, plots.str().c_str());
            printf("%s\n", buf);

            int res = system(buf);
            if(res)
            {
                throw std::logic_error{"Command '" + std::string{buf} + "' returned " + std::to_string(res)};
            }
        }
    }

    inline void plotTogether(const Vec<DVec> &data,
                             const std::string& tmp_filename = "plots/tmp_double_array_traj_%d.gp",
                             bool show = true)
    {
        if (data.size() < 2)
        {
            throw DMP::Exception("Input dimensions must be atleast 2 to plot.");
        }

        for (unsigned int i = 1; i < data.size(); i++)
        {
            char specificFilename[1024];
            sprintf(specificFilename, tmp_filename.c_str(), i);
            writeGNUPlotFileWithXAxis(data[0], data[i], specificFilename);
        }

        if (show)
        {
            char buf[10240];
            stringstream plots;

            for (unsigned int i = 1; i < data.size(); i++)
            {
                char buf_filename[255];
                sprintf(buf_filename, tmp_filename.c_str(), i);
                plots << "\\\"" << buf_filename << "\\\" with lines ls " << i ;

                if (i != data.size() - 1)
                {
                    plots << ", ";
                }
            }


            sprintf(buf, GNUPLOT_COMMAND, plots.str().c_str());
            printf("%s\n", buf);

            int res = system(buf);
            if(res)
            {
                std::cout <<"Command '" + std::string{buf} + "' returned " + std::to_string(res);
            }
        }
    }




    inline void plot(double** &a, unsigned int size, unsigned int dim,
                     const char* tmp_filename = "plots/tmp_double_array_traj_%d.gp",
                     bool show = true, double delta_t = 1.0 / 100)
    {
        if (delta_t == 0.0)
        {
            writeGNUPlotFileWithXAxis(a, size, dim, tmp_filename);
        }
        else
        {
            writeGNUPlotFile(a, size, dim, tmp_filename, delta_t);
        }

        if (show)
        {
            char buf[10240];
            stringstream plots;

            for (unsigned int d = (delta_t == 0.0) ? 1 : 0; d < dim; ++d)
            {
                char buf_filename[255];
                sprintf(buf_filename, tmp_filename, d);
                plots << "\\\"" << buf_filename << "\\\" with lines ls " << d + 2 ;

                if (d != dim - 1)
                {
                    plots << ", ";
                }

                //          sprintf(buf, "gnuplot -e \"set style line 1 lt rgb \\\"blue\\\";"
                //                  "plot \\\"%s\\\" with lines ls 1\" -persist", buf_filename);
                //          system(buf);
            }

            sprintf(buf, GNUPLOT_COMMAND, plots.str().c_str());

            int res = system(buf);
            if(res)
            {
                std::cout<< "Command '" + std::string{buf} + "' returned " + std::to_string(res);
            }
        }
    }

    inline void plot(const Vec<DoubleMap> &dataVec, const char* tmp_filename = "plots/tmp_double_array_traj_%d.gp",
                     bool show = true)
    {
        if (dataVec.size() < 1)
        {
            throw DMP::Exception("Now Input data given");
        }

        for (unsigned int i = 0; i < dataVec.size(); i++)
        {
            char specificFilename[10240];
            sprintf(specificFilename, tmp_filename, i);
            writeGNUPlotFileWithXAxis(dataVec.at(i), specificFilename);
        }

        if (show)
        {
            char buf[1024];
            stringstream plots;

            for (unsigned int i = 0; i < dataVec.size(); i++)
            {
                char buf_filename[255];
                sprintf(buf_filename, tmp_filename, i);
                plots << "\\\"" << buf_filename << "\\\" with lines ls " << i ;

                if (i != dataVec.size() - 1)
                {
                    plots << ", ";
                }
            }

            sprintf(buf, GNUPLOT_COMMAND, plots.str().c_str());
            int res = system(buf);
            (void) res;
        }

    }

    inline void plot(const Vec<DVec2d > &dataVec, const char* tmp_filename = "plots/tmp_double_array_traj_%d.gp",
                     bool show = true)
    {
        if (dataVec.size() < 1)
        {
            throw DMP::Exception("Now Input data given");
        }

        for (unsigned int i = 0; i < dataVec.size(); i++)
        {
            char specificFilename[10240];
            sprintf(specificFilename, tmp_filename, i);
            writeGNUPlotFileWithXAxis(dataVec.at(i)[0], dataVec.at(i)[1], specificFilename);
        }

        if (show)
        {
            char buf[1024];
            stringstream plots;

            for (unsigned int i = 0; i < dataVec.size(); i++)
            {
                char buf_filename[255];
                sprintf(buf_filename, tmp_filename, i);
                plots << "\\\"" << buf_filename << "\\\" with lines ls " << i ;

                if (i != dataVec.size() - 1)
                {
                    plots << ", ";
                }
            }

            sprintf(buf, GNUPLOT_COMMAND, plots.str().c_str());
            system(buf);
        }

    }


    inline DoubleMap convertArraysToMap(const DVec& keyVec, const DVec& valueVec)
    {
        DoubleMap result;
        unsigned int size = std::min(keyVec.size(), valueVec.size());

        for (unsigned int i = 0; i < size; i++)
        {
            result[keyVec[i]] = valueVec[i];
        }

        return result;
    }

    inline void plot(const DoubleMap& data, const char* tmp_filename = "plots/tmp_double_array_traj_%d.gp",
                     bool show = true, double delta_t = 1.0 / 100)
    {
        double** dataArray = new double*[data.size()];
        DoubleMap::const_iterator it = data.begin();

        for (unsigned int i = 0; i < data.size(); i++, it++)
        {
            dataArray[i] = new double[2];
            dataArray[i][0] = it->first;
            dataArray[i][1] = it->second;

        }

        plot(dataArray, data.size(), 2, tmp_filename, show, delta_t);

        for (unsigned int i = 0; i < data.size(); i++)
        {
            delete [] dataArray[i];
        }

        delete [] dataArray;
    }

    inline double norm2(const DVec& v)
    {
        double res = 0.0;

        for (unsigned int i = 0; i < v.size(); ++i)
        {
            res += v[i] * v[i];
        }

        return sqrt(res);
    }

    inline double weightedNorm2(const DVec& v, const DVec& weights)
    {
        double res = 0.0;

        for (unsigned int i = 0; i < v.size(); ++i)
        {
            res += weights[i] * v[i] * v[i];
        }

        return sqrt(res);
    }

    inline bool getNextLine(FILE* fp, char* buf)
    {
        while (!feof(fp))
        {
            fgets(buf, J_LINE_BUF_SIZE, fp);

            if (strlen(buf) > 1)
            {
                return true;
            }
        }

        return false;
    }


#define ROTATION_ORDER_ZYX 0
#define ROTATION_ORDER_ZYZ 1
#define ROTATION_ORDER_ZXY 2
#define ROTATION_ORDER_ZXZ 3
#define ROTATION_ORDER_YXZ 4
#define ROTATION_ORDER_YXY 5
#define ROTATION_ORDER_YZX 6
#define ROTATION_ORDER_YZY 7
#define ROTATION_ORDER_XYZ 8
#define ROTATION_ORDER_XYX 9
#define ROTATION_ORDER_XZY 10
#define ROTATION_ORDER_XZX 11

    inline Eigen::Quaterniond angle2quat(double yaw, double pitch, double roll, int mode = ROTATION_ORDER_ZYX){
        double cang[3], sang[3];
        cang[0] = cos(yaw / 2);
        cang[1] = cos(pitch / 2);
        cang[2] = cos(roll / 2);

        sang[0] = sin(yaw / 2);
        sang[1] = sin(pitch / 2);
        sang[2] = sin(roll / 2);

        Eigen::Quaterniond q;

        switch(mode){
        case ROTATION_ORDER_ZYX:
            q.w() = cang[0] * cang[1] * cang[2] + sang[0] * sang[1] * sang[2];
            q.x() = cang[0] * cang[1] * sang[2] - sang[0] * sang[1] * cang[2];
            q.y() = cang[0] * sang[1] * cang[2] + sang[0] * cang[1] * sang[2];
            q.z() = sang[0] * cang[1] * cang[2] - cang[0] * sang[1] * sang[2];
            break;
        case ROTATION_ORDER_ZYZ:
            q.w() = cang[0] * cang[1] * cang[2] - sang[0] * cang[1] * sang[2];
            q.x() = cang[0] * sang[1] * sang[2] - sang[0] * sang[1] * cang[2];
            q.y() = cang[0] * sang[1] * cang[2] + sang[0] * sang[1] * sang[2];
            q.z() = sang[0] * cang[1] * cang[2] + cang[0] * cang[1] * sang[2];
            break;
        case ROTATION_ORDER_ZXY:
            q.w() = cang[0] * cang[1] * cang[2] - sang[0] * sang[1] * sang[2];
            q.x() = cang[0] * sang[1] * cang[2] - sang[0] * cang[1] * sang[2];
            q.y() = cang[0] * cang[1] * sang[2] + sang[0] * sang[1] * cang[2];
            q.z() = cang[0] * sang[1] * sang[2] + sang[0] * cang[1] * cang[2];
            break;
        case ROTATION_ORDER_ZXZ:
            q.w() = cang[0] * cang[1] * cang[2] - sang[0] * cang[1] * sang[2];
            q.x() = cang[0] * sang[1] * cang[2] + sang[0] * sang[1] * sang[2];
            q.y() = sang[0] * sang[1] * cang[2] - cang[0] * sang[1] * sang[2];
            q.z() = cang[0] * cang[1] * sang[2] + sang[0] * cang[1] * cang[2];
            break;
        case ROTATION_ORDER_YXZ:
            q.w() = cang[0] * cang[1] * cang[2] + sang[0] * sang[1] * sang[2];
            q.x() = cang[0] * sang[1] * cang[2] + sang[0] * cang[1] * sang[2];
            q.y() = sang[0] * cang[1] * cang[2] - cang[0] * sang[1] * sang[2];
            q.z() = cang[0] * cang[1] * sang[2] - sang[0] * sang[1] * cang[2];
            break;
        case ROTATION_ORDER_YXY:
            q.w() = cang[0] * cang[1] * cang[2] - sang[0] * cang[1] * sang[2];
            q.x() = cang[0] * sang[1] * cang[2] + sang[0] * sang[1] * sang[2];
            q.y() = sang[0] * cang[1] * cang[2] + cang[0] * cang[1] * sang[2];
            q.z() = cang[0] * sang[1] * sang[2] - sang[0] * sang[1] * cang[2];
            break;
        case ROTATION_ORDER_YZX:
            q.w() = cang[0] * cang[1] * cang[2] - sang[0] * sang[1] * sang[2];
            q.x() = cang[0] * cang[1] * sang[2] + sang[0] * sang[1] * cang[2];
            q.y() = cang[0] * sang[1] * sang[2] + sang[0] * cang[1] * cang[2];
            q.z() = cang[0] * sang[1] * cang[2] - sang[0] * cang[1] * sang[2];
            break;
        case ROTATION_ORDER_YZY:
            q.w() = cang[0] * cang[1] * cang[2] - sang[0] * cang[1] * sang[2];
            q.x() = sang[0] * sang[1] * cang[2] - cang[0] * sang[1] * sang[2];
            q.y() = cang[0] * cang[1] * sang[2] + sang[0] * cang[1] * cang[2];
            q.z() = cang[0] * sang[1] * cang[2] + sang[0] * sang[1] * sang[2];
            break;
        case ROTATION_ORDER_XYZ:
            q.w() = cang[0] * cang[1] * cang[2] - sang[0] * sang[1] * sang[2];
            q.x() = cang[0] * sang[1] * sang[2] + sang[0] * cang[1] * cang[2];
            q.y() = cang[0] * sang[1] * cang[2] - sang[0] * cang[1] * sang[2];
            q.z() = cang[0] * cang[1] * sang[2] + sang[0] * sang[1] * cang[2];
            break;
        case ROTATION_ORDER_XYX:
            q.w() = cang[0] * cang[1] * cang[2] - sang[0] * cang[1] * sang[2];
            q.x() = cang[0] * cang[1] * sang[2] + sang[0] * cang[1] * cang[2];
            q.y() = cang[0] * sang[1] * cang[2] + sang[0] * sang[1] * sang[2];
            q.z() = sang[0] * sang[1] * cang[2] - cang[0] * sang[1] * sang[2];
            break;
        case ROTATION_ORDER_XZY:
            q.w() = cang[0] * cang[1] * cang[2] + sang[0] * sang[1] * sang[2];
            q.x() = sang[0] * cang[1] * cang[2] - cang[0] * sang[1] * sang[2];
            q.y() = cang[0] * cang[1] * sang[2] - sang[0] * sang[1] * cang[2];
            q.z() = cang[0] * sang[1] * cang[2] + sang[0] * cang[1] * sang[2];
            break;
        case ROTATION_ORDER_XZX:
            q.w() = cang[0] * cang[1] * cang[2] - sang[0] * cang[1] * sang[2];
            q.x() = cang[0] * cang[1] * sang[2] + sang[0] * cang[1] * cang[2];
            q.y() = cang[0] * sang[1] * sang[2] - sang[0] * sang[1] * cang[2];
            q.z() = cang[0] * sang[1] * cang[2] + sang[0] * sang[1] * sang[2];
            break;
        default:
            throw DMP::Exception("invalid mode: please use mode parameters such as ROTATION_ORDER_ZYX");
        }

        q.normalize();
        return q;
    }


    inline double* threeaxisrot(double r11, double r12, double r21, double r31, double r32){
        double* res = new double[3];
        res[0] = atan2(r11, r12);
        res[1] = asin(r21);
        res[2] = atan2(r31, r32);
        return res;
    }

    inline double* twoaxisrot(double r11, double r12, double r21, double r31, double r32){
        double* res = new double[3];
        res[0] = atan2(r11, r12);
        res[1] = acos(r21);
        res[2] = atan2(r31, r32);
        return res;
    }

    inline Eigen::Vector3d quat2angle(Eigen::Quaterniond q, int mode = ROTATION_ORDER_ZYX) {
        q.normalize();

        double qin[4];
        qin[0] = q.w();
        qin[1] = q.x();
        qin[2] = q.y();
        qin[3] = q.z();

        double r11, r12, r21, r31, r32;
        double* re;
        switch(mode){
        case ROTATION_ORDER_ZYX:
            r11 = 2 * (qin[1] * qin[2] + qin[0] * qin[3]);
            r12 = qin[0] * qin[0] + qin[1] * qin[1] - qin[2] * qin[2] - qin[3] * qin[3];
            r21 = -2 * (qin[1] * qin[3] - qin[0] * qin[2]);
            r31 = 2 * (qin[2] * qin[3] + qin[0] * qin[1]);
            r32 = qin[0] * qin[0] - qin[1] * qin[1] - qin[2] * qin[2] + qin[3] * qin[3];
            re = threeaxisrot(r11,r12,r21,r31,r32);
            break;
        case ROTATION_ORDER_ZYZ:
            r11 = 2 * (qin[2] * qin[3] - qin[0] * qin[1]);
            r12 = 2 * (qin[1] * qin[3] + qin[0] * qin[2]);
            r21 = qin[0] * qin[0] - qin[1] * qin[1] - qin[2] * qin[2] + qin[3] * qin[3];
            r31 = 2 * (qin[2] * qin[3] + qin[0] * qin[1]);
            r32 = -2 * (qin[1] * qin[3] - qin[0] * qin[2]);
            re = twoaxisrot(r11,r12,r21,r31,r32);
            break;
        case ROTATION_ORDER_ZXY:
            r11 = -2 * (qin[1] * qin[2] - qin[0] * qin[3]);
            r12 = qin[0] * qin[0] - qin[1] * qin[1] + qin[2] * qin[2] - qin[3] * qin[3];
            r21 = 2 * (qin[2] * qin[3] + qin[0] * qin[1]);
            r31 = -2 * (qin[1] * qin[3] - qin[0] * qin[2]);
            r32 = qin[0] * qin[0] - qin[1] * qin[1] - qin[2] * qin[2] + qin[3] * qin[3];
            re = threeaxisrot(r11,r12,r21,r31,r32);
            break;
        case ROTATION_ORDER_ZXZ:
            r11 = 2 * (qin[1] * qin[3] + qin[0] * qin[2]);
            r12 = -2 * (qin[2] * qin[3] - qin[0] * qin[1]);
            r21 = qin[0] * qin[0] - qin[1] * qin[1] - qin[2] * qin[2] + qin[3] * qin[3];
            r31 = 2 * (qin[1] * qin[3] - qin[0] * qin[2]);
            r32 = 2 * (qin[2] * qin[3] + qin[0] * qin[1]);
            re = twoaxisrot(r11,r12,r21,r31,r32);
            break;
        case ROTATION_ORDER_YXZ:
            r11 = 2 * (qin[1] * qin[3] + qin[0] * qin[2]);
            r12 = qin[0] * qin[0] - qin[1] * qin[1] - qin[2] * qin[2] + qin[3] * qin[3];
            r21 = -2 * (qin[2] * qin[3] - qin[0] * qin[1]);
            r31 = 2 * (qin[1] * qin[2] + qin[0] * qin[3]);
            r32 = qin[0] * qin[0] - qin[1] * qin[1] + qin[2] * qin[2] - qin[3] * qin[3];
            re = threeaxisrot(r11,r12,r21,r31,r32);
            break;
        case ROTATION_ORDER_YXY:
            r11 = 2 * (qin[1] * qin[2] - qin[0] * qin[3]);
            r12 = 2 * (qin[2] * qin[3] + qin[0] * qin[1]);
            r21 = qin[0] * qin[0] - qin[1] * qin[1] + qin[2] * qin[2] - qin[3] * qin[3];
            r31 = 2 * (qin[1] * qin[2] + qin[0] * qin[3]);
            r32 = -2 * (qin[2] * qin[3] - qin[0] * qin[1]);
            re = twoaxisrot(r11,r12,r21,r31,r32);
            break;
        case ROTATION_ORDER_YZX:
            r11 = -2 * (qin[1] * qin[3] - qin[0] * qin[2]);
            r12 = qin[0] * qin[0] + qin[1] * qin[1] - qin[2] * qin[2] - qin[3] * qin[3];
            r21 = 2 * (qin[1] * qin[2] + qin[0] * qin[3]);
            r31 = -2 * (qin[2] * qin[3] - qin[0] * qin[1]);
            r32 = qin[0] * qin[0] - qin[1] * qin[1] + qin[2] * qin[2] - qin[3] * qin[3];
            re = threeaxisrot(r11,r12,r21,r31,r32);
            break;
        case ROTATION_ORDER_YZY:
            r11 = 2 * (qin[2] * qin[3] + qin[0] * qin[1]);
            r12 = -2 * (qin[1] * qin[2] - qin[0] * qin[3]);
            r21 = qin[0] * qin[0] - qin[1] * qin[1] + qin[2] * qin[2] - qin[3] * qin[3];
            r31 = 2 * (qin[2] * qin[3] - qin[0] * qin[1]);
            r32 = 2 * (qin[1] * qin[2] + qin[0] * qin[3]);
            re = twoaxisrot(r11,r12,r21,r31,r32);
            break;
        case ROTATION_ORDER_XYZ:
            r11 = -2 * (qin[2] * qin[3] - qin[0] * qin[1]);
            r12 = qin[0] * qin[0] - qin[1] * qin[1] - qin[2] * qin[2] + qin[3] * qin[3];
            r21 = 2 * (qin[1] * qin[3] + qin[0] * qin[2]);
            r31 = -2 * (qin[1] * qin[2] - qin[0] * qin[3]);
            r32 = qin[0] * qin[0] + qin[1] * qin[1] - qin[2] * qin[2] - qin[3] * qin[3];
            re = threeaxisrot(r11,r12,r21,r31,r32);
            break;
        case ROTATION_ORDER_XYX:
            r11 = 2 * (qin[1] * qin[2] + qin[0] * qin[3]);
            r12 = -2 * (qin[1] * qin[3] - qin[0] * qin[2]);
            r21 = qin[0] * qin[0] + qin[1] * qin[1] - qin[2] * qin[2] - qin[3] * qin[3];
            r31 = 2 * (qin[1] * qin[2] - qin[0] * qin[3]);
            r32 = 2 * (qin[1] * qin[3] + qin[0] * qin[2]);
            re = twoaxisrot(r11,r12,r21,r31,r32);
            break;
        case ROTATION_ORDER_XZY:
            r11 = 2 * (qin[2] * qin[3] + qin[0] * qin[1]);
            r12 = qin[0] * qin[0] - qin[1] * qin[1] + qin[2] * qin[2] - qin[3] * qin[3];
            r21 = -2 * (qin[1] * qin[2] - qin[0] * qin[3]);
            r31 = 2 * (qin[1] * qin[3] + qin[0] * qin[2]);
            r32 = qin[0] * qin[0] + qin[1] * qin[1] - qin[2] * qin[2] - qin[3] * qin[3];
            re = threeaxisrot(r11,r12,r21,r31,r32);
            break;
        case ROTATION_ORDER_XZX:
            r11 = 2 * (qin[1] * qin[3] - qin[0] * qin[2]);
            r12 = 2 * (qin[1] * qin[2] + qin[0] * qin[3]);
            r21 = qin[0] * qin[0] + qin[1] * qin[1] - qin[2] * qin[2] - qin[3] * qin[3];
            r31 = 2 * (qin[1] * qin[3] + qin[0] * qin[2]);
            r32 = -2 * (qin[1] * qin[2] - qin[0] * qin[3]);
            re = twoaxisrot(r11,r12,r21,r31,r32);
            break;
        default:
            throw DMP::Exception("invalid mode: please use mode parameters such as ROTATION_ORDER_ZYX");
        }

        Eigen::Vector3d res;
        res << re[0], re[1], re[2];
        return res;

    }


    inline Eigen::Vector3d qlog(Eigen::Quaterniond a) {
        if (a.vec() != Eigen::Vector3d::Zero()) {
            a.normalize();
            return Eigen::Vector3d(acos(a.w()) * a.vec() / a.vec().norm());
        }
        else return Eigen::Vector3d::Zero();

    }

    inline Eigen::Quaterniond quatlog(const Eigen::Quaterniond& a) {
        Eigen::Quaterniond q;
        Eigen::Vector3d v = a.vec();
        double norm = a.norm();

        if(v.norm() != 0){
            q.w() = std::log(norm);
            v = v / v.norm();
            double scale = acos(a.w() / norm);
            q.vec() = v * scale;
        } else {
            q = Eigen::Quaterniond::Identity();
        }

        return q;
    }

    inline Eigen::Quaterniond quatexp(const Eigen::Quaterniond &q)
    {
        double vecnorm = q.vec().norm();

        double wpart, vecpart;
        if(vecnorm != 0) {
            wpart = std::exp(q.w()) * cos(vecnorm);
            vecpart = std::exp(q.w()) * sin(vecnorm) / vecnorm;
        } else {
            return Eigen::Quaterniond(1,0,0,0);
        }
        return Eigen::Quaterniond(wpart, q.x() * vecpart, q.y() * vecpart, q.z() * vecpart);
    }


    inline Eigen::Quaterniond qexp(Eigen::Vector3d a) {
        if (a != Eigen::Vector3d::Zero()) {
            Eigen::Quaterniond res;
            res.w() = cos(a.norm());
            res.vec() = sin(a.norm())/ a.norm() * a ;
            return res;
        }
        else
            return Eigen::Quaterniond(1,0,0,0);

    }

    inline Eigen::Quaterniond operator+ (Eigen::Quaterniond op1, Eigen::Quaterniond op2)
    {
        return Eigen::Quaterniond(op1.w() + op2.w(),
                                  op1.x() + op2.x(),
                                  op1.y() + op2.y(),
                                  op1.z() + op2.z());
    }

    inline Eigen::Quaterniond operator- (Eigen::Quaterniond op1, Eigen::Quaterniond op2)
    {
        return Eigen::Quaterniond(op1.w() - op2.w(),
                                  op1.x() - op2.x(),
                                  op1.y() - op2.y(),
                                  op1.z() - op2.z());
    }

    inline Eigen::Quaterniond operator* (double op1, Eigen::Quaterniond op2)
    {
        return Eigen::Quaterniond(op1 * op2.w(),
                                  op1 * op2.x(),
                                  op1 * op2.y(),
                                  op1 * op2.z());
    }

    inline Eigen::Quaterniond operator* (Eigen::Quaterniond op2 ,double op1)
    {
        return op1 * op2;
    }

    inline Eigen::Quaterniond operator/ (Eigen::Quaterniond op, double scale)
    {
        return Eigen::Quaterniond(op.w() / scale,
                                  op.x() / scale,
                                  op.y() / scale,
                                  op.z() / scale);
    }

    inline void equalize(Eigen::Quaterniond& op, double scale)
    {
        op.w() = scale;
        op.x() = scale;
        op.y() = scale;
        op.z() = scale;
    }

    inline void dlmwrite(const std::string& filename, const std::map<double, DVec>& source, char dlm = ',')
    {
        std::ofstream of(filename);

        for(std::map<double, DVec>::const_iterator it = source.begin(); it != source.end(); it++)
        {
            of << it->first;
            of << dlm;
            DVec data = it->second;

            for(size_t i = 0; i < data.size(); i++){
                of << data[i];

                if(i != data.size() - 1)
                    of << dlm;
            }
            of << '\n';
        }

        of.close();

    }

    inline void dlmwrite(const std::string& filename, const std::vector<std::vector<double> > & source, char dlm = ',')
    {
        std::ofstream of(filename);

        for(size_t i = 0; i < source.size(); i++)
        {

            std::vector<double> data = source[i];

            for(size_t j = 0; j < data.size(); j++){
                of << data[j];

                if(j != data.size() - 1)
                    of << dlm;
            }
            of << '\n';
        }

        of.close();

    }

    inline void dlmwrite(const std::string& filename, const DVec2d & source, char dlm = ',')
    {
        std::ofstream of(filename);

        for(size_t i = 0; i < source.size(); i++)
        {

            std::vector<double> data = source[i];

            for(size_t j = 0; j < data.size(); j++){
                of << data[j];

                if(j != data.size() - 1)
                    of << dlm;
            }
            of << '\n';
        }

        of.close();

    }

    inline void dlmwrite(const std::string& filename, const Eigen::MatrixXd & source, char dlm = ',')
    {
        std::ofstream of(filename);

        for(int i = 0; i < source.rows(); i++)
        {
            for(int j = 0; j < source.cols(); j++){
                of << source(i,j);

                if(j != source.cols() - 1)
                    of << dlm;
            }
            of << '\n';
        }

        of.close();

    }

    inline Eigen::MatrixXd readMat(const std::string& filename, unsigned int rows, unsigned int cols)
    {
        Eigen::MatrixXd res(rows, cols);
        std::ifstream ifs(filename);
        using Tokenizer = boost::tokenizer< boost::escaped_list_separator<char> >;
        vector< string > stringVec;
        string line;

        unsigned int i = 0;

        while (getline(ifs, line) && i < rows)
        {
            Tokenizer tok(line);
            stringVec.assign(tok.begin(), tok.end());

            for(unsigned int j = 0; j < cols; j++)
            {
                res(i,j) = fromString<double>(stringVec[j]);
            }

            i++;
        }

        ifs.close();
        return res;
    }

    inline void operator << (ostream& os, Eigen::Quaterniond q) {
        os << q.w() << '\t' << q.x() << '\t' << q.y() << '\t' << q.z() << std::endl;
    }

    inline bool operator == (const Eigen::Quaterniond& op1, const Eigen::Quaterniond& op2) {
        return std::abs(op1.w() - op2.w()) < 1e-3 &&
               std::abs(op1.x() - op2.x()) < 1e-3 &&
               std::abs(op1.y() - op2.y()) < 1e-3 &&
               std::abs(op1.z() - op2.z()) < 1e-3;
    }

    inline DVec2d importdata(const std::string& fileName)
    {
        std::ifstream ifs(fileName);
        using Tokenizer = boost::tokenizer< boost::escaped_list_separator<char> >;
        vector< string > stringVec;
        string line;

        DVec2d data;
        while (getline(ifs, line))
        {
            Tokenizer tok(line);
            stringVec.assign(tok.begin(), tok.end());
            DVec rowVec;
            for(unsigned int j = 0; j < stringVec.size(); j++)
            {
                rowVec.push_back(fromString<double>(stringVec[j]));
            }
            data.push_back(rowVec);
        }

        ifs.close();

        return data;
    }

}

#endif // HELPERS_H
