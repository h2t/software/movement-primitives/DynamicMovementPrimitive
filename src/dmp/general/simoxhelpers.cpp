#include "simoxhelpers.h"
#include "helpers.h"
using namespace DMP;

Eigen::Matrix4f RobotHelpers::getLocalPose(const Eigen::Matrix4f &newCoordinate, const Eigen::Matrix4f &globalTargetPose)
{
    Eigen::Matrix4f localPose = Eigen::Matrix4f::Identity();

    localPose.block<3, 3>(0, 0) = newCoordinate.block<3, 3>(0, 0).inverse() * globalTargetPose.block<3, 3>(0, 0);
    localPose.block<3, 1>(0, 3) = newCoordinate.block<3, 3>(0, 0).inverse() * (globalTargetPose.block<3, 1>(0, 3) - newCoordinate.block<3, 1>(0, 3));
    return localPose;
}

DMP::SampledTrajectoryV2 DMP::RobotHelpers::getRelativeTraj(const DMP::SampledTrajectoryV2 &globalTraj, const DMP::SampledTrajectoryV2 &refTraj)
{
    DVec timestamps = globalTraj.getTimestamps();

    SampledTrajectoryV2 resTraj = globalTraj;

    for(size_t i = 0; i < timestamps.size(); ++i)
    {
        double t = timestamps[i];
        DVec gposvec = globalTraj.getStates(t, 0);
        Eigen::Matrix4f gpose = VirtualRobot::MathTools::quat2eigen4f(gposvec[4],gposvec[5],gposvec[6],gposvec[3]);
        gpose(0,3) = gposvec[0];
        gpose(1,3) = gposvec[1];
        gpose(2,3) = gposvec[2];

        DVec rposvec = refTraj.getStates(t,0);
        Eigen::Matrix4f rpose = VirtualRobot::MathTools::quat2eigen4f(rposvec[4],rposvec[5],rposvec[6],rposvec[3]);
        rpose(0,3) = rposvec[0];
        rpose(1,3) = rposvec[1];
        rpose(2,3) = rposvec[2];

        Eigen::Matrix4f lpose = RobotHelpers::getLocalPose(rpose, gpose);
        VirtualRobot::MathTools::Quaternion q = VirtualRobot::MathTools::eigen4f2quat(lpose);

        resTraj.setPositionEntry(t, 0, lpose(0,3));
        resTraj.setPositionEntry(t, 1, lpose(1,3));
        resTraj.setPositionEntry(t, 2, lpose(2,3));
        resTraj.setPositionEntry(t, 3, q.w);
        resTraj.setPositionEntry(t, 4, q.x);
        resTraj.setPositionEntry(t, 5, q.y);
        resTraj.setPositionEntry(t, 6, q.z);
    }

    return resTraj;
}

SampledTrajectoryV2 RobotHelpers::getRelativeTrajFromFiles(const string &globalFile, const string &refFile)
{
    SampledTrajectoryV2 globalTraj;
    globalTraj.readFromCSVFile(globalFile);
    globalTraj = SampledTrajectoryV2::normalizeTimestamps(globalTraj, 0, 1);

    SampledTrajectoryV2 refTraj;
    refTraj.readFromCSVFile(refFile);
    refTraj = SampledTrajectoryV2::normalizeTimestamps(refTraj, 0, 1);

    return RobotHelpers::getRelativeTraj(globalTraj, refTraj);
}

void RobotHelpers::plot3D(const Vec<SampledTrajectoryV2> &trajs, const vector<string> &titles, vector<int> view)
{
    stringstream plots;
    char buf[10240];

    if(view.size() >= 4)
    {
        plots << "set view " << view[0] << ", " << view[1] << ", " << view[2] << ", " << view[3];
        plots << "; ";
    }

    for(size_t i = 0; i < trajs.size(); ++i)
    {
        SampledTrajectoryV2 traj = trajs[i];

        if(traj.dim() < 3)
        {
            continue;
        }
        std::string tmp_file = "tmp_3d_traj_file_" + std::to_string(i) + ".gp";
        traj.writeToCSVFile(tmp_file, false, '\t');
        plots << "splot \\\"" << tmp_file << "\\\" using " << 2 << ":" << 3 << ":" << 4; //every ::0::m

        if (i < titles.size())
        {
            plots << " title '" << titles.at(i) << "'";
        }

        plots << " with lines";

        if (i != trajs.size() - 1)
        {
            plots << ", ";
        }
    }


    sprintf(buf, GNUPLOT_COMMAND_NORMAL, plots.str().c_str());
    printf("%s\n", buf);

    int res = system(buf);
    if(res)
    {
        throw std::logic_error{"Command '" + std::string{buf} + "' returned " + std::to_string(res)};
    }
}
