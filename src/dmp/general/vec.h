/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef VEC_H
#define VEC_H

#include "defines.h"
#include <vector>
#include <stdarg.h>

#include <boost/serialization/serialization.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/string.hpp>
#include <boost/serialization/vector.hpp>

namespace DMP
{
    /**
     * Vec is a class that extends the Vec with a new constructor that
     * takes a value of type 'type' and constructs a Vec of size 1.
     *
     * @note Do no implement something in the destructor. It won't be called if
     * someone casts to Vec pointer and deletes this pointer.
     */
    template <typename type>
    class Vec : public std::vector<type>
    {
    public:
        Vec(const typename std::vector<type>::allocator_type& alloc = _TYPENAME std::vector<type>::allocator_type()) : std::vector<type>(alloc) {}
        Vec(typename std::vector<type>::size_type n, const  typename std::vector<type>::value_type& val,
            const  typename std::vector<type>::allocator_type& alloc = _TYPENAME std::vector<type>::allocator_type()): std::vector<type>(n, val, alloc) {}
        template <class InputIterator>
        Vec(InputIterator first, InputIterator last,
            const  typename std::vector<type>::allocator_type& alloc = _TYPENAME std::vector<type>::allocator_type()) : std::vector<type>(first, last, alloc) {}
        Vec(const Vec<type>& x): std::vector<type>(x) {}
        Vec(const std::vector<type>& x): std::vector<type>(x) {}

        //new constructor for implicit Vec creation
        Vec(const type& value): std::vector<type>(1, value) {}


        ~Vec()
        {
            // Do no implement something in the destructor. It won't be called if
            // someone casts to std::vector pointer and deletes this pointer.
        }

        Vec<type>& operator, (const type& value)
        {
            std::vector<type>::push_back(value);
            return *this;
        }
        Vec<type>& operator<<(const type& value)
        {
            std::vector<type>::push_back(value);
            return *this;
        }
        void connect(const Vec<type>& values)
        {
            for(size_t i = 0; i < values.size(); ++i)
            {
                this->push_back(values[i]);
            }
        }

    private:
        //        friend class boost::serialization::access;
        //        template<class Archive>
        //        void serialize(Archive & ar, const unsigned int file_version){
        //            FunctionApproximationInterface& base = boost::serialization::base_object<FunctionApproximationInterface>(*this);
        //            ar & BOOST_SERIALIZATION_NVP(base);


        //            ar & BOOST_SERIALIZATION_NVP(__widthFactor);
        //            ar & BOOST_SERIALIZATION_NVP(__useWeightGradients);
        //            ar & BOOST_SERIALIZATION_NVP(__convertToZeroOutOfBoundaries);
        //            ar & BOOST_SERIALIZATION_NVP(__baseKernelCount);
        //            std::vector<KernelData>& kernels = __kernels;
        //            ar & BOOST_SERIALIZATION_NVP(kernels);
        //        }
    };
    typedef Vec<double> DVec;
    typedef Vec<DVec> DVec2d;




}

namespace boost
{
    namespace serialization
    {


        template<class Archive, class U>
        inline void save(
            Archive& ar,
            const DMP::Vec<U> &t,
            const unsigned int /* file_version */,
            boost::mpl::true_
        )
        {
            const boost::serialization::collection_size_type count(t.size());
            ar << BOOST_SERIALIZATION_NVP(count);

            if (!t.empty())
            {
                ar << boost::serialization::make_array(&(t.at(0)), t.size());
            }
        }

        template<class Archive, class U>
        inline void load(
            Archive& ar,
            DMP::Vec<U> &t,
            const unsigned int /* file_version */,
            boost::mpl::true_
        )
        {
            boost::serialization::collection_size_type count(t.size());
            ar >> BOOST_SERIALIZATION_NVP(count);
            t.resize(count);
            unsigned int item_version = 0;

            if (BOOST_SERIALIZATION_VECTOR_VERSIONED(ar.get_library_version()))
            {
                ar >> BOOST_SERIALIZATION_NVP(item_version);
            }

            if (!t.empty())
            {
                ar >> boost::serialization::make_array(&(t.at(0)), t.size());
            }
        }

        template<class Archive, class U>
        inline void serialize(
            Archive& ar,
            DMP::Vec<U> & t,
            const unsigned int file_version
        )
        {
            boost::serialization::split_free(ar, t, file_version);
        }
    }
}

#endif // VEC_H
