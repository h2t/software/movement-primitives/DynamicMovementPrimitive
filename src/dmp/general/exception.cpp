/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/


#include "exception.h"

#ifndef _WIN32
#include <execinfo.h>
#include <cxxabi.h>
#include <dlfcn.h>
#endif

#include <cstdio>
#include <cstdlib>
#include <sstream>

using namespace DMP;

Exception::Exception() :
    backtrace("")
{
    backtrace = generateBacktrace();
    setReason("");
}

Exception::Exception(const Exception& e)

{
    reason << e.reason.str();
    backtrace = e.backtrace;
    resultingMessage = e.resultingMessage;
}



void Exception::setReason(std::string reason)
{
    this->reason.str("");
    this->reason.clear();

    this->reason << reason;

}

const char* Exception::what() const throw()
{
    generateOutputString();
    return resultingMessage.c_str();
}

std::string Exception::generateBacktrace()
{
#ifdef _WIN32
    return "no Backtrace under Windows!";
#else
    int skip = 1;
    void* callstack[128];
    const int nMaxFrames = sizeof(callstack) / sizeof(callstack[0]);
    char buf[1024];

    int nFrames = ::backtrace(callstack, nMaxFrames);
    char** symbols = backtrace_symbols(callstack, nFrames);

    std::ostringstream trace_buf;

    for (int i = skip; i < nFrames; i++)
    {
        Dl_info info;

        if (dladdr(callstack[i], &info) && info.dli_sname)
        {
            char* demangled = nullptr;
            int status = -1;

            if (info.dli_sname[0] == '_')
            {
                demangled = abi::__cxa_demangle(info.dli_sname, nullptr, nullptr, &status);
            }

            snprintf(buf, sizeof(buf), "%-3d %*p %s + %zd\n",
                     i - skip + 1, int(2 + sizeof(void*) * 2), callstack[i],
                     status == 0 ? demangled :
                     info.dli_sname == nullptr ? symbols[i] : info.dli_sname,
                     (char*)callstack[i] - (char*)info.dli_saddr);
            free(demangled);
        }
        else
        {
            snprintf(buf, sizeof(buf), "%-3d %*p %s\n",
                     i - skip + 1, int(2 + sizeof(void*) * 2), callstack[i], symbols[i]);
        }

        trace_buf << buf;
    }

    free(symbols);

    if (nFrames == nMaxFrames)
    {
        trace_buf << "[truncated]\n";
    }

    return trace_buf.str();
#endif
}

void Exception::generateOutputString() const
{
    std::stringstream what_buf;


    what_buf << std::endl;
    what_buf << "Reason: " << ((reason.str().empty()) ? "undefined" : reason.str()) << std::endl;
    what_buf << "Backtrace: " << std::endl;
    what_buf << backtrace << std::endl;

    resultingMessage = what_buf.str();
}
