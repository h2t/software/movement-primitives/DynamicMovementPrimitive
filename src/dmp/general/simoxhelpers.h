#ifndef SIMOXHELPERS_H
#define SIMOXHELPERS_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>
#include <cstring>
#include <limits>

#include <cstdio>
#include <cstdlib>
#include <map>

#include "defines.h"
#include "exception.h"

#include "vec.h"

#include <Eigen/Geometry>
#include <boost/tokenizer.hpp>
#include "dmp/representation/trajectory.h"
#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/MathTools.h>

namespace DMP
{
    class RobotHelpers
    {
    public:
        RobotHelpers(){}

        static Eigen::Matrix4f getLocalPose(const Eigen::Matrix4f& newCoordinate, const Eigen::Matrix4f& globalTargetPose);
        static SampledTrajectoryV2 getRelativeTraj(const SampledTrajectoryV2& globalTraj, const SampledTrajectoryV2& refTraj);
        static SampledTrajectoryV2 getRelativeTrajFromFiles(const std::string& globalFile, const std::string& refFile);

        static void plot3D(const Vec<SampledTrajectoryV2>& trajs, const vector<string>& titles, vector<int> view = vector<int>());

    };

}

#endif // SIMOXHELPERS_H
