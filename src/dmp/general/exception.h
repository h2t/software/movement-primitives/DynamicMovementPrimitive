/**
* This file is part of DMP.
*
* DMP is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* DMP is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    DMP::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#ifndef DMP_EXCEPTION_H
#define DMP_EXCEPTION_H

#include <exception>
#include <string>
#include <sstream>

namespace DMP
{
    /**
     * \class Exception
     * \brief Standard exception type.
     *
     * \ingroup Exceptions
     *
     * Offers a backtrace to where the exception was thrown.<br/>
     * All local exceptions must inherit from this class.
     */
    class Exception: public std::exception
    {
        mutable std::string resultingMessage;
    public:
        Exception();
        Exception(const Exception& e);
        Exception(std::string reason)
        {
            backtrace = generateBacktrace();
            setReason(reason);
        }

        ~Exception() noexcept override { }
        const char* what() const noexcept override;
        virtual std::string name() const
        {
            return "DMP::Exception";
        }
        void setReason(std::string reason);

        static std::string generateBacktrace();

        template<typename T>
        Exception& operator<<(const T& message)
        {
            reason << message;
            return *this;
        }

    private:

        void generateOutputString() const;

        std::stringstream reason;
        std::string backtrace;
    };

}
#endif // DMP_EXCEPTION_H
