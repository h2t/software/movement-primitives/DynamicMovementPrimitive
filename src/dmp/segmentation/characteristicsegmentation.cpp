/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

#include "characteristicsegmentation.h"

#include <math/pca.h>
#include <iomanip>

#include <general/gnuplot_i.hpp>

using namespace DMP;

CharacteristicSegmentation::CharacteristicSegmentation(double minSegmentSize, double similarityThreshold, int deriv, double weightRoot, double windowSize) :
    minSegmentSize(minSegmentSize),
    windowSize(windowSize),
    segmentThreshold(similarityThreshold),
    maxAmplitude(0.0),
    deriv(deriv),
    weightRoot(weightRoot)
{
}

CharacteristicSegmentation::CharacteristicSegmentation(const SampledTrajectoryV2 &traj, double minSegmentSize, double similarityThreshold) :
    traj(traj),
    minSegmentSize(minSegmentSize),
    segmentThreshold(similarityThreshold)
{

}

void CharacteristicSegmentation::calc()
{
    if(traj.size() == 0 || traj.dim() == 0)
    {
        throw Exception() << "Trajectory is empty: " << traj.size() << ":  " << traj.dim();
    }

//    traj.plotAllDimensions(2);
    DVec amplitudes;
    for(size_t d = 0; d < traj.dim(); d++)
    {
        double amplitude = traj.getMax(d, deriv, traj.begin()->getTimestamp(), traj.rbegin()->getTimestamp()) -  traj.getMin(d, deriv, traj.begin()->getTimestamp(), traj.rbegin()->getTimestamp());
        maxAmplitude = std::max(maxAmplitude, amplitude);
        amplitudes.push_back(amplitude);
//        double amplitudeRight = traj.getMax(dimension, deriv, split, rightBorder) -  traj.getMin(dimension, deriv, split, rightBorder);
    }
//    std::cout << "bestSplit at: " << findBestSplit(traj.begin()->getTimestamp(), traj.rbegin()->getTimestamp()) << std::endl;
//    std::cout << findBestSplitsSlidingWindow(traj.begin()->getTimestamp(), traj.rbegin()->getTimestamp(), 0.02) << std::endl;
    findBestSplitsSlidingWindowRecursive(keyframes, traj.begin()->getTimestamp(), traj.rbegin()->getTimestamp(), 0.05);
//    std::cout << "segments: " << keyframes << std::endl;
//    double t2 = findBestSplitsSlidingWindow(t, traj.rbegin()->getTimestamp(), 0.02);
//    std::cout << "bestSplit at: " << t << std::endl;
//    std::cout << "avg acc: " << traj.getDimensionDataAsEigen(0, 2, traj.begin()->getTimestamp(), t).cwiseAbs().colwise().mean() << std::endl;
//    Eigen::VectorXd acc = traj.getDimensionDataAsEigen(0, 2, t, traj.rbegin()->getTimestamp()).cwiseAbs();
//    Vec<DVec> plots;
//    plots.push_back(traj.getTimestamps());
//    plots.push_back(traj.getDimensionData(0,2));
//    plotTogether(plots, "plots/acc_%d.gp");
////    std::cout << "acc: " << acc << std::endl;
//    std::cout << "avg acc: " << acc.colwise().mean() << std::endl;
//    std::cout << "bestSplit at: " << t2 << std::endl;
//    std::cout << "avg acc: " << traj.getDimensionDataAsEigen(0, 2, traj.begin()->getTimestamp(), t2).cwiseAbs().colwise().mean() << std::endl;
    //    std::cout << "avg acc: " << traj.getDimensionDataAsEigen(0, 2, t2, t).cwiseAbs().colwise().mean() << std::endl;
}

void CharacteristicSegmentation::plot(ImageType imageType, SampledTrajectoryV2* origTraj)
{
    //    std::vector<double> frames = getColumn(__3DPositions, 0);
    std::vector<DVec> plots;
    std::vector<std::string> titles;
    //    frames.resize(450, 0);
    int deriv = 0;
    if(!origTraj )
    {
        origTraj = &traj;
        deriv = 2;
    }
//    origTraj->removeDimension(2);
    DVec frames = origTraj->getTimestamps();
    plots.push_back(frames);
    titles.push_back("frames");
    titles.push_back("X");
    titles.push_back("Y");
    titles.push_back("Z");
    for(size_t d = 0; d < origTraj->dim(); d++)
    {
        plots.push_back(origTraj->getDimensionData(d,deriv));
//        std::stringstream str;
//        str << "axis " << d;
//        titles.push_back(str.str());
    }


    std::stringstream add;
    add << "set xrange ["<< *frames.begin() << ":"<< *frames.rbegin() << "]\n";
//    add << "set yrange [" << origTraj->getMin(0,0, origTraj->begin()->getTimestamp(), origTraj->rbegin()->getTimestamp())  << ":"<< origTraj->getMax(0,0, origTraj->begin()->getTimestamp(), origTraj->rbegin()->getTimestamp()) << "]\n";
    add << "set key left top\n";
    add << " set xlabel 'Time in seconds'\n" ;
    add << " set ylabel 'Position in mm'\n";
    add << "set style line 6 lt 0 lw 3 lc rgb \"black\" \n";
    DoubleMap::iterator it = keyframes.begin();
    int i=1;
    for(; it != keyframes.end(); it++)
    {
        add << "set arrow from " << it->first << ",graph(0,0) to " << it->first << ",graph(1,1) nohead ls 6;\n";
        i++;
    }
//    add << "set arrow from "<< *frames.begin() <<"," << distanceThreshold << " to "<< *frames.rbegin() << "," <<distanceThreshold << " nohead lt 5 lc rgb 'black';\n";

    // svg
//    if(imageType == eSVG)
//    {
//        add << "set terminal svg size 1800,600 fname 'Verdana' fsize 10\n";
//        add << "set output 'plots/comparison.svg'\n";
//    }
//    else
//    {
//        //    // eps
//        add << "set terminal postscript eps enhanced color font 'Verdana,20' linewidth 3" << std::endl
//            <<" set output 'plots/segmentation.eps'";
//    }
    std::stringstream str;
    str << "plots/aas_" << origTraj->getMin(0,0, origTraj->begin()->getTimestamp(), origTraj->rbegin()->getTimestamp())  << "_%d.gp";
    plotTogether(plots,titles,str.str().c_str(), true, add.str());

}

void CharacteristicSegmentation::plotAnimated(ImageType imageType, SampledTrajectoryV2* origTraj)
{
    //    std::vector<double> frames = getColumn(__3DPositions, 0);
    std::vector<DVec> plots;
    std::vector<std::string> titles;
    //    frames.resize(450, 0);
    int deriv = 0;
    if(!origTraj )
    {
        origTraj = &traj;
        deriv = 2;
    }
//    origTraj->removeDimension(2);
    DVec frames = origTraj->getTimestamps();
//    plots.push_back(frames);
    titles.push_back("frames");
    titles.push_back("X");
    titles.push_back("Y");
    titles.push_back("Z");



    std::stringstream add;
    add << "set xrange ["<< *frames.begin() << ":"<< *frames.rbegin() << "]\n";
//    add << "set yrange [" << origTraj->getMin(0,0, origTraj->begin()->getTimestamp(), origTraj->rbegin()->getTimestamp())  << ":"<< origTraj->getMax(0,0, origTraj->begin()->getTimestamp(), origTraj->rbegin()->getTimestamp()) << "]\n";
    add << "set key left top\n";
    add << " set xlabel 'Time in seconds'\n" ;
    add << " set ylabel 'Position in mm'\n";
    add << "set style line 6 lt 0 lw 3 lc rgb \"black\" \n";
    add << "n=0\nm=0\n";
    add << "do for [ii=" << (*frames.begin()*100) << ":" << (*frames.rbegin()*100) << "] {\n\
           n=n+1	\n";
    DoubleMap::iterator it = keyframes.begin();
    int i=1;
    for(; it != keyframes.end(); it++)
    {
        add << "\tif (m>" << (it->first*100)<< "){set arrow from " << it->first << ",graph(0,0) to " << it->first << ",graph(1,1) nohead ls 6}\n";

        i++;
    }
    add << "\tset output sprintf('plots/animation/dmp_segmentation_25fps_%03.0f.png',n)\n\tplot ";
    for(size_t d = 0; d < origTraj->dim(); d++)
    {
        std::stringstream specificFilename;
        specificFilename << "plots/dmp_seg_" << d << ".gp";
        writeGNUPlotFileWithXAxis(frames, origTraj->getDimensionData(d,deriv), specificFilename.str().c_str());
        add << "\"" << specificFilename.str() <<  "\" every ::0::m with lines ls " << (d+1) << " title '" << titles.at(d+1) << "'";
        if(d+1 < origTraj->dim())
            add << ", ";
//        std::stringstream str;
//        str << "axis " << d;
//        titles.push_back(str.str());
    }
/*
        plot \"plots/aas_-19128.2_1.gp\" every ::0::m with lines ls 1 title 'X', \\ \
            \"plots/aas_-19128.2_2.gp\" every ::0::m with lines ls 2 title 'Y', \\ \
            \"plots/aas_-19128.2_3.gp\" every ::0::m with lines ls 3 title 'Z'";
*/
    add << "\nm=m+4\n}";
    std::fstream file;
    std::stringstream scriptfile;
    scriptfile << "plots/dmp_seg.gps";
    file.open(scriptfile.str().c_str(), std::fstream::out);
    file << add.str();
    file.close();
    char buf[10240];
    sprintf(buf, GNUPLOT_COMMAND_FROM_FILE, scriptfile.str().c_str());
    std::cout << buf << std::endl;
    system(buf);

//    add << "set arrow from "<< *frames.begin() <<"," << distanceThreshold << " to "<< *frames.rbegin() << "," <<distanceThreshold << " nohead lt 5 lc rgb 'black';\n";

    // svg
//    if(imageType == eSVG)
//    {
//        add << "set terminal svg size 1800,600 fname 'Verdana' fsize 10\n";
//        add << "set output 'plots/comparison.svg'\n";
//    }
//    else
//    {
//        //    // eps
//        add << "set terminal postscript eps enhanced color font 'Verdana,20' linewidth 3" << std::endl
//            <<" set output 'plots/segmentation.eps'";
//    }



}


DVec CharacteristicSegmentation::getKeyframes() const
{
    DVec result;
    DoubleMap::const_iterator it = keyframes.begin();
    int i=1;
    for(; it != keyframes.end(); it++)
    {
        result.push_back(it->first);
    }
    return result;
}

DoubleMap CharacteristicSegmentation::getKeyframesWithConfidence() const
{
    return keyframes;
}

double CharacteristicSegmentation::findBestSplit(double leftTimestamp, double rightTimestamp)
{
    leftBorder = leftTimestamp;
    rightBorder = rightTimestamp;
    double bestValue = 0.0;
    double bestTimstamp = -1;
    std::string debug;
    double centerValue = calcSplit(leftTimestamp + (rightTimestamp-leftTimestamp)*0.5, 0, debug);
    int i = 0;
    findBestSplitRecursive(leftTimestamp, rightTimestamp, centerValue, 0, bestTimstamp);
    return bestTimstamp;
}

void CharacteristicSegmentation::findBestSplitRecursive(double leftTimestamp, double rightTimestamp, double & bestValue, int iteration, double & bestTimestamp)
{
    if(iteration > 10)
        return;
    double centerTimestamp = leftTimestamp + (rightTimestamp-leftTimestamp)*0.5;
//    std::cout << "bestValue: " << bestValue << " center: " << centerTimestamp << std::endl;
    std::string debug;
    double leftQuarterValue = calcSplit(leftTimestamp + (centerTimestamp-leftTimestamp)*0.5, 0, debug);
    double rightQuarterValue = calcSplit(centerTimestamp + (rightTimestamp-centerTimestamp)*0.5, 0, debug);

    if((leftQuarterValue > rightQuarterValue && !(bestValue > leftQuarterValue && fabs(bestTimestamp-leftTimestamp) > fabs(bestTimestamp-rightTimestamp))) || (bestValue > rightQuarterValue && fabs(bestTimestamp-leftTimestamp) < fabs(bestTimestamp-rightTimestamp)))
    {
        if(leftQuarterValue > bestValue)
            bestTimestamp = leftTimestamp + (centerTimestamp-leftTimestamp)*0.5;

        bestValue = std::max(leftQuarterValue, bestValue);
//        std::cout << "compareValue: " << leftQuarterValue << std::endl;
        findBestSplitRecursive(leftTimestamp, centerTimestamp, bestValue, iteration+1, bestTimestamp);
    }
    else
    {
        if(rightQuarterValue > bestValue)
            bestTimestamp = centerTimestamp + (rightTimestamp-centerTimestamp)*0.5;
//        std::cout << "compareValue: " << rightQuarterValue << std::endl;
        bestValue = std::max(rightQuarterValue, bestValue);
        findBestSplitRecursive(centerTimestamp, rightTimestamp, bestValue, iteration+1, bestTimestamp);
    }
}

double CharacteristicSegmentation::findBestSplitSampling(double leftTimestamp, double rightTimestamp, double stepSize)
{
    leftBorder = leftTimestamp;
    rightBorder = rightTimestamp;
    double bestValue = 0;
    double bestTimestamp = -1;
    double max = traj.getMax(0,0,leftBorder, rightBorder);
    double min = traj.getMin(0,0,leftBorder, rightBorder);
    double space = max - min;
    space *= space ;

    for(double t = leftTimestamp+stepSize; t < rightTimestamp; t+=stepSize)
    {
        std::string debug;
        double value = calcSplit(t, 0, debug);

//        std::cout << "t: " << t <<  " Value: " << value << std::endl;
        if(value > bestValue)
        {
            bestTimestamp = t;
            bestValue = value;
        }
    }

    std::cout << "best value: " << bestValue/space << std::endl;
    return bestTimestamp;
}

DVec CharacteristicSegmentation::findBestSplitsSlidingWindow(double leftTimestamp, double rightTimestamp, double stepSize)
{
    DVec result;
    ValuesAndTimestamps splits;
    leftBorder = leftTimestamp;
    rightBorder = rightTimestamp;
//    double bestValue = 0;
//    double bestTimestamp = -1;
    double max = traj.getMax(0,0,leftBorder, rightBorder);
    double min = traj.getMin(0,0,leftBorder, rightBorder);
    double space = max - min;
    space *= space ;
    std::string debug;
    for(double t = leftTimestamp+minSegmentSize; t < rightTimestamp - minSegmentSize; t+=stepSize)
    {
        double value = calcSplit(t, 0, debug);
        value /= space;
        if(value > segmentThreshold)
            splits.push_back(std::make_pair(value, t));

    }

    std::sort(splits.rbegin(), splits.rend());
//    for(size_t i = 0; i < splits.size(); i++)
    while(splits.size() >  0)
    {
        result.push_back(splits.begin()->second);
        double t = splits.begin()->second;
        for(ValuesAndTimestamps::iterator it = splits.begin(); it != splits.end();)
        {
            if(fabs(t-it->second) < minSegmentSize)
                it = splits.erase(it);
            else it++;
        }
    }
    return result;
}

void CharacteristicSegmentation::findBestSplitsSlidingWindowRecursive(DoubleMap &results, double leftTimestamp, double rightTimestamp, double stepSize)
{
    leftBorder = leftTimestamp;
    rightBorder = rightTimestamp;
    double bestValue = 0;
    double bestTimestamp = -1;
    //double bestDim = 0;
    std::string bestString;
//        double max = traj.getMax(d,0,leftBorder, rightBorder);
//        double min = traj.getMin(d,0,leftBorder, rightBorder);
//        double space = max - min;
//        space *= space ;

    for(double t = leftTimestamp+minSegmentSize; t < rightTimestamp-minSegmentSize; t+=stepSize)
    {
        double leftTempBorder = std::max(leftBorder, t-windowSize);
        double rightTempBorder = std::min(rightBorder, t+windowSize);
//        PCA pca(traj.toEigen(0, leftTempBorder, rightTempBorder).transpose());
//        PCA::ValuesAndIndices pcaResults = pca.getXBestDimensions(3);

        for(size_t d = 0; d < traj.dim(); d++)
        {

            std::string out;
            double value = calcSplit(t,d, out) ;
            double amplitude = traj.getTrajectorySpace(d, deriv,  leftTempBorder, rightTempBorder); //traj.getMax(d, 2, leftTempBorder, rightTempBorder) -  traj.getMin(d, 2, leftTempBorder, rightTempBorder);
//            double eigenValue = pca.getEigenValues()(d).real();
            std::stringstream str;
//            str << " eigenValue: " << eigenValue  << " max: " << pcaResults.front().first ;
//            value *= eigenValue/pcaResults.front().first;
            str << "value before: " << value << " amplitude: " << amplitude  << " maxAmplitude: " << maxAmplitude << " " << out;
            value *= pow(amplitude, 1.0/weightRoot)/pow(maxAmplitude, 1.0/weightRoot);
            //    bestValue /= space;
            if(value > bestValue)
            {
                //bestDim = d;
                bestTimestamp = t;
                bestValue = value;
                bestString = str.str();
            }
        }
    }

//    std::cout << "best value: " << bestValue << "\t at " << bestTimestamp << " in dim " << bestDim << " between " << leftTimestamp << " and " << rightTimestamp  << bestString << std::endl;
    if(bestValue > segmentThreshold)
    {
        results[bestTimestamp] = bestValue;
        findBestSplitsSlidingWindowRecursive(results, leftTimestamp, bestTimestamp, stepSize);
        findBestSplitsSlidingWindowRecursive(results, bestTimestamp, rightTimestamp, stepSize);
    }

//    return bestTimestamp;
}





double CharacteristicSegmentation::calcSplit(double split, int dimension, std::string & out)
{
    double leftTempBorder = std::max(leftBorder, split-windowSize);
    double rightTempBorder = std::min(rightBorder, split+windowSize);
    double mean = traj.getDimensionDataAsEigen(dimension, deriv, leftTempBorder, rightTempBorder).mean();

//    DVec minima = traj.getMinima(dimension, deriv, leftTempBorder, split);
//    DVec maxima = traj.getMaxima(dimension, deriv, leftTempBorder, split);
//    double amplitudeLeft = vecSum(maxima)/maxima.size() - vecSum(minima)/minima.size();
//    minima = traj.getMinima(dimension, deriv, split, rightTempBorder);
//    maxima = traj.getMaxima(dimension, deriv, split, rightTempBorder);
//    double amplitudeRight = vecSum(maxima)/maxima.size() - vecSum(minima)/minima.size();
    double amplitudeLeft = traj.getTrajectorySpace(dimension, deriv, leftTempBorder, split) ;//-  traj.getMin(dimension, deriv, leftTempBorder, split);
    double amplitudeRight = traj.getTrajectorySpace(dimension, deriv, split, rightTempBorder) ;//-  traj.getMin(dimension, deriv, split, rightTempBorder);
    double min = traj.getMin(dimension, deriv, leftTempBorder, rightTempBorder);
    double leftCurveLength = calcCharacteristic(dimension, deriv, leftTempBorder, split, mean, split);
    double rightCurveLength = calcCharacteristic(dimension, deriv, split, rightTempBorder, mean, split);
    // normalize to time
    leftCurveLength /= (split-leftTempBorder);
    rightCurveLength /= rightTempBorder-split;

    leftCurveLength *= pow(amplitudeLeft/amplitudeRight, 2.0);
    rightCurveLength *= pow(amplitudeRight/amplitudeLeft, 2.0);

    double result;
    if(leftCurveLength > rightCurveLength)
        result = fabs(leftCurveLength / rightCurveLength);
    else
        result = fabs(rightCurveLength / leftCurveLength);
    std::stringstream str;
    str << " at " << split << ": " << leftCurveLength << ", " << rightCurveLength << " \t result: " << result
        << " amp: " << amplitudeLeft << " vs " << amplitudeRight << " leftTempBorder: " << leftTempBorder << " split: " << split << " rightborder: " << rightTempBorder <<std::endl;
    out = str.str();
    return result;

}

// gauss macro
#define g(x)  exp(-double((x)*(x))/(2*sigma*sigma)) / ( sigma * sqrt(2*3.14159265))
#define round(x) int(0.5+x)

double CharacteristicSegmentation::calcCharacteristic(size_t dimension, size_t deriv, double leftBorder, double rightBorder, double mean, double center)
{
    double length = 0.0;
    const SampledTrajectoryV2::ordered_view& ordv = traj.data().get<SampledTrajectoryV2::TagOrdered>();
//    timestamp_view::iterator it = dataMap.find(startTime);
//    DVec data = getDimensionData(0);
//    timestamp_view::iterator itEnd= dataMap.find(endTime);
    SampledTrajectoryV2::ordered_view::iterator itO = ordv.lower_bound(leftBorder);
    SampledTrajectoryV2::ordered_view::iterator itOEnd = ordv.upper_bound(rightBorder);
    if(itO == ordv.end())
        return 0.0;
    double prevValue = itO->getDeriv(dimension, deriv);
    double exponent = 1.0;
    double weightedSum = 0;
    double sumOfWeight = 0;
    const double sigma = windowSize / 2.5;
    const double sqrt2PI = sqrt(2 * M_PI);
    for (;
         itO != itOEnd;
         itO++
        )
    {

        double value = fabs(pow(prevValue/*-mean*/, exponent) - pow(itO->getDeriv(dimension, deriv)/*-mean*/, exponent));
        double diff = (itO->getTimestamp() - center);
        double squared = diff * diff;
        const double gaussValue = 1;//= exp(-squared / (2 * sigma * sigma)) / (sigma * sqrt2PI);
        sumOfWeight += gaussValue;
        weightedSum += gaussValue * value;
        prevValue = itO->getDeriv(dimension, deriv);
    }
    length = weightedSum/*/sumOfWeight*/;
    return length;
}


CharacteristicSegmentationOnPerturbationTerm::CharacteristicSegmentationOnPerturbationTerm(const SampledTrajectoryV2 &traj, double minSegmentSize, double similarityThreshold, int deriv, double windowSize, float weightRoot) :
    CharacteristicSegmentation(minSegmentSize, similarityThreshold, deriv, weightRoot, windowSize)
{
//    learnFromTrajectory(traj);
    DVec timestamps = traj.getTimestamps();
    DVec faketime;
    for(size_t i = 0; i < timestamps.size(); i++)
        faketime.push_back(1+timestamps.at(i)/pow(*timestamps.rbegin(),2.0));


    // guarantee the size of amplitudes
    while(amplitudes.size() < traj.dim())
        amplitudes.push_back(1.0);

    for(size_t d = 0; d < traj.dim(); d++)
    {
        DVec values;
        DoubleMap valueMap = calcPerturbationForceSamples(d, faketime, traj);
//        FunctionApproximationInterfacePtr func = _getFunctionApproximator(d);
        for(size_t i = 0; i < timestamps.size(); i++)
        {
            double curTime = (double)(i)/timestamps.size();
            values.push_back(valueMap.at(1+timestamps.at(i)/pow(*timestamps.rbegin(),2.0)));
//            values.push_back((*func)(DVec(1,timestamps.at(i))).at(0));
        }
        this->traj.addDimension(timestamps, values);
    }
//    this->traj.plotAllDimensions(0);
}


SampledTrajectoryV2 CharacteristicSegmentationData::getSubsegment(double firstKeyFrame)
{
    DoubleMap::const_iterator it = keyframesWithConfidence.find(firstKeyFrame);
    if(it == keyframesWithConfidence.end())
        throw Exception("Cannot find keyframe: ") << firstKeyFrame;
    it++;
    double secondKeyFrame = it->first;
    if(it == keyframesWithConfidence.end())
        secondKeyFrame = trajectorySegment.rbegin()->getTimestamp();
    return trajectorySegment.getPart(firstKeyFrame,secondKeyFrame,2);
}


void SemanticSegmentData::plot()
{
    //    std::vector<double> frames = getColumn(__3DPositions, 0);
    std::vector<DVec> plots;
    std::vector<std::string> titles;
    //    frames.resize(450, 0);
    int deriv = 0;
    std::stringstream add;
    add << "set style line 5 lt 0 lw 4 lc rgb \"red\" \n";
    add << "set style line 6 lt 0 lw 2 lc rgb \"black\" \n";
    SampledTrajectoryV2 traj;
    for(auto data : segments)
    {
       for(size_t d=0; d < data.trajectorySegment.dim(); ++d)
       {
           traj.addPositionsToDimension(d, data.trajectorySegment.getTimestamps(), data.trajectorySegment.getDimensionData(d));

       }

       double maxConf = 0;
       DoubleMap::const_iterator it = data.keyframesWithConfidence.begin();
       for(; it != data.keyframesWithConfidence.end(); ++it)
       {
           maxConf = std::max(maxConf, it->second);
       }
       if(maxConf >  0)
       {
           it = data.keyframesWithConfidence.begin();
           for(; it != data.keyframesWithConfidence.end(); ++it)
           {
               if(it->second == 0)
                   continue;
               std::stringstream color;
               color << std::hex << std::setw(2)  << std::setfill('0') << (int)(it->second/maxConf*255) << "00" <<  (int)((1-it->second/maxConf)*256);
               std::cout << "color: " << color.str() << std::endl;
//               add << "set style line 6 lt 0 lw 2 lc rgb \"#" << color.str() << "\" \n";
               add << std::dec;
               add << "set arrow from " << it->first << ",graph(0,0) to " << it->first << ",graph(1,1) nohead ls 6;\n";
           }
       }

       add << "set arrow from " << data.trajectorySegment.rbegin()->getTimestamp() << ",graph(0,0) to " << data.trajectorySegment.rbegin()->getTimestamp() << ",graph(1,1) nohead ls 5;\n";
    }


    DVec frames = traj.getTimestamps();
    plots.push_back(frames);
    titles.push_back("Time in seconds");
    titles.push_back("X Position");
    titles.push_back("Y Position");
    titles.push_back("Z Position");
    for(size_t d = 0; d < traj.dim(); d++)
    {
        plots.push_back(traj.getDimensionData(d,deriv));
//        std::stringstream str;
//        str << "axis " << d;
//        titles.push_back(str.str());
    }





    add << "set xrange [0.00:"<< *frames.rbegin() << "]\n";
//    add << "set yrange [" << origTraj->getMin(0,0, origTraj->begin()->getTimestamp(), origTraj->rbegin()->getTimestamp())  << ":"<< origTraj->getMax(0,0, origTraj->begin()->getTimestamp(), origTraj->rbegin()->getTimestamp()) << "]\n";
    add << "set key left top\n";
    add << " set xlabel 'Time'\n" ;
    add << " set ylabel 'Position'\n";


//    add << "set arrow from "<< *frames.begin() <<"," << distanceThreshold << " to "<< *frames.rbegin() << "," <<distanceThreshold << " nohead lt 5 lc rgb 'black';\n";

    // svg
//    if(imageType == eSVG)
    {
//        add << "set terminal svg size 1800,600 fname 'Verdana' fsize 10\n";
//        add << "set output 'plots/comparison.svg'\n";
    }
//    else
//    {
//        //    // eps
        add << "set terminal postscript eps enhanced color font 'Verdana,20' linewidth 3" << std::endl
            <<" set output 'plots/aas.eps'";
//    }
    std::stringstream str;
    str << "plots/aas_%d.gp";
    plotTogether(plots,titles,str.str().c_str(), true, add.str());



//    Gnuplot gnuplot;
//    DVec x = segments.begin()->trajectorySegment.getTimestamps();
//    DVec y = segments.begin()->trajectorySegment.getDimensionData(0,0);
//    DVec y2 = segments.begin()->trajectorySegment.getDimensionData(1,0);
//    gnuplot
//            .cmd("set terminal svg size 1800,600 fname \"Verdana\" fsize 10")
//                .cmd("set output \"sec.svg\"")
////            .savetofigure("./sec.ps", "postscript color")
//            .set_style("lines")
////            .showonscreen()
//            .plot_xy(x,y)
//           .plot_xy(x,y2);
//            ;
////    gnuplot.plot_equation("sin(12*x)*exp(-x)").plot_equation("exp(-x)");
//    std::cout << "plot done";

}

SampledTrajectoryV2 CharacteristicSegmentationData::getSubsegment(double firstKeyFrame, double secondKeyFrame)
{
    return trajectorySegment.getPart(firstKeyFrame,secondKeyFrame,2);

}
