/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2014
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/
#ifndef _DMP_CHARACTERISTICSEGMENTATION_H
#define _DMP_CHARACTERISTICSEGMENTATION_H

#include <dmp/representation/trajectory.h>

#include <dmp/representation/dmp/basicdmp.h>

#include <dmp/representation/dmp/endveloforcefielddmp.h>
#include <dmp/representation/dmp/forcefielddmp.h>

#include <boost/serialization/map.hpp>
namespace DMP
{
    typedef std::map<double, double> DoubleMap;
    class CharacteristicSegmentation
    {
    public:
        enum ImageType{
            eSVG,
            eEPS
        };
        typedef std::pair<double, double> ValueAndTimestamp;
        typedef std::vector<ValueAndTimestamp> ValuesAndTimestamps;


        CharacteristicSegmentation(double minSegmentSize, double similarityThreshold, int deriv, double weightRoot, double windowSize);
        CharacteristicSegmentation(const SampledTrajectoryV2& traj, double minSegmentSize, double similarityThreshold);
        void calc();
        void plot(ImageType imageType = eSVG, SampledTrajectoryV2 *origTraj = NULL);
        void plotAnimated(ImageType imageType = eSVG, SampledTrajectoryV2 *origTraj = NULL);
        DVec getKeyframes() const;
        DoubleMap getKeyframesWithConfidence() const;
    protected:
        double findBestSplit(double leftTimestamp, double rightTimestamp);
        void findBestSplitRecursive(double leftTimestamp, double rightTimestamp, double &bestValue, int iteration, double &bestTimestamp);

        double findBestSplitSampling(double leftTimestamp, double rightTimestamp, double stepSize);
        DVec findBestSplitsSlidingWindow(double leftTimestamp, double rightTimestamp, double stepSize);
        void findBestSplitsSlidingWindowRecursive(DoubleMap& results, double leftTimestamp, double rightTimestamp, double stepSize);

        double calcSplit(double split, int dimension, string &out);
        double calcCharacteristic(size_t dimension, size_t deriv, double leftBorder, double rightBorder, double mean, double center);
        double leftBorder, rightBorder;
        SampledTrajectoryV2 traj;
        double minSegmentSize;
        double windowSize;
        double segmentThreshold;
        DoubleMap keyframes;
        double maxAmplitude;
        int deriv;
        double weightRoot;

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            ar& BOOST_SERIALIZATION_NVP(minSegmentSize);
            ar& BOOST_SERIALIZATION_NVP(segmentThreshold);
            ar& BOOST_SERIALIZATION_NVP(windowSize);
            ar& BOOST_SERIALIZATION_NVP(keyframes);
            ar& BOOST_SERIALIZATION_NVP(maxAmplitude);
//            ar& BOOST_SERIALIZATION_NVP(traj);

        }

    };


    class CharacteristicSegmentationData
    {
    public:

        std::string mainObject;
        std::map<double, std::string> actionLabels;
        std::vector<std::string> objectsInContact;
        DoubleMap keyframesWithConfidence;
        SampledTrajectoryV2 trajectorySegment;
        SampledTrajectoryV2 getSubsegment(double firstKeyFrame);
        SampledTrajectoryV2 getSubsegment(double firstKeyFrame, double secondKeyFrame);
    private:

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            ar& BOOST_SERIALIZATION_NVP(mainObject);
            ar& BOOST_SERIALIZATION_NVP(actionLabels);
            ar& BOOST_SERIALIZATION_NVP(objectsInContact);
            ar& BOOST_SERIALIZATION_NVP(keyframesWithConfidence);
            ar& BOOST_SERIALIZATION_NVP(trajectorySegment);
        }
    };

    class SemanticSegmentData
    {
    public:
        std::string mainObject;
        std::vector<CharacteristicSegmentationData> segments;
        void plot();
    private:

        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
            ar& BOOST_SERIALIZATION_NVP(mainObject);
            ar& BOOST_SERIALIZATION_NVP(segments);
        }
    };

    class CharacteristicSegmentationOnPerturbationTerm :
            virtual public CharacteristicSegmentation,
            virtual protected BasicDMP
    {
    public:
        CharacteristicSegmentationOnPerturbationTerm(const SampledTrajectoryV2& traj, double minSegmentSize, double similarityThreshold, int deriv, double windowSize, float weightRoot);
    protected:
        friend class boost::serialization::access;
        template<class Archive>
        void serialize(Archive& ar, const unsigned int file_version)
        {
//            ar& BOOST_SERIALIZATION_NVP(traj);
            CharacteristicSegmentation& base = boost::serialization::base_object<CharacteristicSegmentation >(*this);
            ar& BOOST_SERIALIZATION_NVP(base);

        }
    };
}

#endif
