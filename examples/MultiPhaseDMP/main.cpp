

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <dmp/representation/dmp/multiphasedmp.h>
#include <boost/token_iterator.hpp>
#include <dmp/functionapproximation/radialbasisfunctioninterpolator.h>
#include <sys/time.h>
#include <dmp/functionapproximation/lwprwrapper.h>
#include <sys/time.h>
#include <dmp/representation/dmpfactory.h>

using namespace DMP;


int main(int argc, char* argv[])
{
    Vec<DVec> plots;

    // Load the trajectory
    SampledTrajectoryV2 traj;
    std::string path = DATA_DIR;
    path += "/ExampleTrajectories/pickplace7d.csv";
    traj.readFromCSVFile(path);
    traj.gaussianFilter(0.01);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);


    // train the DMP
    DMPInterfacePtr dmp;

//    dmp.reset(new MultiPhaseDMP(2));
//    dmp->setConfiguration(DMP::SpringFactor, 0);
//    dmp->setConfiguration(DMP::DampingFactor, 0);
    struct timeval start, end;
    gettimeofday(&start, nullptr);

    dmp->learnFromTrajectory(traj);

    gettimeofday(&end, nullptr);
    double delta = ((end.tv_sec  - start.tv_sec) * 1000000u +
                    end.tv_usec - start.tv_usec) / 1.e6;

    std::cout << "time: " << delta << std::endl;


    // test the DMP

    DVec2d initialState;
    DVec goal;
    for(size_t i = 0; i < 7; ++i)
    {
        DVec state;
        state.push_back(traj.begin()->getPosition(i));
        state.push_back(traj.begin()->getDeriv(i,1));
        initialState.push_back(state);

        goal.push_back(traj.rbegin()->getPosition(i));
    }

    DVec initialCanonicalValues;
    initialCanonicalValues.push_back(1.0);
    initialCanonicalValues.push_back(1.0);

    double temporalFactor = 1.0;

    SampledTrajectoryV2 newTraj =
            dmp->calculateTrajectory(traj.getTimestamps(), goal, initialState, initialCanonicalValues, temporalFactor);


    // Plotting
    for(size_t i = 0; i < traj.dim(); i++)
    {
        plots.clear();
        plots.push_back(traj.getTimestamps());
        plots.push_back(traj.getDimensionData(i, 0));
        plots.push_back(newTraj.getDimensionData(i, 0));
        plotTogether(plots, "plots/trajComparison_%d.gp");
    }

    return 0;


}


