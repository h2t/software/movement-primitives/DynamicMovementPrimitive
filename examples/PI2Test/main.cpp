/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <general/helpers.h>
#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/forcefielddmp.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <dmp/representation/dmpfactory.h>

#include <sys/time.h>

#include <iostream>

#include <dmp/costfunction/simplecostfcn.h>

using namespace DMP;


void SimpleCostTest()
{
    int res = KILL_ALL_PLOT_WINDOWS;
    if(res)
    {
        throw std::logic_error{"Killing all plot windows returned " + std::to_string(res)};
    }
    res = system("mkdir plots");
    if(res)
    {
        throw std::logic_error{"Command 'mkdir plots' returned " + std::to_string(res)};
    }
    Vec<DVec> plots;

    SampledTrajectoryV2 traj;

    std::string path = DATA_DIR;
    path += "/ExampleTrajectories/sampletraj.csv";
    traj.readFromCSVFile(path);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
    traj = traj.normalizeTraj();

    SampledTrajectoryV2 sampleTraj;
    sampleTraj = traj;

    DVec2d initialState;
    DVec goals;
    for(size_t i = 0; i < traj.dim(); i++)
    {
        DVec state;
        state.push_back(traj.begin()->getPosition(i));
        state.push_back(traj.begin()->getDeriv(i,1));
        initialState.push_back(state);

        goals.push_back(traj.rbegin()->getPosition(i));
    }

    DVec timestamps = traj.getTimestamps();

    double T = timestamps[timestamps.size() - 1];
    CostFunctionPtr cfn(new SimpleCostFunction());
    cfn->traj = sampleTraj;

    // train the DMP
    DMPFactory dmpfactory;
    DMPInterfacePtr dmp = dmpfactory.getDMP("BasicDMP",50);

    dmp->learnFromTrajectory(traj);
    SampledTrajectoryV2 newTraj = dmp->calculateTrajectory(timestamps, goals, initialState, 1.0, 1);

    std::vector<SampledTrajectoryV2> trajRes;
    std::normal_distribution<double> distribution(0, 1);

    struct timeval start, end;
    gettimeofday(&start, NULL);
    double precost = cfn->getCost(timestamps, goals, T, sampleTraj);

    std::cout << "T: " << T << std::endl;
    std::cout << "initial cost: " <<  precost << std::endl;


    for(int i = 0; i < 10; i++)
    {
        std::cout << "epoch: " << i << std::endl;

        dmp->PI2updateV2(20, 10, distribution, cfn, timestamps, T, goals, initialState, 1.0, 1);

        std::cout << "finished learning ... " << std::endl;
        SampledTrajectoryV2 newTraj2 = dmp->calculateTrajectory(timestamps, goals, initialState, 1.0, 1);
        trajRes.push_back(newTraj2);

        double cost = cfn->getCost(timestamps, goals, T, newTraj2);

        std::cout << "final cost: " << cost << std::endl;

        std::cout << "cost error: " << fabs(precost - cost) << std::endl;
        if(fabs(precost - cost) < 1e-3)
            break;

        precost = cost;
    }

    gettimeofday(&end, NULL);
    double delta = ((end.tv_sec  - start.tv_sec) * 1000000u +
                    end.tv_usec - start.tv_usec) / 1.e6;
    std::cout << "time for pi2update: " << delta << std::endl;
    //     Plotting

    plots.clear();
    plots.push_back(timestamps);

    for(int i = 0; i < newTraj.dim(); ++i)
    {
        plots.push_back(newTraj.getDimensionData(i, 0));
    }

    plotTogether(plots, "plots/trajComparison_%d.gp");

    plots.clear();
    plots.push_back(timestamps);
    for(int j = 0; j < trajRes.size(); ++j)
    {
        SampledTrajectoryV2 ctraj = trajRes.at(j);
        for(int i = 0; i < ctraj.dim(); ++i)
        {
            plots.push_back(trajRes.rbegin()->getDimensionData(i,0));
        }
    }
    plotTogether(plots, "plots/trajComparison_%d.gp");

}



int main(int argc, char* argv[])
{
    SimpleCostTest();
}


