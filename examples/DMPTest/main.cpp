#include <stdio.h>
#include <queue>
#include <ostream>

#include <dmp/representation/trajectory.h>
#include <dmp/representation/dmp/basicdmp.h>
#include <representation/dmp/getlearningerrordmp.h>
#include <representation/dmp/simpleendvelodmp.h>

// DMP::DVec -> typedef std::vector<double> DVec
using namespace DMP;

void testSplitting(SampledTrajectoryV2);

int main(int argc, char* pArgs[])
{
    // timestamps and poses of the origin trajectory
    DVec timestamps;
    DVec poses;

    // some sample data
    for (int i = 0; i < 40; ++i)
    {
        timestamps.push_back(double(i));
    }

    for (int i = 0; i < 40; ++i)
    {
        if (i < 7) // increase
            poses.push_back(double(i));
        else if (i < 12) // decrease
            poses.push_back(double(12 - i));
        else if (i < 15) // increase
            poses.push_back(double(i - 10));
        else if (i < 22) // decrease
            poses.push_back(double(18 - i));
        else if (i < 28) // increase
            poses.push_back(double(-24 + i));
        else if (i < 40)
            poses.push_back(double(30 - i));
    }

    // create trajectory from timestamp and poses
    SampledTrajectoryV2 trajectory;
    trajectory.addDimension(timestamps, poses);

    std::cerr << "testing start" << std::endl;
    testSplitting(trajectory);
    std::cerr << "testing end" << std::endl;

    return 0;
}

// stores one trajectory and the frame-offset from the original trajectory
typedef std::pair<SampledTrajectoryV2, int> TrajectorySplit; // first: trajectory, second: offset

////////////////////
/// iterative splitting and trajectory learning function
////////////////////
void testSplitting(SampledTrajectoryV2 trajectory)
{
    std::ofstream f;
    f.open("testResults.csv"); // write results to this file

    std::queue<TrajectorySplit> q; // queue contains trajectories (splitted trajectories will be pushed in the process)
    q.push(TrajectorySplit(trajectory, 0)); // push initial work trajectory

    // work loop, run until queue is empty
    while (!q.empty())
    {
        // get trajectory-offset pair from queue (pop)
        TrajectorySplit trajSplit = q.front();
        q.pop();


        // extract trajectory
        SampledTrajectoryV2 &traj = trajSplit.first;
        // normalize timestamps of trajectory to [0,1]
        traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);
        //traj.gaussianFilter(0.2);
        // set start and end points of trajectory for calculation
        Vec<DMPState> states = DMPState(traj.begin()->getPosition(0), traj.begin()->getDeriv(0, 1)); // start (pos and velocity)
        DVec goals = traj.rbegin()->getPosition(0); // end (dim 0)

        // train the DMP
        GetLearningErrorDMP dmp;
        FunctionApproximationInterfacePtr fa(new LWR<DMP::GaussKernel>(10));
//        FunctionApproximationInterfacePtr fa(new ExactReproduction());
        //dmp.setGoals(goals); // no effect...
        dmp.setFunctionApproximator(fa);
        dmp.setShowPlots(true);

        // output original trajectory
        traj.plotAllDimensions(0);

        std::vector<std::pair<DoubleMap, DoubleMap>> learningResults = dmp.getLearningApproximationError(traj);
        DoubleMap errorValues;

        for (DoubleMap::const_iterator i = learningResults[0].first.begin(); i != learningResults[0].first.end(); ++i)
        {
            errorValues[i->first] = 0.0;
        }

        for (int dim = 0; dim < learningResults.size(); ++dim)
        {
            std::pair<DoubleMap, DoubleMap> learningResultsDim = learningResults[dim];
            for (DoubleMap::const_iterator i = learningResultsDim.first.begin(), j = learningResultsDim.second.begin(); i != learningResultsDim.first.end() && j != learningResultsDim.second.end(); ++i, ++j)
            {
                if (i->first != j->first) {
                    std::cerr << "Timestamps do not match!!" << std::endl;
                    return;
                }
                f << i->first << "," << i->second << "," << j->second << "\n";
                errorValues[i->first] += fabs(i->second - j->second);
            }
        }

        for (DoubleMap::const_iterator i = errorValues.begin(); i != errorValues.end(); ++i)
        {
            std::cout << "Error in all dimensions for t = " << i->first << " is: " << i->second << std::endl;
        }

        break;

        /*
        std::cout << "Orig: " << traj.getDimensionData(0) << std::endl;
        dmp.learnFromTrajectory(traj);

        // prepare data for reconstruction
        DVec tstamps = traj.getTimestamps();
        SampledTrajectoryV2 reconstTrajectory;

        try
        {
            // reconstruct trajectory
            reconstTrajectory = dmp.calculateTrajectory(tstamps, goals, states, 0);
        }
        catch (DMP::Exception e)
        {
            std::cerr << e.name() << std::endl << e.what() << std::endl << e.generateBacktrace() << std::endl;
            continue;
        }
        std::cout << "Orig: " << traj.getDimensionData(0) << std::endl;
        std::cout << "Result: " << reconstTrajectory.getDimensionData(0) << std::endl;
        reconstTrajectory.plotAllDimensions(0, "plots/result");
        double tmp; dmp.evaluateReproduction(0, tmp);

        /////////////////////////
        /// some error handling
        if (reconstTrajectory.size() != traj.size())
        {
            std::cerr << "Warning: Original and reconstructed trajectory size mismatch! or: "
                      << traj.size() << " re: " << reconstTrajectory.size() << std::endl;
        }
        for (unsigned d = 0; d < reconstTrajectory.dim(); ++d)
        {
            if (fabs(goals[d] - reconstTrajectory.rbegin()->getPosition(d)) > 0.001)
            {
                std::cerr << "Warning: Reconstructed trajectory goal mismatch! goal[" << d << "]: " << goals[d]
                          << " re[" << d << "]: " << reconstTrajectory.rbegin()->getPosition(d) << " diff: "
                          << fabs(goals[d] - reconstTrajectory.rbegin()->getPosition(d)) << " orig end: "
                          << traj.rbegin()->getPosition(d) << std::endl;
            }
        }
        /////////////////////////

        // write all results in file
        DVec oTimeS = traj.getTimestamps(); // timestamps original trajectory
        DVec rTimeS = reconstTrajectory.getTimestamps(); // timestmaps reconst trajectory

        if (oTimeS.size() != rTimeS.size())
            std::cerr << "wrong size of trajectories" << std::endl;

        for (int i = 0; i < oTimeS.size(); ++i)
        {
            f << oTimeS[i] << ",";
        }

        f << "\n";

        for (int i = 0; i < oTimeS.size(); ++i)
        {
            f << traj.getDimensionData(0)[i] << ",";
        }

        f << "\n";

        for (int i = 0; i < rTimeS.size(); ++i)
        {
            f << reconstTrajectory.getDimensionData(0)[i] << ",";
        }

        f << "\n\n";
        // end write to file


        /// test: split always at the midpoint
        int frameToSeg = traj.size() / 2;
        if (frameToSeg > 0)
        {
            SampledTrajectoryV2 t = traj.getPart(traj.begin()->getTimestamp(), traj.getTimestamps()[frameToSeg - 1]);
            if (t.size() > 2)
            {
                q.push(TrajectorySplit(t, trajSplit.second));
            }
        }
        if (frameToSeg < traj.getTimestamps().size() - 1)
        {
            SampledTrajectoryV2 t = traj.getPart(traj.getTimestamps()[frameToSeg + 1], traj.rbegin()->getTimestamp());
            if (t.size() > 2)
            {
                q.push(TrajectorySplit(t, trajSplit.second + frameToSeg + 1));
            }
        }*/
    }
    f.close();
}
