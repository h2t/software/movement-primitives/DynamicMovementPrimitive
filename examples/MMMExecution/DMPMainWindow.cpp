/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#include "DMPMainWindow.h"
#include "ui_DMPMainWindow.h"
#include <VirtualRobot/Robot.h>
#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/forcefielddmp.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <dmp/io/MMMConverter.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <MMM/XMLTools.h>
#include <VirtualRobot/IK/ConstrainedOptimizationIK.h>
#include <MMMSimoxTools/MMMSimoxTools.h>
#include <iostream>
#include <VirtualRobot/IK/constraints/PositionConstraint.h>
#include <VirtualRobot/IK/constraints/ReferenceConfigurationConstraint.h>
#include <Inventor/nodes/SoCube.h>
#include <Inventor/nodes/SoSphere.h>
#include <Inventor/nodes/SoTransform.h>
#include <Inventor/nodes/SoUnits.h>

#include <Inventor/actions/SoLineHighlightRenderAction.h>

#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>

using namespace VirtualRobot;

using namespace DMP;

DMPMainWindow::DMPMainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::DMPMainWindow)
{
    ui->setupUi(this);

    calculateDMP();

    robotSep = new SoSeparator;
    robotSep->ref();

    trajSep = new SoSeparator;
    trajSep->ref();

    auto unit = new SoUnits;
    unit->units = SoUnits::MILLIMETERS;

    mainSep = new SoSeparator;
    mainSep->addChild(unit);
    mainSep->addChild(robotSep);
    mainSep->addChild(trajSep);

    auto visualization = mmm->getVisualization<CoinVisualization>();
    SoNode* visualisationNode = NULL;

    if (visualization)
    {
        visualisationNode = visualization->getCoinVisualization();
    }

    if (visualisationNode)
    {
        robotSep->addChild(visualisationNode);
    }


    viewer = new SoQtExaminerViewer(ui->centralwidget, "", TRUE, SoQtExaminerViewer::BUILD_POPUP);

    // setup
    viewer->setBackgroundColor(SbColor(1.0f, 1.0f, 1.0f));
    viewer->setAccumulationBuffer(true);
    viewer->setAntialiasing(true, 4);

    viewer->setGLRenderAction(new SoLineHighlightRenderAction);
    viewer->setTransparencyType(SoGLRenderAction::SORTED_OBJECT_BLEND);
    viewer->setFeedbackVisibility(true);
    viewer->setSceneGraph(mainSep);
    viewer->viewAll();
    viewer->setAntialiasing(true, 4);

    viewer->viewAll();


}

DMPMainWindow::~DMPMainWindow()
{
    robotSep->unref();
    delete ui;
}

int DMPMainWindow::main()
{
    SoQt::show(this);
    SoQt::mainLoop();
    return 0;
}


void DMPMainWindow::moveTCP(Eigen::Vector3f offset)
{
    Eigen::Vector3f newPosition = nodeSet->getTCP()->getPositionInRootFrame() + offset ;
    std::cout << "New position: " << newPosition << std::endl;
    VirtualRobot::ConstraintPtr posConstraint(new VirtualRobot::PositionConstraint(mmm, nodeSet, nodeSet->getTCP(), newPosition));
    Eigen::VectorXf jointConfig;
    nodeSet->getJointValues(jointConfig);
    VirtualRobot::ConstraintPtr referenceConfigConstraint(new VirtualRobot::ReferenceConfigurationConstraint(mmm, nodeSet, jointConfig));
    referenceConfigConstraint->setOptimizationFunctionFactor(0.01);


    // Instantiate solver and generate IK solution
    VirtualRobot::ConstrainedOptimizationIK ikSolver(mmm, nodeSet);
    ikSolver.addConstraint(posConstraint);
    ikSolver.addConstraint(referenceConfigConstraint);

    ikSolver.initialize();
    bool success = ikSolver.solve();
}

void DMPMainWindow::buildModel(MMM::MotionPtr motion)
{
    mmm = MMM::SimoxTools::buildModel(motion->getModel());
    std::vector<std::string> jointNames = motion->getJointNames();
    if (jointNames.size()>0)
    {
        std::string rnsName("MMMViewerRNS");
        if (mmm->hasRobotNodeSet(rnsName))
            mmm->deregisterRobotNodeSet(mmm->getRobotNodeSet(rnsName));
        nodeSet = VirtualRobot::RobotNodeSet::createRobotNodeSet(mmm, rnsName, jointNames, "", "RWy_joint", true);
    }
}

DMP::Vec<DMP::DMPState> DMPMainWindow::getStartConfiguration(const SampledTrajectoryV2& trajectory)
{
    Vec<DMPState> initialStates;
    for (int dim = 0; dim < trajectory.dim(); ++dim) {
        DMPState initialState;
        initialState.pos = trajectory.begin()->getPosition(dim);
        initialState.vel = trajectory.begin()->getDeriv(dim, 1);
        initialStates.push_back(initialState);
    }
    return initialStates;
}

void DMPMainWindow::calculateDMP()
{
    std::string path = MMMTOOLS_DATA_DIR;
    std::string MMMRelativePath = "/Model/Winter/";
    MMM::MotionReaderXML reader;
    path += MMMRelativePath + "punch_right01.xml";
    MMM::MotionPtr motion = reader.loadMotion(path);
    // get only a part of the motion
    motion = motion->getSegmentMotion(0, 194);

    // convert to DMP format
    SampledTrajectoryV2 trajectory = MMMConverter::fromMMMJoints(motion);
    // normalize length of trajectory to 1 second duration
//    trajectory = SampledTrajectoryV2::normalizeTimestamps(trajectory, 0, 1);


    // train the DMP
//    BasicDMP dmp;
    EndVeloForceFieldDMP dmp;
    dmp.learnFromTrajectory(trajectory);

    // Get start  configuration from original motion for DMP parameterization for all dimensions of the dmps (all joints)
    Vec<DMPState> initialStates = getStartConfiguration(trajectory);

    // Build model for inverse kinematics and visualization
    buildModel(motion);

    // set joint values to end configuration of the trajectory to calculate a new position relative to it
    mmm->setJointValues(nodeSet, motion->getMotionFrame(motion->getNumFrames()-1)->joint);

    // Move TCP in cartesian space with inverse kinematics
    moveTCP(Eigen::Vector3f(0, -500,0));  // position delta from current hand position
    auto jointValues = nodeSet->getJointValues();
    DVec goals;
    goals.assign(jointValues.begin(), jointValues.end());


    double speedFactor = 2; // timestamps need to be adapted accordingly
    // generate timestamps for which we want to calculate the trajectory points
    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0/speedFactor, 1.0 / (-0.5 + trajectory.getTimestamps().size()));

    // calculate the full, new trajectory from a new start to a new end point with a new duration
    SampledTrajectoryV2 newTraj = dmp.calculateTrajectory(timestamps, goals, initialStates, 1.0/speedFactor);

    // convert back to MMM Motion for storing in file and visualization
    MMM::MotionPtr newMotion = MMMConverter::toMMMJoints(newTraj, motion);
    MMM::XML::saveXML(std::string(MMMTOOLS_DATA_DIR) + MMMRelativePath + "newMotion.xml", newMotion->toXML());

    playMotion(newMotion);

}

void DMPMainWindow::playMotion(MMM::MotionPtr motion)
{
    this->motion = motion;
    startTimer(10);
}



void DMPMainWindow::drawTrajectoryPoint()
{
    auto it = trajPointSeparators.find(currentMotionFrame);
    if(it == trajPointSeparators.end())
    {
        SoSeparator* sep = new SoSeparator;
        SoMaterial* mat = new SoMaterial;
        mat->ambientColor.setValue(1,0,0);
        mat->diffuseColor.setValue(1,1,0);
        mat->transparency.setValue(0.7);
        sep->addChild(mat);

        SoTransform* tr = new SoTransform;
        Eigen::Vector3f position = nodeSet->getTCP()->getGlobalPose().block<3,1>(0,3);
        tr->translation.setValue(position[0], position[1], position[2]);
        sep->addChild(tr);

        SoCube* cube = new SoCube;
        cube->width = cube->height = cube->depth = 10;
        sep->addChild(cube);
        trajSep->addChild(sep);
        trajPointSeparators[currentMotionFrame] = sep;
    }
    else
    {
        SoSeparator* sep = it->second;
        auto transform = dynamic_cast<SoTransform*>(sep->getChild(1));
        Eigen::Vector3f position = nodeSet->getTCP()->getGlobalPose().block<3,1>(0,3);
        transform->translation.setValue(position[0], position[1], position[2]);
    }
}

void DMPMainWindow::timerEvent(QTimerEvent *)
{
    if(currentMotionFrame <  0)
    {
        currentMotionFrame = 0;
        forwardPlaying = true;
    }
    else if(currentMotionFrame >= motion->getNumFrames())
    {
        currentMotionFrame--;
        forwardPlaying = false;
    }


    MMM::MotionFramePtr frame = motion->getMotionFrame(currentMotionFrame);

    mmm->setJointValues(nodeSet, frame->joint);
    mmm->setGlobalPose(frame->getRootPose());

    drawTrajectoryPoint();

    if(forwardPlaying)
        currentMotionFrame++;
    else
        currentMotionFrame--;
}
