/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ArmarX
 * @author     Mirko Waechter( mirko.waechter at kit dot edu)
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#ifndef DMPMAINWINDOW_H
#define DMPMAINWINDOW_H

#include <QMainWindow>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Robot.h>
#include <VirtualRobot/VirtualRobotException.h>
#include <VirtualRobot/Nodes/RobotNode.h>
#include <VirtualRobot/XML/SceneIO.h>
#include <VirtualRobot/Visualization/VisualizationFactory.h>
#include <VirtualRobot/Visualization/CoinVisualization/CoinVisualization.h>
#include <VirtualRobot/Obstacle.h>
#include <string.h>
#include <QtCore/QtGlobal>
#include <QtGui/QtGui>
#include <QtCore/QtCore>

#include <Inventor/sensors/SoTimerSensor.h>
#include <Inventor/nodes/SoEventCallback.h>
#include <Inventor/Qt/viewers/SoQtExaminerViewer.h>
#include <Inventor/Qt/SoQt.h>
#include <Inventor/nodes/SoSeparator.h>
#include <MMM/Motion/Motion.h>
#include <general/vec.h>
#include <representation/systemstatus.h>
#include <representation/trajectory.h>


namespace Ui {
class DMPMainWindow;
}

class DMPMainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit DMPMainWindow(QWidget *parent = 0);
    ~DMPMainWindow();
    int main();

    void calculateDMP();

    void playMotion(MMM::MotionPtr motion);

    void drawTrajectoryPoint();

    void moveTCP(Eigen::Vector3f offset);
    void buildModel(MMM::MotionPtr motion);
    
    DMP::Vec<DMP::DMPState> getStartConfiguration(const DMP::SampledTrajectoryV2 &trajectory);
    
private:
    Ui::DMPMainWindow *ui;
    VirtualRobot::RobotPtr mmm;
    VirtualRobot::RobotNodeSetPtr nodeSet;

    SoSeparator* mainSep;
    SoSeparator* robotSep;
    SoSeparator* trajSep;
    std::map<int, SoSeparator*> trajPointSeparators;
    SoQtExaminerViewer* viewer; /*!< Viewer to display the 3D model of the robot and the environment. */
    MMM::MotionPtr motion;
    int currentMotionFrame = 0;
    bool forwardPlaying = true;


    // QObject interface
protected:
    void timerEvent(QTimerEvent *);
};

#endif // DMPMAINWINDOW_H
