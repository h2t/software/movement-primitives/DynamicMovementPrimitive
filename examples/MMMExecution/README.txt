# Task Description:

Your task is to train a Dynamic Movement Primitive (DMP) with a motion from the Human Motion Database
and execute it again with different parameters, e.g. a different grasping pose for a grasping motion.

There is already a stub application in DMP/examples/MMMExecution, which setups a 3D Window,
visualizes a robot and capable of playing a motion.

You should complete the function DMPMainWindow::calculateDMP() in DMP/examples/MMMExecution/DMPMainWindow.cpp.

-------------------

## Stage 1: Learning and Executing a Dynamic Movement Primitive

1. Select a motion from the Human Motion Database https://motion-database.humanoids.kit.edu/
  * The Motion should be a targeted motion like a grasp, a punch or a step in the MMM XML format.
  * Download it to MMMTools/data/Model/Winter/.
  * You can inspect the Motion with the MMMViewer application in MMMTools/build/bin.

2. Load the motion in the program
  * The path is specified in the DMPMainWindow::calculateDMP() function. Adjust this path.
  * You probably need to extract a segment from a motion since the demonstrated motion does
    not end where you need the end (e.g. the grasping pose). See MMM::Motion::getSegmentMotion().

3. Train the DMP with the motion
  * You need to train a dmp instance with the example motion
  * Some of the DMPS variations are:
      * DMP::BasicDMP
      * DMP::ForceFieldDMP
      * DMP::SimpleEndVeloDMP
      * DMP::EndVeloForceFieldDMP

4. Reproduce the trajectory with the same start and end configuration to see if the learning worked

5. Convert the motion back to a MMM::Motion

6. Play the new motion
  * Pass the motion to the DMPMainWindow::playMotion() function

## Stage 2: Reparameterization of a DMP

1. Move the tool center point (TCP) to a new position
  * The function DMPMainWindow::moveTCP() moves the TCP relative to the current position of the TCP in robot base coordinates
    * This function uses inverse kinematics to calculate the joint values needed to reach the desired Cartesian TCP pose
  * The application RobotViewer can be used to inspect a robot model
  * The joints of the mmm model are updated after calling moveTCP() and be queried for reparameterizing the goal of a dmp


## Stage 3: Sequencing of DMPs

1. Select several motions and sequence them to a long, seamless motion

Hints
=====

### The main classes you need are:
* DMPMainWindow
* MMM::Motion
* MMMConverter
* DMP::BasicDMP
* DMP::ForceFieldDMP
* DMP::SimpleEndVeloDMP
* DMP::EndVeloForceFieldDMP
* DMP::SampledTrajectoryV2

### Needed helper functions:
* SampledTrajectoryV2::generateTimestamps(...)
* MMMConverter::fromMMMJoints(motion)
* MMMConverter::toMMMJoints(newTraj, motion)
* MMM::XML::saveXML(...)
* DMPMainWindow::moveTCP(vector)
* MMMMotion::getSegmentMotion(start, end)
* DMPMainWindow::playMotion(motion)
* DMPMainWindow::getStartConfiguration(trajectory)

