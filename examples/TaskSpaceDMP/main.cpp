/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/taskspacedmp.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <dmp/representation/dmpfactory.h>

#include <sys/time.h>

#include <iostream>

using namespace DMP;

DVec evaluateDifference(const SampledTrajectoryV2& traj1, const SampledTrajectoryV2 traj2)
{
    SampledTrajectoryV2 t1 = SampledTrajectoryV2::normalizeTimestamps(traj1,0,1);
    SampledTrajectoryV2 t2 = SampledTrajectoryV2::normalizeTimestamps(traj2,0,1);

    typename SampledTrajectoryV2::ordered_view::const_iterator itTraj1 = t1.begin();
    typename SampledTrajectoryV2::ordered_view::const_iterator itTraj2 = t2.begin();

    double posDiffSum = 0;
    double oriDiffSum = 0;
    double count = 0;
    for(;itTraj1 != t1.end() && itTraj2 != t2.end(); ++itTraj1, ++itTraj2)
    {
        double posDiff = 0;

        for(unsigned int i = 0; i < 3; ++i)
        {
            double val1 = itTraj1->getPosition(i);
            double val2 = itTraj2->getPosition(i);
            posDiff += (val1 - val2) * (val1 - val2);
        }
        posDiffSum += sqrt(posDiff);

        Eigen::Quaterniond q1(itTraj1->getPosition(3),
                              itTraj1->getPosition(4),
                              itTraj1->getPosition(5),
                              itTraj1->getPosition(6));
        Eigen::Quaterniond q2(itTraj2->getPosition(3),
                              itTraj2->getPosition(4),
                              itTraj2->getPosition(5),
                              itTraj2->getPosition(6));
        Eigen::Quaterniond dq = q1 * q2.conjugate();
        Eigen::AngleAxisd diffangle(dq);

        oriDiffSum += diffangle.angle();
        count = count + 1;
    }

    posDiffSum /= count;
    oriDiffSum /= count;

    DVec res;
    res.push_back(posDiffSum);
    res.push_back(oriDiffSum);

    return res;

}


int main(int argc, char* argv[])
{
    int res = KILL_ALL_PLOT_WINDOWS;
    if(res)
    {
        throw std::logic_error{"Killing all plot windows returned " + std::to_string(res)};
    }
    res = system("mkdir plots");
    if(res)
    {
        throw std::logic_error{"Command 'mkdir plots' returned " + std::to_string(res)};
    }

    // Load the trajectory
    SampledTrajectoryV2 traj;
    std::string path = DATA_DIR;
    path += "/ExampleTrajectories/pickplace7d.csv";
    traj.readFromCSVFile(path);
//    traj.removeDimension(1);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);

    // train the DMP
    DMPFactory dmpfactory;
    DMPInterfacePtr dmp = dmpfactory.getDMP("TaskSpaceDMP",100);

    struct timeval start, end;
    gettimeofday(&start, nullptr);

    dmp->learnFromTrajectory(traj);

    gettimeofday(&end, nullptr);
    double delta = ((end.tv_sec  - start.tv_sec) * 1000000u +
                    end.tv_usec - start.tv_usec) / 1.e6;
    std::cout << "time for learning: " << delta << std::endl;

    std::stringstream sts;
    boost::archive::binary_oarchive ar(sts);
    ar << boost::serialization::make_nvp("dmp",dmp);

    DMPInterfacePtr dmp2;
    boost::archive::binary_iarchive ari(sts);
    ari >> boost::serialization::make_nvp("dmp",dmp2);
    // set initial states
    DVec2d initialState;
    DVec goal;

    // set position initial states;

    for(unsigned int i = 0; i < 3 ; ++i)
    {
        DVec state;
        state.resize(2);
        state[0] = traj.begin()->getPosition(TASKSPACEDMP_POSSTARTDIM + i);
        state[1] = traj.begin()->getDeriv(TASKSPACEDMP_POSSTARTDIM + i,1);
        initialState.push_back(state);

        goal.push_back(traj.rbegin()->getPosition(TASKSPACEDMP_POSSTARTDIM+i));
    }


    // set orientation initial states
    double qw = traj.begin()->getPosition(TASKSPACEDMP_ORISTARTDIM);
    double qx = traj.begin()->getPosition(TASKSPACEDMP_ORISTARTDIM+1);
    double qy = traj.begin()->getPosition(TASKSPACEDMP_ORISTARTDIM+2);
    double qz = traj.begin()->getPosition(TASKSPACEDMP_ORISTARTDIM+3);

    typename SampledTrajectoryV2::ordered_view::const_iterator itqtraj = traj.begin();
    itqtraj++;

    Eigen::Quaterniond startq(qw,qx,qy,qz);
    Eigen::Quaterniond nextq(itqtraj->getPosition(TASKSPACEDMP_ORISTARTDIM),
                             itqtraj->getPosition(TASKSPACEDMP_ORISTARTDIM+1),
                             itqtraj->getPosition(TASKSPACEDMP_ORISTARTDIM+2),
                             itqtraj->getPosition(TASKSPACEDMP_ORISTARTDIM+3));

    double dt = itqtraj->getTimestamp() - traj.begin()->getTimestamp();
    Eigen::Quaterniond dq = (nextq - startq) / dt;
    Eigen::Quaterniond wq = 2 * dq * nextq.conjugate();

    DVec oriState;
    oriState.resize(2);

    bool isSetDifferentInitialState = false;
    if(isSetDifferentInitialState)
    {
        Eigen::Quaterniond sq(5,1,1,1);
        sq = sq/ sq.norm();

        std::cout << sq;
        std::cout << sq.norm();

        oriState[0] = sq.w();
        oriState[1] = 0;
        initialState.push_back(oriState);

        oriState[0] = sq.x();
        oriState[1] = 0.01;
        initialState.push_back(oriState);

        oriState[0] = sq.y();
        oriState[1] = 0.01;
        initialState.push_back(oriState);

        oriState[0] = sq.z();
        oriState[1] = 0.01;
        initialState.push_back(oriState);

    }
    else
    {
        oriState[0] = qw;
        oriState[1] = wq.w();
        initialState.push_back(oriState);

        oriState[0] = qx;
        oriState[1] = wq.x();
        initialState.push_back(oriState);

        oriState[0] = qy;
        oriState[1] = wq.y();
        initialState.push_back(oriState);

        oriState[0] = qz;
        oriState[1] = wq.z();
        initialState.push_back(oriState);
    }


    // set orientation goal

    for(unsigned int i = 0; i < 4; ++i)
    {
        goal.push_back(traj.rbegin()->getPosition(TASKSPACEDMP_ORISTARTDIM+i));
    }

    std::cout << "goal: " << goal << std::endl;

    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj.getTimestamps().size()));


    SampledTrajectoryV2 newTraj = dmp2->calculateTrajectory(timestamps, goal, initialState, 1.0, 1);


    DVec diffres = evaluateDifference(traj,newTraj);

    std::cout << "res: " << diffres[0] << " " << diffres[1] << std::endl;

//    traj.plotAllDimensions(0,"plots/traj_%d.gp");
//    newTraj.plotAllDimensions(0,"plots/newTraj_%d.gp");

    Vec<DVec> plots;
    plots.push_back(timestamps);
    for(unsigned int i = 3; i < 7; ++i)
    {
        plots.push_back(traj.getDimensionData(i,0));
        plots.push_back(newTraj.getDimensionData(i,0));
    }
    plotTogether(plots, "plots/trajComparison_%d.gp");



//    std::stringstream sts;
//    boost::archive::binary_oarchive ar(sts);
////    boost::archive::text_oarchive ar(ofs);
//    ar << boost::serialization::make_nvp("dmp",dmp);

}


