/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/forcefielddmp.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <dmp/representation/dmpfactory.h>

//#include <dmp/costfunction/manipulabilitycostfcn.h>

#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Import/SimoxXMLFactory.h>

#include <sys/time.h>

#include <iostream>

using namespace DMP;


int main(int argc, char* argv[])
{
    int res = KILL_ALL_PLOT_WINDOWS;
    if(res)
    {
        throw std::logic_error{"Killing all plot windows returned " + std::to_string(res)};
    }
    res = system("mkdir plots");
    if(res)
    {
        throw std::logic_error{"Command 'mkdir plots' returned " + std::to_string(res)};
    }
    Vec<DVec> plots;

    // Load the trajectory
    SampledTrajectoryV2 traj;
    std::string path = DATA_DIR;
    path += "/ExampleTrajectories/pickplace_tmp.csv";
    traj.readFromCSVFile(path);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);

    DVec2d initialState;
    DVec goals;
    for(size_t i = 0; i < traj.dim(); i++)
    {
        DVec state;
        state.push_back(traj.begin()->getPosition(i));
        state.push_back(traj.begin()->getDeriv(i,1));
        initialState.push_back(state);
        goals.push_back(traj.rbegin()->getPosition(i));
    }

    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj.getTimestamps().size()));

    CostFunctionPtr cfn(new ManipulabilityCostFunction(100));

    VirtualRobot::SimoxXMLFactory sxf;
    VirtualRobot::RobotPtr robot =
            sxf.loadFromFile("/home/zhou/home/armarx/RobotAPI/data/RobotAPI/robots/Armar3/ArmarIII.xml");

    VirtualRobot::RobotNodeSetPtr kc = robot->getRobotNodeSet("RightArm");

    cfn->setRobot(robot);
    cfn->setKinematicChain(kc);

    double T = timestamps[timestamps.size() - 1];

    double cost = cfn->getCost(timestamps, goals, T, traj);

    std::cout << "cost: " << cost << std::endl;

}


