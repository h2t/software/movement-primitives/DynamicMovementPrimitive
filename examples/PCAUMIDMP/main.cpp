#include <fstream>

#include <boost/unordered_map.hpp>

#include <sys/time.h>

#include <iostream>

#include "dmp/representation/dmp/umitsmp.h"
#include "dmp/representation/dmp/umidmp.h"
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/text_iarchive.hpp>
using namespace DMP;


int main(int argc, char* argv[])
{
    int res = KILL_ALL_PLOT_WINDOWS;
    res = system("mkdir plots");
    Vec<DVec> plots;

    // Load the trajectory
    std::string path0 = DATA_DIR;


    std::vector<std::string> paths;
    paths.push_back(path0+ "/PCATest/approaching_obj_1.csv");
    paths.push_back(path0+ "/PCATest/approaching_obj_2.csv");
    paths.push_back(path0+ "/PCATest/approaching_obj_3.csv");
    paths.push_back(path0+ "/PCATest/approaching_obj_4.csv");
    paths.push_back(path0+ "/PCATest/approaching_obj_5.csv");
    Vec<SampledTrajectoryV2 > trajs;

    int dim = 2;
    for(size_t i = 0; i < paths.size(); ++i)
    {
        SampledTrajectoryV2 traj0;
        traj0.readFromCSVFile(paths[i]);
        traj0 = SampledTrajectoryV2::normalizeTimestamps(traj0, 0, 1);
        traj0.gaussianFilter(0.1);
        trajs.push_back(traj0);
    }

    int kernel_num = 100;
    // train the DMP
    UMIDMPPtr dmp(new UMIDMP(kernel_num));
    Vec<DMPState> initialState;
    DVec goals;

    SampledTrajectoryV2 traj0 = trajs[3];
    for(size_t i = 0; i < dim; i++)
    {
        DMPState state;
        state.pos = traj0.begin()->getPosition(i) ;
        state.vel = 0;
        initialState.push_back(state);
        goals.push_back(traj0.rbegin()->getPosition(i));
    }

    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + trajs[0].getTimestamps().size()));

    dmp->learnFromTrajectories(trajs);
    dmp->prepareExecution(goals, initialState, 1, 1);

    DVec ratios(std::vector<double>{0.0, 0.0, 0, 0.5, 0.5});
    dmp->styleParas = dmp->getStyleParasWithRatio(ratios);
    std::cout << dmp->styleParas << std::endl;
    SampledTrajectoryV2 newTraj1 = dmp->calculateTrajectory(timestamps, goals, initialState, 1.0, 1.0);

    // test serialization
    std::stringstream ss;
    boost::archive::text_oarchive oa{ss};
    oa << dmp.get();
    UMIDMP* dmpPtr;
    boost::archive::text_iarchive ia{ss};
    ia >> dmpPtr;
    UMIDMPPtr dmp2(dmpPtr);
    dmp2->prepareExecution(goals, initialState, 1, 1);
    SampledTrajectoryV2 newTraj2 = dmp2->calculateTrajectory(timestamps, goals, initialState, 1.0, 1.0);


//    // test weights
//    UMIDMP dmp3(kernel_num);
//    std::cout << dmp->getWeights();
//    dmp3.setupWithWeights(dmp->getWeightsVec2d());
//    dmp3.prepareExecution(goals, initialState, 1, 1);
//    SampledTrajectoryV2 newTraj3 = dmp3.calculateTrajectory(timestamps, goals, initialState, 1.0, 1.0);


    trajs.push_back(newTraj1);
    trajs.push_back(newTraj2);
    plots.clear();

    Vec<std::string> titles;
    for(size_t i = 0; i < trajs.size(); ++i)
    {
        plots.push_back(trajs[i].getDimensionData(0, 0));
        plots.push_back(trajs[i].getDimensionData(1, 0));

        titles.push_back("traj_"+std::to_string(i));
    }

    plot2DTogether(plots, titles, "plots/trajComparison_%d.gp");

}


