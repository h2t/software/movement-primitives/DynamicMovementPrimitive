/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/forcefielddmp.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <dmp/representation/dmpfactory.h>

#include <sys/time.h>

#include <iostream>

using namespace DMP;


int main(int argc, char* argv[])
{
    int res = KILL_ALL_PLOT_WINDOWS;
    /*if(res)
    {
        throw std::logic_error{"Killing all plot windows returned " + std::to_string(res)};
    }*/
    res = system("mkdir plots");
    /*if(res)
    {
        throw std::logic_error{"Command 'mkdir plots' returned " + std::to_string(res)};
    }*/
    Vec<DVec> plots;

    // Load the trajectory
    SampledTrajectoryV2 traj;
    std::string path = DATA_DIR;
    path += "/ExampleTrajectories/sampletraj.csv";
//    std::string path = "sampletraj.csv";
    traj.readFromCSVFile(path);

//    traj.removeDimension(2);
//    traj.removeDimension(1);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);

    // train the DMP
    DMPFactory dmpfactory;
    DMPInterfacePtr dmp = dmpfactory.getDMP("BasicDMP",500);
    std::cout << "Kernel Size: " << dmp->getKernelSize() << std::endl;

    DVec2d initialState;
    DVec goals;
    for(size_t i = 0; i < traj.dim(); i++)
    {
        DVec state;
        state.push_back(traj.begin()->getPosition(i));
        state.push_back(traj.begin()->getDeriv(i,1));
//        state.push_back(0);
        initialState.push_back(state);
        /*
        double goal = 0;
        goals.push_back(goal);*/

        goals.push_back(traj.rbegin()->getPosition(i));
//        goals.push_back(0);
    }

    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj.getTimestamps().size()));


    struct timeval start, end;
    gettimeofday(&start, NULL);

    dmp->learnFromTrajectory(traj);

    gettimeofday(&end, NULL);
    double delta = ((end.tv_sec  - start.tv_sec) * 1000000u +
                    end.tv_usec - start.tv_usec) / 1.e6;
    std::cout << "time for learning: " << delta << std::endl;

    SampledTrajectoryV2 newTraj = dmp->calculateTrajectory(timestamps, goals, initialState, 1.0, 1);


//    traj.plotAllDimensions(0);
//    newTraj.plotAllDimensions(0);
    // Reproduction

    //    double maxError;
    //    double error = dmp.evaluateReproduction(1,maxError);
    //    std::cout << "Mean relative reproduction error: " << error << " max error: " << maxError << std::endl;

    // Configuration
//    DMPState initialState;
//    initialState.pos = traj.begin()->getPosition(0);
//    initialState.vel = traj.begin()->getDeriv(0, 1);
//    double goal = traj.rbegin()->getPosition(0);
//    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj.getTimestamps().size()));

//    SampledTrajectoryV2 newTraj = dmp.calculateTrajectory(timestamps, DVec(1, goal), Vec<DMPState>(1, initialState), 1.0, 1);

//    Vec<DMPState> nextState = dmp.calculateTrajectoryPoint(timestamps.at(0), DVec(1,goal), timestamps.at(1), Vec<DMPState>(1,initialState), DVec(1,1.0), 1);

//    std::cout << nextState << std::endl;
    // Plotting
//    newTraj.plot(0, 0, "plots/newTraj");
//    newTraj.plot(0, 1, "plots/newTraj_vel");
//    plots.clear();
//    plots.push_back(timestamps);
//    plots.push_back(traj.getDimensionData(0, 0));
//    plots.push_back(newTraj.getDimensionData(0, 0));
//    plotTogether(plots, "plots/trajComparison_%d.gp");

    for(size_t i = 0; i < traj.dim(); i++)
    {
        plots.clear();
        plots.push_back(timestamps);
        plots.push_back(traj.getDimensionData(i, 0));
        plots.push_back(newTraj.getDimensionData(i, 0));
        plotTogether(plots, "plots/trajComparison_%d.gp");
    }

//    double mxErrorOut;
//    dmp->evaluateReproduction(1.0,mxErrorOut,true);
//    plots.clear();
//    plots.push_back(timestamps);
//    plots.push_back(dmp.getTrainingTraj(0).getDimensionData(0, 1));
//    plots.push_back(newTraj.getDimensionData(0, 1));
//    plotTogether(plots, "plots/trajComparison_vel_%d.gp");
//    plots.clear();
//    plots.push_back(timestamps);
//    plots.push_back(dmp.getTrainingTraj(0).getDimensionData(0, 2));
//    plots.push_back(newTraj.getDimensionData(0, 2));
//    plotTogether(plots, "plots/trajComparison_acc_%d.gp");

//    std::stringstream sts;
////    std::stringstream ofs;
//    boost::archive::binary_oarchive ar(sts);
////    boost::archive::text_oarchive ar(ofs);
//    ar << boost::serialization::make_nvp("dmp",dmp);

    DMPInterface* dmpPtr = dmp.get();
    std::ofstream ofs("basicDMP.xml");
    boost::archive::xml_oarchive ar(ofs);
    ar << boost::serialization::make_nvp("dmp",dmpPtr);


}


