#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/forcefielddmp.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <dmp/representation/dmpfactory.h>

#include <sys/time.h>

#include <iostream>
#include "dmp/general/simoxhelpers.h"

using namespace DMP;


int main(int argc, char* argv[])
{
    std::string base_path = DATA_DIR;
    std::string globalFile = base_path + "/BimanualHolding/1000g_01_left.csv";
    std::string refFile =  base_path + "/BimanualHolding/1000g_01_right.csv";

    SampledTrajectoryV2 globalTraj;
    globalTraj.readFromCSVFile(globalFile);

    SampledTrajectoryV2 refTraj;
    refTraj.readFromCSVFile(refFile);

    SampledTrajectoryV2 traj = RobotHelpers::getRelativeTrajFromFiles(globalFile, refFile);

    Vec<SampledTrajectoryV2> trajs;
    trajs.push_back(traj);
    std::vector<std::string> titles;
    titles.push_back("relativeTraj");

    std::vector<int> view = {0, 0, 1, 1};
    RobotHelpers::plot3D(trajs, titles, view);
}


