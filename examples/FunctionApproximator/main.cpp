

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <dmp/representation/dmp/multiphasedmp.h>
#include <boost/token_iterator.hpp>
#include <dmp/functionapproximation/radialbasisfunctioninterpolator.h>
#include <dmp/functionapproximation/receptivefieldweightedregression.h>
#include <sys/time.h>
#include <dmp/functionapproximation/lwprwrapper.h>
#include <dmp/functionapproximation/locallyweightedlearning.h>
#include <boost/type_traits.hpp>

using namespace DMP;


DVec2d readFromCSVFile(const string & path)
{
    DVec2d uValues;

    using Tokenizer = boost::tokenizer<boost::escaped_list_separator<char> >;

    string line;
     vector< string > stringVec;
    ifstream uValf(path.c_str());
    if (uValf.is_open())
    {
        while (getline(uValf,line))
        {
            Tokenizer tok(line);
            stringVec.assign(tok.begin(), tok.end());

            DVec uValue;
            uValue  << fromString<double>(stringVec[0]) , fromString<double>(stringVec[1]);
            uValues.push_back(uValue);

        }
        uValf.close();
    }

    return uValues;
}

//#define uInv(x) -log(x)*6.90776
//#define uInv(x) x

void approximator1DTest(FunctionApproximationInterfacePtr approximator, const std::string& outputFilename = "testresult.csv")
{
    std::string path;
    path += "/home/zhou/dmp/build/bin/basicDMP_perturbation.csv";
    DVec2d data = readFromCSVFile(path);

    DVec values;

    DVec2d uValues;
    for(size_t i = 0; i < data.size(); ++i)
    {
        DVec cx;
        cx.push_back(data.at(i).at(0));
        uValues.push_back(cx);
        values.push_back(data.at(i).at(1));
    }

    double maxVal = max(values);

    for(size_t i = 0; i < values.size(); ++i)
    {
        values[i] /= 10000;
    }




//    RBFInterpolator<GaussRadialBasisFunction> approximator(kernelSizes, widthFactors);


    struct timeval start, end;
    gettimeofday(&start, nullptr);

    DVec2d vals;
    vals.push_back(values);

//    approximator->learn(uValues,vals);

    std::cout << "usize: " << uValues.size() << std::endl;

    DVec xmin = uValues[0];
    DVec xmax = uValues[uValues.size()-1];
    approximator->createKernels(xmin, xmax);
    for(size_t i = 0; i < uValues.size(); ++i)
    {
        DVec x = uValues[i];
        double y = values[i];
        approximator->ilearn(x,y);
    }

//    for(size_t i = 0; i < uValues.size(); ++i)
//    {
//        DVec x = uValues[i];
//        double y = values[i] + 10;
//        approximator->ilearn(x,y);
//    }


    gettimeofday(&end, nullptr);
    double delta = ((end.tv_sec  - start.tv_sec) * 1000000u +
                    end.tv_usec - start.tv_usec) / 1.e6;

    std::cout << "time: " << delta << std::endl;

    double errsum = 0;
    ofstream res(outputFilename);
    for(size_t i = 0; i < uValues.size(); ++i)
    {
        DVec uValue = uValues[i];
        DVec val = (*approximator)(uValue);
        res << uValue[0] << "," << values[i] << "," << val[0] << std::endl;
        std::cout << uValue[0] << "," << values[i]<< "," << val[0] << std::endl;
        errsum += (val - values[i] - 10) * (val - values[i] - 10);
    }

    std::cout << "MSE: " << sqrt(errsum / uValues.size()) << std::endl;
    res.close();
}

void approximator2DTest()
{
    DVec2d uValues;
    std::string path = DATA_DIR;
    path += "/ExampleTrajectories/uValues_2.csv";

    using Tokenizer = boost::tokenizer<boost::escaped_list_separator<char> >;

    string line;
     vector< string > stringVec;
    ifstream uValf(path.c_str());
    if (uValf.is_open())
    {
        while (getline(uValf,line))
        {
            Tokenizer tok(line);
            stringVec.assign(tok.begin(), tok.end());

            DVec uValue;
            uValue  << fromString<double>(stringVec[0]) , fromString<double>(stringVec[1]);
            uValues.push_back(uValue);

        }
        uValf.close();
    }


    DVec values;
    path = DATA_DIR;
    path += "/ExampleTrajectories/values_2.csv";

    ifstream valf(path.c_str());
    if (valf.is_open())
    {
        while (getline(valf,line))
        {
            double value = fromString<double>(line);
            values.push_back(value);
        }
        valf.close();
    }

    double maxVal = max(values);
    double minVal = min(values);
//    for(size_t i = 0; i < values.size(); ++i)
//    {
//        values[i] /= 1000;
//    }


    DVec widthFactors;
    widthFactors << 0.01,0.01;


    RBFInterpolator<GaussRadialBasisFunction> approximator(2, widthFactors);

    //    approximator.createKernels(xmin,xmax, 100);

    approximator.setForgettingFactor(0.5);
//    LWPRWrapper approximator(2,1,1,0.99);
//    RFWR approximator;

    struct timeval start, end;
    gettimeofday(&start, nullptr);

    for(unsigned int i = 0; i < uValues.size(); i++)
    {
//        std::cout << "uValues: " << uValues[i][0] << " " << uValues[i][1];
//        std::cout << " values: " << values[i] << std::endl;
        approximator.ilearn(uValues[i], values[i]);
    }

    gettimeofday(&end, nullptr);
    double delta = ((end.tv_sec  - start.tv_sec) * 1000000u +
                    end.tv_usec - start.tv_usec) / 1.e6;

    std::cout << "learning time: " << delta << std::endl;

    double errsum = 0;

    std::cout << "sub prediction result: ... " << std::endl;


    ofstream subres("subtestresult_forget_rbfi.csv");

    for(size_t i = 0; i < uValues.size(); ++i)
    {
        DVec uValue = uValues[i];
        DVec ypred = approximator(uValue);
        double yreal = values[i];

        subres << uValue[0] << "," << uValue[1] << "," << yreal << "," << ypred[0] << std::endl;

        std::cout << uValue[0] << "," << uValue[1] << ": " << yreal << "," << ypred[0] << std::endl;
        errsum += (yreal - ypred[0]) * (yreal - ypred[0]);

    }
    subres.close();
    std::cout << "subMSE: " << errsum / uValues.size() << std::endl;
    std::cout << "===========================================" << std::endl;

//    errsum = 0;

//    for(size_t i = 0; i < 100; ++i)
//    {
//        double x = rand() % 100;
//        double y = rand() % 100;
//        double z = sin(x+y);


//        DVec uValue;
//        uValue.push_back(x);
//        uValue.push_back(y);
//        DVec val = approximator(uValue);
//        res << x << "," << y << "," << val[0] << std::endl;
//        std::cout << x << "," << y << ": " << z << "," << val[0] << std::endl;

//        errsum += (val - z) * (val - z);
//    }

//    std::cout << "MSE: " << errsum / 100 << std::endl;
//
}

int main(int argc, char* argv[])
{
    // First: Locally weight regression with Gauss kernels and regularly arranged kernels' centers;

//    std::cout << "Test 1: Locally weighted regression ... " << std::endl;
//    FunctionApproximationInterfacePtr approximater(new LWR<GaussKernel>(100));
//    approximator1DTest(approximater,"lwr_result.csv");

//    // Second: 1D RBF Interpolator with Gaussian kernels
//    std::cout << "Test 2: 1D RBF Interpolator ... " << std::endl;
//    DVec widthFactors;
//    widthFactors << 0.5;
//    FunctionApproximationInterfacePtr approximator2(new RBFInterpolator<GaussRadialBasisFunction>(1, widthFactors));
//    approximator1DTest(approximator2,"rbfi_result.csv");

    // Third: 2D RBF Interpolator
//    std::cout << "Test 3: 2D RBF Interpolator ... " << std::endl;
//    approximator2DTest();

    // Fourth: 1D RFWR
//    FunctionApproximationInterfacePtr approximator3(new RFWR());
//    approximator1DTest(approximator3,"aaa.csv");

    // Third: 2D RFWR
    approximator2DTest();
}


