/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/quaterniondmp.h>
#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <Eigen/Core>
#include <Eigen/Geometry>


using namespace Eigen;
using namespace DMP;


int main(int argc, char* argv[])
{
    int res = KILL_ALL_PLOT_WINDOWS;
//    if(res)
//    {
//        throw std::logic_error{"Killing all plot windows returned " + std::to_string(res)};
//    }
    res = system("mkdir plots");
    if(res)
    {
        throw std::logic_error{"Command 'mkdir plots' returned " + std::to_string(res)};
    }
    Vec<DVec> plots;

    // Load the trajectory
    DMP::SampledTrajectoryV2 qtraj;
    std::string path = DATA_DIR;
    path += "/ExampleTrajectories/quaterniontest.csv";


    qtraj.readFromCSVFile(path);
    qtraj = SampledTrajectoryV2::normalizeTimestamps(qtraj, 0, 1);

    //test traj to qtraj transformation
//    SampledTrajectoryV2 qtraj = traj.traj2qtraj();
//    traj.plotAllDimensions(0, "/home/zhou/dmp/data/results/angletrajectory");
//    qtraj.plotAllDimensions(0, "/home/zhou/dmp/data/results/quattrajectory");


    // test Quaternion algebra using Eigen

    // test QuaternionDMP
    DMP::QuaternionDMP qdmp(500);
    qdmp.learnFromTrajectory(qtraj);

//    SampledTrajectoryV2 traj = qtraj.qtraj2traj();


    Vec<DMPState> initialState;


    bool isSetDifferentStartStates = false;
    // set different startPositions
    if(isSetDifferentStartStates)
    {
        Eigen::Quaterniond sq(5,1,1,1);
        sq = sq/ sq.norm();

        std::cout << sq;
        std::cout << sq.norm();

        initialState.push_back(DMPState(sq.w(), 0));
        initialState.push_back(DMPState(sq.x(), 0.01));
        initialState.push_back(DMPState(sq.y(), 0.01));
        initialState.push_back(DMPState(sq.z(), 0.01));
    }
    else
    {
        double qw = qtraj.begin()->getPosition(0);
        double qx = qtraj.begin()->getPosition(1);
        double qy = qtraj.begin()->getPosition(2);
        double qz = qtraj.begin()->getPosition(3);

        typename SampledTrajectoryV2::ordered_view::const_iterator itqtraj = qtraj.begin();
        itqtraj++;

        Eigen::Quaterniond startq(qw,qx,qy,qz);
        Eigen::Quaterniond nextq(itqtraj->getPosition(0),
                                 itqtraj->getPosition(1),
                                 itqtraj->getPosition(2),
                                 itqtraj->getPosition(3));

        double dt = itqtraj->getTimestamp() - qtraj.begin()->getTimestamp();
        Eigen::Quaterniond dq = (nextq - startq) / dt;
        Eigen::Quaterniond wq = 2 * dq * nextq.conjugate();

        std::cout << "start quaternion: " <<  startq;
        std::cout << "start angular velocity: " << wq;


        initialState.push_back(DMPState(qw, wq.w()));
        initialState.push_back(DMPState(qx, wq.x()));
        initialState.push_back(DMPState(qy, wq.y()));
        initialState.push_back(DMPState(qz, wq.z()));
    }


    DVec goal;

    goal.push_back(qtraj.rbegin()->getPosition(0));
    goal.push_back(qtraj.rbegin()->getPosition(1));
    goal.push_back(qtraj.rbegin()->getPosition(2));
    goal.push_back(qtraj.rbegin()->getPosition(3));

    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + qtraj.getTimestamps().size()));


    SampledTrajectoryV2 newqTraj = qdmp.calculateTrajectory(timestamps, goal, initialState, 1.0, 1);

    SampledTrajectoryV2 newaTraj = newqTraj.qtraj2traj();

    Eigen::Quaterniond newgoal, oldgoal;

    oldgoal.w() = goal[0];
    oldgoal.x() = goal[1];
    oldgoal.y() = goal[2];
    oldgoal.z() = goal[3];

    newgoal.w() = newqTraj.rbegin()->getPosition(0);
    newgoal.x() = newqTraj.rbegin()->getPosition(1);
    newgoal.y() = newqTraj.rbegin()->getPosition(2);
    newgoal.z() = newqTraj.rbegin()->getPosition(3);

    Eigen::Quaterniond diffgoal = oldgoal * newgoal.conjugate();
    Eigen::AngleAxisd diffangle(diffgoal);
    std::cout << "different of the goals: " << diffangle.angle() << std::endl;


    std::string dir = DATA_DIR;
    std::string curpath = dir + "/results/oldQtrajectory";
    qtraj.plotAllDimensions(0, curpath);

    curpath = dir + "/results/newQtrajectory";
    newqTraj.plotAllDimensions(0, curpath);

//    curpath = dir + "/results/oldAtrajectory";
//    traj.plotAllDimensions(0, curpath);


    curpath = dir + "/results/newAtrajectory";
    newaTraj.plotAllDimensions(0, curpath);

    curpath = dir + "/results/angular_vel_in";
    dlmwrite(curpath, qdmp.angular_vels_in);

    curpath = dir + "/results/angular_vel_tr";
    dlmwrite(curpath, qdmp.angular_vels_tr);

//    double maxErrorOut;
//    qdmp.evaluateReproduction(1.0, maxErrorOut);

}




