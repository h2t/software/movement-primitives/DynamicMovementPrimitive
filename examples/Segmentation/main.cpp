/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/forcefielddmp.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/filesystem.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <representation/dmp/endveloforcefielddmp.h>
#include <io/MMMConverter.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <dmp/math/pca.h>
#include <dmp/segmentation/characteristicsegmentation.h>


using namespace DMP;



void testTrajectory(std::string trajectoryFilePath)
{
    SampledTrajectoryV2 traj;
    if(trajectoryFilePath.find(".xml") != std::string::npos)
    {
        MMM::MotionReaderXML reader;
        MMM::MotionPtr motion = reader.loadMotion(trajectoryFilePath, "rightHand");
        traj = MMMConverter::fromMMM(motion, true);

//        std::cout << "mat:\n" << mat << std::endl;
//        traj.plot(0,0);
//        traj.plot(0,2, "plots/trajectory_acc");
        traj.gaussianFilter(0.1);
        Eigen::MatrixXd mat = traj./*getPart(0, 1).*/toEigen();
//        traj.plot(0,0, "plots/trajectory_gauss_filtered");
//        traj.plot(0,2, "plots/trajectory_gauss_filtered_acc");
//        traj.removeDimension(6);
//        traj.removeDimension(5);
//        traj.removeDimension(4);
//        traj.removeDimension(3);
//        traj.plotAllDimensions(2);
//        traj.removeDimension(2);
//        traj.removeDimension(1);
    }
    else
    {
        traj.readFromCSVFile(trajectoryFilePath);
        traj.gaussianFilter(10);
        traj.removeDimension(1);
    }
//    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);

    SampledTrajectoryV2 trajpart = traj.getPart(0, 3.62,2);
//    trajpart.plotAllDimensions(2);
    CharacteristicSegmentationOnPerturbationTerm seg(traj, 1, 100, 2, 1, 2);
    seg.calc();
    seg.plot(CharacteristicSegmentation::eSVG, &traj);
    boost::filesystem::path path(trajectoryFilePath);
    std::string outputfile = "Segmentated" + path.filename().string();
    std::ofstream file(outputfile.c_str());
    boost::archive::xml_oarchive ar(file);
    ar << BOOST_SERIALIZATION_NVP(seg);
}




int main(int argc, char* argv[])
{
    int res = system("mkdir plots");
    if(res)
    {
        throw std::logic_error{"Command 'mkdir plots' returned " + std::to_string(res)};
    }
    Vec<DVec> plots;
    // Load the trajectory
    SampledTrajectoryV2 traj;
    std::string path = DATA_DIR;
//    path += "/ExampleTrajectories/sampletraj.csv";
//    path += "/ExampleTrajectories/pouring_x_short.csv";
//    path += "/ExampleTrajectories/CosMotion.csv";

//    traj.readFromCSVFile(path);
//    traj.removeDimension(1);
//    traj = SampledTrajectoryV2::normalizeTimestamps(traj, 0, 1);





//    testTrajectory("/home/waechter/projects/MMMTools/data/Motions/Wiping_Trial04_MMM.xml");
//    testTrajectory("/home/waechter/projects/MMMTools/data/Motions/BowlPolishing_Trial15_MMM.xml");
    testTrajectory("/home/waechter/projects/MMMTools/data/Motions/Shaking_Trial10_MMM.xml");
//    testTrajectory(dmp, "/home/waechter/projects/MMMTools/data/Motions/Trial12_whisk_lying_MMM.xml", 0, 0, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/sampletraj.csv", 0, 0, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/sampletraj.csv", 0, 84, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/sampletraj.csv", 50, -300, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/pouring_x_short.csv", 0, -326.549, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/pouring_x_short.csv", 0, 326.549, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/CosMotion.csv", 50, 30, 1);
//    testTrajectory(dmp, path + "/ExampleTrajectories/CosMotion.csv", 0, -30, 1);
    return 0;


}



