/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>

#include "Image/ByteImage.h"
#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/forcefielddmp.h>

#include <boost/archive/xml_iarchive.hpp>
#include <boost/filesystem.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <representation/dmp/endveloforcefielddmp.h>
#include <io/MMMConverter.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <dmp/math/pca.h>
#include <dmp/segmentation/characteristicsegmentation.h>
#include <dmp/recognition/recognition.h>

using namespace DMP;




bool isSegmentPeriodic()
{
    return true;
    return false;
}


int main(int argc, char* argv[])
{
    int res = system("mkdir plots");
    if(res)
    {
        throw std::logic_error{"Command 'mkdir plots' returned " + std::to_string(res)};
    }
    Vec<DVec> plots;
    // Load the trajectory
//std::string path = DATA_DIR + std::string("/SegmentedTrajectories/Shaking_Trial10_MMM_Seg_Labeled.xml");
    std::string path = DATA_DIR + std::string("/SegmentedTrajectories/Wiping_Trial04_MMM_Labeled.xml");
    std::map<string, SemanticSegmentData>  data;
    std::ifstream file(path.c_str());
    if(!file.is_open())
    {
        throw Exception("cannot open file");
    }
    boost::archive::xml_iarchive ar(file);
    ar >> boost::serialization::make_nvp("SegData", data);

    std::string path2 = DATA_DIR + std::string("/SegmentedTrajectories/Shaking_Trial10_MMM_Seg_Labeled.xml");
    std::map<string, SemanticSegmentData>  data2;
    std::ifstream file2(path2.c_str());
    if(!file2.is_open())
    {
        throw Exception("cannot open file");
    }
    boost::archive::xml_iarchive ar2(file2);
    ar2 >> boost::serialization::make_nvp("SegData", data2);


    Recognition recognitionModule;
    int actionLabeli = 0;
    int actionLabelj = 0;
    for (int i = 0; i < data["rightHand"].segments.size(); i++)
    {
        actionLabeli = 0;

        DoubleMap::iterator it = data["rightHand"].segments[i].keyframesWithConfidence.begin();
        for (it = data["rightHand"].segments[i].keyframesWithConfidence.begin(); it != data["rightHand"].segments[i].keyframesWithConfidence.end(); it++)
        {

            actionLabeli++;
            double keyframe1 = it->first;
            SampledTrajectoryV2 subsegment1 = data["rightHand"].segments[i].getSubsegment(keyframe1);

            for (int j = 0; j < data2["rightHand"].segments.size(); j++)
            {

                DoubleMap::iterator it2 = data2["rightHand"].segments[j].keyframesWithConfidence.begin();
                actionLabelj = 0;
                for (it2 = data2["rightHand"].segments[j].keyframesWithConfidence.begin(); it2 != data2["rightHand"].segments[j].keyframesWithConfidence.end(); it2++)
                {

                    actionLabelj++;
                    double keyframe2 = it2->first;
                    SampledTrajectoryV2 subsegment2 = data2["rightHand"].segments[j].getSubsegment(keyframe2);
                    //data["rightHand"].segments[i].
                    double score = recognitionModule.compareSegments(subsegment1,subsegment2);
                    std::cout << "Motion 1 Semantic segment " <<i << "/" << data["rightHand"].segments.size();
                    std::cout << "Subsegment " <<actionLabeli << std::endl;
                    std::cout << "Motion 2 Semantic segment " <<j << "/" << data2["rightHand"].segments.size();
                    std::cout << "Subsegment " <<actionLabelj << std::endl;
                    printf("Score %f\n",score);
                }

            }
        }
    }
/*

    std::vector<DVec> DMPfeatureVector1 = computeFeatureVector(data);
    std::vector<DVec> DMPfeatureVector2 = computeFeatureVector(data2);

        //    this->traj.plotAllDimensions(0);
    //get weights
    CByteImage *image = new CByteImage(DMPfeatureVector1.size()*100,DMPfeatureVector2.size()*100,CByteImage::eGrayScale);
 for (int i = 0; i < DMPfeatureVector1.size(); i++){
      for (int j = 0; j < DMPfeatureVector2.size(); j++){
          double coefficient = 0.0;
          double bhat_coefficient = 0.0;
          double featVectLengthi = 0.0;
          double featVectLengthj = 0.0;
          for (int k = 0; k < DMPfeatureVector1[i].size(); k++){
              featVectLengthi += DMPfeatureVector1[i][k]*DMPfeatureVector1[i][k];
                 featVectLengthj += DMPfeatureVector2[j][k]*DMPfeatureVector2[j][k];
          }
          featVectLengthi = sqrt(featVectLengthi);
          featVectLengthj = sqrt(featVectLengthj);
          for (int k = 0; k < DMPfeatureVector1[i].size(); k++){
            coefficient += DMPfeatureVector1[i][k]*DMPfeatureVector2[j][k];// pow((DMPfeatureVectors[i][k]*DMPfeatureVectors[j][k]),2.0);

          }
          coefficient /=  featVectLengthi*featVectLengthj;
          coefficient = (coefficient*coefficient);

          for (int k = 0; k < 100; k++)
              for (int l = 0; l < 100; l++)
                  image->pixels[j*100*image->width+k*image->width+(i*100)+l] = 254-int(coefficient*254);
          std::cout << coefficient*255 << ",";
      }

        std::cout << std::endl;
 }
 if (!image->SaveToFile("/home/martin/FeatureImage.bmp"))
     std::cout  << "Saving failed" << std::endl;
    //Generate dmp for each trajectory subsegment
    //check subsegment size
 std::map<double, std::string>::iterator actIt;
 DoubleMap::iterator keyIt;
 std::cout << "actions 1" << std::endl;

 for (int i = 0; i < data["rightHand"].segments.size();i++)
     for (keyIt = data["rightHand"].segments[i].keyframesWithConfidence.begin();keyIt != data["rightHand"].segments[i].keyframesWithConfidence.end(); keyIt++)
         std::cout << keyIt->first << " " << data["rightHand"].segments[i].actionLabels[keyIt->first] <<std::endl;
 std::cout << "actions 2" << std::endl;

 for (int i = 0; i < data2["rightHand"].segments.size();i++)
     for (keyIt = data2["rightHand"].segments[i].keyframesWithConfidence.begin();keyIt != data2["rightHand"].segments[i].keyframesWithConfidence.end(); keyIt++)
         std::cout << keyIt->first << " " << data2["rightHand"].segments[i].actionLabels[keyIt->first] << std::endl;
         */
    return 0;


}



