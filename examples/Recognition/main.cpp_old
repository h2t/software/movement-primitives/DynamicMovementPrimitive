/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <fstream>

#include "Image/ByteImage.h"
#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/forcefielddmp.h>

#include <boost/archive/xml_iarchive.hpp>
#include <boost/filesystem.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <representation/dmp/endveloforcefielddmp.h>
#include <io/MMMConverter.h>
#include <MMM/Motion/MotionReaderXML.h>
#include <dmp/math/pca.h>
#include <dmp/segmentation/characteristicsegmentation.h>


using namespace DMP;



std::vector<DVec> computeFeatureVector(std::map<string, SemanticSegmentData>  data)
{
     std::vector<DVec> DMPfeatureVectors;
    for (int i = 0; i < data["rightHand"].segments.size(); i++)
    {

        DoubleMap::iterator it = data["rightHand"].segments[i].keyframesWithConfidence.begin();
        for (it = data["rightHand"].segments[i].keyframesWithConfidence.begin(); it != data["rightHand"].segments[i].keyframesWithConfidence.end(); it++)
        {DVec featureVector;
            double keyframe = it->first;

        SampledTrajectoryV2 subsegment = data["rightHand"].segments[i].getSubsegment(keyframe);
        BasicDMP dmp(20,1); //!!! need to change initial canonical value from 1 to 0 for SimpleEndVeloDMP!!!
        dmp.learnFromTrajectories(subsegment);
        subsegment = SampledTrajectoryV2::normalizeTimestamps(subsegment, 0.0, 1.0);
    //subsegment.differentiateDiscretly(2);
            DVec timestamps = subsegment.getTimestamps();
            Vec<DVec> uValuesOrig;
            std::vector<double> maxValues;
            std::vector<double> minValues;
            for(size_t d = 0; d < subsegment.dim(); d++)
            {
                maxValues.push_back(1.0);
                minValues.push_back(1.0);
                DoubleMap valueMap = dmp.getLearnedPertubationForces(d);
                DoubleMap::iterator vit = valueMap.begin();
                for(vit = valueMap.begin(); vit != valueMap.end(); vit++)
                {
                    if (vit->second < minValues[d])
                        minValues[d] = vit->second;
                    else if (vit->second > maxValues[d])
                        maxValues[d] = vit->second;
                }
            }

            for(size_t d = 0; d < subsegment.dim(); d++)
            {
                bool dimChanged =true;
                DoubleMap valueMap = dmp.getLearnedPertubationForces(d);
                DoubleMap::iterator vit = valueMap.begin();
                int vitindex = 0;
                double pprevForce = 0.0;

                double prevForce = 0.0;
                double maxForce = -10000000.0;
                double minForce = 10000000.0;
                double maxForceTime = 0.0;
                double minForceTime = 0.0;
                double meanForce = 0.0;
                for(vit = valueMap.begin(); vit != valueMap.end(); vit++)
                {

                       //featureVector.push_back(vit->second/std::max(fabs(maxValues[d]),fabs(minValues[d])));
                    meanForce += vit->second;
                    std::cout << vit->first << " ; " << vit->second << " " << maxForce << " " << minForce << std::endl;
                    if (vitindex > 2 && vitindex < valueMap.size()-2)
                    {
                        DoubleMap::iterator vitnext = vit;
                        vitnext++;
                       // if ((prevForce-pprevForce)*(vitnext->second-vit->second) < 0)
                        //{
                            if (vit->second < minForce)
                            {
                                minForce =vit->second;
                                minForceTime =((subsegment.getState(vit->first,d)-subsegment.getState(0.0,d))/(subsegment.getState(1.0,d)-subsegment.getState(0.0,d)));
                            }
                            else if (vit->second > maxForce)
                            {
                                maxForce =vit->second;
                                maxForceTime =((subsegment.getState(vit->first,d)-subsegment.getState(0.0,d))/(subsegment.getState(1.0,d)-subsegment.getState(0.0,d)));
                            }
                            /*if (dimChanged){
                                featureVector.push_back(vit->first);
                                dimChanged =false;
                            }
                            else if (vit->first-featureVector[featureVector.size()-1] > 0.01)
                                featureVector.push_back(vit->first);*/
                        //}
                        pprevForce = prevForce;
                        prevForce = vit->second;
                    }
                    else
                    {
                        prevForce = vit->second;
                    }
                    vitindex++;
                }
                if (minForceTime <maxForceTime)
                {
                featureVector.push_back(minForceTime);
                featureVector.push_back(minForce/(maxValues[d]-minValues[d]));
                featureVector.push_back(maxForceTime);
                featureVector.push_back(maxForce/(maxValues[d]-minValues[d]));
                featureVector.push_back((meanForce/double(vitindex))/(maxValues[d]-minValues[d]));
                }
                else
                {

                featureVector.push_back(maxForceTime);
                featureVector.push_back(maxForce/(maxValues[d]-minValues[d]));
                featureVector.push_back(minForceTime);
                featureVector.push_back(minForce/(maxValues[d]-minValues[d]));
                featureVector.push_back((meanForce/double(vitindex))/(maxValues[d]-minValues[d]));
                }
        //        FunctionApproximationInterfacePtr func = _getFunctionApproximator(d);
                std::cout << d << ": ";
                for (int k = 0; k < featureVector.size(); k++)
                    {
                    std::cout << featureVector[k] << " | ";
                    }
                std::cout << std::endl;


            }
            DMPfeatureVectors.push_back(featureVector);
        }
    }
    return DMPfeatureVectors;
}


bool isSegmentPeriodic()
{
    return true;
    return false;
}


int main(int argc, char* argv[])
{
    int res = system("mkdir plots");
    if(res)
    {
        throw std::logic_error{"Command 'mkdir plots' returned " + std::to_string(res)};
    }
    Vec<DVec> plots;
    // Load the trajectory
//std::string path = DATA_DIR + std::string("/SegmentedTrajectories/Shaking_Trial10_MMM_Seg_Labeled.xml");
    std::string path = DATA_DIR + std::string("/SegmentedTrajectories/Wiping_Trial04_MMM_Labeled.xml");
    std::map<string, SemanticSegmentData>  data;
    std::ifstream file(path.c_str());
    if(!file.is_open())
    {
        throw Exception("cannot open file");
    }
    boost::archive::xml_iarchive ar(file);
    ar >> boost::serialization::make_nvp("SegData", data);

    std::string path2 = DATA_DIR + std::string("/SegmentedTrajectories/Shaking_Trial10_MMM_Seg_Labeled.xml");
    std::map<string, SemanticSegmentData>  data2;
    std::ifstream file2(path2.c_str());
    if(!file2.is_open())
    {
        throw Exception("cannot open file");
    }
    boost::archive::xml_iarchive ar2(file2);
    ar2 >> boost::serialization::make_nvp("SegData", data2);



    std::vector<DVec> DMPfeatureVector1 = computeFeatureVector(data);
    std::vector<DVec> DMPfeatureVector2 = computeFeatureVector(data2);

        //    this->traj.plotAllDimensions(0);
    //get weights
    CByteImage *image = new CByteImage(DMPfeatureVector1.size()*100,DMPfeatureVector2.size()*100,CByteImage::eGrayScale);
 for (int i = 0; i < DMPfeatureVector1.size(); i++){
      for (int j = 0; j < DMPfeatureVector2.size(); j++){
          double coefficient = 0.0;
          double bhat_coefficient = 0.0;
          double featVectLengthi = 0.0;
          double featVectLengthj = 0.0;
          for (int k = 0; k < DMPfeatureVector1[i].size(); k++){
              featVectLengthi += DMPfeatureVector1[i][k]*DMPfeatureVector1[i][k];
                 featVectLengthj += DMPfeatureVector2[j][k]*DMPfeatureVector2[j][k];
          }
          featVectLengthi = sqrt(featVectLengthi);
          featVectLengthj = sqrt(featVectLengthj);
          for (int k = 0; k < DMPfeatureVector1[i].size(); k++){
            coefficient += DMPfeatureVector1[i][k]*DMPfeatureVector2[j][k];// pow((DMPfeatureVectors[i][k]*DMPfeatureVectors[j][k]),2.0);

          }
          coefficient /=  featVectLengthi*featVectLengthj;
          coefficient = (coefficient*coefficient);

          for (int k = 0; k < 100; k++)
              for (int l = 0; l < 100; l++)
                  image->pixels[j*100*image->width+k*image->width+(i*100)+l] = 254-int(coefficient*254);
          std::cout << coefficient*255 << ",";
      }

        std::cout << std::endl;
 }
 if (!image->SaveToFile("/home/martin/FeatureImage.bmp"))
     std::cout  << "Saving failed" << std::endl;
    //Generate dmp for each trajectory subsegment
    //check subsegment size
 std::map<double, std::string>::iterator actIt;
 DoubleMap::iterator keyIt;
 std::cout << "actions 1" << std::endl;

 for (int i = 0; i < data["rightHand"].segments.size();i++)
     for (keyIt = data["rightHand"].segments[i].keyframesWithConfidence.begin();keyIt != data["rightHand"].segments[i].keyframesWithConfidence.end(); keyIt++)
         std::cout << keyIt->first << " " << data["rightHand"].segments[i].actionLabels[keyIt->first] <<std::endl;
 std::cout << "actions 2" << std::endl;

 for (int i = 0; i < data2["rightHand"].segments.size();i++)
     for (keyIt = data2["rightHand"].segments[i].keyframesWithConfidence.begin();keyIt != data2["rightHand"].segments[i].keyframesWithConfidence.end(); keyIt++)
         std::cout << keyIt->first << " " << data2["rightHand"].segments[i].actionLabels[keyIt->first] << std::endl;
    return 0;


}



