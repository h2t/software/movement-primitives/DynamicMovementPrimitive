#include <fstream>

#include <boost/unordered_map.hpp>
#include <boost/range/iterator_range.hpp>

#include <sys/time.h>

#include <iostream>

#include "dmp/functionapproximation/radialbasisfunctionnetwork.h"
#include "dmp/representation/dmp/umidmp.h"

#ifdef USE_GCC8
#include <filesystem>
using namespace std;
#else
#include <boost/filesystem.hpp>
using namespace boost;
#endif


using namespace DMP;

int main(int argc, char* argv[])
{
    int wdim = 50;
    size_t trajdim;
    filesystem::path p(argc > 1 ? argv[1] : ".");
    if (!filesystem::is_directory(p))
    {
        std::cerr << p << " is not a valid path ... " << std::endl;
        return 0;
    }


    filesystem::path ptrain(p.string() + "/train");

    if (!filesystem::is_directory(ptrain))
    {
        std::cerr << ptrain << " is not a valid path ... " << std::endl;
        return 0;
    }

    Vec<RBFN<> > nets;
    DVec2d queries;
    Vec<DVec2d > weights_num;
    SampledTrajectoryV2 traj0;

    UMIDMP dmp(wdim, 20, 1);
    for (auto& entry : boost::make_iterator_range(filesystem::directory_iterator(ptrain), {}))
    {
        std::string filepath = entry.path().string();
        SampledTrajectoryV2 traj;
        DVec query;
        traj.readFromCSVFileWithQuery(filepath, query, false);
        queries.push_back(query);

        dmp.learnFromTrajectories(traj);
        DVec2d ws = dmp.getWeights();

        trajdim = ws.size();
        weights_num.push_back(ws);
    }


    for (size_t i = 0; i < trajdim; ++i)
    {
        DVec2d wq;

        for (size_t j = 0; j < weights_num.size(); ++j)
        {
            wq.push_back(weights_num[j][i]);
        }


        RBFN<> net;
        DVec widthFactor;
        widthFactor.push_back(10);
        net.setWidthFactor(widthFactor);
        net.learn(queries, wq);
        nets.push_back(net);
    }


    // evaluation
    filesystem::path ptest(p.string() + "/test");
    if (!filesystem::is_directory(ptest))
    {
        std::cerr << ptest << " is not a valid path ... " << std::endl;
        return 0;
    }

    int res = std::system("mkdir plots");
    for (auto& entry : boost::make_iterator_range(filesystem::directory_iterator(ptest), {}))
    {
        std::string filepath = entry.path().string();
        SampledTrajectoryV2 traj0;
        DVec query;
        traj0.readFromCSVFileWithQuery(filepath, query);

        DVec2d weights;
        for (size_t i = 0; i < trajdim; ++i)
        {
            weights.push_back(nets[i](query));
        }
        dmp.setWeights(weights);

        Vec<DMPState> initialState;
        DVec goals;
        for (size_t i = 0; i < traj0.dim(); i++)
        {
            DMPState state;
            state.pos = traj0.begin()->getPosition(i) ;
            state.vel = traj0.begin()->getDeriv(i, 1);
            initialState.push_back(state);
            goals.push_back(traj0.rbegin()->getPosition(i));
        }

        DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + traj0.getTimestamps().size()));
        SampledTrajectoryV2 newTraj = dmp.calculateTrajectory(timestamps, goals, initialState, 1.0, 1.0);

        Vec<DVec> plots;

        for (size_t i = 0; i < newTraj.dim(); i++)
        {
            plots.clear();
            plots.push_back(timestamps);
            plots.push_back(traj0.getDimensionData(i, 0));
            plots.push_back(newTraj.getDimensionData(i, 0));
            plotTogether(plots, "plots/trajComparison_%d.gp");
        }
    }
}


