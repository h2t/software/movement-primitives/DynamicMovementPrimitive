#include <fstream>

#include <boost/unordered_map.hpp>

#include <sys/time.h>

#include <iostream>

#include "dmp/math/gmm.h"

using namespace DMP;


int main(int argc, char* argv[])
{

    std::string path = DATA_DIR;
    path += "/python/GMMTest/_1SupTo2SupLF.csv";
    mat X;
    X.load(path);

    GaussianMixtureModel gmm(5, X.n_cols);
    gmm.runKmeans(X);
    gmm.runEM(X);
    gmm.print();


    gmm.runArmaEM(X);
    gmm.print();
}


