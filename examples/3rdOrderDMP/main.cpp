/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/



#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/dmp3rdorderforcefield.h>
#include <dmp/representation/dmp/adaptive3rdorderdmp.h>

#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/endveloforcefielddmp.h>
#include <dmp/representation/dmp/endveloforcefieldwithobjrepulsiondmp.h>

using namespace DMP;


int main(int argc, char *argv[])
{
    Vec<DVec> plots;
    std::cout << "press a button.." << std::endl;
    std::cin.get();
    SampledTrajectoryV2 traj;
//    traj.readFromCSVFile("../DMP_Discrete_Test/sampletraj_withvel.csv");
//    traj.readFromCSVFile("concat_traj_2.csv");
//    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/approaching.csv",2);
    traj.readFromCSVFile(std::string(DATA_DIR) + "/ExampleTrajectories/pickplace7d.csv",true);
//    traj.gaussianFilter(0.05* *traj.getTimestamps().rbegin());

//    traj.removeDimension(2);
//    traj.removeDimension(1);
    traj = SampledTrajectoryV2::normalizeTimestamps(traj,0,1);
    traj.differentiateDiscretly(3);


//    plots.push_back(traj.getTimestamps());
//    plots.push_back(traj.getDimensionData(0,0));
//    plots.push_back(traj.getDimensionData(0,1));
//    plots.push_back(traj.getDimensionData(0,2));
//    plots.push_back(traj.getDimensionData(0,3));
//    plotTogether(plots,"plots/originalTraj%d.gp");

//        DMP3rdOrder dmp(5);
    DMP::AdaptiveGoal3rdOrderDMP dmp(5);
//    DMP::DMP3rdOrderForceField dmp(5);
        dmp.setFunctionApproximator(FunctionApproximationInterfacePtr(new ExactReproduction()));
//    DMP3rdOrderForceField dmp(5);
    dmp.setStopAtEnd(true);
    dmp.setShowPlots(false);
    dmp.learnFromTrajectories(traj);

//    double maxError;
//    double error = dmp.evaluateReproduction(0,maxError);
//    std::cout << "Mean relative reproduction error: " << error << " max error: " << maxError << std::endl;

    // Configuration
    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0,1, 1.0/(traj.getTimestamps().size()-1));
    DVec accs = SampledTrajectoryV2::DifferentiateDiscretly(traj.getTimestamps(), traj.getDimensionData(0,1));


//    double maxError;
//    double error = dmp.evaluateReproduction(0,maxError);
//    std::cout << "Mean relative reproduction error: " << error << " max error: " << maxError << std::endl;


//    Vec<_3rdOrderDMP> initialStates;
    Vec<_3rdOrderDMP> previousState;
    DVec startPositions;
    DVec goals;

    previousState.resize(traj.dim());
    previousState[0].pos = -89.39; //-319;
//    previousState[1].pos = 513;
//    previousState[2].pos = 1077;

    for (unsigned int i = 0; i < traj.dim(); ++i) {
        _3rdOrderDMP initialState;
        initialState.pos = previousState[i].pos ;//traj.begin()->data[i]->pos;
//        startPositions.push_back(initialState.pos);
        initialState.vel = traj.begin()->getDeriv(0,1);
        initialState.acc = traj.begin()->getDeriv(0,2);
        goals.push_back(traj.rbegin()->getPosition(i));//-300);
//        initialStates.push_back(initialState);
        previousState[i] = DMP3rdOrder::convertStateTo3rdOrderDMPState(initialState, dmp.getFilterConstant());
        startPositions.push_back(previousState[i].pos);
//        goals.push_back( traj.rbegin()->data[i]->pos);
    }

//    startPositions.push_back(513);
//    startPositions.push_back(1077);

//    goals.push_back(700);
//    goals.push_back(1000);
    std::cout << "goal before: " << goals << std::endl;
    // = dmp.calculateTrajectory(timestamps, DVec(1,goal), Vec<_3rdOrderDMP>(1,initialState), 0.0, 1);


    dmp.setStartPositions(startPositions);
    DVec::const_iterator it = timestamps.begin();
//    Vec<_3rdOrderDMP> previousState = Vec<_3rdOrderDMP>(1,DMP3rdOrder::convertStateTo3rdOrderDMPState(initialStates, dmp.getFilterConstant()));
    std::map<double, Vec<_3rdOrderDMP> > resultingSystemStatesMap;
    DVec canonicalValues = 0;
    double prevTimestamp = *it;
    for(; it != timestamps.end(); it++)
    {
//        if(*it < 1)
//            previousState[0].pos *= ((float)(rand()%10)-5)/100 + 1;
//        previousState[0].vel *= ((float)(rand()%10)-5)/100 + 1;
//        previousState[0].acc *= ((float)(rand()%10)-5)/1000 + 1;
//        goal *= ((float)(rand()%10))/1000 + 1;
//        for (int i = 0; i < traj.dim(); ++i) {
//            previousState.at(i).acc = 0;
//        }
        previousState = dmp.calculateTrajectoryPoint(*it, goals, prevTimestamp, previousState, canonicalValues, 1);

        prevTimestamp = *it;
        resultingSystemStatesMap[*it] = previousState;

    }
    std::cout << "goal after: " << goals << std::endl;
    SampledTrajectoryV2 newTraj(resultingSystemStatesMap);

    newTraj.differentiateDiscretly(1);
    newTraj.differentiateDiscretly(2);

//    DVec newAccs = SampledTrajectoryV2<_3rdOrderDMP>::differentiateDiscretely(newTraj.getTimestamps(), newTraj.differentiateDiscretelyForDim(0, 2));

    for(size_t i = 0; i < traj.dim(); i++)
    {
        plots.clear();
        plots.push_back(traj.getTimestamps());
        plots.push_back(traj.getDimensionData(i, 0));
        plots.push_back(newTraj.getDimensionData(i, 0));
        plotTogether(plots, "plots/trajComparison_%d.gp");
    }

}


