/**
* This file is part of ArmarX.
*
* ArmarX is free software; you can redistribute it and/or modify
* it under the terms of the GNU Lesser General Public License as
* published by the Free Software Foundation; either version 2 of
* the License, or (at your option) any later version.
*
* ArmarX is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU Lesser General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program. If not, see <http://www.gnu.org/licenses/>.
*
* @package    ArmarX::
* @author     Mirko Waechter ( mirko.waechter at kit dot edu)
* @date       2012
* @copyright  http://www.gnu.org/licenses/gpl.txt
*             GNU General Public License
*/

//#include <QCoreApplication>

#include <general/helpers.h>
#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/forcefielddmp.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/xml_iarchive.hpp>

#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <dmp/representation/dmpfactory.h>

#include <sys/time.h>

#include <iostream>

#include <dmp/costfunction/manipulabilitycostfcn.h>
#include <dmp/costfunction/simplecostfcn.h>


#include <VirtualRobot/VirtualRobot.h>
#include <VirtualRobot/Import/SimoxXMLFactory.h>

#include <MMM/Motion/MotionReaderXML.h>
#include "MMMSimoxTools/MMMSimoxTools.h"

#include "io/MMMConverter.h"

using namespace DMP;


void dmp2motion()
{
    std::string file = "pi2dmp_test_rightarm.xml";
    std::string path = "pickplace_rightarm_joints.csv";
    std::string mfile = "pickplace.xml";
    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());
    MMM::MotionPtr muster = r->loadMotion(mfile);

    SampledTrajectoryV2 sampleTraj;
    sampleTraj.readFromCSVFile(path);
    sampleTraj = SampledTrajectoryV2::normalizeTimestamps(sampleTraj, 0, 1);

    DVec timestamps = SampledTrajectoryV2::generateTimestamps(0, 1.0, 1.0 / (-0.5 + sampleTraj.getTimestamps().size()));
    DVec2d initialState;
    DVec goals;
    for(size_t i = 0; i < sampleTraj.dim(); i++)
    {
        DVec state;
        state.push_back(sampleTraj.begin()->getPosition(i));
        state.push_back(sampleTraj.begin()->getDeriv(i,1));
        initialState.push_back(state);

        goals.push_back(sampleTraj.rbegin()->getPosition(i));
    }

    // load dmp
    std::ifstream ifs(file.c_str());
    std::cout << ifs.is_open() << std::endl;
    boost::archive::xml_iarchive ar(ifs);
    DMPInterface* readPtr;
    ar >> boost::serialization::make_nvp("dmp",readPtr);
    DMPInterfacePtr dmp(readPtr);

    DMP::SampledTrajectoryV2 traj = dmp->calculateTrajectory(timestamps, goals, initialState, 1.0, 1);

    traj.plotAllDimensions(0);


    VirtualRobot::RobotPtr robot = MMM::SimoxTools::buildModel(muster->getModel(),false);
    std::vector<VirtualRobot::RobotNodePtr > kcnodes = robot->getRobotNodeSet("RightArm")->getAllRobotNodes();
    std::vector<std::string> kcnames;

    for(size_t i = 0; i < kcnodes.size(); ++i)
    {
        kcnames.push_back(kcnodes[i]->getName());
    }

    MMM::MotionPtr motion = MMMConverter::getMotionFromJointsTraj(traj, kcnames, muster);

    motion->print("testmotion.xml");

}

DVec trainDMP(DMPInterfacePtr dmp, const SampledTrajectoryV2& traj)
{
    DVec2d initialState;
    DVec goals;
    for(size_t i = 0; i < traj.dim(); i++)
    {
        DVec state;
        state.push_back(traj.begin()->getPosition(i));
        state.push_back(traj.begin()->getDeriv(i,1));
        initialState.push_back(state);

        goals.push_back(traj.rbegin()->getPosition(i));
    }

    dmp->learnFromTrajectory(traj);

}

int main(int argc, char* argv[])
{
    std::string filepath = "pouring_x_short.csv";
    SampledTrajectoryV2 traj;
    traj.readFromCSVFile(filepath);
    traj.removeDimension(1);

    traj.plotAllDimensions(0);
    std::vector<SampledTrajectoryV2 > trajs = traj.getSegments(10);

    DVec2d weights;
    for(size_t i = 0; i < trajs.size(); ++i)
    {
        DMPFactory dmpfactory;
        DMPInterfacePtr dmp = dmpfactory.getDMP("BasicDMP",20);

        SampledTrajectoryV2 trj = trajs[i];
        trj = SampledTrajectoryV2::normalizeTimestamps(trj, 0, 1);
        dmp->learnFromTrajectory(trj);

        std::cout << dmp->getWeights()[0] << std::endl;
        weights.push_back(dmp->getWeights()[0]);
    }

    dlmwrite("./result/pouring_x_short_seg10_dim20.csv", weights);
}


