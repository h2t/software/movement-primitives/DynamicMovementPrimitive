#ifdef MMM_FOUND
#include <fstream>

#include <boost/unordered_map.hpp>

#include <dmp/representation/dmp/basicdmp.h>
#include <dmp/representation/dmp/collisionavoidancedmp.h>
#include <dmp/representation/dmp/endvelodmp.h>
#include <dmp/representation/dmp/simpleendvelodmp.h>
#include <dmp/representation/dmp/dmp3rdorder.h>
#include <dmp/representation/dmp/forcefielddmp.h>

#include <boost/archive/xml_oarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/archive/binary_oarchive.hpp>
#include <boost/archive/binary_iarchive.hpp>
#include <dmp/representation/dmp/dmpregistration.h>
#include <dmp/representation/dmpfactory.h>

#include <sys/time.h>

#include <iostream>
#include <MMM/Motion/MotionReaderXML.h>
#include <io/MMMConverter.h>


// for iksolver test
#include "VirtualRobot/VirtualRobot.h"
#include "VirtualRobot/IK/IKSolver.h"
#include "VirtualRobot/IK/GenericIKSolver.h"
#include "MMMSimoxTools/MMMSimoxTools.h"


using namespace DMP;

int main(int argc, char* argv[])
{
    int res = KILL_ALL_PLOT_WINDOWS;
    if(res)
    {
        throw std::logic_error{"Killing all plot windows returned " + std::to_string(res)};
    }
    res = system("mkdir plots");
    if(res)
    {
        throw std::logic_error{"Command 'mkdir plots' returned " + std::to_string(res)};
    }
    Vec<DVec> plots;

//    // Load the motion
    std::string path = DATA_DIR;
    path += "/ExampleTrajectories/pickplace.xml";

    MMM::MotionReaderXMLPtr r(new MMM::MotionReaderXML());
    MMM::MotionPtr motion = r->loadMotion(path);

    std::vector<std::string> nodeSet;
    nodeSet.push_back("RightHandSegment_joint");
    SampledTrajectoryV2 traj = MMMConverter::getNodeTrajInTaskSpace(motion, nodeSet, false, true);

    traj.plotAllDimensions(0);

//    SampledTrajectoryV2 traj2 = MMMConverter::getNodeTrajInJointSpace(motion, nodeSet);
//    traj2.plotAllDimensions(0);

//    std::vector<std::string> kcnames;
//    kcnames.push_back("RightHipArm");
//    MMM::MotionPtr newMotion = MMMConverter::getMotionFromTCPTraj(traj, kcnames, motion, true);
//    newMotion->print("new1.xml");

//    SampledTrajectoryV2 traj3 = MMMConverter::getNodeTrajInTaskSpace(newMotion, nodeSet, false, false);
//    traj3.plotAllDimensions(0);

//    MMM::MotionPtr newMotion2 = MMMConverter::getMotionFromJointsTraj(traj2, nodeSet, motion);
//    newMotion2->print("new2.xml");

}


#endif
