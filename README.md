# Dynamic Movement Primitive Library


DMPLib is a simple-to-use library developed in KIT to realize versatile tools for DMP application

### Requirement

In order to use DMPLib, you need the following extra libraries:

- CMake (required minimun version: 2.6) [https://cmake.org/] 
- Eigen3 [http://eigen.tuxfamily.org/index.php?title=Main_Page]
- GSL (tested on GSL1.15) [https://www.gnu.org/software/gsl/]  
- BOOST [http://www.boost.org/]
 
All libraries are available as default packages in Ubuntu 14.04 and are tested with them.
They can be installed with:
```sh
$ sudo apt-get install libboost-dev libboost-test-dev libboost-serialization-dev libeigen3-dev libgsl0-dev cmake g++ git  libblas-dev swig libpython-dev
```

### Download


You can download our DMP library as a normal git repository:
```sh
$ git clone https://gitlab.com/h2t/DynamicMovementPrimitive.git dmp
$ cd dmp/build
$ cmake ..
$ make
```

### Examples

After you build DMP, open your IDE (recommended: QT). You will find the folder "examples". We have several examples, from which you can learn how to use DMPLib. You can also find the executable files in the "/build/bin/" directory, where you can have a first look at the result given by DMP.

### Structure

DMPLib contains several parts:

- representation (contains different types of DMPs supported in our DMPLib)
- functionapproximation (contains different types of regression models)
- general (contains some general helper functions)

### Usage

##### Learn and Reproduce a Trajectory
In DMPLib, we have also a special data structure to represent sampled trajectories (SampledTrajectoryV2). We show a simple example here to illustrate the usage of DMPLib. You can find more examples in the "examples/" directory.

You can learn a DMP from a trajectory.
```c++
SampledTrajectoryV2 traj;
traj.readFromCSVFile(filepath);
DMPFactory dmpfactory;
DMPInterfacePtr dmp = dmpfactory.getDMP("BasicDMP", 100);
dmp->learnFromTrajectory(traj);
```
You can get a reproduced trajectory from a learned DMP. "DVec" is a more general double vector than C++ vector.

```c++
DVec timestamps = SampledTrajectoryV2::generateTimestamps(startTime, endTime, stepSize);
DVec goals;
DVec2d initialStates;
for(size_t i = 0; i < traj.dim(); i++)
{
    DVec state;
    state.push_back(traj.begin()->getPosition(i)); 
    state.push_back(traj.begin()->getDeriv(i,1));
    initialStates.push_back(state);
    goals.push_back(traj.rbegin()->getPosition(i));
}
DVec initialCanonicalValue = DVec(1.0);
double temporalFactor = 1.0;
SampledTrajectoryV2 newtraj = dmp->calculateTrajectory(timestamps, goals, initialStates, initialCanonicalValue, temporalFactor);
```


